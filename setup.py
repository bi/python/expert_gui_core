"""
setup.py for expert_gui_core.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()

REQUIREMENTS: dict = {
    'core': [
        "numpy",
        "pandas",
        "pyarrow",
        "fontawesome",
        "PyOpenGL",
        "matplotlib",
        "fxpmath",
        "scikit-learn",
        "scikit-image",
        "scipy",
        "natsort",
        "qtawesome",
        "jpype1",
        "pytimber",
        # "pyqtgraph==0.13.0",
        "pyjapc",
        "pyrbac==1.1.0",
        "pyda",
        "pyda-japc",
        "pyda-rda3",
        "accwidgets",
        "PyCCDA",
        "nxcals",
        "xmltodict",
        "pyrbac",
        "opencv-contrib-python-headless"
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
}

setup(
    name='expert-gui-core',

    author='<Insert your name>',
    author_email='<Insert your email>@cern.ch',
    description='SHORT DESCRIPTION OF PROJECT',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='~=3.9',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
)
