import unittest

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtTest import QSignalSpy
from PyQt5.QtWidgets import QApplication, QWidget

from expert_gui_core.gui.widgets.combiner.devicesfesawidget import *
from expert_gui_core.gui.widgets.common.subscriptionwidget import *
from expert_gui_core.tools.formatting import format_value_to_string
from tests.devicesfesawidget_fixtures import DevicesFesaWidgetFixtures
from tests.subscriptionwidget_fixtures import TestCustomPropertySubscriptionWidget, get_response_example
