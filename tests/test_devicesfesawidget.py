from tests import *


class TestDeviceButtonWidget(unittest.TestCase):
    """
    Tests basic setters and getters of DeviceButtonWidget from expert_gui_core.gui.widgets.combiner.devicesfesawidget.
    DeviceButtonWidgets are added to DeviceClassPanelWidget, which is part of DevicesFesaWidget.
    """

    @classmethod
    def setUpClass(cls):
        cls.app = QApplication([])

    def setUp(self):
        self.widget = DeviceButtonWidget()

    def test_initialization(self):
        self.assertEqual(self.widget._title, "")
        self.assertEqual(self.widget._subtitle, "")
        self.assertEqual(self.widget._text, "")
        self.assertIsNone(self.widget._color)
        self.assertEqual(self.widget.get_title_label().text(), "")
        self.assertEqual(self.widget.get_subtitle_label().text(), "")
        self.assertEqual(self.widget.get_text_label().text(), "")

    def test_set_title(self):
        spy = QSignalSpy(self.widget.update_signal)
        self.widget.set_title("New Title", color="red")
        self.assertEqual(self.widget._title, "New Title")
        self.assertEqual(self.widget._title_color, "red")
        self.assertEqual(self.widget.get_title_label().text(), "New Title")
        self.assertIn("color: red;", self.widget.get_title_label().styleSheet())
        self.assertEqual(len(spy), 1)  # Check that the signal was emitted once

    def test_set_subtitle(self):
        spy = QSignalSpy(self.widget.update_signal)
        self.widget.set_subtitle("New Subtitle", color="blue")
        self.assertEqual(self.widget._subtitle, "New Subtitle")
        self.assertEqual(self.widget._subtitle_color, "blue")
        self.assertEqual(self.widget.get_subtitle_label().text(), "New Subtitle")
        self.assertIn("color: blue;", self.widget.get_subtitle_label().styleSheet())
        self.assertEqual(len(spy), 1)

    def test_set_text(self):
        spy = QSignalSpy(self.widget.update_signal)
        self.widget.set_text("New Text", color="green")
        self.assertEqual(self.widget._text, "New Text")
        self.assertEqual(self.widget._text_color, "green")
        self.assertEqual(self.widget.get_text_label().text(), "New Text")
        self.assertIn("color: green;", self.widget.get_text_label().styleSheet())
        self.assertEqual(len(spy), 1)

    def test_set_color(self):
        spy = QSignalSpy(self.widget.update_signal)
        self.widget.set_color("yellow")
        self.assertEqual(self.widget._color, "yellow")
        self.assertIn("background-color: yellow;", self.widget.styleSheet())
        self.assertEqual(len(spy), 1)

    @classmethod
    def tearDownClass(cls):
        cls.app.quit()


class TestDeviceClassPanelWidget(unittest.TestCase):
    """
    Tests basic setters and getters of DeviceClassPanelWidget from
    expert_gui_core.gui.widgets.combiner.devicesfesawidget. DeviceClassPanelWidgets have DeviceButtonWidgets,
    and are part of DevicesFesaWidget. DeviceClassPanelWidgets are bound to a class name by definition.
    """

    @classmethod
    def setUpClass(cls):
        cls.app = QApplication([])

    def setUp(self):
        self._dummy_window = QMainWindow()
        self.fixtures = DevicesFesaWidgetFixtures()
        class_device, flattened_fecs = self.fixtures.class_bstlhc, self.fixtures.fecs_bstlhc
        self.devices_class_panel_widget = DeviceClassPanelWidget(class_name=class_device, fecs=flattened_fecs)

    def test_get_devices(self):
        devices = self.devices_class_panel_widget.get_devices()
        expected_devices = ['LHC.BST.CCR.B1.F3', 'LHC.BST.CCR.B2.F3', 'LAB.BST']
        self.assertListEqual(devices, expected_devices)

    def test_get_devices_visibility(self):
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {'LHC.BST.CCR.B1.F3': True, 'LHC.BST.CCR.B2.F3': True, 'LAB.BST': True}
        self.assertDictEqual(visibility, expected_visibility)

    def test_get_device_buttons(self):
        device_buttons = self.devices_class_panel_widget.get_device_buttons(mapped=True)
        self.assertTrue(isinstance(device_buttons, dict))
        self.assertIn('LHC.BST.CCR.B1.F3', device_buttons)
        self.assertIsInstance(device_buttons['LHC.BST.CCR.B1.F3'], DeviceButtonWidget)

    def test_get_device_button(self):
        device_button = self.devices_class_panel_widget.get_device_button('LHC.BST.CCR.B1.F3')
        self.assertIsInstance(device_button, DeviceButtonWidget)

    def test_set_title(self):
        device_name = 'LHC.BST.CCR.B1.F3'
        new_title = 'New Title'
        self.devices_class_panel_widget.set_title(device_name, new_title)
        self.assertEqual(self.devices_class_panel_widget.get_device_button(device_name).get_title_label().text(),
                         new_title)

    def test_set_text(self):
        device_name = 'LHC.BST.CCR.B1.F3'
        new_text = 'New Text'
        self.devices_class_panel_widget.set_text(device_name, new_text)
        self.assertEqual(self.devices_class_panel_widget.get_device_button(device_name).get_text_label().text(),
                         new_text)

    def test_set_color(self):
        device_name = 'LHC.BST.CCR.B1.F3'
        new_color = 'red'  # Red color
        self.devices_class_panel_widget.set_color(device_name, new_color)
        self.assertEqual(self.devices_class_panel_widget.get_device_button(device_name)._color, new_color)

    def test_filter_fields(self):
        # Filter with 'B1' should only show LHC.BST.CCR.B1.F3
        self.devices_class_panel_widget.filter_fields('B1')
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {
            'LHC.BST.CCR.B1.F3': True,
            'LHC.BST.CCR.B2.F3': False,
            'LAB.BST': False
        }
        self.assertDictEqual(visibility, expected_visibility)

        # Filter with 'LAB' should only show LAB.BST
        self.devices_class_panel_widget.filter_fields('LAB')
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {
            'LHC.BST.CCR.B1.F3': False,
            'LHC.BST.CCR.B2.F3': False,
            'LAB.BST': True
        }
        self.assertDictEqual(visibility, expected_visibility)

        # Filter with 'CFV' should show ALL three
        self.devices_class_panel_widget.filter_fields('cfv')
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {
            'LHC.BST.CCR.B1.F3': True,
            'LHC.BST.CCR.B2.F3': True,
            'LAB.BST': True
        }
        self.assertDictEqual(visibility, expected_visibility)

    def test_update_panel_devices(self):
        # Hide all devices initially
        for device in self.devices_class_panel_widget.get_devices():
            self.devices_class_panel_widget._device_buttons_show[device] = False

        # Update the panel, no devices should be shown
        self.devices_class_panel_widget.update_panel_devices()
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {
            'LHC.BST.CCR.B1.F3': False,
            'LHC.BST.CCR.B2.F3': False,
            'LAB.BST': False
        }
        self.assertDictEqual(visibility, expected_visibility)

        # Show one device and update the panel
        self.devices_class_panel_widget._device_buttons_show['LHC.BST.CCR.B1.F3'] = True
        self.devices_class_panel_widget.update_panel_devices()
        visibility = self.devices_class_panel_widget.get_devices_visibility()
        expected_visibility = {
            'LHC.BST.CCR.B1.F3': True,
            'LHC.BST.CCR.B2.F3': False,
            'LAB.BST': False
        }
        self.assertDictEqual(visibility, expected_visibility)

    @classmethod
    def tearDownClass(cls):
        cls.app.quit()


if __name__ == '__main__':
    unittest.main()
