class DevicesFesaWidgetFixtures:
    """
    Definition of some Data Fixtures that are used in test_devicesfesawidget.py.
    """

    def __init__(self):
        """
        class_bobr(str): Example of possible class name.
        fecs_bobr(Dict[str, List[str]): Flatten fecs that are extracted from the _devices attribute of DevicesFesaWidget
        """
        self.class_bobr = "BOBR"
        self.fecs_bobr = {'cfv-ua47-bqht': ['BOBR_cfv-ua47-bqht'], 'cfv-866-bidev3': ['TST.BOBR.cfv-866-bidev3'],
                          'cfv-ccr-ctlbst1': ['BOBR_cfv-ccr-ctlbst1'], 'cfv-ccr-ctlbst2': ['BOBR_cfv-ccr-ctlbst2'],
                          'cfv-usc55-bra': ['BOBR_cfv-usc55-bra'], 'cfv-us45-bsra': ['BOBR_cfv-us45-bsra'],
                          'cfv-ua43-bsr': ['BOBR_cfv-ua43-bsr'], 'cfv-ua27-bra': ['BOBR_cfv-ua27-bra'],
                          'cfv-ua47-bsr': ['BOBR_cfv-ua47-bsr'], 'cfv-us45-bsrtb1': ['BOBR_cfv-us45-bsrtb1'],
                          'cfv-us45-bsrtb2': ['BOBR_cfv-us45-bsrtb2'], 'cfv-ua87-bra': ['BOBR_cfv-ua87-bra'],
                          'cfv-ua83-bra': ['BOBR_cfv-ua83-bra'], 'cfv-ua23-bra': ['BOBR_cfv-ua23-bra'],
                          'cfv-866-bidev10': ['BOBR_cfv-866-bidev10'], 'cfv-sr1-blml': ['BOBR_cfv-sr1-blml'],
                          'cfv-sr1-blmr': ['BOBR_cfv-sr1-blmr'], 'cfv-sr1-blmc': ['BOBR_cfv-sr1-blmc'],
                          'cfv-sr1-bpmb1la': ['BOBR_cfv-sr1-bpmb1la'], 'cfv-sr2-blmc': ['BOBR_cfv-sr2-blmc'],
                          'cfv-sr3-blmr': ['BOBR_cfv-sr3-blmr'], 'cfv-sr5-blmc': ['BOBR_cfv-sr5-blmc'],
                          'cfv-sr5-blml': ['BOBR_cfv-sr5-blml'], 'cfv-sr7-blmc': ['BOBR_cfv-sr7-blmc'],
                          'cfv-sr7-blmr': ['BOBR_cfv-sr7-blmr'], 'cfv-sr6-blmr': ['BOBR_cfv-sr6-blmr'],
                          'cfv-sr2-blmr': ['BOBR_cfv-sr2-blmr'], 'cfv-sx4-blmc': ['BOBR_cfv-sx4-blmc'],
                          'cfv-sx4-blmr': ['BOBR_cfv-sx4-blmr'], 'cfv-sr5-blmr': ['BOBR_cfv-sr5-blmr'],
                          'cfv-sr8-blml': ['BOBR_cfv-sr8-blml'], 'cfv-sr6-blmc': ['BOBR_cfv-sr6-blmc'],
                          'cfv-sr7-blml': ['BOBR_cfv-sr7-blml'], 'cfv-sr8-blmc': ['BOBR_cfv-sr8-blmc'],
                          'cfv-sr7-blme': ['BOBR_cfv-sr7-blme'], 'cfv-sr8-blmi': ['BOBR_cfv-sr8-blmi'],
                          'cfv-sr2-blml': ['BOBR_cfv-sr2-blml'], 'cfv-sx4-blml': ['BOBR_cfv-sx4-blml'],
                          'cfv-sr3-blml': ['BOBR_cfv-sr3-blml'], 'cfv-sr8-blmr': ['BOBR_cfv-sr8-blmr'],
                          'cfv-sr6-blml': ['BOBR_cfv-sr6-blml'], 'cfv-sr3-blmc': ['BOBR_cfv-sr3-blmc'],
                          'cfv-sr2-blmi': ['BOBR_cfv-sr2-blmi'], 'cfv-sr2-bpmb1ra': ['BOBR_cfv-sr2-bpmb1ra'],
                          'cfv-sr2-bpmb1lb': ['BOBR_cfv-sr2-bpmb1lb'], 'cfv-sr2-bpmb2lb': ['BOBR_cfv-sr2-bpmb2lb'],
                          'cfv-sr2-bpmb2la': ['BOBR_cfv-sr2-bpmb2la'], 'cfv-sr2-bpmb2ra': ['BOBR_cfv-sr2-bpmb2ra'],
                          'cfv-sr2-bpmb2rb': ['BOBR_cfv-sr2-bpmb2rb'], 'cfv-sr2-bpmb1la': ['BOBR_cfv-sr2-bpmb1la'],
                          'cfv-sr2-bpmb1rb': ['BOBR_cfv-sr2-bpmb1rb'], 'cfv-sr3-bpmb2la': ['BOBR_cfv-sr3-bpmb2la'],
                          'cfv-sr3-bpmb1lb': ['BOBR_cfv-sr3-bpmb1lb'], 'cfv-sr3-bpmb2lb': ['BOBR_cfv-sr3-bpmb2lb'],
                          'cfv-sr3-bpmb1rb': ['BOBR_cfv-sr3-bpmb1rb'], 'cfv-sr3-bpmb1ra': ['BOBR_cfv-sr3-bpmb1ra'],
                          'cfv-sr3-bpmb2ra': ['BOBR_cfv-sr3-bpmb2ra'], 'cfv-sr3-bpmb1la': ['BOBR_cfv-sr3-bpmb1la'],
                          'cfv-sr3-bpmb2rb': ['BOBR_cfv-sr3-bpmb2rb'], 'cfv-sr6-bpmint1': ['BOBR_cfv-sr6-bpmint1'],
                          'cfv-sr8-bpmb1rb': ['BOBR_cfv-sr8-bpmb1rb'], 'cfv-sx4-bpmb2rb': ['BOBR_cfv-sx4-bpmb2rb'],
                          'cfv-sr7-bpmb2lb': ['BOBR_cfv-sr7-bpmb2lb'], 'cfv-sr7-bpmb1la': ['BOBR_cfv-sr7-bpmb1la'],
                          'cfv-sr8-bpmb1la': ['BOBR_cfv-sr8-bpmb1la'], 'cfv-sr8-bpmb2ra': ['BOBR_cfv-sr8-bpmb2ra'],
                          'cfv-sr8-bpmb1ra': ['BOBR_cfv-sr8-bpmb1ra'], 'cfv-sr8-bpmb2lb': ['BOBR_cfv-sr8-bpmb2lb'],
                          'cfv-sx4-bpmb2la': ['BOBR_cfv-sx4-bpmb2la'], 'cfv-sr8-bpmb2la': ['BOBR_cfv-sr8-bpmb2la'],
                          'cfv-us45-bwsb2': ['BOBR_cfv-us45-bwsb2'], 'cfv-sr7-bpmb2ra': ['BOBR_cfv-sr7-bpmb2ra'],
                          'cfv-sr6-bpmb1ra': ['BOBR_cfv-sr6-bpmb1ra'], 'cfv-sr8-bpmb2rb': ['BOBR_cfv-sr8-bpmb2rb'],
                          'cfv-sr5-bpmb2lt': ['BOBR_cfv-sr5-bpmb2lt'], 'cfv-sr7-bpmb1ra': ['BOBR_cfv-sr7-bpmb1ra'],
                          'cfv-sr5-bpmb2la': ['BOBR_cfv-sr5-bpmb2la'], 'cfv-sr5-bpmb1lb': ['BOBR_cfv-sr5-bpmb1lb'],
                          'cfv-sr1-bpmb2lb': ['BOBR_cfv-sr1-bpmb2lb'], 'cfv-sr1-bpmb1rb': ['BOBR_cfv-sr1-bpmb1rb'],
                          'cfv-sr5-bpmb2rb': ['BOBR_cfv-sr5-bpmb2rb'], 'cfv-sr6-bpmb1lb': ['BOBR_cfv-sr6-bpmb1lb'],
                          'cfv-sr1-bpmb2la': ['BOBR_cfv-sr1-bpmb2la'], 'cfv-sr1-bpmb2rb': ['BOBR_cfv-sr1-bpmb2rb'],
                          'cfv-sx4-bpmb2lb': ['BOBR_cfv-sx4-bpmb2lb'], 'cfv-sr7-bpmb2rb': ['BOBR_cfv-sr7-bpmb2rb'],
                          'cfv-sr1-bpmb1ra': ['BOBR_cfv-sr1-bpmb1ra'], 'cfv-sx4-bpmb1rb': ['BOBR_cfv-sx4-bpmb1rb'],
                          'cfv-sx4-bpmb2ra': ['BOBR_cfv-sx4-bpmb2ra'], 'cfv-sx4-bpmb1la': ['BOBR_cfv-sx4-bpmb1la'],
                          'cfv-sr6-bpmb2lb': ['BOBR_cfv-sr6-bpmb2lb'], 'cfv-sr5-bpmb1rb': ['BOBR_cfv-sr5-bpmb1rb'],
                          'cfv-sr6-bpmb2ra': ['BOBR_cfv-sr6-bpmb2ra'], 'cfv-sr6-bpmb1rb': ['BOBR_cfv-sr6-bpmb1rb'],
                          'cfv-sr6-bpmb2rb': ['BOBR_cfv-sr6-bpmb2rb'], 'cfv-sr7-bpmb1lb': ['BOBR_cfv-sr7-bpmb1lb'],
                          'cfv-sx4-bpmb1lb': ['BOBR_cfv-sx4-bpmb1lb'], 'cfv-sr6-bpmb2la': ['BOBR_cfv-sr6-bpmb2la'],
                          'cfv-sr5-bpmb1la': ['BOBR_cfv-sr5-bpmb1la']}
        self.class_bsiso = "BSISO"
        self.fecs_bsiso = {
            'cfv-865-bidev3': ['TEST_SCANNER_ISO', 'TEST_SCANNER_ISO2', 'TEST_SCANNER_ISO3', 'TEST_SCANNER_ISO4',
                               'TEST_SCANNER_ISO5', 'TEST_SCANNER_ISO6', 'TEST_SCANNER_ISO7', 'TEST_SCANNER_ISO8'],
            'cfv-179-bisoscan': ['SPARE_HOOD_SC8', 'SPARE_HOOD_SC1', 'SPARE_HOOD_SC2', 'SPARE_HOOD_SC3',
                                 'SPARE_HOOD_SC4', 'SPARE_HOOD_SC5', 'SPARE_HOOD_SC6', 'SPARE_HOOD_SC7'],
            'cfv-179-bsmed': ['YMD3.BSC20'],
            'cfv-26-bsoffl2': ['YOFFL1.BSC330', 'YOFFL2.BSC70', 'YOFFL2.BSC100', 'YOFFL2.BSC170'],
            'cfv-197-bsiso': ['YRB0.BSC1500', 'SPARE2.SC', 'COLLECT.SC', 'YCA0.BSC0200', 'YCA0.BSC0660', 'YCA0.BSC0800',
                              'YCB0.BSC0800', 'YCC0.BSC0800', 'YCD0.BSC0800', 'YGHM.BSC0800', 'YGLM.BSC0800',
                              'YGPS.BSC4820', 'YGPS.BSC4830', 'YGPS.BSC5560', 'YHRS.BSC2600', 'YHRS.BSC4820',
                              'YHRS.BSC4830', 'YHRS.BSC6800', 'YHRS.BSC7460', 'YLA0.BSC0800', 'YLA2.BSC0800',
                              'YLA1.BSC0800', 'YRA0.BSC0800', 'YRB0.BSC0800', 'YRC0.BSC0150', 'YRC0.BSC0800',
                              'YRC2.BSC0800', 'YRC3.BSC0800', 'YRC4.BSC0800', 'YRC6.BSC0800', 'YLA3.BSC2600']}
        self.class_bstlhc = "BSTLHC"
        self.fecs_bstlhc = {'cfv-ccr-ctlbst1': ['LHC.BST.CCR.B1.F3'], 'cfv-ccr-ctlbst2': ['LHC.BST.CCR.B2.F3'],
                            'cfv-774-cttstbst': ['LAB.BST']}
