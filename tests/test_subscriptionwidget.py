import tests.subscriptionwidget_fixtures
from tests import *


class TestSubscriptionWidget(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.app = QApplication([])  # Initialize the QApplication for QWidget testing

    def setUp(self):

        self.threshold_tuples = {
            'voltageVioBM2C': (2.492, 2.499)
        }

        self.subscription_widget = TestCustomPropertySubscriptionWidget(
            device_name='TESTBCTFLHCB1',
            property_name='Diagnostics',
            title=f"Property Subscription Widget TESTBCTFLHCB1/Diagnostics",
            # 'currentVioBM2C' is the custom field
            fields=['bstTime', 'bstOk', 'voltageVioBM2C', 'currentVioBM2C', 'temperatureBoard'],
            thresholds=self.threshold_tuples,
            formats={"bstTime": ("date", 0.000000001), "bstOk": ("status", None)},
            scales={'currentVioBM2C': 1000},
            units={'voltageVioBM2C': 'V', 'currentVioBM2C': 'mA', 'temperatureBoard': 'C'},
            aliases={'bstTime': 'BST Time (bstTime)', 'bstOk': 'BST Status (bstOk)', 'voltageVioBM2C': 'Voltage BM2C',
                     'currentVioBM2C': 'Current BM2C', 'temperatureBoard': 'Temperature Board'}
        )

    def test_initial_label_setup(self):
        for field_widget in self.subscription_widget.field_widgets.values():
            if field_widget.field_name == 'bstTime':
                self.assertEqual(field_widget.label.text(), 'BST Time (bstTime)')
            elif field_widget.field_name == 'bstOk':
                self.assertEqual(field_widget.label.text(), 'BST Status (bstOk)')
            elif field_widget.field_name == 'voltageVioBM2C':
                self.assertEqual(field_widget.label.text(), 'Voltage BM2C')
            elif field_widget.field_name == 'currentVioBM2C':
                self.assertEqual(field_widget.label.text(), 'Current BM2C')
            elif field_widget.field_name == 'temperatureBoard':
                self.assertEqual(field_widget.label.text(), 'Temperature Board')
            self.assertEqual(field_widget.value_label.text(), 'N/A')

    def test_update_value_labels(self):
        response = get_response_example()

        self.subscription_widget.handle_event(name='TESTBCTFLHCB1/Diagnostics', value=response)

        expected_timestamp = formatting.format_value_to_string(response['bstTime'], type_factor=0.000000001,
                                                               type_format="date")

        expected_values = {
            'bstTime': expected_timestamp,
            'bstOk': "True" if response['bstOk'] == 1 else "False",
            'voltageVioBM2C': f"{response['voltageVioBM2C']} V",
            'currentVioBM2C': f"{(response['voltageVioBM2C'] / 100) * 1000} mA",
            'temperatureBoard': f"{response['temperatureBoard']} C"
        }

        # Check the updated value labels for each field
        for field_widget in self.subscription_widget.field_widgets.values():
            field_name = field_widget.field_name
            expected_value = expected_values.get(field_name)
            if expected_value:
                self.assertEqual(field_widget.value_label.text(), expected_value,
                                 f"Value label for {field_name} is incorrect.")

    def test_update_value_labels_style(self):
        response = get_response_example()

        self.subscription_widget.handle_event(name='TESTBCTFLHCB1/Diagnostics', value=response)

        t_low, t_high = self.threshold_tuples['voltageVioBM2C']

        expected_values = {
            'bstOk': Colors.STR_COLOR_GREEN if response['bstOk'] == 1 else Colors.STR_COLOR_RED,
            'voltageVioBM2C': f"{Colors.STR_COLOR_GREEN if (t_low <= response['voltageVioBM2C'] <= t_high) else Colors.STR_COLOR_RED}",
        }

        # Check the updated value labels for each field
        for field_widget in self.subscription_widget.field_widgets.values():
            field_name = field_widget.field_name
            expected_value = expected_values.get(field_name)
            if expected_value:
                self.assertEqual(field_widget.value_label.styleSheet().strip('color:').strip().strip(";"),
                                 expected_value,
                                 f"Value label color for {field_name} is incorrect.")


if __name__ == '__main__':
    unittest.main()
