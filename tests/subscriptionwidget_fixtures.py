from random import choice
from expert_gui_core.gui.widgets.common.subscriptionwidget import *

RESPONSES = [
    {'releaseApp': '24071501', 'intLevel': 3, 'intEnable': 1, 'voltageVadj': 2.493896484375, 'intMask': 242,
     'intSrcRdCount': 22360513,
     'releaseSys': '2.6.0', 'identSys': 'VFC-HD', 'bstTime': 1725874906001321000, 'voltageV3p3Fpga': 3.33251953125,
     'bstMaster': 'LHC1',
     'intAckCount': 22400162, 'temperatureBoard': 32.75, 'uniqueID': 'CB00000A59F4BD28', 'voltageV2p5': 2.537841796875,
     'wrBunchClkOk': 1,
     'bstOptPwr': -12.865095138549805, 'voltageVccPdAdj': 2.5439453125, 'bstOk': 1, 'temperatureChip': 48.0,
     'voltageVioBM2C': 2.49755859375,
     'intReqCount': 22360513, 'voltageV3p3Fmc': 3.22509765625, 'vfcAssetCode': 'HCBOEVA003-CR010257',
     'voltageV12p0Fmc': 12.05474853515625,
     'vfcLUN': 7, 'voltagePreVadj': 2.52197265625, 'identApp': 'BI_BCTFDI', 'intVector': 97, 'wrTurnClkOk': 1,
     '_cycle': 'no-selector',
     '_time': 1725874906000.364},
    {'releaseApp': '24071501', 'intLevel': 3, 'intEnable': 1, 'voltageVadj': 2.49267578125, 'intMask': 242,
     'intSrcRdCount': 22360922,
     'releaseSys': '2.6.0', 'identSys': 'VFC-HD', 'bstTime': 1725874914000874000, 'voltageV3p3Fpga': 3.330078125,
     'bstMaster': 'LHC1',
     'intAckCount': 22400571, 'temperatureBoard': 32.75, 'uniqueID': 'CB00000A59F4BD28', 'voltageV2p5': 2.535400390625,
     'wrBunchClkOk': 1,
     'bstOptPwr': -12.984319686889648, 'voltageVccPdAdj': 2.545166015625, 'bstOk': 0, 'temperatureChip': 48.0,
     'voltageVioBM2C': 2.498779296875,
     'intReqCount': 22360922, 'voltageV3p3Fmc': 3.223876953125, 'vfcAssetCode': 'HCBOEVA003-CR010257',
     'voltageV12p0Fmc': 12.05474853515625,
     'vfcLUN': 7, 'voltagePreVadj': 2.52197265625, 'identApp': 'BI_BCTFDI', 'intVector': 97, 'wrTurnClkOk': 1,
     '_cycle': 'no-selector',
     '_time': 1725874914000.364},
    {'releaseApp': '24071501', 'intLevel': 3, 'intEnable': 1, 'voltageVadj': 2.49267578125, 'intMask': 242,
     'intSrcRdCount': 22361639,
     'releaseSys': '2.6.0', 'identSys': 'VFC-HD', 'bstTime': 1725874928000871000, 'voltageV3p3Fpga': 3.331298828125,
     'bstMaster': 'LHC1',
     'intAckCount': 22401288, 'temperatureBoard': 32.75, 'uniqueID': 'CB00000A59F4BD28', 'voltageV2p5': 2.537841796875,
     'wrBunchClkOk': 1,
     'bstOptPwr': -12.984319686889648, 'voltageVccPdAdj': 2.545166015625, 'bstOk': 1, 'temperatureChip': 48.0,
     'voltageVioBM2C': 2.498779296875,
     'intReqCount': 22361639, 'voltageV3p3Fmc': 3.226318359375, 'vfcAssetCode': 'HCBOEVA003-CR010257',
     'voltageV12p0Fmc': 12.05474853515625,
     'vfcLUN': 7, 'voltagePreVadj': 2.523193359375, 'identApp': 'BI_BCTFDI', 'intVector': 97, 'wrTurnClkOk': 1,
     '_cycle': 'no-selector',
     '_time': 1725874928000.364},
    {'releaseApp': '24071501', 'intLevel': 3, 'intEnable': 1, 'voltageVadj': 2.493896484375, 'intMask': 242,
     'intSrcRdCount': 22361690,
     'releaseSys': '2.6.0', 'identSys': 'VFC-HD', 'bstTime': 1725874929001360000, 'voltageV3p3Fpga': 3.331298828125,
     'bstMaster': 'LHC1',
     'intAckCount': 22401339, 'temperatureBoard': 32.75, 'uniqueID': 'CB00000A59F4BD28', 'voltageV2p5': 2.53662109375,
     'wrBunchClkOk': 1,
     'bstOptPwr': -12.984319686889648, 'voltageVccPdAdj': 2.5439453125, 'bstOk': 1, 'temperatureChip': 48.0,
     'voltageVioBM2C': 2.5,
     'intReqCount': 22361690, 'voltageV3p3Fmc': 3.223876953125, 'vfcAssetCode': 'HCBOEVA003-CR010257',
     'voltageV12p0Fmc': 12.05474853515625,
     'vfcLUN': 7, 'voltagePreVadj': 2.52197265625, 'identApp': 'BI_BCTFDI', 'intVector': 97, 'wrTurnClkOk': 1,
     '_cycle': 'no-selector',
     '_time': 1725874929000.364}
]


def get_response_example() -> Dict:
    """
    Pics a random response from the set of Fixtures.
    The responses have been copied from real get events of TESTBCTFLHCB1/Diagnostics
    """
    return choice(RESPONSES)


class TestCustomPropertySubscriptionWidget(PropertySubscriptionWidget):
    def __init__(self, device_name, property_name, fields, units, formats, scales, aliases, title, thresholds):
        super().__init__(device_name=device_name, property_name=property_name, fields=fields, units=units,
                         scales=scales, formats=formats, aliases=aliases, title=title, thresholds=thresholds)

    def add_fields(self, response: Dict) -> Dict:
        """This implementation adds a single calculated field."""
        from copy import deepcopy

        new_response = deepcopy(response)
        fake_R = 100  # Ohms
        new_response['currentVioBM2C'] = response['voltageVioBM2C'] / fake_R
        return new_response
