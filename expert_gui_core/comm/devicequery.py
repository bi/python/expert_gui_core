"""
This module provides Class abstractions for obtaining grouped query results based on attributes
like Accelerator, FEC, Device class, and Device names.

Usage:
    1. Instantiate a `DeviceQuery` object, optionally providing lists of devices, FECs, accelerators, and classes
    to filter by.

    2. Apply a `DeviceQueryFilter` to further refine the list of devices based on inclusion/exclusion
    criteria.

    3. Use the `group_by` parameter to specify how the results should be grouped. You can use predefined types
    from the `GroupingType` enum or pass a custom list of grouping keys.

    4. Call the `dispatch()` method to execute the query and return the grouped results.
"""

import sys
from typing import List, Dict, Any
from enum import Enum

from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout
from pyccda.sync_models import Device

from expert_gui_core.comm import ccda
from expert_gui_core.gui import biapplicationframe
from expert_gui_core.gui.widgets.pyqt.treewidget import TreeWidget


class GroupingType(Enum):
    """Enum to define the types of grouping available for the DeviceQuery class.

    Attributes:
        DEVICES (str): Group by device names.
        FECS (str): Group by FEC names, then by device names.
        ACCELERATORS (str): Group by accelerator names, then FEC names, then device names.
        CLASSES (str): Group by device classes, then accelerator names, FEC names, and device names.
    """
    DEVICES = "devices"
    FECS = "fecs"
    ACCELERATORS = "accelerators"
    CLASSES = "classes"


GROUPING_KEYS = {
    GroupingType.DEVICES: ['name'],
    GroupingType.FECS: ['fec_name', 'name'],
    GroupingType.ACCELERATORS: ['accelerator_name', 'fec_name', 'name'],
    GroupingType.CLASSES: ['device_class_info.name', 'accelerator_name', 'fec_name', 'name']
}

# Map the GroupingType enum to corresponding labels
GROUP_LABELS = {
    'device_class_info.name': 'CLASSES',
    'accelerator_name': 'ACCELERATORS',
    'fec_name': 'FECS',
    'name': 'DEVICES'
}


class DeviceQueryFilter:
    """Class to organize filter objects used in DeviceQuery class.

    Holds the names to be included/excluded from the ccda query result obtained after the dispatch call on the
    DeviceQuery.

    Arguments:
        devs_include (List[str], optional): List of device names to include in the results, defaults to None.
        devs_exclude (List[str], optional): List of device names to exclude from the results, defaults to None.
        fecs_include (List[str], optional): List of FEC names to include in the results, defaults to None.
        fecs_exclude (List[str], optional): List of FEC names to exclude from the results, defaults to None.
        accs_include (List[str], optional): List of accelerator names to include in the results, defaults to None.
        accs_exclude (List[str], optional): List of accelerator names to exclude from the results, defaults to None.
        classes_include (List[str], optional): List of device classes to include in the results, defaults to None.
        classes_exclude (List[str], optional): List of device classes to exclude from the results, defaults to None.
    """

    def __init__(self, devices_include: List[str] = None, devices_exclude: List[str] = None,
                 fecs_include: List[str] = None, fecs_exclude: List[str] = None,
                 accelerators_include: List[str] = None, accelerators_exclude: List[str] = None,
                 classes_include: List[str] = None, classes_exclude: List[str] = None):
        self._devs_include = devices_include or []
        self._devs_exclude = devices_exclude or []
        self._fecs_include = fecs_include or []
        self._fecs_exclude = fecs_exclude or []
        self._accs_include = accelerators_include or []
        self._accs_exclude = accelerators_exclude or []
        self._classes_include = classes_include or []
        self._classes_exclude = classes_exclude or []

    def apply_filter(self, devices: List[Device]) -> List[Device]:
        """Applies the filter to the provided list of devices and returns the filtered list.

        Arguments:
            devices (List[Device]): List of Device objects to filter.

        Returns:
            filtered_devices (List[Device]) The filtered list of devices.
        """
        filtered_devices = []
        for device in devices:
            if (self._matches_filter(device.name, self._devs_include, self._devs_exclude) and
                    self._matches_filter(device.fec_name, self._fecs_include, self._fecs_exclude) and
                    self._matches_filter(device.accelerator_name, self._accs_include, self._accs_exclude) and
                    self._matches_filter(device.device_class_info.name, self._classes_include, self._classes_exclude)):
                filtered_devices.append(device)
        return filtered_devices

    @staticmethod
    def _matches_filter(name: str, include: List[str], exclude: List[str]) -> bool:
        """Checks if a name matches the inclusion and exclusion criteria."""
        if include and name not in include:
            return False
        if name in exclude:
            return False
        return True


class DeviceQuery:
    """A class to query and group devices based on various attributes like accelerators, FECs, and device classes.

    Depending on the GroupingType chosen, the class will return a dictionary or a list. If 'group_by=GroupingType.CLASS'
    you get a Dict e.g., {<Class Name A>: {<FEC Name>: [<Device Name 1>, <Device Name 2>, ...] ...}}. If no group_by
    is passed, you will get a List of Device objects.

    Arguments:
        accs (List[str], optional): List of accelerators to include in the query, defaults to None.
        classes (List[str], optional): List of device classes to include in the query, defaults to None.
        fecs (List[str], optional): List of FECs to include in the query, defaults to None.
        devices (List[str], optional): List of device names to include in the query, defaults to None.
        group_by (GroupingType | List[str], optional): Defines how the devices should be grouped.
                                                       Can be a GroupingType enum or a custom list of keys,
                                                       defaults to GroupingType.DEVICES.
        query_filter (DeviceQueryFilter): An optional filter to apply to the list of devices, defaults to None.

    Attributes:
        result (List[Device] | Dict | None): Query result.

    Example:
        >>> dq = DeviceQuery(devices=['*YGLM*'], fecs=['*197*'], accs=['ISO'], group_by=GroupingType.FECS)
        ... result = dq.dispatch()
        ... print(result)
    """

    def __init__(self, accs: List[str] = None, classes: List[str] = None, fecs: List[str] = None,
                 devices: List[str] = None, group_by: GroupingType | List[str] = GroupingType.DEVICES,
                 query_filter: DeviceQueryFilter = None):
        self.result = None
        self._accs = accs
        self._classes = classes
        self._fecs = fecs
        self._devices = devices
        self._group_by: GroupingType | List[str] = group_by
        self._query_filter = query_filter

    def dispatch(self) -> Dict | List[Device]:
        """Executes the query and returns the grouped devices based on the specified grouping.

        Returns:
            result (Dict | List[Device]):
        """
        devices_list = ccda.query_devices(device_query=self._devices, fec_query=self._fecs,
                                          acc_query=self._accs, class_query=self._classes)
        self.result = self._query_filter.apply_filter(devices=devices_list) if self._query_filter else devices_list

        if self._group_by:
            return self._group_by_attribute(self._group_by)
        return self.result

    def _group_by_attribute(self, group_by: GroupingType | List[str]) -> Dict:
        """Groups the devices by the specified grouping type or custom keys."""
        if isinstance(group_by, GroupingType):
            keys = GROUPING_KEYS[group_by]
        else:
            keys = group_by

        return self._group_devices_by_keys(self.result, keys)

    @staticmethod
    def _group_devices_by_keys(devices: List[Dict[str, Any]], keys: List[str]) -> Dict:
        """Groups a list of devices based on the provided keys and includes labels for better organization."""
        grouped = {}

        for device in devices:
            current_level = grouped
            for i, key in enumerate(keys):
                # Determine the label for this level
                label = GROUP_LABELS.get(key, key)
                current_key_value = device.__getattribute__(
                    key) if key != 'device_class_info.name' else device.device_class_info.name

                # Traverse or create the necessary dictionary structure
                if label not in current_level:
                    current_level[label] = {}
                if current_key_value not in current_level[label]:
                    current_level[label][current_key_value] = {} if i < len(keys) - 1 else []

                current_level = current_level[label][current_key_value]

            # Append the device at the final level
            current_level.append(device)

        return grouped


class BaseExample(biapplicationframe.BIApplicationFrame):
    """Base class for demonstrating the different types of queries."""

    def __init__(self, q_app=None, example_title="Property Subscription Widget Example", use_rbac=True):
        super().__init__(app=q_app, use_log_console=True, use_rbac=use_rbac, use_timing_bar=False)
        self.setWindowTitle(example_title)

        self.query = None
        self.result = None

        self._init_ui()
        self.dispatch()

        if self.rba_widget:
            self.rba_widget.loginSucceeded.connect(self.on_pyrbac_login)
            self.rba_widget.loginSucceeded.connect(self.dispatch)

    def _init_ui(self):
        self.log_console.console.expanded = False
        self.query = self._add_query_example()

        central_widget = QWidget()
        layout = QVBoxLayout(central_widget)

        # Initialize TreeWidget with default settings
        self.tree_widget = TreeWidget(data={}, header_hidden=True, include_filter=True)
        layout.addWidget(self.tree_widget)

        # Set the central widget and layout
        self._tabs.addTab(central_widget, "Query Result")
        self._tabs.setTabsClosable(False)
        self.setGeometry(0, 0, 800, 1000)

    def _add_query_example(self):
        raise NotImplementedError("Subclasses should implement this method.")

    def dispatch(self):
        self.result = self.query.dispatch()
        self.update_tree_widget()

    def update_tree_widget(self):
        """Update TreeWidget with the latest query result."""
        if isinstance(self.result, dict):
            self.tree_widget.update_model_data(self.result)

    def dark_(self):
        """
        Set dark theme.
        """
        super().dark_()
        self.tree_widget.dark_()

    def light_(self):
        """
        Set light theme.
        """
        super().light_()
        self.tree_widget.light_()


class Example1(BaseExample):
    """Example 1: Test DeviceQuery with devices_include."""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        # Test Case 1: Group by CLASSES with class filter
        filter_1 = DeviceQueryFilter(
            devices_include=['BOBR_cfv-3524-cmsbcm2', 'BOBR_cfv-774-cttstbst', 'BOBR_cfv-865-blm8',
                             'BOBR_cfv-865-bsrlhcdev', 'BOBR_cfv-865-bsrspsdev',
                             'SPARE_HOOD_SC2', 'SPARE_HOOD_SC3', 'FAKE_SPARE_HOOD_1', 'FAKE_SPARE_HOOD_2',
                             'SPARE_HOOD_SC4', 'COLLECT.SC', 'FAKE_COLLECT.SC']
        )

        query = DeviceQuery(classes=["BOBR", "BSISO"], group_by=GroupingType.FECS, query_filter=filter_1)
        return query


class Example2(BaseExample):
    """Example 2"""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        query = DeviceQuery(classes=["BSISO", "BOBR", "BSTLHC"], group_by=GroupingType.FECS)
        return query


class Example3(BaseExample):
    """Example 3"""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        query = DeviceQuery(classes=["BOBR"], accs=["LHC"])
        return query


class Example4(BaseExample):
    """Example 4"""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        filter_4 = DeviceQueryFilter(devices_include=['YGLM.THC.1010', 'YGLM.THC.1020'],
                                     devices_exclude=['YGLMX.OPEN-BGATE'],
                                     fecs_include=['cfc-197-tisomove'],
                                     fecs_exclude=['cfc-197-rplc'])

        query = DeviceQuery(devices=['*YGLM*'], fecs=['*197*'], accs=['ISO'], group_by=GroupingType.FECS,
                            query_filter=filter_4)
        return query


class Example5(BaseExample):
    """Example 5"""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        filter_5 = DeviceQueryFilter(accelerators_include=['ISO', 'LHC'],
                                     accelerators_exclude=['LHC'],
                                     devices_include=['YGLM.THC.1010', 'YGLM.THC.1020'],
                                     fecs_exclude=['cfc-197-rplc'])

        query = DeviceQuery(devices=['*YGLM*'], fecs=['*197*'], group_by=GroupingType.FECS, query_filter=filter_5)
        return query


class Example6(BaseExample):
    """Example 6: Grouping by arbitrary key from Device object."""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    def _add_query_example(self):
        filter_6 = DeviceQueryFilter(classes_include=['PowPLC'])

        query = DeviceQuery(devices=['*YGLM*'], fecs=['*197*'], accs=['ISO'], group_by=['description'],
                            query_filter=filter_6)
        return query


class Example7(BaseExample):
    """Example 7: Test response formatting used in devicesfesawidget."""

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

    @staticmethod
    def transform_classes(data):
        """Simplifies the mapping to: {<Class Name A>: {<FEC Name>: [<Device Name 1>, <Device Name 2>, ...] ...}}
        
        It requires a DeviceQuery with class args (e.g. 'BOBR', "BSISO"), and group_by=GroupingType.CLASSES.
        """
        transformed_data = {}

        # Iterate over each top-level class
        for class_name, class_data in data.get('CLASSES', {}).items():
            new_class_data = {}

            # Iterate over 'ACCELERATORS' keys (like 'LHC', 'SPS', 'NONE', etc.)
            for accelerators_key in class_data.get('ACCELERATORS', {}):
                accelerators_data = class_data['ACCELERATORS'][accelerators_key]

                # Iterate over 'FECS' keys (e.g., 'cfv-865-bidev3') and get all device_names that belong to it.
                for fec_key in accelerators_data.get('FECS', {}):
                    device_names = list(accelerators_data['FECS'][fec_key]['DEVICES'].keys())

                    # Reconstruct the new format
                    new_class_data[fec_key] = device_names

            # Assign the transformed data to the corresponding class name
            transformed_data[class_name] = new_class_data

        return transformed_data

    @staticmethod
    def build_device_mapping(data):
        """Builds a mapping of each item in the sub-dictionaries to their corresponding top-level key.

        E.g. {<Device Name 1>: <Class Name A>, <Device Name 2>: <Class Name A>, ..., <Device Name 99>: <Class Name B>}
        """
        result = {}

        for top_key, sub_dict in data.items():
            # Iterate through the sub-dictionaries
            for device, items in sub_dict.items():
                # Assign each item to the top-level key in the result dictionary
                for item in items:
                    result[item] = top_key

        return result

    def _add_query_example(self):
        query = DeviceQuery(classes=['BOBR', "BSISO"], group_by=GroupingType.CLASSES)

        return query

    def dispatch(self):
        self.result = self.query.dispatch()
        self.result = self.transform_classes(self.result)
        print(self.build_device_mapping(self.result))
        self.update_tree_widget()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    # Create instances of each example window
    example = Example6(application=app, example_title="Example6")
    example.light_()
    # Show all example windows
    example.show()

    sys.exit(app.exec_())
