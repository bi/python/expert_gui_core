import numpy as np


class DataObject():
    """    
    Data object to process data and history before plot.
    
    :param max: max length in history
    :type max: int, optional
    :param history: keep history data with a length = max (default is 20)            
    :type history: bool, optional
    """

    def __init__(self, history=False, max=None):
        """
        Initialize class.
        """
        self._reduce_array = True
        if max is not None:
            self._reduce_array = False
            self.MAX_ARRAY_NUMBER = max
        else:
            self.MAX_ARRAY_NUMBER = 60
        self._data = {}
        self._datax = {}
        self._time = {}
        self._history = history
        self._optimize = False

    def clear_data(self):
        """"
        Clear data buffer.
        """

        for name in self._data:
            self._data[name] = []
            self._datax[name] = []
            self._time[name] = []

    def pop(self, index=0):
        for name in self._data:
            try:
                self._data[name].pop(index)
                self._datax[name].pop(index)
                self._time[name].pop(index)
            except Exception as e:
                print(e)
                pass

    def add(self, data, datax=None, time_value=0, name="default"):
        """
        Set new data (scalar or array) for a given name.
        
        :param data: Data to input
        :type data: all
        :param datax: Data x to input (not mandatory)
        :type datax: all, optional
        :param time_value: Timestamp (not used)
        :type time_value: long, optional
        :param name: Name of the measurement
        :type name: str, optional
        """

        # check data 

        if data is None:
            return

        # init data

        if name not in self._data:
            self._data[name] = []
            self._time[name] = []
            self._datax[name] = None

        # size array should be the same-

        check_2d = True
        try:
            le = len(data[0])
        except:
            check_2d = False

        len_data = 0
        try:
            len_data = len(self._data[name])
        except Exception as e:
            try:
                len_data = self._data[name].size()
            except Exception as ee:
                len_data = 0

        if not check_2d:

            leng = 0
            try:
                leng = len(data)
            except:
                try:
                    leng = data.size()
                except:
                    leng = 0

            if len_data > 0:
                len_first = 0
                try:
                    len_first = len(self._data[name][0])
                except:
                    try:
                        len_first = self._data[name][0].size()
                    except:
                        len_first = 0

                if len_first > 0:
                    if leng > len_first:
                        data = data[0:len_first]
                        if datax is not None:
                            datax = datax[0:len_first]
                    elif leng < len_first:
                        zer = []
                        zerx = []
                        for i in range(leng, len_first):
                            zer.append(0)
                            if datax is not None:
                                zerx.append(i)
                        data = np.append(data, zer)
                        if datax is not None:
                            datax = np.append(datax, zerx)

        try:
            if data.dtype == 'int16':
                data = data.astype(np.int32)
            if datax is not None:
                if datax.dtype == 'int16':
                    datax = datax.astype(np.int32)
        except:
            pass

        # check datax order

        try:
            if datax is not None:
                if len(datax) > 1:
                    if datax[0] > datax[1]:
                        datax = datax[::-1]
                        data = data[::-1]
        except:
            pass

        # history

        if self._history:

            # check size (check maximum and accepted 2d size)

            if type(data) is list:
                if len(data) > 100000:
                    self.MAX_ARRAY_NUMBER = 10
            else:
                try:
                    if data.size > 100000:
                        self.MAX_ARRAY_NUMBER = 10
                except:
                    pass
            if len_data >= self.MAX_ARRAY_NUMBER:
                self._data[name] = self._data[name][1:]
                self._time[name] = self._time[name][1:]

            # append data

            self._data[name].append(data)
            self._time[name].append(time_value)

            if datax is not None:

                if self._datax[name] is None:
                    self._datax[name] = []

                if len_data >= self.MAX_ARRAY_NUMBER:
                    self._datax[name] = self._datax[name][1:]
                self._datax[name].append(datax)

        # normal

        else:

            # check type and append data

            if type(data) is list:
                try:
                    data = np.array(data)
                    self._data[name] = data
                except:

                    datatmp = np.zeros([len(data), len(max(data, key=lambda x: len(x)))])

                    for i, j in enumerate(data):
                        datatmp[i][0:len(j)] = j

                    self._data[name] = datatmp
            else:

                # append

                self._data[name] = data

            # same with datax

            if datax is not None:
                if self._datax[name] is None:
                    self._datax[name] = []
                if type(datax) is list:
                    datax = np.array(datax)

            if datax is not None:
                self._datax[name] = datax

            # check 2d array type (size)

            try:

                # 2d?

                twod = False
                try:
                    if len(self._data[name][0]) > 0:
                        twod = True
                except:
                    pass

                # 2d size > 120

                if self._reduce_array and twod is True and len(self._data[name]) > 120:
                    if len(self._data[name][0]) > 1000:
                        self.MAX_ARRAY_NUMBER = int(5000000 / len(self._data[name][0]))
                    else:
                        self.MAX_ARRAY_NUMBER = 120
                    self._optimize = True

                # < 120 but > 5000 pts per line ?

                elif not twod:
                    if len(self._data[name]) > 0:
                        if len(self._data[name]) > 50000:
                            self.MAX_ARRAY_NUMBER = int(5000000 / len(self._data[name][0]))
                            self._optimize = True

                # minimum 10 arrays

                if len(self._data[name]) > 10 and self.MAX_ARRAY_NUMBER < 10:
                    self.MAX_ARRAY_NUMBER = 10

                # maximum 120 arrays

                if self._reduce_array and self.MAX_ARRAY_NUMBER > 120:
                    self.MAX_ARRAY_NUMBER = 120
                    self._optimize = True

            except Exception as e:
                pass

    def clear(self):
        """
        Clear data object.
        """
        self._data = {}
        self._datax = {}

    def get_last(self, name="default"):
        """
        Get the latest array for a given name.
        
        :param name: Name of the measurement
        :type name: str, optional
        :return: Last value (scalar or array)
        :rtype: all
        """
        return self._data[name][-1]

    def get_lastx(self, name="default"):
        """
        Get the latest array for a given name.
        
        :param name: Name of the measurement
        :type name: str, optional
        :return: Last xvalue (scalar)
        :rtype: all
        """
        if (self._datax is not None) and (self._datax[name] is not None) and (len(self._datax[name]) > 0):
            return self._datax[name][-1]
        return None

    def get(self, name="default"):
        """
        Get the whole array for a given name.
        
        :param name: Name of the measurement
        :type name: str, optional
        :return: data object
        :rtype: scalar or array or 2D array
        """
        return self._data[name]

    def getx(self, name="default"):
        """
        Get the whole array for a given name.
            
        :param name: Name of the measurement
        :type name: str, optional
        :return: data object
        :rtype: scalar or array or 2D array
        """
        return self._datax[name]

    def gett(self, name="default"):
        """
        Get the whole time array for a given name.
            
        :param name: Name of the measurement
        :type name: str, optional
        :return: data object
        :rtype: scalar or array or 2D array
        """
        return self._time[name]

    def keys(self):
        """
        Return all data keys.
        :return: List of data keys
        :rtype: list
        """
        return self._data.keys()

    def change_max_array_number(self, new_number):
        """
        Change the MAX_ARRAY_NUMBER
        :param new_number: New array number
        :type new_number: int
        """
        self.MAX_ARRAY_NUMBER = new_number
