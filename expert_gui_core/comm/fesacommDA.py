import sys
from time import sleep
import numpy as np
import logging
from abc import abstractmethod

from PyQt5.QtWidgets import QApplication

import pyda
import pyda_japc

from expert_gui_core.tools import formatting
from expert_gui_core.comm import ccda

class FesaCommListener():
    """
    Listener to FESA notifiction events.
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        pass


class FesaComm:
    """
    FESA communication wrapper.

    :param dev: Device name.
    :type param: str
    :param prop: Property name.
    :type prop: str
    :param listener: FesaListener object.
    :type listener: object, optional
    :param field: Field name.
    :type field: str, optional
    :param inca: Inca or not JAPC communication (not used).
    :type inca: bool, optional

    """

    providerDA = None
    clientDA = None
    clientSDA = None

    def __init__(self, dev, prop, listener=None, field=None, min_time_between_event=20, inca=True, check_data=True,
                 class_name=None):
        """
        Initialize class.
        """

        self._device = dev
        self._property = prop
        self._field = field
        self._sub = None

        if FesaComm.providerDA is None:

            if inca is None:
                self.providerDA = pyda_japc.JapcProvider(enable_inca=False)
            else:
                self.providerDA = pyda_japc.JapcProvider()

            self.clientDA = pyda.CallbackClient(provider=self.providerDA)
            self.clientSDA = pyda.SimpleClient(provider=self.providerDA)

        self._lock = False
        self._time_now = 0
        self.it = 0

        self._properties = ccda.get_properties_object_from_device(dev, class_name=class_name)
        self._check_data = check_data

        self._listener = listener
        self._min_time_between_event = min_time_between_event

    def get(self, cycle=None):
        """
        Get FESA property.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Datamap object
        :rtype: dict
        """

        if cycle == "":
            cycle = None

        logging.info("Get : " + self._device + "/" + self._property)

        try:
            if cycle is None:
                result = self.clientSDA.get(self._device + "/" + self._property)
            else:
                result = self.clientSDA.get(self._device + "/" + self._property, context=cycle)

        except Exception as e:
            try:
                result = self.clientSDA.get(self._device + "/" + self._property)
            except:
                return None

        value = result.value.mutable_copy()

        for val in value:
            if type(value[val]) == pyda.data.EnumValue:
                value[val] = value[val].value

        return self.check(value)

    def get_action(self, cycle=None, event=True):
        """
        Get FESA property and trig handle event.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :param event: Trigger or not the event function.
        :type event: bool
        :return: Datamap object
        :rtype: dict
        """

        if cycle == "":
            cycle = None

        try:
            if cycle is None:
                result = self.clientSDA.get(self._device + "/" + self._property)
            else:
                result = self.clientSDA.get(self._device + "/" + self._property, context=cycle)

        except Exception as e:
            try:
                result = self.clientSDA.get(self._device + "/" + self._property)
            except:
                return None

        if event:
            self.handle_event(result)

        return result

    def set(self, result, cycle=None):
        """
        Set a FESA property.

        :param result: Datamap to set
        :type result: dict
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        """

        if cycle == "":
            cycle = None

        msg = "Set : " + self._device + "/" + self._property
        logging.info(msg)

        result = self.check(result)

        resp = None
        try:
            if cycle is None:
                resp = self.clientSDA.set(self._device + "/" + self._property, value=result)
            else:
                resp = self.clientSDA.set(self._device + "/" + self._property, value=result, context=cycle)
        except Exception as e:
            print("ERROR", e)
            try:
                resp = self.clientSDA.set(self._device + "/" + self._property, value=result)
            except Exception as e2:
                msg = "Error set : " + self._device + "/" + self._property + " " + str(e2)
                logging.error(msg)
                return {"err": 1, "msg": msg}

        print("end set data", resp)

        return {"err": 0, "msg": msg}

    def void(self, response):
        pass

    def subscribe(self, cycle=None):
        """
        Subscribe to a FESA device property.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Status object {err value, msg}
        :rtype: dict
        """

        if cycle == "":
            cycle = None

        if self._sub is not None:
            self.unsubscribe()

        if cycle is not None:
            logging.info(cycle + " " + self._device + "/" + self._property)
        else:
            logging.info(self._device + "/" + self._property)

        try:

            if cycle is not None:
                self._sub = self.clientDA.subscribe(self._device + "/" + self._property, callback=self.handle_event,
                                                    context=cycle)
            else:
                self._sub = self.clientDA.subscribe(self._device + "/" + self._property, callback=self.handle_event)

            self._sub.start()

            if cycle is not None:
                msg = "subscribe ok to cycle " + cycle
            else:
                msg = "subscribe ok"

            logging.info(msg)

            return {"err": 0, "msg": msg}

        except Exception as e:

            logging.warning("problem", e)

            try:

                self._sub = self.clientDA.subscribe(self._device + "/" + self._property, callback=self.handle_event)
                self._sub.start()
            except:

                msg = "subscribe not ok! : empty cycle : " + self._device + "/" + self._property
                logging.error(msg)

                return {"err": 1, "msg": msg}

    def unsubscribe(self):
        """
        Unsubscribe to a FESA device property.
        """

        if self._sub is not None:
            self._sub.stop()
            self._sub = None

    def check(self, result):
        if self._check_data:
            if len(self._properties) > 0:
                for propertyfesa in self._properties:
                    if propertyfesa is not None:
                        if self._property == propertyfesa.name:
                            for elt in result.keys():
                                print(elt)
                                for pf in propertyfesa.data_fields:
                                    if pf.name == elt:
                                        self.check_data(pf.data_type, pf.primitive_data_type, pf.name, result)

        return result

    def check_data(self, data_type, primitive_type, name_field, result):

        # 2D
        if "array2D" in str(data_type):
            try:

                str_primitive_type = str(primitive_type).lower()
                try:
                    shape = result[name_field].shape
                except:
                    shape = [0]

                # 1D -> should be 2D
                if len(shape) == 1:

                    size2 = shape[0]
                    single = False

                    # 1D single -> should be 2D
                    if size2 == 0:
                        single = True
                        size2 = 1

                    if "int" in str_primitive_type:
                        if type(result[name_field][0]) == pyda.data.EnumValue:
                            val = np.zeros(shape=(1, size2), dtype=np.intc)
                            for i in range(0, size2):
                                val[0][i] = int(result[name_field][i].value)
                            result[name_field] = val
                        else:
                            doit = False
                            if single and ("int" not in str(type(result[name_field]))):
                                doit = True
                            elif "int" not in str(type(result[name_field][0])):
                                doit = True
                            if doit:
                                val = np.zeros(shape=(1, size2), dtype=np.intc)
                                if single:
                                    val[0][0] = result[name_field]
                                else:
                                    val[0] = result[name_field]
                                result[name_field] = val
                    elif "short" in str_primitive_type:
                        doit = False
                        if single and ("int" not in str(type(result[name_field]))):
                            doit = True
                        elif "int" not in str(type(result[name_field][0])):
                            doit = True
                        if doit:
                            val = np.zeros(shape=(1, size2), dtype=np.short)
                            if single:
                                val[0][0] = result[name_field]
                            else:
                                val[0] = result[name_field]
                            result[name_field] = val
                    elif "long" in str_primitive_type:
                        doit = False
                        if single and ("int" not in str(type(result[name_field]))):
                            doit = True
                        elif "int" not in str(type(result[name_field][0])):
                            doit = True
                        if doit:
                            val = np.zeros(shape=(1, size2), dtype=np.int_)
                            if single:
                                val[0][0] = result[name_field]
                            else:
                                val[0] = result[name_field]
                            result[name_field] = val
                    elif "float" in str_primitive_type:
                        doit = False
                        if single and ("float" not in str(type(result[name_field]))):
                            doit = True
                        elif "float" not in str(type(result[name_field][0])):
                            doit = True
                        if doit:
                            val = np.zeros(shape=(1, size2), dtype=np.single)
                            if single:
                                val[0][0] = result[name_field]
                            else:
                                val[0] = result[name_field]
                            result[name_field] = val
                    elif "double" in str_primitive_type:
                        doit = False
                        if single and ("double" not in str(type(result[name_field]))):
                            doit = True
                        elif "double" not in str(type(result[name_field][0])):
                            doit = True
                        if doit:
                            val = np.zeros(shape=(1, size2), dtype=np.double)
                            if single:
                                val[0][0] = result[name_field]
                            else:
                                val[0] = result[name_field]
                            result[name_field] = val
                    elif "bool" in str_primitive_type:
                        doit = False
                        if single and ("bool" not in str(type(result[name_field]))):
                            doit = True
                        elif "bool" not in str(type(result[name_field][0])):
                            doit = True
                        if doit:
                            val = np.zeros(shape=(1, size2), dtype=bool)
                            if single:
                                val[0][0] = result[name_field]
                            else:
                                val[0] = result[name_field]
                            result[name_field] = val
                elif type(result[name_field][0][0]) == pyda.data.EnumValue:
                    val = np.zeros(shape=(shape[0], shape[1]), dtype=np.intc)
                    for i in range(0, shape[0]):
                        for j in range(0, shape[1]):
                            val[i][j] = int(result[name_field][i][j].value)
                    result[name_field] = val
            except Exception as e:
                try:
                    val = np.zeros((1, result[name_field].size))
                    val[0] = result[name_field]
                    result[name_field] = val
                except:
                    val = np.zeros((1, 1))
                    val[0] = result[name_field]
                    result[name_field] = val
        elif "array" in str(data_type):
            try:
                str_primitive_type = str(primitive_type).lower()
                if len(result[name_field].shape) == 0:
                    if "int" in str_primitive_type:
                        if type(result[name_field][0]) == pyda.data.EnumValue:
                            val = np.zeros(shape=(result[name_field].shape[0]), dtype=np.intc)
                            for i in range(0, result[name_field].shape[0]):
                                val[i] = int(result[name_field][i].value)
                            result[name_field] = val
                        else:
                            val = np.zeros(shape=(1), dtype=np.intc)
                            val[0] = result[name_field]
                            result[name_field] = val
                    elif "short" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.short)
                        val[0] = result[name_field]
                        result[name_field] = val
                    elif "long" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.int_)
                        val[0] = result[name_field]
                        result[name_field] = val
                    elif "float" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.single)
                        val[0] = result[name_field]
                        result[name_field] = val
                    elif "double" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.double)
                        val[0] = result[name_field]
                        result[name_field] = val
                    elif "bool" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=bool)
                        val[0] = result[name_field]
                        result[name_field] = val
                elif type(result[name_field][0]) == pyda.data.EnumValue:
                    val = np.zeros(shape=(result[name_field].shape[0]), dtype=np.intc)
                    for i in range(0, result[name_field].shape[0]):
                        val[i] = int(result[name_field][i].value)
                    result[name_field] = val
            except:
                if "int" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.intc)
                elif "short" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.short)
                elif "long" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.int_)
                elif "float" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.single)
                elif "double" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.double)
                elif "bool" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=bool)
                val[0] = result[name_field]
                result[name_field] = val
        else:
            str_primitive_type = str(primitive_type).lower()
            str_type = str(type(result[name_field])).lower()
            if "int" in str_primitive_type:
                if type(result[name_field]) == pyda.data.EnumValue:
                    result[name_field] = int(result[name_field].value)
                elif "int" not in str_type:
                    val = int(result[name_field])
                    result[name_field] = val
            elif "short" in str_primitive_type:
                if "int" not in str_type:
                    val = int(result[name_field])
                    result[name_field] = val
            elif "long" in str_primitive_type:
                if "int" not in str_type:
                    val = int(result[name_field])
                    result[name_field] = val
            elif "float" in str_primitive_type:
                if "float" not in str_type:
                    val = float(result[name_field])
                    result[name_field] = val
            elif "double" in str_primitive_type:
                if "float" not in str_type:
                    val = float(result[name_field])
                    result[name_field] = val
            elif "bool" in str_primitive_type:
                if "bool" not in str_type:
                    val = bool(result[name_field])
                    result[name_field] = val
            elif type(result[name_field]) == pyda.data.EnumValue:
                result[name_field] = int(result[name_field].value)

    def handle_event(self, response):
        """
        Notification received.

        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...).
        """

        if formatting.current_milli_time() - self._time_now < self._min_time_between_event:
            return

        self._time_now = formatting.current_milli_time()

        try:
            value = response.value.mutable_copy()
        except Exception as e:
            return

        # add time and cycle in data map

        try:
            if response.value.header is not None:
                value['_time'] = response.value.header['acquisition_time'].timestamp() * 1000
            else:
                value['_time'] = self._time_now
        except:
            value['_time'] = self._time_now

        try:
            if response.value.header is not None:
                value['_cycle'] = response.value.header['criteria']
            else:
                value['_cycle'] = ""
        except:
            value['_cycle'] = ""

        value = self.check(value)

        # notification

        if self._listener is not None:
            self._listener.handle_event(response.query, value)


class _Example(FesaCommListener):
    """
    Example to test.
    """

    _update_time = 0

    def get(self):
        """
        Get FESA property.

        :rtype: dict
        """
        # fesa_comm = FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)
        # fesa_comm = FesaComm("BISWRef2", "Acquisition", listener=self)
        # fesa_comm = FesaComm("BSRA_TEST1", "ExpertSetting", listener=self)
        fesa_comm = FesaComm("BSRA_TEST1", "Setting_AmpCtrl", listener=self)
        # fesa_comm = FesaComm("BISWRef2", "Settings", listener=self)

        # fesa_comm = FesaComm("SPS.BWS.41677.V", "SPSSetting", listener=self)

        return fesa_comm.get("")
        # return fesa_comm.get("SPS.USER.LHCMD2")

    def subscribe(self):
        """
        Subscribe to a FESA device property.
        """

        # fesa_comm = FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)

        fesa_comm = FesaComm("SPS.BWS.41677.V", "SPSSetting", listener=self)

        # fesa_comm = FesaComm("BGC.DigiCam", "LastImage", listener=self)
        # fesa_comm = FesaComm("BISWRef2", "Acquisition", listener=self)

        fesa_comm.subscribe("SPS.USER.LHCMD2")

        sleep(20000)

    def handle_event(self, name, value):
        """
        Notification received.

        :param header:
        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        # if value['_time'] - self._update_time > 150:

        # print(value)

        # print("# ", (formatting.current_milli_time() - self._update_time))
        # # self.profile.set_data(value['image2D'])
        self._update_time = formatting.current_milli_time()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    ex = _Example()
    # ex.subscribe()
    res = ex.get()
    print(res)

    sys.exit(app.exec_())
