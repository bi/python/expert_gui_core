import sys
from time import sleep
import numpy as np
import logging
from abc import abstractmethod

from PyQt5.QtWidgets import QApplication

from pyjapc import PyJapc

from expert_gui_core.tools import formatting
from expert_gui_core.comm import ccda


class FesaCommListener():
    """
    Listener to FESA notifiction events.
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        pass


class FesaComm:
    """
    FESA communication wrapper.

    :param dev: Device name.
    :type param: str
    :param prop: Property name.
    :type prop: str
    :param listener: FesaListener object.
    :type listener: object, optional
    :param field: Field name.
    :type field: str, optional
    :param inca: Inca or not JAPC communication (not used).
    :type inca: bool, optional
    """

    JAPC = None

    def __init__(self, dev, prop, listener=None, field=None, min_time_between_event=20, noPyConversion=False, inca=None,
                 check_data=True, rbac=False, class_name=None):
        """
        Initialize class.
        """
        self._device = dev
        self._property = prop
        self._field = field
        self._sub = None
        self._noPyConversion = noPyConversion

        # from time import process_time
        # t1_start = process_time()

        if FesaComm.JAPC is None:
            FesaComm.JAPC = PyJapc(incaAcceleratorName=None)
            if rbac:
                FesaComm.JAPC.rbacLogin(loginDialog=True)

        # t1_stop = process_time()
        # print("Elapsed time during the whole program in seconds:", t1_stop - t1_start)

        self._properties = ccda.get_properties_object_from_device(dev, class_name=class_name)
        self._lock = False
        self._time_now = 0
        self.it = 0
        self._check_data = check_data
        self._listener = listener
        try:
            if len(self._listener) > 0:
                pass
        except:
            self._listener = [listener]
        self._min_time_between_event = min_time_between_event

    def add_listener(self, listener):
        self._listener.append(listener)

    def get(self, cycle=None):
        """
        Get FESA property.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Datamap object
        :rtype: dict
        """

        if cycle == "":
            cycle = None

        if cycle is None:
            FesaComm.JAPC.setSelector("")
        else:
            FesaComm.JAPC.setSelector(cycle)

        logging.info("Get : " + self._device + "/" + self._property)

        try:
            result = FesaComm.JAPC.getParam(self._device + "/" + self._property, getHeader=True)
        except:
            try:
                FesaComm.JAPC.setSelector("")
                result = FesaComm.JAPC.getParam(self._device + "/" + self._property, getHeader=True)
            except:
                return None

        return [self.check(result[0]), result[1]]

    def get_action(self, cycle=None, event=True, extra=True):
        """
        Get FESA property and trig handle event.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :param event: Trigger or not the event function.
        :type event: bool
        :return: Datamap object
        :rtype: dict
        """

        if cycle == "":
            cycle = None

        result_get = self.get(cycle)

        if result_get is None:
            logging.error("Get result none for : " + self._device + "/" + self._property)
            return

        if len(result_get) > 1:

            result = result_get[0]
            header = result_get[1]
            result['_cycle'] = header['selector']
            result['_time'] = header['acqStamp'].timestamp() * 1000

            if self._property == '_DeviceInfo':
                result["acqStamp"] = int(header['acqStamp'].timestamp())
        else:
            result = result_get

        if extra:
            result['_time'] = formatting.current_milli_time()
            result['_cycle'] = cycle

        if event:
            self.handle_event(self._device + "/" + self._property, result)

        return result

    def set(self, result, cycle=None):
        """
        Set a FESA property.

        :param result: Datamap to set
        :type result: dict
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        """

        if cycle == "":
            cycle = None

        if cycle is None:
            FesaComm.JAPC.setSelector("")
        else:
            FesaComm.JAPC.setSelector(cycle)

        msg = "Set : " + self._device + "/" + self._property

        logging.info(msg)

        result = self.check_set(result)

        try:
            FesaComm.JAPC.setParam(self._device + "/" + self._property, result, checkDims=False)
        except Exception as e:
            try:
                FesaComm.JAPC.setSelector("")
                FesaComm.JAPC.setParam(self._device + "/" + self._property, result, checkDims=False)
            except Exception as e2:
                msg = "Error1 set : " + self._device + "/" + self._property + " " + str(e)
                msg = msg + " Error2 set : " + self._device + "/" + self._property + " " + str(e2)
                logging.error(msg)
                return {"err": 1, "msg": msg}
        return {"err": 0, "msg": msg}

    def subscribe(self, cycle=None):
        """
        Subscribe to a FESA device property.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Status object {err value, msg}
        :rtype: dict
        """
        if cycle == "":
            cycle = None

        if self._sub is not None:
            self.unsubscribe()

        if cycle is not None:
            logging.info(cycle + " " + self._device + "/" + self._property)
        else:
            logging.info(self._device + "/" + self._property)

        if cycle is None:
            FesaComm.JAPC.setSelector("")
        else:
            FesaComm.JAPC.setSelector(cycle)

        try:
            self._sub = FesaComm.JAPC.subscribeParam(parameterName=(self._device + "/" + self._property),
                                                     onValueReceived=self.handle_event, getHeader=True)
            self._sub.startMonitoring()

            if cycle is not None:
                msg = "subscribe ok to cycle " + cycle
            else:
                msg = "subscribe ok"

            logging.info(msg)

            return {"err": 0, "msg": msg}
        except:
            if cycle is not None:
                logging.warning("problem cycle" + cycle)
            try:
                FesaComm.JAPC.setSelector("")
                self._sub = FesaComm.JAPC.subscribeParam(self._device + "/" + self._property, self.handle_event,
                                                         getHeader=True)
                self._sub.startMonitoring()

            except:

                msg = "subscribe not ok! : empty cycle : " + self._device + "/" + self._property
                logging.error(msg)
                self._sub = None
                return {"err": 1, "msg": msg}

    def unsubscribe(self):
        """
        Unsubscribe to a FESA device property.
        """
        if self._sub is not None:

            try:
                self._sub.stopMonitoring()
            except Exception as e:
                print(e)

            self._sub = None

    def handle_event(self, name, value, header=None):
        """
        Notification received.

        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...).
        """

        if formatting.current_milli_time() - self._time_now < self._min_time_between_event:
            return

        self._time_now = formatting.current_milli_time()

        # add time and cycle in data map
        try:
            if header is not None:
                value['_cycle'] = header['selector']
                value['_time'] = header['acqStamp'].timestamp() * 1000
            else:
                value['_cycle'] = ""
                value['_time'] = self._time_now
        except:
            value['_cycle'] = ""
            value['_time'] = self._time_now

        # notification

        value = self.check(value)

        if self._listener is not None:
            for list in self._listener:
                list.handle_event(name, value)

    def check(self, result):
        if self._check_data:
            if len(self._properties) > 0:
                for propertyfesa in self._properties:
                    if propertyfesa is not None:
                        if self._property == propertyfesa.name:
                            for elt in result.keys():
                                for pf in propertyfesa.data_fields:
                                    if pf.name == elt:
                                        self.check_data(pf.data_type, pf.primitive_data_type, pf.name, result)
        return result

    def check_set(self, result):
        if self._check_data:
            if len(self._properties) > 0:
                for propertyfesa in self._properties:
                    if propertyfesa is not None:
                        if self._property == propertyfesa.name:
                            for elt in result.keys():
                                for pf in propertyfesa.data_fields:
                                    if pf.name == elt:
                                        self.check_data_set(pf.data_type, pf.primitive_data_type, pf.name, result)
        return result

    # type_value_to_java

    def check_data(self, data_type, primitive_type, name_field, result):
        if "array2D" in str(data_type):
            try:
                str_primitive_type = str(primitive_type).lower()
                try:
                    shape = result[name_field].shape
                except:
                    shape = [0]
                if len(shape) == 1:
                    size2 = shape[0]
                    single = False
                    if size2 == 0:
                        single = True
                        size2 = 1
                    if "int" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.intc)
                    elif "short" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.short)
                    elif "long" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.int_)
                    elif "float" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.single)
                    elif "double" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.double)
                    elif "byte" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.uint8)
                    elif "bool" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=bool)
                    elif "string" in str_primitive_type:
                        val = np.empty(shape=(1, size2), dtype=object)
                    if single:
                        val[0][0] = result[name_field]
                    else:
                        val[0] = result[name_field]
                    result[name_field] = val
            except:
                try:
                    val = np.zeros((1, result[name_field].size))
                    val[0] = result[name_field]
                    result[name_field] = val
                except:
                    val = np.zeros((1, 1))
                    val[0] = result[name_field]
                    result[name_field] = val
        elif "array" in str(data_type):
            try:
                str_primitive_type = str(primitive_type).lower()
                if len(result[name_field].shape) == 0:
                    if "int" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.intc)
                    elif "short" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.short)
                    elif "long" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.int_)
                    elif "float" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.single)
                    elif "double" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.double)
                    elif "byte" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=np.uint8)
                    elif "bool" in str_primitive_type:
                        val = np.zeros(shape=(1), dtype=bool)
                    elif "string" in str_primitive_type:
                        val = np.empty(shape=(1), dtype=object)
                    val[0] = result[name_field]
                    result[name_field] = val
            except Exception as e:
                if "int" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.intc)
                elif "short" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.short)
                elif "long" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.int_)
                elif "float" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.single)
                elif "double" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.double)
                elif "byte" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=np.uint8)
                elif "bool" in str_primitive_type:
                    val = np.zeros(shape=(1), dtype=bool)
                elif "string" in str_primitive_type:
                    val = np.empty(shape=(1), dtype=object)
                val[0] = result[name_field]
                result[name_field] = val
        else:
            str_primitive_type = str(primitive_type).lower()
            str_type = str(type(result[name_field])).lower()
            val = result[name_field]
            if "int" in str_primitive_type:
                if ("j" in str_type) or "int" not in str_type:
                    val = np.intc(result[name_field])
            elif "short" in str_primitive_type:
                if ("j" in str_type) or "short" not in str_type:
                    val = np.short(result[name_field])
            elif "long" in str_primitive_type:
                if ("j" in str_type) or "long" not in str_type:
                    val = np.int_(result[name_field])
            elif "float" in str_primitive_type:
                if ("j" in str_type) or "float" not in str_type:
                    val = np.single(result[name_field])
            elif "double" in str_primitive_type:
                if ("j" in str_type) or "double" not in str_type:
                    val = np.double(result[name_field])
            elif "byte" in str_primitive_type:
                if ("j" in str_type) or "byte" not in str_type:
                    val = np.uint8(result[name_field])
            elif "bool" in str_primitive_type:
                if ("j" in str_type) or "bool" not in str_type:
                    val = bool(result[name_field])
            elif "string" in str_primitive_type:
                if ("j" in str_type) or "string" not in str_type:
                    val = str(result[name_field])
            result[name_field] = val

    def check_data_set(self, data_type, primitive_type, name_field, result):
        if "array2D" in str(data_type):
            try:
                str_primitive_type = str(primitive_type).lower()
                try:
                    shape = result[name_field].shape
                except:
                    shape = [0]
                if len(shape) == 1:
                    size2 = shape[0]
                    single = False
                    if size2 == 0:
                        single = True
                        size2 = 1
                    if "int" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.intc)
                    elif "short" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.short)
                    elif "long" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.int_)
                    elif "float" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.single)
                    elif "double" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.double)
                    elif "byte" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.uint8)
                    elif "bool" in str_primitive_type:
                        val = np.zeros(shape=(1, size2), dtype=np.bool)
                    elif "string" in str_primitive_type:
                        val = np.empty(shape=(1, size2), dtype=object)
                    if single:
                        val[0][0] = result[name_field]
                    else:
                        val[0] = result[name_field]
                    result[name_field] = val
            except:
                try:
                    val = np.zeros((1, result[name_field].size))
                    val[0] = result[name_field][0]
                    result[name_field] = val
                except:
                    val = np.zeros((1, 1))
                    val[0] = result[name_field][0]
                    result[name_field] = val
        elif "array" in str(data_type):
            try:
                str_primitive_type = str(primitive_type).lower()
                if len(result[name_field].shape) == 0:
                    if "int" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.intc)
                    elif "short" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.short)
                    elif "long" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.int_)
                    elif "float" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.single)
                    elif "double" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.double)
                    elif "byte" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.uint8)
                    elif "bool" in str_primitive_type:
                        val = np.zeros(shape=1, dtype=np.bool)
                    elif "string" in str_primitive_type:
                        val = np.empty(shape=1, dtype=object)
                    val[0] = result[name_field]
                    result[name_field] = val
            except:
                if "int" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.intc)
                elif "short" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.short)
                elif "long" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.int_)
                elif "float" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.single)
                elif "double" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.double)
                elif "byte" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.uint8)
                elif "bool" in str_primitive_type:
                    val = np.zeros(shape=1, dtype=np.bool)
                elif "string" in str_primitive_type:
                    val = np.empty(shape=1, dtype=object)

                val[0] = result[name_field][0]
                result[name_field] = val
        else:
            str_primitive_type = str(primitive_type).lower()
            if "byte" in str_primitive_type:
                jstr_primitive_type = "JByte"
            elif "short" in str_primitive_type:
                jstr_primitive_type = "JShort"
            elif "int" in str_primitive_type:
                jstr_primitive_type = "JInt"
            elif "double" in str_primitive_type:
                jstr_primitive_type = "JDouble"
            elif "float" in str_primitive_type:
                jstr_primitive_type = "JFloat"
            elif "long" in str_primitive_type:
                jstr_primitive_type = "JLong"
            elif "string" in str_primitive_type:
                jstr_primitive_type = "JString"
            elif "bool" in str_primitive_type:
                jstr_primitive_type = "JBool"

            result[name_field] = formatting.type_value_to_java(result[name_field], jstr_primitive_type, si=0)


class _Example2(FesaCommListener):
    def handle_event(self, name, value):
        print("recv2")


class _Example(FesaCommListener):
    """
    Example to test.
    """

    _update_time = 0

    def get(self):
        """
        Get FESA property.

        :rtype: dict
        """
        fesa_comm = FesaComm("LHC.BWSLIUEXP.B1H2", "ExpertSetting", listener=self)
        return fesa_comm.get(None)

    def subscribe(self):
        """
        Subscribe to a FESA device property.
        """

        # fesa_comm = FesaComm("BT.BLM", "Acquisition",listener=self)
        # fesa_comm.subscribe("PSB.USER.ZERO")
        # fesa_comm = FesaComm("SPS.BCSPILLSPS.DEV", "RawDataSlow",listener=self)
        # fesa_comm.subscribe(None)
        # sleep(20000)
        # fesa_comm.unsubscribe()

        # FesaComm.JAPC = PyJapc(incaAcceleratorName=None)
        #
        # FesaComm.JAPC2 = PyJapc(incaAcceleratorName=None)
        #
        # FesaComm.JAPC.setSelector("CPS.USER.ALL")
        # FesaComm.JAPC2.setSelector("PSB.USER.ALL")
        #
        # _sub = FesaComm.JAPC.subscribeParam("PR.BPM/Status", self.handle_event, getHeader=True)
        # _sub.startMonitoring()
        #
        # _sub2 = FesaComm.JAPC2.subscribeParam("BR1.BPM/Status", self.handle_event, getHeader=True)
        # _sub2.startMonitoring()
        #
        # sleep(20000)
        #
        # _sub.stopMonitoring()

        # fesa_comm = FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)#, noPyConversion=True)
        # fesa_comm = FesaComm("BISWRef2", "Acquisition", listener=self)

        # fesa_comm = FesaComm("BGC.DigiCam", "LastImage", listener=self)

        # fesa_comm = FesaComm("HC.BLM.SR1.L", "CollimationAcquisition", listener=self)

        # fesa_comm = FesaComm("SPS.BWS.41677.V", "SPSSetting", listener=self)
        # fesa_comm.subscribe("SPS.USER.LHCMD2")

        # fesa_comm = FesaComm("BT.BLM", "Acquisition", listener=self)
        # ex2 = _Example2()
        # fesa_comm.add_listener(ex2)
        # fesa_comm.subscribe("PSB.USER.ALL")
        time_now = formatting.current_milli_time()
        t1 = time_now
        devices = ccda.get_devices_from_class("BSISO")
        # print((formatting.current_milli_time()-time_now))
        time_now = formatting.current_milli_time()
        for dev in devices:
            # print(dev,(formatting.current_milli_time()-time_now))
            time_now = formatting.current_milli_time()
            fesa_comm = FesaComm(dev, "Acquisition", listener=self, class_name="BSISO")

        print((formatting.current_milli_time() - t1))
        sleep(20000)

    def handle_event(self, name, value):
        """
        Notification received.

        :param header:
        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """

        # print("# ", (formatting.current_milli_time() - self._update_time))
        # print(value)
        print("recv1")
        self._update_time = formatting.current_milli_time()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    ex = _Example()
    # ex.subscribe()

    sleep(20)

    sys.exit(app.exec_())
