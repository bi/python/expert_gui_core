import sys
import time

# import pyda_rda3

from PyQt5.QtWidgets import QApplication

from expert_gui_core.tools import formatting


class _ExamplePyDA():

    def __init__(self):

        import pyda
        import pyda_japc
        import pyda_rda3

        self.provider = pyda_japc.JapcProvider()
        # self.provider = pyda_rda3.RdaProvider()
        self.client = pyda.CallbackClient(provider=self.provider)

        self._time_now = 0

        self.subscription = self.client.subscribe("BTVDEV.DigiCam.CAM2/LastImage", callback=self.handle_event)

    def subscribe(self):
        self.subscription.start()

    def handle_event(self, response):
        print("PYDA : ",formatting.current_milli_time() - self._time_now)
        self._time_now = formatting.current_milli_time()

class _Example():

    _update_time = 0

    def subscribe(self):

        import pyjapc

        self.japc = pyjapc.PyJapc(noSet=True)
        self.japc.setSelector("")

        self._time_now = 0

        self._sub = self.japc.subscribeParam("BTVDEV.DigiCam.CAM2/LastImage",
                                             onValueReceived=self.handle_event,
                                             getHeader=True)

        self._sub.startMonitoring()


    def handle_event(self, name, value, header=None):
        print("PYJAPC", formatting.current_milli_time() - self._time_now)
        self._time_now = formatting.current_milli_time()


class _ExampleNoPy():
    _update_time = 0

    def subscribe(self):
        import pyjapc

        self.japc = pyjapc.PyJapc(noSet=True)
        self.japc.setSelector("")

        self._time_now = 0

        self._sub = self.japc.subscribeParam("BTVDEV.DigiCam.CAM2/LastImage",
                                             onValueReceived=self.handle_event,
                                             getHeader=True,
                                             noPyConversion=True)
        self._sub.startMonitoring()

        time.sleep(20000)

    def handle_event(self, name, value, header=None):
        print("PYJAPC", formatting.current_milli_time() - self._time_now)
        self._time_now = formatting.current_milli_time()

if __name__ == "__main__":
    app = QApplication(sys.argv)

    ex = _ExamplePyDA()
    ex.subscribe()

    sys.exit(app.exec_())