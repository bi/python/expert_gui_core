from time import sleep

from expert_gui_core.comm import fesacomm
from expert_gui_core.comm import fesalistener
from expert_gui_core.comm import fesadispatcher


class FesaDispatcher:
    """
    FESA event dispatcher.    
    For a specific device.
    Able to gather a list of Fesa communication objects (property).
    This is to delegate Threading mechanism to the lower RDA subscription mechanism.

    :param parent: Parent class 
    :type parent: object
    :param device: Device name.
    :type device: str
    :param properties: Property list.
    :type properties: list
    """

    def __init__(self, parent, device, properties, cycle=None, min_time_between_event=50, class_name=None):
        """
        Initialize class.
        """
        self._parent = parent
        self._device = device
        self._properties = properties
        self._cycle = cycle
        self._listener = {}
        self._fesacomm = {}
        self._status = {}
        for property_fesa in self._properties:
            prop = self._properties[property_fesa]
            try:
                if len(self._properties[property_fesa]) > 0:
                    pass
            except:
                prop = [self._properties[property_fesa]]
            self._listener[property_fesa] = fesalistener.FesaListener(self._parent, prop)
            fesa_comm = fesacomm.FesaComm(self._device, property_fesa, listener=self._listener[property_fesa],
                                          min_time_between_event=min_time_between_event, class_name=class_name)
            self._fesacomm[property_fesa] = fesa_comm
            self._status[property_fesa] = {"err": 0, "msg": "fesacomm created"}

    def add_function(self, property_fesa, function):
        self._listener[property_fesa].func.append(function)

    def get(self, property_fesa, cycle):
        """
        Get FESA property.

        :param property_fesa: Name fesa property.
        :type cycle: str    
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Datamap object
        :rtype: dict
        """
        return self._fesacomm[property_fesa].get(cycle)

    def set(self, property_fesa, result, cycle):
        """
        Set a FESA property.

        :param property_fesa: Name fesa property.
        :type cycle: str 
        :param result: Datamap to set
        :type result: dict
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        """
        return self._fesacomm[property_fesa].set(result, cycle)

    def get_fesa_comm(self, property_fesa):
        """
        Get FESA communication object.
            
        :param property_fesa: Property name. 
        :type property_fesa: str
        :return: Fesa comm object
        :rtype: object
        """
        return self._fesacomm[property_fesa]

    def start(self, cycle=""):
        """
        Start subscription.
        
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str, optional        
        """
        if self._cycle is not None and (cycle == "" or cycle is None):
            cycle = self._cycle

        for property_fesa in self._properties:
            self._status[property_fesa] = self._fesacomm[property_fesa].subscribe(cycle)
            if self._status[property_fesa]["err"] == 1:
                self._fesacomm[property_fesa].unsubscribe()

    def stop(self):
        """
        Stop subscription.
        """
        for property_fesa in self._properties:
            self._fesacomm[property_fesa].unsubscribe()

    def get_status(self, property_fesa):
        """
        Get status.

        :param property_fesa: Property name.         
        :type property_fesa: str
        :return: Last status comm
        :rtype: dict
        """
        return self._status[property_fesa]


class _Example:

    def subscribe(self):
        """
        Do a subscription.
        """
        # self._dispatcher = FesaDispatcher(self, "YCA0.BSC0200", {"Status": self.handle_event})
        # # self._dispatcher.add_function("Acquisition", self.handle_event2)
        # self._dispatcher.start()

        self._dispatcher = fesadispatcher.FesaDispatcher(self, "YCA0.BSC0200", {
            "Status": self.handle_event,
            "Acquisition": self.handle_event},
                                                                  class_name="BSISO")

        self._dispatcher.start()
        sleep(200)
        self._dispatcher.stop()

    def handle_event(self, name, value):
        """
        Notification received.
            :name: Name of the notification key (device/property).
            :value: Datamap received.            
        """
        print("Recv1 " + name)
        # print(value)

    def handle_event2(self, name, value):
        """
        Notification received.
            :name: Name of the notification key (device/property).
            :value: Datamap received.
        """
        print("Recv2 " + name)
        # print(value)


if __name__ == "__main__":
    ex = _Example()
    ex.subscribe()
