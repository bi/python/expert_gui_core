import sys
from time import time
import logging
from abc import abstractmethod
from datetime import datetime

from PyQt5.QtCore import QTimer, QDateTime

from PyQt5.QtWidgets import QApplication, QMainWindow

from nxcals.spark_session_builder import get_or_create
from nxcals.api.extraction.data.builders import DataQuery

from expert_gui_core.tools import formatting
from expert_gui_core.comm import ccda

import pytimber

MAX_COUNT_STAT = 25
MAX_COUNT_VECTOR = 3000
MAX_COUNT_SCALAR = 10000000


class NXCALSCommListener:
    """
    Listener to Edge notification events.
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        pass


class _NXCALSTimerObject:
    """
        Timer Object for NXCALSComm.

        :param _nxcalscomm: NXCALScomm object.
        :type _nxcalscomm: object
        :param instance: A block instance defined in the
        :type instance: str
        :param registers: A list of registers.
        :type registers: list[str]
        """

    def __init__(self, nxcalscomm, name: str, selector=None):
        """
        Initialize the _NXCALSTimerObject.
        """

        self.timer = QTimer()
        self.timer.timeout.connect(self.handleTimerTimeout)
        self.timer_period = 1000
        self._nxcalscomm = nxcalscomm
        self._name = name
        self._selector = selector

    def startMonitoring(self, timer_period=1000):
        """
        Start monitoring with a specified timer period.

        :param timer_period: Timer period in milliseconds.
        :type timer_period: int
        """

        self.timer_period = timer_period

        if not self.timer.isActive():
            formatting.print_nxcals("Start polling!")

            self.timer.start(timer_period)

    def stopMonitoring(self):
        """
        Stop monitoring.
        """

        if self.timer is not None:
            formatting.print_nxcals("Stop polling!")
            self.timer.stop()
            self.timer = None

    def handleTimerTimeout(self):
        """
        Handle timer timeout event.
        Retrieve data from edgecomm.get and send it to edgecomm.handle_event.
        """

        # retrieve data from edgecomm

        now = QDateTime.currentMSecsSinceEpoch() - 2 * 3600000

        time_to = QDateTime()
        time_to.setMSecsSinceEpoch(now)

        time_from = QDateTime()
        time_from.setMSecsSinceEpoch(now - self.timer_period)

        stfrom = time_from.toString('yyyy-MM-dd hh:mm:ss.zzz')
        stto = time_to.toString('yyyy-MM-dd hh:mm:ss.zzz')

        data = self._nxcalscomm.get(name=self._name, tfrom=stfrom, tto=stto, selector=self._selector, auto=False)

        if data is None:
            data = {}

        # send data to nxcalscomm.handle_event

        self._nxcalscomm.handle_event(self._name, data)


class NXCALSComm:
    """
    NXCALS communication wrapper.

    :param listener: NXCALSCommListener object
    :type listener: object, optional
    """

    def __init__(self, listener=None):
        """
        Initialize class.
        """

        # self.ldb = pytimber.LoggingDB(source="nxcals")




        import os
        os.system("/bin/bash -c \"/user/sbartped/pyguis/delspark.sh\"")

        formatting.print_nxcals("Init NXCALS session")

        try:
            self.spark_session = get_or_create(app_name='MY_APP',
                                               conf={
                                                   'spark.executor.instances': '8',
                                                   'spark.sql.parquet.columnarReaderBatchSize': '16',
                                                   'spark.network.timeout': '300000',
                                                   'spark.driver.memory': '8g'}
                                               )
        except Exception as e:
            logging.getLogger().error(str(e))

        formatting.print_nxcals("NXCALS session created")
        self._sub = None
        self._lock = False
        self._time_now = 0
        self._listener = listener

    def find_entity_ids_for_variables_like(self, variable_name_pattern):
        """
        Find entity id.
        """

        _nxcals_api = self.spark_session._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.extraction.metadata.queries.Variables
        variableService = ServiceClientFactory.createVariableService()
        variables = variableService.findAll(
            getattr(Variables.suchThat().systemName().eq("CMW"), 'and')().variableName().like(variable_name_pattern))

        return [variable.getConfigs().last().getEntityId() for variable in variables]

    def test(self):

        # name = "FTA.BTV.9064.DigiCam:LastImage:image2D"
        name = "LHC.BSRA.US45.B1:ABORT_GAP_VOLTAGE"
        tfrom = "2024-04-25 11:59:49.109"
        tto = "2024-04-25 12:09:49.109"

        formatting.print_nxcals("Get stat", name, tfrom, tto)

        self.get(name, tfrom, tto)

        name = "LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"
        tfrom = "2024-04-25 11:59:49.109"
        tto = "2024-04-25 12:09:49.109"

        self.get(name, tfrom, tto)

        # data = DataQuery.builder(self.spark_session).variables() \
        #     .system('CMW') \
        #     .nameLike(name) \
        #     .timeWindow(tfrom, tto) \
        #     .build()
        #
        # formatting.print_nxcals("Get stat done")
        #
        # a = data.rdd.count()
        #
        # formatting.print_nxcals("done!", a)

    def get_stat(self, name, tfrom, tto, selector=None):
        """
        Get statistics between from and to.
        """

        formatting.print_nxcals("Get stat", name, tfrom, tto)

        try:
            data = DataQuery.builder(self.spark_session).variables() \
                .system('CMW') \
                .nameLike(name) \
                .timeWindow(tfrom, tto) \
                .build()
        except Exception as e:
            print("ERROR!", e)
            return

        stat = {}

        if selector is not None and selector != "None":
            try:
                d2 = data.where("selector = '" + selector + "'")
                stat["ValueCount"] = int(d2.count())
            except:
                stat["ValueCount"] = 0

            vector = False
            for dd in data.dtypes:

                if "array" in str(dd):
                    vector = True
                if "_value" in str(dd) and "string" in str(dd):
                    return None

            stat["vector"] = vector

            return stat

        stat["ValueCount"] = int(data.count())

        vector = False
        for dd in data.dtypes:
            if "nxcals_value" in str(dd) and "array" in str(dd):
                vector = True
            if "nxcals_value" in str(dd) and "string" in str(dd):
                return None

        stat["vector"] = vector

        return stat

    def java_string_to_instant(self, timestamp):
        """
        Java string to instant.
        """

        time_utils = self.spark_session._jvm.cern.nxcals.api.utils.TimeUtils
        return time_utils.getInstantFromString(timestamp)

    def java_instant_to_string(self, timestamp):
        """
        Java instant to string.
        """

        time_utils = self.spark_session._jvm.cern.nxcals.api.utils.TimeUtils
        return time_utils.getStringFromInstant(timestamp)

    def get_last_fill_number(self):
        """
        Get last fill number.
        """

        now = QDateTime.currentMSecsSinceEpoch()

        time_to = QDateTime()
        time_to.setMSecsSinceEpoch(now)

        time_from = QDateTime()
        time_from.setMSecsSinceEpoch(now - 1000 * 3600 * 24 * 100)

        stfrom = time_from.toString('yyyy-MM-dd hh:mm:ss.zzz')
        stto = time_to.toString('yyyy-MM-dd hh:mm:ss.zzz')

        fills_number = self.get_fill_number_from_time(stfrom, stto)

        if len(fills_number) > 0:
            return fills_number[-1]

        return -1

    def get_fill_number_from_time(self, tfrom, tto):
        """
        Get LHC fill number from time.
        """

        services = self.spark_session._jvm.cern.nxcals.api.custom.service.Services
        fill_service = services.newInstance(self.spark_session._jsparkSession).fillService()

        start_time = self.java_string_to_instant(tfrom)
        end_time = self.java_string_to_instant(tto)

        fills = fill_service.findFills(start_time, end_time)

        fills_number = []

        for fill in fills:
            fills_number.append(fill.getNumber())

        return fills_number

    def get_time_window_for_fill(self, fill_number):
        """
        Get LHC time window from LHC fill number.
        """

        services = self.spark_session._jvm.cern.nxcals.api.custom.service.Services
        fill_service = services.newInstance(self.spark_session._jsparkSession).fillService()

        maybe_fill = fill_service.findFill(fill_number)

        if maybe_fill.isEmpty():
            raise ValueError("No such a fill")

        fill = maybe_fill.get()

        start_as_instant = fill.getValidity().getStartTime()
        end_as_instant = fill.getValidity().getEndTime()

        start = self.java_instant_to_string(start_as_instant)
        end = self.java_instant_to_string(end_as_instant)

        return start, end

    def get_time_window_mode_for_time(self, tfrom, tto, mode_name, mode_index=0):
        """
        Get LHC time window from time, mode and mode index.
        """

        fill_numbers = self.get_fill_number_from_time(tfrom, tto)

        if len(fill_numbers) > 0:
            fill_number = fill_numbers[len(fill_numbers) - 1]
            return self.get_time_window_mode_for_fill(fill_number, mode_name, mode_index=mode_index)

        return None

    def get_time_window_mode_for_fill(self, fill_number, mode_name, mode_index=0):
        """
        Get LHC time window from fill number, mode and mode index.
        """

        services = self.spark_session._jvm.cern.nxcals.api.custom.service.Services

        fill_service = services.newInstance(self.spark_session._jsparkSession).fillService()

        maybe_fill = fill_service.findFill(fill_number)
        if maybe_fill.isEmpty():
            raise ValueError("No such a fill")

        fill = maybe_fill.get()

        beamModes = fill.getBeamModes()

        ind = 0
        if beamModes is not None:
            for beamMode in beamModes:
                mode = beamMode.getBeamModeValue()
                if (mode == mode_name) and mode_index == ind:
                    start_as_instant = beamMode.getValidity().getStartTime()
                    end_as_instant = beamMode.getValidity().getEndTime()
                    start = self.java_instant_to_string(start_as_instant)
                    end = self.java_instant_to_string(end_as_instant)
                    return start, end

        return None

    def get_lhc_modes(self, fill_number):
        """
        Get LHC modes for a specific LHC fill number.
        """

        services = self.spark_session._jvm.cern.nxcals.api.custom.service.Services

        fill_service = services.newInstance(self.spark_session._jsparkSession).fillService()

        maybe_fill = fill_service.findFill(fill_number)
        if maybe_fill.isEmpty():
            raise ValueError("No such a fill")

        fill = maybe_fill.get()

        return fill.getBeamModes()

    # Example modes : NOBEAM SETUP INJPROB INJPHYS PRERAMP RAMP FLATTOP ADJUST STABLE BEAMDUMP RAMPDOWN

    def get_fill_with_mode(self, variable_name, fill_number, mode_name, mode_index=0):
        """
        Get data for a specific name, fill number, mode and mode index.
        """

        start, end = self.get_time_window_for_fill(fill_number, mode_name=mode_name, mode_index=mode_index)
        return self.get(variable_name, start, end, auto=False)

    def get_fill(self, variable_name, fill_number):
        """
        Get data for a specific name, fill number.
        """

        start, end = self.get_time_window_for_fill(fill_number)
        return self.get(variable_name, start, end, auto=False)

    def get_data_from_nxcals(self, name="", tfrom=None, tto=None, selector=None):
        """
        Get data.

        :param name:
        :param var_name: Name of the NXCALS variable.
        :type var_name: str
        :param tfrom: Time from (ex:2015-05-13 00:00:00.000)
        :type tfrom: str
        :param tto: Time from (ex:2015-05-13 00:00:00.000)
        :type tto: str
        :return: Data result as {x, y}
        :rtype: dict
        """

        if tfrom is None or tto is None:
            return None
        if tfrom >= tto:
            return None

        data = DataQuery.builder(self.spark_session).variables().system('CMW').nameLike(name).timeWindow(tfrom,
                                                                                                         tto).build().orderBy(
            'nxcals_timestamp', ascending=False)

        formatting.print_nxcals("Search", name)

        if selector is not None and selector != "None":
            try:
                d2 = data.where("selector = '" + selector + "'")
                formatting.print_nxcals("Search done!", name)
                return d2.collect()
            except:
                return None

        formatting.print_nxcals("Search done!", name)

        return data.collect()

    def get_data(self, name: str = "", tfrom: str = None, tto: str = None, selector: str = None,
                 list_time: list = None):
        """
        Get data.

        :param name:
        :param var_name: Name of the NXCALS variable.
        :type var_name: str
        :param tfrom: Time from (ex:2015-05-13 00:00:00.000)
        :type tfrom: str
        :param tto: Time from (ex:2015-05-13 00:00:00.000)
        :type tto: str
        :return: Data result as {x, y}
        :rtype: dict
        """

        result = {}
        split = [tfrom, tto, 0, 0, False]
        result["split"] = split
        result["data"] = []
        result["vector"] = True

        # No timestamps!

        if list_time is None:

            dat = self.get_data_from_nxcals(name, tfrom=tfrom, tto=tto, selector=selector)

            formatting.print_nxcals("Search done!", name)

            values = []
            times = []

            for res in dat:

                try:
                    dat_array = res["nxcals_value"]["elements"]
                except:
                    dat_array = res["nxcals_value"]

                values.append(dat_array)
                times.append(res["nxcals_timestamp"] / 1000000000.)

            values.reverse()
            times.reverse()

            result["data"].append(values)
            result["data"].append(times)

            # https: // nxcals - docs.web.cern.ch / current / python - nxcals - docs / api / pytimber.LoggingDB.html

            return result

        else:

            df = None

            formatting.print_nxcals("Search", name)

            if len(list_time) > 2 and list_time[1] == "all":

                for index_time in range(0, len(list_time), 3):

                    timefrom_ = float(formatting.format_value_to_string(list_time[index_time], type_format="idate"))
                    timeto_ = float(formatting.format_value_to_string(list_time[index_time + 2], type_format="idate"))

                    date_from = datetime.fromtimestamp(timefrom_ - 0.001).isoformat(sep=' ', timespec='milliseconds')
                    date_to = datetime.fromtimestamp(timeto_ + 0.001).isoformat(sep=' ', timespec='milliseconds')

                    data = DataQuery.builder(self.spark_session). \
                        variables().system('CMW'). \
                        nameLike(name). \
                        timeWindow(date_from, date_to). \
                        build().orderBy('nxcals_timestamp', ascending=False)

                    formatting.print_nxcals("Search times", name, date_from, date_to)

                    if selector is not None and selector != "None":
                        try:
                            d2 = data.where("selector = '" + selector + "'")
                            formatting.print_nxcals("Search done!", name)
                            if df is None:
                                df = d2.collect()
                            else:
                                df = df + d2.collect()
                        except Exception as e:
                            return None

                    if df is None:
                        df = data.collect()
                    else:
                        df = df + data.collect()

            else:

                for ti in list_time:

                    time_ = float(formatting.format_value_to_string(ti, type_format="idate"))

                    date_from = datetime.fromtimestamp(time_ - 0.001).isoformat(sep=' ', timespec='milliseconds')
                    date_to = datetime.fromtimestamp(time_ + 0.001).isoformat(sep=' ', timespec='milliseconds')

                    data = DataQuery.builder(self.spark_session). \
                        variables().system('CMW'). \
                        nameLike(name). \
                        timeWindow(date_from, date_to). \
                        build().orderBy('nxcals_timestamp', ascending=False)

                    formatting.print_nxcals("Search times", name, ti, date_from, date_to)

                    if selector is not None and selector != "None":
                        try:
                            d2 = data.where("selector = '" + selector + "'")
                            formatting.print_nxcals("Search done!", name)
                            if df is None:
                                df = d2.collect()
                            else:
                                df = df + d2.collect()
                        except Exception as e:
                            return None

                    print(data.collect())

                    if df is None:
                        df = data.collect()
                    else:
                        df = df + data.collect()

            formatting.print_nxcals("Search done!", name)

            if df is None:
                result["data"] = []
            else:

                values = []
                times = []

                for res in df:

                    if len(values) < MAX_COUNT_VECTOR:
                        try:
                            dat_array = res["nxcals_value"]["elements"]
                        except:
                            dat_array = res["nxcals_value"]
                        values.append(dat_array)
                        times.append(res["nxcals_timestamp"] / 1000000000.)

                values.reverse()
                times.reverse()

                result["data"].append(values)
                result["data"].append(times)

            split = [tfrom, tto, len(values), 0, False]

            result["split"] = split

            return result

        return None

    def get_properties(self, device_name=""):
        """
        Get property list configured in NXCALS from a device name.

        :param device_name: FESA device name.
        :type device_name: str
        :return: List of properties
        :rtype: list
        """
        try:
            nxcals_subscriptions = ccda.sync_api.NxcalsSubscription.search(
                'nxcalsSubscriptionFields.nxcalsVariables.variableName=="*' + device_name + '*";propertyName=="*"')
        except Exception as e:
            formatting.print_nxcals(e, level=logging.ERROR)

        properties = []
        for nxcals_subscription in nxcals_subscriptions:
            if nxcals_subscription.property_name not in properties:
                properties.append(nxcals_subscription.property_name)

        return properties

    def get_variable_description(self, field_name):
        """
        Get variable description.

        :param field_name: Name field.
        :type field_name: str
        :return: Description
        :rtype: str
        """

        return self.ldb.search(field_name)

    def get_variables(self, device_name="", property_name="", field_name="*"):
        """
        Get the list of NXCALS variables using the device and prioperty names.

        :param device_name: Device name.
        :type device_name: str
        :param property_name: Property name.
        :type property_name: str
        :return: List of variables
        :rtype: list
        """

        var_names = []
        try:
            nxcals_subscriptions = ccda.sync_api.NxcalsSubscription.search(
                'deviceName=="' + device_name + '";propertyName=="' + property_name + '";nxcalsSubscriptionFields.fieldName=="*"')
            # print('nxcalsSubscriptionFields.nxcalsVariables.variableName=="*'+device_name+'*";propertyName=="'+property_name+'";nxcalsSubscriptionFields.fieldName=="*"')
            for nxcals_subscription in nxcals_subscriptions:
                for nxcals_subscription_field in nxcals_subscription.nxcals_subscription_fields:
                    for nxcals_variable in nxcals_subscription_field.nxcals_variables:
                        try:
                            if field_name.index("*") >= 0:
                                var_names.append(nxcals_variable.variable_name)
                            else:
                                if nxcals_subscription_field.field_name == field_name:
                                    var_names.append(nxcals_variable.variable_name)
                        except:
                            pass
        except Exception as e:
            formatting.print_nxcals(e, level=logging.ERROR)
        return var_names

    def find_split_interval(self, name, t1, t2, is_vector=False, selector=None, counter=0, size_max=-1):
        """
        Recursive function to split the time interval if data number is too big.
        """

        if selector is not None:
            stat = self.get_stat(name, tfrom=t1, tto=t2, selector=selector)
        else:
            stat = self.get_stat(name, tfrom=t1, tto=t2)

        its1 = float(formatting.format_value_to_string(t1, type_format="idate"))
        its2 = float(formatting.format_value_to_string(t2, type_format="idate"))

        counter = counter + 1

        formatting.print_nxcals(name, counter)

        if stat is None or stat["ValueCount"] is None:
            stat = {}
            stat["ValueCount"] = 0
            stat["vector"] = False

        is_vector = stat["vector"]

        if size_max != -1:
            max_count_vector = size_max
        else:
            max_count_vector = MAX_COUNT_VECTOR

        if is_vector:
            formatting.print_nxcals(name, "VECTOR", level=logging.WARNING)
            if counter < MAX_COUNT_STAT and stat["ValueCount"] > max_count_vector:
                its2 = its2
                nsplit = max_count_vector / stat["ValueCount"]
                its1 = its2 - nsplit * (its2 - its1)
                t2 = formatting.format_value_to_string(its2, type_format="date")
                t1 = formatting.format_value_to_string(its1, type_format="date")
                formatting.print_nxcals("Count : " + str(stat["ValueCount"]), level=logging.WARNING)
                # formatting.print_nxcals("CASE 1 : "+t1+" "+t2)
                return self.find_split_interval(name=name, t1=t1, t2=t2, is_vector=is_vector, selector=selector,
                                                counter=counter, size_max=size_max)
            else:
                if stat["ValueCount"] == 0 and counter < MAX_COUNT_STAT:
                    itstmp = its1
                    its1 = its1 - (its2 - its1)
                    its2 = itstmp
                    t2 = formatting.format_value_to_string(its2, type_format="date")
                    t1 = formatting.format_value_to_string(its1, type_format="date")
                    # formatting.print_nxcals("CASE 2 : "+t1+" "+t2)
                    return self.find_split_interval(name=name, t1=t1, t2=t2, is_vector=is_vector, selector=selector,
                                                    counter=counter, size_max=size_max)
                else:
                    formatting.print_nxcals("Count : " + str(stat["ValueCount"]), level=logging.WARNING)
                    # formatting.print_nxcals("CASE 3 : "+t1+" "+t2)
                    return [t1, t2, counter, stat["ValueCount"], is_vector]
        else:
            formatting.print_nxcals(name, "SCALAR", level=logging.WARNING)
            if counter < MAX_COUNT_STAT and stat["ValueCount"] > MAX_COUNT_SCALAR:
                nsplit = MAX_COUNT_SCALAR / stat["ValueCount"]
                its2 = its2
                its1 = its2 - nsplit * (its2 - its1)
                t2 = formatting.format_value_to_string(its2, type_format="date")
                t1 = formatting.format_value_to_string(its1, type_format="date")
                # formatting.print_nxcals("CASE 4 : "+t1+" "+t2)
                return self.find_split_interval(name=name, t1=t1, t2=t2, is_vector=is_vector, selector=selector,
                                                counter=counter, size_max=size_max)
            else:
                if stat["ValueCount"] == 0 and counter < MAX_COUNT_STAT:
                    itstmp = its1
                    its1 = its1 - (its2 - its1)
                    its2 = itstmp
                    t2 = formatting.format_value_to_string(its2, type_format="date")
                    t1 = formatting.format_value_to_string(its1, type_format="date")
                    # formatting.print_nxcals("CASE 5 : "+t1+" "+t2)
                    return self.find_split_interval(name=name, t1=t1, t2=t2, is_vector=is_vector, selector=selector,
                                                    counter=counter, size_max=size_max)
                else:
                    formatting.print_nxcals("Count : " + str(stat["ValueCount"]), level=logging.WARNING)
                    # formatting.print_nxcals("CASE 6 : "+t1+" "+t2)
                    return [t1, t2, counter, stat["ValueCount"], is_vector]

    def get_time(self, name="", tfrom=None, tto=None, selector=None):

        result = {}

        result["data"] = []
        result["vector"] = False

        its1 = float(formatting.format_value_to_string(tfrom, type_format="idate"))
        its2 = float(formatting.format_value_to_string(tto, type_format="idate"))

        formatting.print_nxcals("Get timestamps (NXCALSComm)", tfrom + " " + tto, level=logging.CRITICAL)

        if selector is not None and selector != "None":

            try:
                dat = self.get_data_from_nxcals(name=name, tfrom=tfrom, tto=tto, selector=selector)
                times = []
                if dat is not None:
                    for res in dat:
                        times.append(res["nxcals_timestamp"] / 1000000000.)
                    times.reverse()
                    result["data"].append(times)
            except Exception as e:
                formatting.print_nxcals(e, level=logging.ERROR)

        else:

            try:
                dat = self.get_data_from_nxcals(name=name, tfrom=tfrom, tto=tto)
                times = []
                if dat is not None:
                    for res in dat:
                        # print(res["nxcals_timestamp"])
                        times.append(res["nxcals_timestamp"] / 1000000000.)
                    times.reverse()
                    result["data"].append(times)
            except Exception as e:
                formatting.print_nxcals(e, level=logging.ERROR)

        return result

    def get(self, name="", tfrom=None, tto=None, selector=None, auto=True, big=False):
        """
        Get data.
        """

        result = {}

        split = [0, 0, 0, 0, False]

        result["split"] = split
        result["data"] = []
        result["vector"] = False

        ts1 = tfrom
        ts2 = tto

        its1 = float(formatting.format_value_to_string(ts1, type_format="idate"))
        its2 = float(formatting.format_value_to_string(ts2, type_format="idate"))

        size_max = -1

        if big:
            delta = its2-its1
            size_max = 50

        formatting.print_nxcals("Get (NXCALSComm)", tfrom + " " + tto, level=logging.CRITICAL)

        is_vector = False

        if auto:
            split = self.find_split_interval(name=name, t1=tfrom, t2=tto, is_vector=is_vector, selector=selector, size_max=size_max)
            t1 = split[0]
            t2 = split[1]
        else:
            split = self.find_split_interval(name=name, t1=tfrom, t2=tto, is_vector=is_vector, selector=selector, counter=MAX_COUNT_STAT)
            t1 = split[0]
            t2 = split[1]

        # # Convert time zone
        # from datetime import datetime
        # from dateutil import tz
        #
        # from_zone = tz.tzutc()
        # to_zone = tz.tzlocal()
        #
        # utc = datetime.strptime(split[0], '%Y-%m-%d %H:%M:%S.%f')
        # utc = utc.replace(tzinfo=from_zone)
        # split[0] = utc.astimezone(to_zone).strftime('%Y-%m-%d %H:%M:%S.%f')
        #
        # utc = datetime.strptime(split[1], '%Y-%m-%d %H:%M:%S.%f')
        # utc = utc.replace(tzinfo=from_zone)
        # split[1] = utc.astimezone(to_zone).strftime('%Y-%m-%d %H:%M:%S.%f')

        formatting.print_nxcals("split", split)

        result["split"] = split

        try:
            is_vector = split[4]
        except:
            pass

        if split[3] == 0:
            return result

        if is_vector:
            if selector is not None:
                try:
                    dat = self.get_data(name=name, tfrom=t1, tto=t2, selector=selector)
                    if dat is None:
                        result["data"] = []
                    else:
                        values = []
                        times = []
                        for res in dat:
                            if len(values) < MAX_COUNT_VECTOR:
                                dat_array = res["nxcals_value"]["elements"]
                                values.append(dat_array)
                                times.append(res["nxcals_timestamp"]/1000000000.)
                        values.reverse()
                        times.reverse()
                        result["data"].append(values)
                        result["data"].append(times)
                except Exception as e:
                    formatting.print_nxcals(e, level=logging.ERROR)
                    result["data"] = []
            else:
                try:
                    dat = self.get_data(name=name, tfrom=t1, tto=t2)
                    values = []
                    times = []
                    for res in dat:
                        if len(values) < MAX_COUNT_VECTOR:
                            dat_array = res["nxcals_value"]["elements"]
                            values.append(dat_array)
                            times.append(res["nxcals_timestamp"]/1000000000.)
                    values.reverse()
                    times.reverse()
                    result["data"].append(values)
                    result["data"].append(times)
                except Exception as e:
                    formatting.print_nxcals(e, level=logging.ERROR)
                    result["data"] = []

            result["vector"] = True

        else:
            if selector is not None:
                try:
                    dat = self.get_data(name=name, tfrom=t1, tto=t2, selector=selector)
                    values = []
                    times = []
                    for res in dat:
                        if len(values) < MAX_COUNT_SCALAR:
                            values.append(res["nxcals_value"])
                            times.append(res["nxcals_timestamp"]/1000000000.)
                    values.reverse()
                    times.reverse()
                    result["data"].append(values)
                    result["data"].append(times)
                except Exception as e:
                    formatting.print_nxcals(e, level=logging.ERROR)
                    result["data"] = []
            else:
                try:
                    dat = self.get_data(name=name, tfrom=t1, tto=t2)
                    values = []
                    times = []
                    for res in dat:
                        if len(values) < MAX_COUNT_SCALAR:
                            values.append(res["nxcals_value"])
                            times.append(res["nxcals_timestamp"]/1000000000.)
                    values.reverse()
                    times.reverse()
                    result["data"].append(values)
                    result["data"].append(times)
                except Exception as e:
                    formatting.print_nxcals(e, level=logging.ERROR)
                    result["data"] = []

            result["vector"] = False

        return result

    def subscribe(self, name, selector=None, timer_period=1000):
        """
        Subscribe to a NXCALS Simulated device property.

        :param condition: Specific condition for the data
        :type condition: function that returns a bool, optional
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Status object {err value, msg}
        :rtype: dict
        """

        if self._sub is not None:
            self.unsubscribe()

        try:
            self._sub = _NXCALSTimerObject(self, name, selector=selector)
            formatting.print_nxcals("Start polling", level=logging.WARNING)
            self._sub.startMonitoring(timer_period=timer_period)
            msg = "subscribe ok"
            return {"err": 0, "msg": msg}
        except Exception as e:
            formatting.print_nxcals(e, level=logging.ERROR)

    def unsubscribe(self):
        """
        Unsubscribe to a FESA Simulated device property.
        """

        if self._sub is not None:
            self._sub.stopMonitoring()
            self._sub = None

    def handle_event(self, name, value, header=None):
        """
        Notification received.

        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...), optional
        """

        # check if already locked

        if self._lock:
            logging.debug(name + " event lost!")
            return

        self._lock = True

        # check 2 consecutive timestamps

        if round(time.time() * 1000) - self._time_now < 20:
            self._lock = False
            return
        else:
            pass

        self._time_now = round(time.time() * 1000)

        # add time and cycle in data map

        value['_time'] = self._time_now

        try:
            if header is not None:
                value['_cycle'] = header['selector']
            else:
                value['_cycle'] = ""
        except Exception as e:
            formatting.print_nxcals(e, level=logging.ERROR)
            value['_cycle'] = ""

        # notification

        if self._listener is not None:
            self._listener.handle_event(name, value)

        # release lock

        self._lock = False


class _Example(NXCALSCommListener):

    def __init__(self):
        nxcals_comm = NXCALSComm(self)
        nxcals_comm.test()

        # res = nxcals_comm.get_variables(device_name="YCA0.BSC060", property_name="*", field_name="*")

        ts1 = '2023-09-05 05:40:39.000'
        ts2 = '2023-09-06 05:50:39.000'

        # ts1="2018-04-25 00:00:00.000000000"
        # ts2="2018-04-28 00:00:00.000000000"

        # nxcals_comm.get_fill_number_from_time(ts1, ts2)
        # res = nxcals_comm.get_last_fill_number()
        # formatting.print_nxcals(res)

        # stat = nxcals_comm.get_fill("LHC.BSRA.US45.B1:ABORT_GAP_ENERGY", 9193)

        # stat = nxcals_comm.get("BSPH043626", tfrom=ts1, tto=ts2)
        # stat = nxcals_comm.get("LHC.BSRA.US45.B1:ABORT_GAP_ENERGY", tfrom=ts1, tto=ts2, auto=False)

        # stat = nxcals_comm.get_stat("PR.BPM:ACQ.ORBIT:position", tfrom=ts1, tto=ts2)#, selector="CPS.USER.TOF")

        # stat = nxcals_comm.get_stat("LHC.BSRA.US45.B1:ABORT_GAP_ENERGY", tfrom=ts1, tto=ts2)

        # formatting.print_nxcals(stat)

    def subscribe(self):
        self._nxcals_comm = NXCALSComm(listener=self)
        self._nxcals_comm.subscribe("LHC.BSRA.US45.B1:ABORT_GAP_ENERGY")

        formatting.print_nxcals("Wait!")

    def unsubscribe(self):
        self._nxcals_comm.unsubscribe()

    def handle_event(self, name, value):
        formatting.print_nxcals("Recv!")
        formatting.print_nxcals(value)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = QMainWindow()
    w.show()

    ex = _Example()
    # ex.subscribe()

    # time.sleep(200)

    # ex.unsubscribe()

    formatting.print_nxcals("done!")

    sys.exit(app.exec_())
