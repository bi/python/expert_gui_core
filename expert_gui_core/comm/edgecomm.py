
import logging
from importlib import import_module
from os import listdir
import subprocess
import sys
from abc import abstractmethod
from pathlib import Path

from PyQt5.QtCore import QTimer

from expert_gui_core.tools import formatting


class EdgeCommListener:
    """
    Listener to Edge notification events.
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        pass


class _EdgeTimerObject:
    """
        Timer Object for Edgecomm.

        :param _edgecomm: Edgecomm object.
        :type _edgecomm: object
        :param instance: A block instance defined in the EDGE CSV.
        :type instance: str
        :param registers: A list of registers.
        :type registers: list[str]
        """

    def __init__(self, edgecomm, instance: str, registers: list[str] = None):
        """
        Initialize the _EdgeTimerObject.
        """
        self.timer = QTimer()
        self.timer.timeout.connect(self.handleTimerTimeout)
        self._edgecomm = edgecomm
        self._instance = instance
        self._registers = registers
        self._mode_set = False

    def startMonitoring(self, timer_period=1000, mode_set=False):
        """
        Start monitoring with a specified timer period.

        :param mode_set:
        :param timer_period: Timer period in milliseconds.
        :type timer_period: int
        """
        self._mode_set = mode_set
        if not self.timer.isActive():
            logging.info("Start polling!")
            self.timer.start(timer_period)

    def stopMonitoring(self):
        """
        Stop monitoring.
        """
        if self.timer is not None:
            logging.info("Stop polling!")
            self.timer.stop()
            self.timer = None

    def handleTimerTimeout(self):
        """
        Handle timer timeout event.
        Retrieve data from edgecomm.get and send it to edgecomm.handle_event.

        """

        # Retrieve data from edgecomm

        data = {}
        if not self._mode_set:
            data = self._edgecomm.get(self._instance, registers=self._registers)

        # Send data to edgecomm.handle_event

        self._edgecomm.handle_event(self._instance, data)


class EdgeComm:
    """
    WARNING: this class supports EDGE >= 3.0.0

    NOTE: this class internally open an EDGE driver when constructed and close it
    when destructed.

    Usage example:
    edge_comm = EdgeComm("bi_blmcsdab", 17)

    # get the value of all registers of the given instance
    edge_comm.get("inst1")

    # get the value of the given register for the given instance
    edge_comm.get("inst1", ["reg1"])

    # get the value of the given list of registers for the given instance
    edge_comm.get("inst1", ["reg1", "reg2"])
    """

    def __init__(self, module: str, lun: int, listener=None):
        """
        Initialize the class:
            - search for the modules installed in the given crate,
            - if the provided module is found in the previous result, open its EDGE handle.
            - Get a flat register list of the previously opened EDGE driver handle.

        :param module: The name of the EDGE module installed in a given crate.
        :type module: str
        :param lun: The LUN of the given module.
        :type lun: int
        :param timer_period: Timer period for subscription.
        :type timer_period: int
        """
        self._modules = self.get_modules()

        if module not in self._modules:
            raise Exception(f"Module {module} does not exist")
        else:
            self._module = module

        if lun not in self._modules[module]:
            raise Exception(f"LUN {lun} does not exist for the module {module}")
        else:
            self._lun = lun

        self._drv_hdl = self._get_driver_handle()
        self._registers = self._get_register_list()
        self._listener = listener
        self._lock = False
        self._time_now = 0
        self._sub = {}

    def __del__(self):
        """
        Delete the class instance:
            - close the EDGE driver handle.
        """
        self._drv_hdl.close()

    @staticmethod
    def get_modules() -> dict:
        """
        Return the list of modules installed in the crate as a dict.
        E.g.:
        {
            "bi_blmcsdab": [17],
            "bi_blmtcdab": [0, 1, 10, 11, 12, 13, 14, 15, 16, 2, 3, 4, 5, 6, 7, 9],
            "bi_bobr":     [0]
        }

        :return: A dictionary of modules with their associated LUNs in a list.
        :rtype: dict
        """
        root = "/sys/class"
        result = {}

        for module_name in listdir(root):
            module_path = Path(root) / module_name
            hw_version_path = module_path / "hw_version"
            if hw_version_path.exists():
                modules = [file for file in module_path.iterdir() if "." in file.name
                           and file.name.split(".")[0] == module_name]
                result[module_name] = [int(file.name.split(".")[-1]) for file in modules if file.name.split(".")[-1]]
        return result

    def get_hwdesc(self) -> dict:
        """
        Get the list of registers in a dictionary with their attributes.
        E.g.:
        {
            "inst1": [
                          {
                              "depth": 65536,
                              "dwith": 32,
                              "name": "reg1",
                              "rwmode": "RW",
                              "type": "array"
                          }
                      ],
             "inst2": [
                          {
                              "depth": 65536,
                              "dwith": 32,
                              "name": "reg1",
                              "rwmode": "RW",
                              "type": "array"
                          },
                          {
                              "depth": 1,
                              "dwith": 32,
                              "name": "reg2",
                              "rwmode": "RW",
                              "type": "scalar"
                          }
                      ]
        }


        :return: A dictionary of instances with their register attributes in a sub dictionary.
        :rtype: dict
        """
        registers = {}
        for string in self._drv_hdl.get_reg_paths():
            reg_path = string.split(".", 1)
            if len(reg_path) == 2:
                key, value = reg_path
                reg_hdl = self._drv_hdl.get_reg_from_reg_path(string)
                reg_attr = {
                    "name": value,
                    "type": "scalar" if reg_hdl.reg_attr.depth == 1 else "array",
                    "rwmode": reg_hdl.rwmode(reg_hdl.reg_attr.rwmode).name,
                    "depth": reg_hdl.reg_attr.depth,
                    "dwith": reg_hdl.reg_attr.dwidth
                }
                if key in registers:
                    registers[key].append(reg_attr)
                else:
                    registers[key] = [reg_attr]
        return registers

    def get(self, instance: str, registers: list[str] = None) -> dict:
        """
        Get the value of the provided registers and returns a dict with
        registers as key and their read values in a numpy array as value.
        E.g.:
        {
            "reg1": array([42, 128], dtype=uint32),
            "reg2": array([0], dtype=uint32)
        }

        :param instance: A block instance defined in the EDGE CSV.
        :type instance: str
        :param registers: A list of registers.
        :type registers: list[str]
        """
        edge_hdl = self._get_driver_handle()
        logging.info("Get : " + instance)
        if registers is None:
            registers = [reg["name"] for reg in self.get_hwdesc()[instance]]
        result = {}
        for reg in registers:
            reg_path_str = f"{instance}.{reg}"
            reg_hdl = edge_hdl.get_reg_from_reg_path(reg_path_str)
            if reg_hdl.reg_attr.rwmode in {reg_hdl.rwmode.RO, reg_hdl.rwmode.RW}:
                result['.'.join(reg_path_str.split('.')[1:])] = reg_hdl.read()
        return result

    def get_action(self, block_instance, event=True, extra=True, name_fields=None):
        """
        Get Edge property and trig handle event.

        :param block_instance: Specific block instance.
        :type block_instance: str
        :param event: Trigger or not the event function.
        :type event: bool
        :return: Datamap object
        :rtype: dict
        """
        result = self.get(block_instance, registers=name_fields)
        if result is None:
            logging.error("Get result none for : " + block_instance)
            return
        if extra:
            result['_time'] = formatting.current_milli_time()
            result['_cycle'] = ""
        if event:
            self.handle_event(block_instance, result)
        return result

    def set(self, instance: str, values: dict):
        """
        values is a dictionary of registers with their values to be set as a
        numpy array.
        E.g.:
        {
            "reg1": array([1, 2, 3], dtype=uint32),
            "reg2": array([0], dtype=uint32)
        }

        :param instance: A block instance defined in the EDGE CSV.
        :type instance: str
        :param values: A dictionary of registers with their values to set in a list.
        :type values: dict
        """
        edge_hdl = self._get_driver_handle()
        logging.info("Set : " + instance)
        result = {}
        for reg in values:
            reg_path_str = f"{instance}.{reg}"
            reg_hdl = edge_hdl.get_reg_from_reg_path(reg_path_str)
            if reg_hdl.reg_attr.rwmode == reg_hdl.rwmode.WO or reg_hdl.reg_attr.rwmode == reg_hdl.rwmode.RW:
                result[reg_path_str] = reg_hdl.write(values[reg])

    def _get_driver_handle(self):
        """
        Get the EDGE driver handle from the EDGE module handle.

        :return: An EDGE driver handle.
        :rtype: ?
        """
        module_path = f"/acc/local/share/edge/modules/{self._module}/{self._get_module_version()}"
        sys.path.insert(0, module_path)
        sys.path.insert(0, f"/acc/local/share/edge/{self._get_edge_version()}/bin")
        module = import_module(f"{self._module}io")
        edge_hdl = module.Module.drv_open(self._lun)
        return edge_hdl

    def _get_register_list(self) -> dict:
        """
        Get the list of registers of the driver handle.

        :return: The list of registers of the driver handle.
        :rtype: dict
        """
        registers = {}
        for string in self._drv_hdl.get_reg_paths():
            reg_path = string.split(".", 1)
            if len(reg_path) == 2:
                key, value = reg_path
                if key in registers:
                    registers[key].append(value)
                else:
                    registers[key] = [value]
        return registers

    def _get_edge_version(self) -> str:
        """
        Get the EDGE version.

        :return: The EDGE version.
        :rtype: str
        """
        result = subprocess.run(["cat", f"/sys/class/{self._module}/edge_version"], capture_output=True, text=True)
        return result.stdout.replace("\n", "")

    def _get_module_version(self) -> str:
        """
        Get the EDGE module version.

        :return: The EDGE module version.
        :rtype: str
        """
        result = subprocess.run(["cat", f"/sys/class/{self._module}/hw_version"], capture_output=True, text=True)
        return result.stdout.replace("\n", "")

    def subscribe(self, block_instance, timer_period=1000, registers: list[str] = None, mode_set=False):
        """
        Subscribe to a Edge block instance.

        :param block_instance: A block instance defined in the EDGE CSV.
        :type block_instance: str
        :param registers: A list of registers.
        :type registers: list[str]
        :return: Status object {err value, msg}
        :rtype: dict
        """

        # this is not efficient but 'self._sub[block_instance] is not None' throws an error,

        try:
            if block_instance in self._sub.keys() and self._sub[block_instance] is not None:
                self.unsubscribe(block_instance)
        except:
            pass

        logging.info("Subscribe : " + block_instance)

        try:
            self._sub[block_instance] = _EdgeTimerObject(self, block_instance, registers=registers)
            self._sub[block_instance].startMonitoring(timer_period, mode_set=mode_set)
            msg = "subscribe ok to " + block_instance
            logging.info(msg)
            return {"err": 0, "msg": msg}
        except:
            logging.warning("problem " + block_instance)
            return {"err": 1, "msg": msg}

    def unsubscribe(self, block_instance):
        """
        Unsubscribe to a Edge block instance.
        """

        logging.info("Unsubscribe : " + block_instance)

        # if there is no 'block_instance' key in the subs this will throw an error

        if self._sub[block_instance] is not None:
            self._sub[block_instance].stopMonitoring()
            self._sub[block_instance] = None

            # Delete the block_instance from the _subs.
            # This will return _sub[block_instance] if key exists in the dictionary, and None otherwise

            self._sub.pop(block_instance, None)

    def handle_event(self, name, value, header=None):
        """
        Notification received.

        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...).
        """

        # Check if already locked

        if self._lock:
            logging.debug(name + " event lost!")
            return

        self.lock = True

        logging.info("Recv : " + name)

        # Check 2 consecutive tstamp

        if formatting.current_milli_time() - self._time_now < 20:
            self.lock = False
            return
        else:
            pass

        self._time_now = formatting.current_milli_time()

        # Add time and cycle in data map

        value['_time'] = self._time_now

        try:
            if header is not None:
                value['_cycle'] = header['selector']
            else:
                value['_cycle'] = ""
        except:
            value['_cycle'] = ""

        # notification

        if self._listener is not None:
            self._listener.handle_event(name, value)

        # release lock

        self.lock = False