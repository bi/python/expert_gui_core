import os


def save_to_file(
        text: str,
        file_path: str,
        file_type: str = "txt",
        encoding: str = "utf-8") -> None:
    """
    Saves text to a specified file in the desired format.

    For CSV files, the function detects the separator used in the text and adds
    a 'sep=...' directive as the first line of the file.

    Args:
        text (str): The text data to be saved.
        file_path (str): The destination path of the file (excluding extension).
        file_type (str): The file type to save as, either 'csv' or 'txt'. Defaults to 'txt'.
        encoding (str): File encoding. Defaults to 'utf-8'.

    Raises:
        ValueError: If the file_type is not supported.
    """
    supported_types = {"txt", "csv"}
    file_type = file_type.lower()

    if file_type not in supported_types:
        raise ValueError(f"Unsupported file type '{file_type}'. Supported types are: {supported_types}")

    full_path = f"{os.path.splitext(file_path)[0]}.{file_type}"

    if file_type == "csv":
        if "\t" in text:
            sep_directive = "sep=\\t\n"
        elif " " in text:
            sep_directive = "sep= \n"
        else:
            sep_directive = ""

        text = sep_directive + text

    with open(full_path, "w", encoding=encoding) as file:
        file.write(text)