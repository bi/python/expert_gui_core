# import sys
# from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
# from PyQt5.QtGui import QPainter, QPen, QPainterPath
# from PyQt5.QtCore import Qt, QPoint,QPointF
#
#
# class DrawingApp(QMainWindow):
#     def __init__(self):
#         super().__init__()
#
#         self.setWindowTitle("Drawing Application")
#         self.setGeometry(100, 100, 800, 600)
#
#         self.canvas = CanvasWidget(self)
#         self.setCentralWidget(self.canvas)
#
#
# class CanvasWidget(QWidget):
#     def __init__(self, parent=None):
#         super().__init__(parent)
#         self.parent = parent
#         self.setGeometry(0, 0, 800, 600)
#
#         self.drawing = False
#         self.last_point = QPoint()
#         self.current_point = QPoint()
#         self.shapes = []
#
#     def mousePressEvent(self, event):
#         if event.button() == Qt.LeftButton:
#             self.drawing = True
#             self.last_point = event.pos()
#             self.current_point = event.pos()
#
#     def mouseMoveEvent(self, event):
#         if self.drawing:
#             self.current_point = event.pos()
#             self.update()
#
#     def mouseReleaseEvent(self, event):
#         if event.button() == Qt.LeftButton:
#             self.drawing = False
#             shape = (self.last_point, self.current_point)
#             self.shapes.append(shape)
#             self.update()
#
#     def paintEvent(self, event):
#         painter = QPainter(self)
#         pen = QPen()
#         pen.setWidth(2)
#         pen.setColor(Qt.black)
#         painter.setPen(pen)
#
#         for shape in self.shapes:
#             painter.drawLine(shape[0], shape[1])
#
#         if self.drawing:
#             painter.drawLine(self.last_point, self.current_point)
#
#         startPoint = QPointF(0, 0)
#         controlPoint1 = QPointF(100, 50)
#         controlPoint2 = QPointF(200, 100)
#         endPoint = QPointF(300, 300)
#
#         cubicPath = QPainterPath(startPoint)
#         cubicPath.cubicTo(controlPoint1, controlPoint2, endPoint)
#
#         painter.drawPath(cubicPath)
#
#
#
# def main():
#     app = QApplication(sys.argv)
#     window = DrawingApp()
#     window.show()
#     sys.exit(app.exec_())
#
#
# if __name__ == "__main__":
#     main()

from PyQt5.QtWidgets import QAbstractButton, QStyle, QStylePainter, QStyleOptionButton, QStyleOption, QPushButton, QSizePolicy, QWidget,QGridLayout
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import Qt, QMimeData, QEvent, QSize, QPoint, QRect, QRectF
from PyQt5.QtGui import QDrag,QPalette, QIcon, QLinearGradient, QBrush, QPainter, QFont, QColor, QPen
import fontawesome as fa
import qtawesome as qta

class SimpleButton(QAbstractButton):
    """
    A simple icon button widget.
    """
    def event(self, event):
        if event.type() == QEvent.Enter or event.type() == QEvent.Leave:
            self.update()
        return super().event(event)

    def sizeHint(self):
        self.ensurePolished()
        iconsize = self.iconSize()
        icon = self.icon()
        if not icon.isNull():
            iconsize = icon.actualSize(iconsize)
        margins = self.contentsMargins()
        return QSize(
            iconsize.width() + margins.left() + margins.right(),
            iconsize.height() + margins.top() + margins.bottom()
        )

    def minimumSizeHint(self):
        return self.sizeHint()

    def initStyleOption(self, option: QStyleOptionButton):
        option.initFrom(self)
        option.features |= QStyleOptionButton.Flat
        option.iconSize = self.iconSize()
        option.icon = self.icon()
        option.text = ""
        if self.isDown():
            option.state |= QStyle.State_Sunken

    def set_mouse_pos(self,pos:QPoint):

        if (pos.y() > 2 and pos.y() < 40):
            if (pos.x()> 3 and pos.x() < 20) or (pos.x() < 47 and pos.x() > 30):
                self.edge = True
            else:
                self.edge = False
        else:
            self.edge = False
        self.update()
    def paintEvent(self, event):

        try:
            if not self.edge:
                self.edge = False
        except:
            self.edge = False
        painter = QStylePainter(self)
        option = QStyleOptionButton()
        painter.setRenderHint(QPainter.Antialiasing)
        self.initStyleOption(option)
        if self.isDown():
            pass
        elif option.state & (QStyle.State_MouseOver | QStyle.State_HasFocus):
            painter: QPainter
            option: QStyleOption

        if not option.icon.isNull():
            rect = option.rect.adjusted(1, 1, -1, -1)
            if option.state & QStyle.State_Active:
                mode = (QIcon.Active if option.state & QStyle.State_MouseOver
                        else QIcon.Normal)
            elif option.state & QStyle.State_Enabled:
                mode = QIcon.Normal
            else:
                mode = QIcon.Disabled
            if self.isChecked():
                state = QIcon.On
            else:
                state = QIcon.Off
            option.icon.paint(painter, rect, Qt.AlignCenter, mode, state)

        palette = option.palette
        # g = QLinearGradient(0, 0, 0, 1)
        # g.setCoordinateMode(QLinearGradient.ObjectBoundingMode)
        # base = palette.color(QPalette.Window)
        # base.setAlpha(200)
        # if base.value() < 127:
        #     base_ = base.lighter(170)
        # else:
        #     base_ = base.darker(130)
        # g.setColorAt(0, base_)
        # g.setColorAt(0.6, base)
        # g.setColorAt(1.0, base_)
        # brush = QBrush(base_)
        cg = self.palette_color_group(option.state)
        # if option.state & QStyle.State_HasFocus:
        #     pen = palette.color(cg, QPalette.Highlight)
        # elif option.state & QStyle.State_MouseOver:
        #     pen = palette.color(cg, QPalette.WindowText)
        #     pen.setAlpha(50)
        # else:
        #     pen = Qt.NoPen
        painter.save()
        # painter.setPen(pen)
        # painter.setBrush(brush)
        # painter.translate(0.5, 0.5)
        # painter.setRenderHints(QPainter.Antialiasing, True)
        # painter.drawRoundedRect(option.rect.adjusted(0, 0, -1, -1), 25., 25., )

        if not self.edge:
            pen = palette.color(cg, QPalette.WindowText)
            font = QFont("Verdana")
            font.setPixelSize(7)
            painter.setFont(font)
            painter.setPen(pen)
            painter.drawText(1, 47, "Data Source")
        painter.restore()

        self.draw_arc(self.edge, painter)



    def draw_arc(self,left,painter):
        painter.save()
        if not left:
            pen = QPen(QColor(200,200,200), 2)
            pen1 = QPen(QColor(200,200,200), 1, Qt.DotLine)
            painter.setBrush(QColor(200,200,200))  # Fill color
        else:
            pen = QPen(QColor('gray'), 2)
            pen1 = QPen(QColor('gray'), 1, Qt.DotLine)
            painter.setBrush(QColor('gray'))  # Fill color

        painter.setPen(pen1)

        w_button = 44
        rect = QRect(3, 3, w_button, w_button)  # x, y, width, height


        start_angle = -20*16
        span_angle = 80*16
        painter.drawArc(rect, start_angle, span_angle)

        start_angle = 120 * 16
        span_angle = 80 * 16
        painter.drawArc(rect, start_angle, span_angle)

        painter.setPen(pen)

        rect = QRectF(44, 30, 4, 4)
        painter.drawEllipse(rect)
        rect = QRectF(43, 14, 4, 4)
        painter.drawEllipse(rect)
        rect = QRectF(32, 4, 4, 4)
        painter.drawEllipse(rect)

        rect = QRectF(2, 30, 4, 4)
        painter.drawEllipse(rect)
        rect = QRectF(3, 14, 4, 4)
        painter.drawEllipse(rect)
        rect = QRectF(14, 4, 4, 4)
        painter.drawEllipse(rect)

        painter.restore()

    def palette_color_group(self,state: QStyle.StateFlag) -> QPalette.ColorGroup:
        if not state & QStyle.State_Enabled:
            return QPalette.Disabled
        elif state & QStyle.State_Active:
            return QPalette.Active
        else:
            return QPalette.Inactive

class DragButton(SimpleButton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # match top/bottom margins of MessagesWidget
        self.setStyleSheet("background-color:transparent;")
        self.setContentsMargins(1, 1, 1, 1)
        self.setMouseTracking(True)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def sizeHint(self):
        # Ensure the button has at least font height dimensions.
        sh = super().sizeHint()
        h = self.fontMetrics().lineSpacing()
        return sh.expandedTo(QSize(h, h))


    def mousePressEvent(self, event):
        self.__mousePressPos = None
        self.__mouseMovePos = None
        if event.button() == QtCore.Qt.LeftButton:
            self.__mousePressPos = event.globalPos()
            self.__mouseMovePos = event.globalPos()

        super(DragButton, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):

        # local_pos = event.pos()
        # print(f"Mouse is at Local Position: {local_pos}")

        if event.buttons() == QtCore.Qt.LeftButton:
            currPos = self.mapToGlobal(self.pos())
            globalPos = event.globalPos()
            diff = globalPos - self.__mouseMovePos
            newPos = self.mapFromGlobal(currPos + diff)
            self.move(newPos)

            self.__mouseMovePos = globalPos
        else:
            self.set_mouse_pos(event.pos())

        super(DragButton, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):

        if self.__mousePressPos is not None:
            moved = event.globalPos() - self.__mousePressPos
            if moved.manhattanLength() > 3:
                event.ignore()
                return

        super(DragButton, self).mouseReleaseEvent(event)

    def enterEvent(self, event):
        return super(DragButton, self).enterEvent(event)

    def leaveEvent(self, event):
        self.edge = False
        self.update()
        return super(DragButton, self).enterEvent(event)
class Ui_drag_drop(object):

    def andgdrag(self):
        print("AND press")

    def setupUi(self, drag_drop):
        drag_drop.setObjectName("drag_drop")
        drag_drop.resize(579, 445)
        self.canvas_layout = QtWidgets
        self.centralwidget = QtWidgets.QWidget(drag_drop)
        self.centralwidget.setObjectName("centralwidget")

        drag_drop.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(drag_drop)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 579, 21))
        self.menubar.setObjectName("menubar")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        drag_drop.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(drag_drop)
        self.statusbar.setObjectName("statusbar")
        drag_drop.setStatusBar(self.statusbar)
        self.actionHelp = QtWidgets.QAction(drag_drop)
        self.actionHelp.setObjectName("actionHelp")
        self.menuHelp.addAction(self.actionHelp)
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(drag_drop)
        QtCore.QMetaObject.connectSlotsByName(drag_drop)

        w_button = 50
        # andg
        self.andg = DragButton(self.centralwidget)
        self.andg.setGeometry(QtCore.QRect(10, 0, w_button, w_button))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.andg.setIcon(icon)
        # self.andg.setIconSize(QtCore.QSize(100, 50))
        self.andg.setObjectName("andg")

        self.andg.clicked.connect(self.andgdrag)

        ##nand

        self.nand = DragButton(self.centralwidget)
        self.nand.setGeometry(QtCore.QRect(40, 50, w_button, w_button))
        self.nand.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nand.setIcon(icon1)
        # self.nand.setIconSize(QtCore.QSize(100, 50))
        self.nand.setObjectName("nand")

        # notg
        self.notg = DragButton(self.centralwidget)
        self.notg.setGeometry(QtCore.QRect(10, 110, w_button, w_button))
        self.notg.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.notg.setIcon(icon2)
        # self.notg.setIconSize(QtCore.QSize(100, 50))
        self.notg.setObjectName("notg")

        # org
        self.org = DragButton(self.centralwidget)
        self.org.setGeometry(QtCore.QRect(40, 170, w_button, w_button))
        self.org.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.org.setIcon(icon3)
        # self.org.setIconSize(QtCore.QSize(100, 50))
        self.org.setObjectName("org")

        # nor
        self.nor = DragButton(self.centralwidget)
        self.nor.setGeometry(QtCore.QRect(10, 230, w_button, w_button))
        self.nor.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nor.setIcon(icon4)
        # self.nor.setIconSize(QtCore.QSize(100, 50))
        self.nor.setObjectName("nor")

        # exor
        self.exor = DragButton(self.centralwidget)
        self.exor.setGeometry(QtCore.QRect(40, 290, w_button, w_button))
        self.exor.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.exor.setIcon(icon5)
        # self.exor.setIconSize(QtCore.QSize(100, 50))
        self.exor.setObjectName("exor")

        # exnor
        self.exnor = DragButton(self.centralwidget)
        self.exnor.setGeometry(QtCore.QRect(10, 350, w_button, w_button))
        self.exnor.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap("1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.exnor.setIcon(icon6)
        # self.exnor.setIconSize(QtCore.QSize(100, 50))
        self.exnor.setObjectName("exnor")

        # canvas
        self.canvas = QtWidgets.QLabel(self.centralwidget)
        self.canvas.lower()
        self.canvas.setGeometry(QtCore.QRect(140, 10, 421, 351))
        self.canvas.setMouseTracking(True)
        self.canvas.setAutoFillBackground(False)
        self.canvas.setStyleSheet("color: rgb(255, 255, 255);\n"
                                  "background-color: rgb(255, 255, 255);\n"
                                  "border-color: rgb(0, 0, 0);")
        self.canvas.setText("")
        self.canvas.setObjectName("canvas")

    def retranslateUi(self, drag_drop):
        _translate = QtCore.QCoreApplication.translate
        drag_drop.setWindowTitle(_translate("drag_drop", "MainWindow"))
        self.menuHelp.setTitle(_translate("drag_drop", "More"))
        self.actionHelp.setText(_translate("drag_drop", "Help"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    drag_drop = QtWidgets.QMainWindow()
    ui = Ui_drag_drop()
    ui.setupUi(drag_drop)
    drag_drop.show()
    sys.exit(app.exec_())