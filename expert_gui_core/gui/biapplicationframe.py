import jpype
cern = jpype.JPackage("cern")

import sys
import os
from  pyrbac import AuthenticationClient
import logging

from functools import partial
import cmmnbuild_dep_manager
import qtawesome as qta

from PyQt5.QtGui import QGuiApplication, QPalette
from PyQt5.QtWidgets import QApplication, QTabWidget, QWidget, QGridLayout, \
    QRadioButton, QPushButton, QListView, QComboBox, QDesktopWidget
from PyQt5.QtCore import QTimer, Qt

from accwidgets.app_frame import ApplicationFrame
from accwidgets.qt import exec_app_interruptable

from expert_gui_core.gui.widgets.pyqt import systemmonitor
from expert_gui_core.gui.widgets.pyqt import convertorwidget
from expert_gui_core.gui.widgets.timing import timingwidget
from expert_gui_core.gui.widgets.combiner import metapropertyfesawidget, metanxcalswidget

from expert_gui_core.gui.common.colors import Colors

from expert_gui_core.comm import ccda
from expert_gui_core.comm import fesacomm

from expert_gui_core.gui.widgets.system import configuser


class BIApplicationFrame(ApplicationFrame):

    def __init__(self,
                 app=None,
                 title=None,
                 use_timing_bar=True,
                 use_log_console=False,
                 use_rbac=True,
                 use_fesa=True,
                 use_nxcals=True,
                 domain=None):

        ApplicationFrame.__init__(self,
                                  use_timing_bar=False,
                                  use_log_console=use_log_console,
                                  use_rbac=True)

        qta.icon("fa5.clipboard")

        self._app = app

        # init frame

        self.title = title
        self.left = 300
        self.top = 100
        self.width = 1600
        self.height = 1000
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.default_palette = QGuiApplication.palette()

        # graphical mutex

        self._mutex_graph = False

        # FESA browser

        self._fesa_browser = None
        self._subscribe = {}

        # RBAC

        cmmnbuild_dep_manager.Manager().jvm_required()

        rba_button = self.rba_widget
        if rba_button is not None:
            rba_button.loginSucceeded.connect(self.on_pyrbac_login)
            rba_button.loginFailed.connect(self.on_pyrbac_error)
            rba_button.logoutFinished.connect(self.on_pyrbac_logout)
            self.token = AuthenticationClient().login_kerberos()
            rba_button.model.update_token(self.token)

        # NXCALS browser

        self._nxcals_browser = None

        # Timing panel

        if domain is None:
            domain_sel = ["PSB", "LEI", "CPS", "SPS", "LNA", "ADE"]
        else:
            domain_sel = domain

        self._timing_panel = None

        # Menu

        menu_bar = self.menuBar()

        self.tools_menu = menu_bar.addMenu('&Tools')
        if use_log_console:
            res = self.tools_menu.addAction('Log Console', self.show_hide_log)
            res.setCheckable(True)
            res.setChecked(True)
        if use_timing_bar:
            res_timing = self.tools_menu.addAction('Show/Hide Timing', self.show_hide_timing)
            res_timing.setCheckable(True)
            res_timing.setChecked(False)
        if use_rbac:
            self.res_rbac = self.tools_menu.addAction('Show/Hide RBAC', self.show_hide_rbac)
            self.res_rbac.setCheckable(True)
            self.res_rbac.setChecked(False)
        if use_fesa:
            res = self.tools_menu.addAction('Add FESA Browser', self.add_fesa_browser)
            res.setCheckable(True)
            res.setChecked(False)
        if use_nxcals:
            res = self.tools_menu.addAction('Add NXCALS Browser', self.add_nxcals_browser)
            res.setCheckable(True)
            res.setChecked(False)

        self.res_search = self.tools_menu.addAction('Search Bar', self.add_search_bar)
        self.res_search.setCheckable(True)
        self.res_search.setChecked(False)

        self.tools_menu.addAction('System Monitor', self.show_hide_sysmonitor)
        self.tools_menu.addAction('Data Convertor', self.show_hide_convertor)

        # configuration
        self.config = None
        self.user_config = None
        try:
            self.config = configuser.ConfigUser(self.token.user_name)
            self.user_config = self.token.user_name
        except Exception as e:
            pass

        # system monitor

        self.system_monitor = systemmonitor.SystemMonitorWindow(parent=None, time_period=1 * 1000)
        self.system_monitor.resize(900, 200)
        self.system_monitor.hide()

        # convertor

        self.convertor = convertorwidget.ConvertorWidget(parent=None)
        self.convertor.resize(900, 100)
        self.convertor.hide()

        # log console

        if use_log_console:
            self.log_console.console.expanded = False

        # theme menu

        theme_menu = menu_bar.addMenu('&Theme')
        self.res_dark = theme_menu.addAction('Dark', self.dark_s)
        self.res_dark.setCheckable(True)
        self.res_dark.setChecked(False)
        self.res_light = theme_menu.addAction('Light', self.light_s)
        self.res_light.setCheckable(True)
        self.res_light.setChecked(False)

        # font size menu

        font_menu = menu_bar.addMenu('&Font')
        self.res_font = []
        for i in range(5, 21):
            res = font_menu.addAction(str(i), partial(self.set_font_size, i))
            res.setCheckable(True)
            res.setChecked(False)
            self.res_font.append(res)

        # tab panels

        self.tabpanels = []

        self._tabs = QTabWidget()
        self._tabs.setTabsClosable(True)
        self._tabs.tabCloseRequested.connect(self.closeTab)
        self._tabs.tabBarClicked.connect(self.tab_click)

        # center widget

        center_widget = QWidget()
        self._layout = QGridLayout(center_widget)
        self._layout.setContentsMargins(5, 5, 5, 0)
        self._layout.setSpacing(0)

        self.setCentralWidget(center_widget)
        self.go_button = None

        self.search_bar = self.create_search_panel()
        self._layout.addWidget(self.search_bar, 1, 0)
        self.search_bar.hide()

        self._layout.addWidget(self._tabs, 2, 0)

        self._layout.setRowStretch(0, 0)
        self._layout.setRowStretch(1, 0)
        self._layout.setRowStretch(2, 1)

        # update time used by handle event

        self._update_time = 0

        # show
        try:
            logging.getLogger().warning("User " + str(os.getlogin()))
        except Exception as e:
            pass

        # rbac

        if self.main_toolbar() is not None:
            self.main_toolbar().show()

        # timing

        if use_timing_bar:
            self.create_timing(domain_sel)

        try:
            self._fontsize = 10
            res = self.config.get_config("CORE")

            if res is not None and "theme" in res and res["theme"] == "light":
                self.light_s(save=False)
            elif res is not None and "theme" in res and res["theme"] == "dark":
                self.dark_s(save=False)

            if res is not None and "font-size" in res:
                self.set_font_size(int(res["font-size"]), save=False)


        except Exception as e:
            print(e)
            pass

        self.show()

    def tab_click(self, tab_index):
        """
        Click tabs.
        """
        QTimer.singleShot(1, self.tab_click_process)

    def tab_click_process(self):
        selected_tab_text = self._tabs.tabText(self._tabs.currentIndex())
        items = [self._searched_bar.itemText(index) for index in range(self._searched_bar.count())]
        if selected_tab_text in items:
            self._searched_bar.setCurrentText(selected_tab_text)

    def add_search_bar(self):
        if self.res_search.isChecked():
            self.search_bar.show()
        else:
            self.search_bar.hide()

    def create_search_panel(self):
        panel = QWidget()
        layout = QGridLayout(panel)
        layout.setContentsMargins(2, 2, 2, 2)
        layout.setSpacing(5)

        # config

        res = self.config.get_config("FESA_BROWSER")
        if res is not None:
            queries = res["configs"].split(";")
        else:
            queries = []

        # search combo

        self._search_bar = QComboBox()

        self._search_bar.setMaximumWidth(600)

        self._search_bar_value_set = QListView(self._search_bar)
        self._search_bar.setView(self._search_bar_value_set)
        self._search_bar.setEditable(True)
        self._search_bar.lineEdit().setAlignment(Qt.AlignRight)

        for query in queries:
            self._search_bar.addItem(query)

        layout.addWidget(self._search_bar, 0, 1, 1, 2)

        self._searched_bar = QComboBox()
        self._searched_bar_value_set = QListView(self._searched_bar)
        self._searched_bar.setView(self._searched_bar_value_set)
        self._searched_bar.setEditable(True)
        self._searched_bar.lineEdit().setAlignment(Qt.AlignRight)
        self._searched_bar.currentTextChanged.connect(self.searched_bar_changed)

        layout.addWidget(self._searched_bar, 0, 4)

        # load

        self.load_button = QPushButton("Load")
        layout.addWidget(self.load_button, 0, 0)
        self.load_button.mousePressEvent = self.load_users
        self.load_button.setEnabled(True)

        # search

        self.go_button = QPushButton("Add")
        layout.addWidget(self.go_button, 0, 3)
        self.go_button.mousePressEvent = self.search
        self.go_button.setEnabled(True)

        # subscribe

        self.sub_button = QPushButton("Subscribe")
        layout.addWidget(self.sub_button, 0, 5)
        self.sub_button.mousePressEvent = self.subscribe_all
        self.sub_button.setEnabled(True)

        self._subscribe = {}

        # close

        self.close_button = QPushButton("Close")
        layout.addWidget(self.close_button, 0, 6)
        self.close_button.mousePressEvent = self.close_all
        self.close_button.setEnabled(True)

        # delete

        self.delete_button = QPushButton("Delete")
        layout.addWidget(self.delete_button, 0, 7)
        self.delete_button.mousePressEvent = self.delete_config
        self.delete_button.setEnabled(True)

        # stretch layout

        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)
        layout.setColumnStretch(2, 1)
        layout.setColumnStretch(3, 0)
        layout.setColumnStretch(4, 1)
        layout.setColumnStretch(5, 0)
        layout.setColumnStretch(6, 0)
        layout.setColumnStretch(7, 0)

        return panel

    def searched_bar_changed(self, s):
        if s in self._subscribe:
            if self._subscribe[s]:
                self.sub_button.setText("Stop")
                self.sub_button.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";")
            else:
                self.sub_button.setText("Subscribe")
                if Colors.COLOR_LIGHT:
                    self.sub_button.setStyleSheet(
                        "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
                else:
                    self.sub_button.setStyleSheet(
                        "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

    def subscribe_all(self, e=None):

        name_browser = self._searched_bar.currentText()

        fesa_browser = self._fesa_browser

        if name_browser is not None:
            for i in range(self._tabs.count()):
                if self._tabs.tabText(i) == name_browser:
                    if isinstance(self._tabs.widget(i), metapropertyfesawidget.MetaPropertyFesaWidget):
                        fesa_browser = self._tabs.widget(i)
        else:
            name_browser = "FESA Browser"

        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self.sub_button.text() == "Subscribe":

            if fesa_browser is None:
                return

            if fesa_browser._properties.values():
                self.sub_button.setText("Stop")
                self.sub_button.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";")
                self._subscribe[name_browser] = True
                for pfw in fesa_browser._properties.values():
                    if self._timing_panel is not None and self._timing_panel.isVisible():
                        if self._timing_panel.get_cycle() == "none":
                            pfw.subscribe(None)
                        else:
                            pfw.subscribe(self._timing_panel.get_cycle())
                    else:
                        pfw.subscribe(None)
        else:
            if fesa_browser._properties.values():
                self.sub_button.setText("Subscribe")
                self._subscribe[name_browser] = False
                if Colors.COLOR_LIGHT:
                    self.sub_button.setStyleSheet(
                        "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
                else:
                    self.sub_button.setStyleSheet(
                        "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
                for pfw in fesa_browser._properties.values():
                    pfw.unsubscribe()
        QApplication.restoreOverrideCursor()

    def close_all(self, e=None):

        name_browser = self._searched_bar.currentText()

        fesa_browser = self._fesa_browser

        if name_browser is not None:
            for i in range(self._tabs.count()):
                if self._tabs.tabText(i) == name_browser:
                    if isinstance(self._tabs.widget(i), metapropertyfesawidget.MetaPropertyFesaWidget):
                        fesa_browser = self._tabs.widget(i)

        QApplication.setOverrideCursor(Qt.WaitCursor)
        self._subscribe[name_browser] = False
        self.close_fesa_browser(fesa_browser)
        QApplication.restoreOverrideCursor()

    def close_fesa_browser(self, fesa_browser):
        if fesa_browser is not None:
            self.sub_button.setText("Subscribe")
            if Colors.COLOR_LIGHT:
                self.sub_button.setStyleSheet(
                    "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
            else:
                self.sub_button.setStyleSheet(
                    "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
            for pfw in fesa_browser._properties.values():
                pfw.unsubscribe()
            subs = []
            for name in fesa_browser.mdi._subs:
                subs.append(name)
            for name in subs:
                fesa_browser.mdi._subs[name].close()
            fesa_browser._properties = {}

    def load_users(self, e=None):
        self.user_dialog = _UsersWindow(self)
        self.user_dialog.show()

    def load_config(self, user):
        try:
            self.user_config = user
            self.config = configuser.ConfigUser(user)

            res = self.config.get_config("FESA_BROWSER")

            if res is not None:
                queries = res["configs"].split(";")
            else:
                queries = []

            self._search_bar.clear()

            for query in queries:
                self._search_bar.addItem(query)

            if self.token.user_name != user:
                self.delete_button.setEnabled(False)
            else:
                self.delete_button.setEnabled(True)

        except Exception as e:
            pass

    def delete_config(self, e=None):
        config = self._search_bar.currentText()
        try:
            index = self._search_bar.findText(config)
            if index != -1:
                self._search_bar.removeItem(index)
                items = [self._search_bar.itemText(i) for i in range(self._search_bar.count())]
                config = {
                    "configs": ";".join(items)
                }
                try:
                    if self.token.user_name == self.user_config:
                        self.config.set_config("FESA_BROWSER", config)
                except:
                    pass
        except:
            pass

    def search(self, e=None):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        search_txt = self._search_bar.currentText()

        items = [self._search_bar.itemText(i) for i in range(self._search_bar.count())]

        if search_txt not in items:
            items.append(search_txt)
            self._search_bar.addItem(search_txt)
            config = {
                "configs": ";".join(items)
            }

            try:
                if self.token.user_name == self.user_config:
                    self.config.set_config("FESA_BROWSER", config)
            except:
                pass

        settings = {}

        requests = search_txt.split(" ")

        name_browser = None
        if "[" in search_txt and "]" in search_txt:
            s = search_txt.index("[")
            e = search_txt.index("]")
            if s < e:
                name_browser = search_txt[s+1:e]

        for request in requests:

            sub_requests = request.split("&");

            class_request = None
            devices = None
            props = None
            fields = None
            display = "normal"
            history = False

            for sub_request in sub_requests:
                if "d=" in sub_request:
                    str = sub_request.replace("d=", "")
                    devices = str.split(",")
                elif "c=" in sub_request:
                    str = sub_request.replace("c=", "")
                    class_request = str.split(",")
                elif "p=" in sub_request:
                    str = sub_request.replace("p=", "")
                    props = str
                elif "f=" in sub_request:
                    str = sub_request.replace("f=", "")
                    fields = str
                elif "v=" in sub_request:
                    str = sub_request.replace("v=", "")
                    display = str
                elif "h=" in sub_request:
                    str = sub_request.replace("h=", "")
                    if str == "true":
                        history = True

            if devices is None:
                continue
            else:
                tmp_devices = []
                for dev in devices:
                    if len(dev) > 1 and "*" in dev:
                        if class_request is not None:
                            sdevs = ccda.query_devices(device_query=[dev], class_query=class_request)
                        else:
                            sdevs = ccda.query_devices(device_query=[dev])
                        for sdev in sdevs:
                            if len(tmp_devices) < 100:
                                tmp_devices.append(sdev.name)
                    else:
                        if len(tmp_devices) < 100:
                            tmp_devices.append(dev)
                devices = tmp_devices

            if fields is not None:
                list_fields = fields.split(",")
                for j in range(len(list_fields)):
                    if list_fields[j] == "*":
                        list_fields[j] = "All"
            else:
                list_fields = ["All"]

            if devices is not None:

                class_name = None
                pprops = None

                for i in range(len(devices)):

                    dev_name = devices[i]

                    if i==0:
                        res = ccda.get_class_from_device(dev_name)
                        if res is None:
                            return
                        class_name = res["class"]
                        pprops = props.split(",")

                    tmp_props = []
                    for prop in pprops:
                        if "*" in prop:
                            tmp_props = ccda.get_properties_from_device(dev_name)
                        else:
                            tmp_props.append(prop)
                    pprops = tmp_props

                    for pprop in pprops:
                        prop_name = pprop

                        instance = [class_name, dev_name, prop_name]

                        settings[instance[0] + "/" + instance[1] + "/" + instance[2]] = {}
                        settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["fields"] = list_fields.copy()
                        settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["functions"] = {}
                        settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["display"] = display
                        settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["history"] = history

        fesa_browser = self.add_fesa_browser(name=name_browser)
        if fesa_browser is not None:
            fesa_browser._fesa_search.set_history(history)
            fesa_browser.toggle_panel_search.hide_(None)
            fesa_browser._settings = settings
            fesa_browser.load_settings()
            self._subscribe["FESA Browser"] = False

        QApplication.restoreOverrideCursor()

    @property
    def timing_panel(self) -> timingwidget.TimingPanel:
        return self._timing_panel

    def create_timing(self, domain):
        self._timing_panel = timingwidget.TimingPanel(domain=domain, listener=None)
        self._timing_panel.hide()
        if Colors.COLOR_LIGHT:
            self._timing_panel.light_()
        else:
            self._timing_panel.dark_()
        self._layout.addWidget(self._timing_panel, 0, 0)
        self._layout.setRowStretch(0, 0)

    def add_fesa_browser(self, name=None, localtiming=False):
        """Add fesa browser in the tab panel"""
        if name is None:
            if self._fesa_browser is None:
                if localtiming:
                    self._fesa_browser = metapropertyfesawidget.MetaPropertyFesaWidget(self, None)
                else:
                    self._fesa_browser = metapropertyfesawidget.MetaPropertyFesaWidget(self, self._timing_panel)
                if Colors.COLOR_LIGHT:
                    self._fesa_browser.light_()
                else:
                    self._fesa_browser.dark_()
                self._tabs.addTab(self._fesa_browser, "FESA Browser")
                self.tabpanels.append(self._fesa_browser)
                self._searched_bar.addItem("FESA Browser")
                self._searched_bar.setCurrentText("FESA Browser")
                self._subscribe["FESA Browser"] = False

                QTimer.singleShot(1, self.show_last_tab)

                return self._fesa_browser
        else:

            index = self._searched_bar.findText(name)

            if index > -1:
                for i in range(self._tabs.count()):
                    if self._tabs.tabText(i) == name:
                        return self._tabs.widget(i)
                return None

            if localtiming:
                fesa_browser = metapropertyfesawidget.MetaPropertyFesaWidget(self, None)
            else:
                fesa_browser = metapropertyfesawidget.MetaPropertyFesaWidget(self, self._timing_panel)
            if Colors.COLOR_LIGHT:
                fesa_browser.light_()
            else:
                fesa_browser.dark_()
            self._tabs.addTab(fesa_browser, name)
            self.tabpanels.append(fesa_browser)
            self._searched_bar.addItem(name)
            self._subscribe[name] = False
            self._searched_bar.setCurrentText(name)

            QTimer.singleShot(1, self.show_last_tab)

            return fesa_browser

    def show_last_tab(self):
        if self._tabs.count() > 1:
            self._tabs.setCurrentIndex(self._tabs.count()-1)

    def add_fesa_class_menu(self, classes=None):

        if classes is None:
            return

        # FESA menus

        class_menu = self.menuBar().addMenu('&Class Fec Devices')
        class_menu_item = class_menu.addAction('ALL', self.open_devices_panel)
        fesa_db_fec_class_info = ccda.get_fecs_from_class(classes, operational=False)
        for fesa_class_name, fesa_class_fec_names in fesa_db_fec_class_info.items():
            dict_fesa_fec = ccda.get_devices_from_class_per_fec(fesa_class_name)
            class_menu_item = class_menu.addMenu(fesa_class_name)
            for fec_name in dict_fesa_fec.keys():
                devices = dict_fesa_fec[fec_name]
                fec_menu_item = class_menu_item.addMenu(fec_name)
                for device in devices:
                    fec_menu_item.addAction(device, partial(self.open_tab_panel, device))

    def open_devices_panel(self):
        pass

    def open_tab_panel(self, device=None):
        pass

    def on_pyrbac_login(self, pyrbac_token):
        """RBAC login action"""
        new_token = cern.rbac.common.RbaToken.parseAndValidate(jpype.java.nio.ByteBuffer.wrap(pyrbac_token.encode()))
        cern.rbac.util.holder.ClientTierTokenHolder.setRbaToken(new_token)

    def on_pyrbac_error(self, err):
        """handling of RBAC errors"""
        pass

    def on_pyrbac_logout(self):
        """RBAC logout action"""
        pass

    def _sh_timing(self, expand=True):
        """Show or hide timing panel"""
        if self._timing_panel is not None:
            if self._timing_panel.isVisible():
                self._timing_panel.hide()
            else:
                self._timing_panel.show()
                if expand:
                    self._timing_panel.expand()

    def show_hide_timing(self, expand=True):
        QTimer.singleShot(5, lambda: self._sh_timing(expand))

    def show_hide_rbac(self):
        """Show or hide RBAC toolbar"""
        if self.main_toolbar() is not None:
            if self.main_toolbar().isVisible():
                self.main_toolbar().hide()
                self.res_rbac.setChecked(False)
            else:
                self.res_rbac.setChecked(True)
                self.main_toolbar().show()

    def show_hide_sysmonitor(self):
        """Show or hide Log toolbar"""
        if self.system_monitor is not None:
            if self.system_monitor.isVisible():
                self.system_monitor.hide()
            else:
                self.system_monitor.show()

    def show_hide_convertor(self):
        """Show or hide Log toolbar"""
        if self.convertor is not None:
            if self.convertor.isVisible():
                self.convertor.hide()
            else:
                self.convertor.show()

    def save_core_config(self):
        theme = "dark"
        if Colors.COLOR_LIGHT:
            theme = "light"
        core = {
            'theme': theme,
            'font-size': self._fontsize
        }
        try:
            if self.token.user_name == self.user_config:
                self.config.set_config("CORE", core)
        except:
            pass

    def set_font_size(self, size=8, save=True):
        """Set font size in panels"""
        for res in self.res_font:
            res.setChecked(False)
        self.res_font[size-5].setChecked(True)
        self._fontsize = size
        if save:
            self.save_core_config()
        if self._app is not None:
            font_text_user = self._app.font()
            font_text_user.setPointSize(size)
            self._app.setFont(font_text_user)
        self.setStyleSheet("font-size:" + str(size) + ";")
        try:
            if self.tabpanels is not None:
                for tab in self.tabpanels:
                    try:
                        tab.set_font_size(size=size)
                    except:
                        pass
        except:
            pass
        if Colors.COLOR_LIGHT:
            self.light_s()
        else:
            self.dark_s()

    def add_nxcals_browser(self):
        """Add edge browser in the tab panel"""
        if self._nxcals_browser is None:
            try:
                self._nxcals_browser = metanxcalswidget.MetaNXCALSWidget(self)
                self._tabs.addTab(self._nxcals_browser, "NXCALS Browser")
                self.tabpanels.append(self._nxcals_browser)
                if Colors.COLOR_LIGHT:
                    self._nxcals_browser.light_()
                else:
                    self._nxcals_browser.dark_()
            except Exception as e:
                logging.getLogger().error(str(e))

    def show_hide_log(self):
        """Show or hide Log toolbar"""
        if self.log_console is not None:
            if self.log_console.isVisible():
                self.log_console.hide()
            else:
                self.log_console.show()

    def dark_s(self, save=True):
        self.dark_()
        if save:
            self.save_core_config()

    def dark_(self):
        """Set dark theme"""
        Colors.COLOR_LIGHT = False
        self.res_dark.setChecked(True)
        self.res_light.setChecked(False)
        self._mutex_graph = True
        darkpalette = QPalette()
        darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_L3BLACK)
        # darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
        # darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
        darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
        darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
        self._app.setPalette(darkpalette)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

        try:
            if self._tabs is not None:
                self._tabs.setStyleSheet(
                    "background-color: " + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        except:
            pass

        qss = """
            QToolTip {
                border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;
                color: """ + Colors.STR_COLOR_WHITE + """;
            }
            QMenuBar::item {
                spacing: 2px;
                padding: 2px 10px;
                background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
            }
            QMenuBar::item:selected {
                background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QMenuBar::item:pressed {
                background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QScrollArea {
                background-color: transparent;
            }
            QRadioButton::checked {
                color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QRadioButton::indicator {
                border-radius: 6px;
            }
            QTabBar::tab:selected {
              background: """+Colors.STR_COLOR_L3BLACK+""";
            }
            QRadioButton::indicator::unchecked{
                border-radius: 6px;
                border:2px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;
            }

            QRadioButton::indicator::checked{
                border: 2px solid;
                border-color: """ + Colors.STR_COLOR_L2BLUE + """;
                border-radius: 6px;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QCheckBox {
                color: """ + Colors.STR_COLOR_WHITE + """;
            }
            QCheckBox::indicator {
                max-width:14;
                max-height:14;
                border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;
            }
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QLineEdit {
                background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QComboBox {
                border:none;
                background-color:""" + Colors.STR_COLOR_L1BLACK + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
            }
            QListView {
                background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
            }
        """
        self._app.setStyleSheet(qss)

        if self.go_button is not None:
            self.go_button.setStyleSheet(
                "font-weight:bold;background-color:" + Colors.STR_COLOR_BUTTON_FW + ";color:" + Colors.STR_COLOR_WHITE + ";")
            self.delete_button.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_LIGHTORANGE + ";color:" + Colors.STR_COLOR_WHITE + ";")
            self._search_bar.setStyleSheet(
                "padding:1px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
            self._search_bar_value_set.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")

            self._searched_bar.setStyleSheet(
                "padding:1px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
            self._searched_bar_value_set.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")

        if self._timing_panel is not None:
            self._timing_panel.dark_()
        if self._nxcals_browser is not None:
            self._nxcals_browser.dark_()
        if self._fesa_browser is not None:
            self._fesa_browser.dark_()
        self.system_monitor.dark_()
        self.convertor.dark_()
        self.dark_tabs()
        self._mutex_graph = False

    def light_s(self, save=True):
        self.light_()
        if save:
            self.save_core_config()

    def light_(self, save=True):
        """Set light theme"""
        Colors.COLOR_LIGHT = True

        self.res_dark.setChecked(False)
        self.res_light.setChecked(True)
        self._mutex_graph = True
        QGuiApplication.setPalette(self.default_palette)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        try:
            if self._tabs is not None:
                self._tabs.setStyleSheet(
                    "background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        except:
            pass

        qss = """
            QScrollArea {
                background-color: transparent;
            }
            QToolTip {
                border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;
                background-color: """ + Colors.STR_COLOR_RED + """;`
                color: """ + Colors.STR_COLOR_BLUE + """;
            }
             QCheckBox::indicator {
                max-width:14;
                max-height:14;
                border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;
                background-color: """ + Colors.STR_COLOR_WHITE + """;
            }
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QTabBar::tab:selected {
              background: """+Colors.STR_COLOR_TABWHITE+""";
            }
            QCheckBox {
                color: """ + Colors.STR_COLOR_BLACK + """;
            }
            QRadioButton::checked {
                color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QRadioButton::indicator {
                border-radius: 6px;
            }
            QRadioButton::indicator::unchecked{
                border-radius: 6px;
                border:2px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
                background-color: """ + Colors.STR_COLOR_WHITE + """;
            }

            QRadioButton::indicator::checked{
                border: 2px solid;
                border-color: """ + Colors.STR_COLOR_L2BLUE + """;
                border-radius: 6px;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QLineEdit {
                color:""" + Colors.STR_COLOR_BLACK + """;
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                background-color:""" + Colors.STR_COLOR_WHITE + """;
            }
            QComboBox {
                border:0px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
                background-color:""" + Colors.STR_COLOR_WHITE + """;
                selection-color:""" + Colors.STR_COLOR_BLACK + """;
                color:""" + Colors.STR_COLOR_BLACK + """;
                selection-background-color:""" + Colors.STR_COLOR_DWHITE + """;
            }
            QListView {
                background-color:""" + Colors.STR_COLOR_DWHITE + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_BLACK + """;
                selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
            }
        """

        self._app.setStyleSheet(qss)

        if self.go_button is not None:
            self.go_button.setStyleSheet(
                "font-weight:bold;background-color:" + Colors.STR_COLOR_BUTTON_FW + ";color:" + Colors.STR_COLOR_WHITE + ";")
            self.delete_button.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTORANGE + ";color:" + Colors.STR_COLOR_WHITE + ";")
            self._search_bar.setStyleSheet(
                "padding:1px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
            self._search_bar_value_set.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
            self._searched_bar.setStyleSheet(
                "padding:1px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
            self._searched_bar_value_set.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        if self._timing_panel is not None:
            self._timing_panel.light_()
        if self._nxcals_browser is not None:
            self._nxcals_browser.light_()
        if self._fesa_browser is not None:
            self._fesa_browser.light_()
        self.system_monitor.light_()
        self.convertor.light_()
        self.light_tabs()
        self._mutex_graph = False

    def light_tabs(self):
        """
        Light theme for all tab panels.
        """
        try:
            if self.tabpanels is not None:
                for tabpanel in self.tabpanels:
                    try:
                        tabpanel.light_()
                    except:
                        pass
        except:
            pass

    def dark_tabs(self):
        """
        Light theme for all tab panels.
        """

        try:
            if self.tabpanels is not None:
                for tabpanel in self.tabpanels:
                    try:
                        tabpanel.dark_()
                    except:
                        pass
        except:
            pass

    def closeTab(self, currentIndex):
        """Close tab"""
        try:
            currentQWidget = self._tabs.widget(currentIndex)
            currentQWidget.deleteLater()
            currentQWidget.close()
            if isinstance(currentQWidget, metanxcalswidget.MetaNXCALSWidget):
                self._nxcals_browser = None
            if isinstance(currentQWidget, metapropertyfesawidget.MetaPropertyFesaWidget):
                index = self._searched_bar.findText(self._tabs.tabText(currentIndex))
                if index != -1:
                    self.close_fesa_browser(currentQWidget)
                    self._subscribe.pop(self._tabs.tabText(currentIndex))
                    self._searched_bar.removeItem(index)
                if self._tabs.tabText(currentIndex) == "FESA Browser":
                    self._fesa_browser = None

            self._tabs.removeTab(currentIndex)
        except:
            pass

    def get_cycle(self):
        """Return selected cycle"""
        return self._timing_panel.get_cycle()

    def get_cycle_per_dom(self, domain):
        """Return selected cycle for a specific domain"""
        return self._timing_panel.get_cycle_per_dom(domain)


class _UsersWindow(QWidget):
    def __init__(self, parent):
        super().__init__()

        self.parent = parent

        self.setWindowTitle('Load Configuration')
        self.resize(400,100)

        layout = QGridLayout()

        self.combo_box = QComboBox()
        self.combo_box.addItems(configuser.ConfigUser.get_list_users())
        layout.addWidget(self.combo_box, 0, 0)

        self.button = QPushButton('Load')
        self.button.clicked.connect(self.load_selected)
        layout.addWidget(self.button, 1, 0)

        layout.setRowStretch(0,1)
        layout.setRowStretch(1,0)

        # Set the layout for the window
        self.setLayout(layout)

        self.center_window()

    def center_window(self):

        screen_geometry = QDesktopWidget().availableGeometry()
        window_geometry = self.frameGeometry()
        center_point = screen_geometry.center()
        window_geometry.moveCenter(center_point)
        self.move(window_geometry.topLeft())

    def load_selected(self):
        self.parent.load_config(self.combo_box.currentText())
        self.deleteLater()

        # QMessageBox.information(self, 'Selected Option', f'You selected: {selected_text}')


class _MyBIApplicationFrame(BIApplicationFrame, fesacomm.FesaCommListener):
    def __init__(self, app=None, title=None, use_timing_bar=False,
                 use_log_console=True,
                 use_rbac=True):
        BIApplicationFrame.__init__(self,
                                    app=app,
                                    title=title,
                                    use_timing_bar=use_timing_bar,
                                    use_log_console=use_log_console,
                                    use_rbac=use_rbac)

        mywidget = QWidget(self)
        layout = QGridLayout(mywidget)
        layout.setContentsMargins(20, 10, 10, 2)
        layout.setSpacing(5)
        b = QRadioButton("TEST")
        layout.addWidget(b, 0, 0)
        b.setToolTip("TEST")
        #
        # data_widget_opts = {
        #     "auto": True,
        #     "show_table": False,
        #     "show_image": True,
        #     "show_chart": True,
        #     "show_singlechart": True,
        #     "show_scalar": False,
        #     "show_bitenum": False,
        #     "show_chartgl": False,
        #     "show_chart_grid": False,
        #     "show_chart_waterfall": False,
        #     "show_chartgl_3d": False,
        #     "show_chartgl_points": False,
        #     "show_chartgl_surface": False,
        #     "show_chartgl_lines": False,
        #     "chart_palette": "stroke",
        #     "history": True,
        #     "show_image_value_axis": True,
        #     "show_singlechart_value_axis": True,
        #     "show_chart_value_axis": True,
        #     "size_max_table": 10000,
        #     "statistics": True,
        #     "fitting": False,
        #     "color_amplitude": False,
        #     "low_res_chart": False
        # }
        #
        # self.data_widget = datawidget.DataWidget(self,
        #                                          title=["TEST"],
        #                                          name=["TEST"],
        #                                          max=None,
        #                                          **data_widget_opts)
        # layout.addWidget(self.data_widget, 1, 0)
        #
        # QTimer.singleShot(100, lambda: self.subscribe())
        # self.show_hide_timing()
        #
        # self._tabs.addTab(mywidget, "Main")

    _update_time = 0

    # def subscribe(self):
    #     self.fesa_comm = fesacomm.FesaComm("ADE.BCCCA", "Acquisition", listener=self, min_time_between_event=0)
    #     self.fesa_comm.subscribe("ADE.USER.ADE")
    #
    # def getLPCoefficientsButterworth2Pole(self, samplerate, cutoff):
    #
    #     PI = 3.1415926535897932385
    #     sqrt2 = 1.4142135623730950488
    #
    #     QcRaw = (2 * PI * cutoff) / samplerate
    #     QcWarp = math.tan(QcRaw)
    #
    #     gain = 1 / (1 + sqrt2 / QcWarp + 2 / (QcWarp * QcWarp))
    #
    #     by = [0] * 6
    #
    #     by[2] = (1 - sqrt2 / QcWarp + 2 / (QcWarp * QcWarp)) * gain
    #     by[1] = (2 - 2 * 2 / (QcWarp * QcWarp)) * gain
    #     by[0] = 1
    #
    #     by[3] = 1 * gain
    #     by[4] = 2 * gain
    #     by[5] = 1 * gain
    #
    #     return by
    #
    # def filter2(self, samples):
    #
    #     fs = 200
    #     fd = 0.25
    #
    #     wd = 2 * math.pi * fd / fs
    #     alpha = math.tan(wd / 2)
    #     alphac = alpha * alpha
    #     sqrtd = math.sqrt(2.)
    #     denom = (1 + sqrtd * alpha + alphac)
    #
    #     k = alphac / denom
    #     a1 = 2. * (alphac - 1.) / denom
    #     a2 = (1. - sqrtd * alpha + alphac) / denom
    #     b1 = 2.
    #     b2 = 1.
    #
    #     copysamples = samples.copy()
    #     count = copysamples.size
    #
    #     for i in range(2, count):
    #         copysamples[i] = k * samples[i] + k * b1 * samples[i - 1] + k * b2 * samples[i - 2] - a1 * copysamples[
    #             i - 1] - a2 * copysamples[i - 2]
    #
    #     output2 = samples.copy()
    #     output2[0:output2.size - 180] = copysamples[180:len(copysamples)]
    #
    #     return output2
    #
    # def filter(self, samples):
    #
    #     xv = [0] * 3
    #     yv = [0] * 3
    #     copysamples = samples.copy()
    #     count = copysamples.size
    #
    #     bx = self.getLPCoefficientsButterworth2Pole(200, 0.8)
    #
    #     for i in range(0, count):
    #         xv[2] = xv[1]
    #         xv[1] = xv[0]
    #         xv[0] = copysamples[i]
    #         yv[2] = yv[1]
    #         yv[1] = yv[0]
    #
    #         yv[0] = (bx[3] * xv[0] + bx[4] * xv[1] + bx[5] * xv[2] - bx[1] * yv[0] - bx[2] * yv[1])
    #
    #         copysamples[i] = yv[0]
    #
    #     output2 = samples.copy()
    #     output2[0:output2.size - 40] = copysamples[40:len(copysamples)]
    #
    #     return output2
    #
    # def lowpf(self, input):
    #
    #     fe = 200
    #     fc = 0.05
    #
    #     RC = 1. / (fc * 2. * math.pi)
    #     dt = 1. / fe
    #
    #     alpha = dt / (RC + dt)
    #
    #     output = []
    #     output.append(0)
    #
    #     for i in range(1, input.size):
    #         val = output[i - 1] + (alpha * (input[i] - output[i - 1]))
    #         output.append(val)
    #
    #     output2 = input.copy()
    #     ofs = 400
    #     output2[0:output2.size - ofs] = output[ofs:len(output)]
    #     return output2
    #
    # def highpf(self, input):
    #
    #     fe = 200
    #     fc = 1.
    #
    #     RC = 1. / (fc * 2. * math.pi)
    #     dt = 1. / fe
    #
    #     alpha = dt / (RC + dt)
    #
    #     output = []
    #     output.append(0)
    #
    #     for i in range(1, input.size):
    #         val = alpha * (output[i - 1] + input[i] - input[i - 1])
    #         output.append(val)
    #
    #     output2 = input.copy()
    #     ofs = 340
    #     output2[0:output2.size - ofs] = output[ofs:len(output)]
    #
    #     return output2
    #
    # def handle_event(self, name, value):
    #
    #     print("# ", (value['_time'] - self._update_time))
    #
    #     values = value['beamCurrentA']
    #     values = np.array(values, dtype=np.float64)
    #
    #     len = int(0.15 * values.size)
    #     len = 0
    #     fe = 200000
    #     fc2 = 364
    #     f_nyq = fe / 2
    #
    #     b, a = butter(4, fc2 / f_nyq, 'low', analog=False)
    #     values1 = filtfilt(b, a, values)
    #
    #     values3 = self.lowpf(values)
    #
    #     values6 = self.highpf(values)
    #
    #     values7 = self.filter2(values)
    #
    #     values4 = values3 + values6
    #
    #     values1[len:len + 4100] = values[len:len + 4100]
    #     values4[len:len + 4100] = values[len:len + 4100]
    #     values3[len:len + 4100] = values[len:len + 4100]
    #     values6[len:len + 4100] = values[len:len + 4100]
    #     values7[len:len + 4100] = values[len:len + 4100]
    #
    #     # values5 = values.copy()
    #     # delt=110
    #     # values5[110:] = (values5[110:]+values[:values.size-110])/2
    #
    #     self.data_widget.set_data(values[len:], name="current", name_parent="TEST", color="red")
    #
    #     # self.data_widget.set_data(values4[len:], name="bp", name_parent="TEST",color="blue")
    #
    #     # self.data_widget.set_data(values3[len:], name="lp", name_parent="TEST", color="yellow")
    #
    #     # self.data_widget.set_data(values6[len:], name="hp", name_parent="TEST", color="orange")
    #
    #     # self.data_widget.set_data(values7[len:], name="filter", name_parent="TEST", color="pink")
    #
    #     # self.data_widget.set_data(values1[len:], name="filtfilt", name_parent="TEST", color="cyan")
    #
    #     self._update_time = value['_time']
    #


class Test(ApplicationFrame):
    def __init__(self,
                 app=None,
                 title=None,
                 use_timing_bar=True,
                 use_log_console=True,
                 use_rbac=True):
        ApplicationFrame.__init__(self, use_timing_bar=False,
                                  use_log_console=use_log_console,
                                  use_rbac=use_rbac)

        self.show()


def main():
    #####
    # Create app
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    #####
    # Init font icons

    Colors.init_font()

    #####
    # Create window

    window = _MyBIApplicationFrame(app=app, use_timing_bar=True)


    # window.set_font_size(10)

    #####
    # Window show

    # window.show()

    window.dark_()

    # window = Test(app=app)
    # window.log_console.expanded = False
    # window.log_console.toggleViewAction()

    #####
    # End/close app
    sys.exit(exec_app_interruptable(app))


# Compatibility with acc-py app
if __name__ == "__main__":
    print("start")
    main()
