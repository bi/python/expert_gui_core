import os.path
from configparser import ConfigParser

class ConfigUser():
    """

    GUI Configuration per user

    [FESA_BROWSER]
    settings=akjskfdjdf,dksjlskdjsl,lkdjslkjfsklfj

    [CORE]
    theme= light
    """

    def __init__(self, user):
        self.user = user
        if user is not None:
            self.path = '/user/bdisoft/operational/python/gui/configuration/config' + user + '.cfg'
            self.config = ConfigParser()
            if os.path.exists(self.path):
                self.content = self.config.read(self.path)
            else:
                with open(self.path, 'w') as configfile:
                    self.config.write(configfile)
                os.chmod(self.path, 0o777)

    def get_config(self, name_config:str):
        if self.user is None:
            return None
        try:
            return self.config[name_config]
        except:
            return None

    def set_config(self, name_config, configs):
        if self.user is None:
            return

        self.config[name_config] = configs

        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        os.chmod(self.path, 0o777)

    @staticmethod
    def get_list_users():
        list_users = []
        path = '/user/bdisoft/operational/python/gui/configuration'
        with os.scandir(path) as entries:
            for entry in entries:
                if "config" in entry.name and ".cfg" in entry.name:
                    name_user = entry.name.replace("config","").replace(".cfg","")
                    list_users.append(name_user)
        return list_users

