import sys
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QEvent
from PyQt5.QtGui import QPalette, QFont
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QCheckBox, QMainWindow, QTabWidget, QScrollArea, QLabel, \
    QPushButton, QLineEdit, QGroupBox

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import ccda
from expert_gui_core.comm import fesacomm
from expert_gui_core.gui.widgets.common import headerwidget
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.widgets.datapanels import arraypanelwidget
from expert_gui_core.gui.widgets.datapanels import boolpanelwidget
from expert_gui_core.gui.widgets.datapanels import enumpanelwidget
from expert_gui_core.gui.widgets.datapanels import bitenumpanelwidget
from expert_gui_core.gui.widgets.datapanels import stringpanelwidget


class PropertyFesaWidget(QWidget, fesacomm.FesaCommListener):
    """
    Class widget to display automaic FESA property fields.

    :param parent: Parent object.
    :type parent: object
    :param title: Title to be shown in the top center.
    :type title: str, optional
    :param name_device: Device name.
        :type name_device: str
    :param name_property: Property name.
    :type name_property: str
    :param name_fields: Field names (filtering).
    :type name_fields: list, optional
    :param type_fields: Field types (change the GUI component or visualisation option).
    :type type_fields: list, optional
    :param max_row: Maximum number of rows to show, if the number exceeds then new columns are added (default=10).
    :type max_row: int, optional
    :param history: Keep historical data (default=False).
    :type history: bool, optional
    :param header: Show or not the header bar (default=True).
    :type header: bool, optional
    :param button: Show or not the comm actions panel (get/set/subscribe, default=True).
    :type button: bool, optional
    :param subscribe: Show or not subscribe button (default=True).
    :type subscribe: bool, optional
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent, title=None, name_device="", name_property="", name_fields=None, type_fields=None,
                 function_expression=None, max_row=10, history=False, header=True, button=True, subscribe=True,
                 filter=True, input_struct=None, partial=True, info=True, display="normal"):
        """
        Initialize class.
        """
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self.hide()

        self._parent = parent
        self._components = {}
        self._components_check = {}
        self._components_info = {}
        self._max_row = max_row
        self._light = True
        self._filter = filter
        self._partial = partial
        self._info = info
        self._function_expression = function_expression
        self._input_struct = input_struct

        info_class = ccda.get_class_from_device(name_device)
        if len(info_class.keys()) == 0:
            return

        # FESA information

        name_class = info_class["class"]
        name_class_version = info_class["version"]
        self.name_device = name_device
        self.name_property = name_property
        self._fesacomm = fesacomm.FesaComm(name_device, name_property, listener=self)

        self.property_fields = None

        # layout construction

        self.layout = QGridLayout()
        self.layout.setHorizontalSpacing(0)
        self.layout.setVerticalSpacing(0)
        groupBox = QGroupBox()
        groupBox.setLayout(self.layout)

        # scroll area

        self.scroll = QScrollArea()
        self.scroll.setWidget(groupBox)
        self.scroll.setWidgetResizable(True)

        layout = QGridLayout(self)

        self.setStyleSheet("background-color:transparent;")

        # header

        if header:
            self._header = headerwidget.HeaderWidget()
            layout.addWidget(self._header, 0, 0)
        else:
            self._header = None

        # title

        if title is not None:
            self._label_title = QLabel(title)
            self._label_title.setStyleSheet("background-color:transparent;")
            self._label_title.setAlignment(Qt.AlignCenter)
            layout.addWidget(self._label_title, 1, 0)
        else:
            self._label_title = None

        # add filter and all checkboxes

        panel_filter = QWidget()

        panel_filter.setStyleSheet("background-color:transparent;")
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(10, 5, 0, 0)
        layout_filter.setSpacing(5)

        if self._filter:
            layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)

        self._text_filter = QLineEdit("")
        self._text_filter.installEventFilter(self)
        self.filter_changed.connect(self.filter_fields)

        if self._filter:
            layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
            layout_filter.addWidget(QLabel(" "), 0, 1)

        self._checkbox_all = QCheckBox("ALL")
        self._checkbox_all.setChecked(True)
        self._checkbox_all.toggled.connect(self.press_all)

        if self._partial:
            layout_filter.addWidget(self._checkbox_all, 0, 0)
            layout_filter.setColumnStretch(1, 1)

        layout.addWidget(panel_filter, 2, 0)

        layout.addWidget(self.scroll, 3, 0)

        # panel buttons get/set/subscribe

        self.panel_buttons = QWidget()
        layout_buttons = QGridLayout(self.panel_buttons)
        layout_buttons.setContentsMargins(0, 10, 0, 0)
        layout_buttons.setSpacing(5)
        layout_buttons.addWidget(QLabel(""), 0, 0)
        self._get_button = QPushButton(" Get ")
        self._get_button.mousePressEvent = self.get
        self._set_button = QPushButton(" Set ")
        self._set_button.mousePressEvent = self.set
        self._subscribe_button = QPushButton(" Subscribe ")
        self._subscribe_button.mousePressEvent = self.subscribe
        self._clear_button = QPushButton(" Clear History ")
        self._clear_button.mousePressEvent = self.clear_data

        layout_buttons.addWidget(self._get_button, 0, 1)
        layout_buttons.addWidget(self._set_button, 0, 2)

        if subscribe:
            layout_buttons.addWidget(self._subscribe_button, 0, 3)

        if history:
            layout_buttons.addWidget(self._clear_button, 0, 5)
        layout_buttons.setColumnStretch(0, 1)

        if button:
            layout.addWidget(self.panel_buttons, 4, 0)

        # process name fields

        self.name_fields = name_fields

        get_pffc_opts = {
            'class_name': name_class,
            'property_name': name_property,
            'class_version': name_class_version
        }
        property_fields = ccda.get_property_fields_from_class(**get_pffc_opts)

        if name_fields is not None:
            exclude_mode = False
            exclude_names = []
            for name_field in name_fields:
                if "!" in name_field:
                    exclude_mode = True
                    exclude_names.append(name_field.replace("!",""))
            if exclude_mode:
                self.name_fields = []

                pfield_time = PropertyFesaWidget.dotdict({'array_dim1': None,
                                                          'array_dim2': None,
                                                          'data_type': 'scalar',
                                                          'description': None,
                                                          'field_direction': 'OUT',
                                                          'max_value': None,
                                                          'min_value': None,
                                                          'name': '_time',
                                                          'primitive_data_type': 'INT',
                                                          'property_field_boolean_meaning': None,
                                                          'property_field_enums': [],
                                                          'unit': None,
                                                          'unit_exponent': None})

                pfield_cycle = PropertyFesaWidget.dotdict({'array_dim1': 256,
                                                           'array_dim2': None,
                                                           'data_type': 'scalar',
                                                           'description': None,
                                                           'field_direction': 'OUT',
                                                           'max_value': None,
                                                           'min_value': None,
                                                           'name': '_cycle',
                                                           'primitive_data_type': 'STRING',
                                                           'property_field_boolean_meaning': None,
                                                           'property_field_enums': [],
                                                           'unit': None,
                                                           'unit_exponent': None})

                property_fields.append(pfield_time)
                property_fields.append(pfield_cycle)

                for propertyField in property_fields:
                    if propertyField.name not in exclude_names:
                        self.name_fields.append(propertyField.name)


        # create content panel

        content_pane_opts = {
            'title': title,
            'name_class': name_class,
            'name_property': name_property,
            'name_class_version': name_class_version,
            'name_fields': self.name_fields,
            'type_fields': type_fields,
            'max_row': max_row,
            'history': history,
            "display": display
        }

        self.type_fields = type_fields
        self.create_content_panel(**content_pane_opts)

        groupBox.setStyleSheet("border:none;")

        self._update_time = 0

        self.name_cycle = ""
        self.is_subscribing = False

    def press_all(self, e):
        """
        Click on "all" checkbox.
        """
        for checkbox in self._components_check.values():
            checkbox.setChecked(self._checkbox_all.isChecked())

    def clear_data(self, e):
        """
        Clear all data buffer.
        """
        for name in self._components:
            try:
                self._components[name]._data_widget.clear_data()
            except Exception as e:
                pass

    def subscribe(self, e):
        """
        Start/Stop subscription.
        """
        if self.is_subscribing == True:
            self.unsubscribe()
        else:
            self.is_subscribing = True
            try:
                if self._parent.get_cycle() == "None":
                    self.name_cycle = ""
                else:
                    self.name_cycle = self._parent.get_cycle()
            except:
                self.name_cycle = ""

            if not self._cycle_bound:
                self.name_cycle = ""

            if self._header is not None:
                self._header.set_comment(self.name_cycle)

            self._fesacomm.subscribe(self.name_cycle)
            self._subscribe_button.setText(" Stop ")
            self._subscribe_button.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";")

    def unsubscribe(self):
        """
        Unsubscribe to FESA property.
        """
        self.is_subscribing = False
        self._fesacomm.unsubscribe()
        self._subscribe_button.setText(" Subscribe ")
        if self._light:
            self._subscribe_button.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        else:
            self._subscribe_button.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

    def get(self, e):
        """
        Automatic property Get action calling same handelevent from Listener and using selected cycle.
        """
        try:
            if self._parent.get_cycle() == "None":
                self.name_cycle = ""
            else:
                self.name_cycle = self._parent.get_cycle()
        except:
            self.name_cycle = ""
        self._fesacomm.get_action(self.name_cycle)

    def set(self, e):
        """
        Automatic property Set action calling using selected cycle.
        """
        try:

            if self._parent.get_cycle() == "None":
                self.name_cycle = ""
            else:
                self.name_cycle = self._parent.get_cycle()
        except:
            self.name_cycle = ""

        result = self._fesacomm.get(self.name_cycle)

        if result is None:
            result = {}
            for pf in self.property_fields:
                result[pf.name] = None

        if len(result) == 2:
            result = result[0]

        for name_field in self._components_check:
            if self.name_fields is not None:
                if name_field not in self.name_fields:
                    continue
            if result is None:
                return

            # format result

            if name_field in result:
                if self._components_check[name_field].isChecked():
                    data = self._components[name_field].get_data()
                    result[name_field] = data
                else:
                    result.pop(name_field)

        self._fesacomm.set(result, self.name_cycle)

    def get_expressions(self):
        """
        Get all function expressions.
        """
        self._function_expression = None
        for name in self._components:
            try:
                expression = self._components[name].get_expression()
                if expression is not None:
                    if self._function_expression is None:
                        self._function_expression = {}
                    self._function_expression[name] = expression
            except:
                pass
        return self._function_expression

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
                self._components_check[component_field].show()
                self._components_info[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                    self._components_check[component_field].show()
                    self._components_info[component_field].show()
                else:
                    self._components[component_field].hide()
                    self._components_check[component_field].hide()
                    self._components_info[component_field].hide()
            except:
                self._components[component_field].hide()
                self._components_check[component_field].hide()
                self._components_info[component_field].hide()

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    class dotdict(dict):
        """dot.notation access to dictionary attributes"""
        __getattr__ = dict.get
        __setattr__ = dict.__setitem__
        __delattr__ = dict.__delitem__

    def create_content_panel(self, title=None, name_class="", name_property="", name_class_version=None,
                             name_fields=None, type_fields=None, max_row=10, history=False, display="normal"):
        """
        Create automatic panel with all datamap property fields.

        :param title: Title to be shown on top center.
        :type title: str, optional
        :param name_class: Class name.
        :type class_name: str
        :param name_property: Property name.
        :type property_name: str
        :param name_class_version: Class version.
        :type class_version: str, optional
        :param name_fields: List of field names (filtering).
        :type name_fields: list, optional
        :param type_fields: Field types (change the GUI component or visualisation option).
        :type type_fields: list, optional
        :param max_row: Maximum number of rows to show, if the number exceeds then new columns are added (default=10)
        :type max_row: int, optional
        :param history: Keep historical data (default=False)
        :type history: bool, optional
        """
        if self._input_struct is None:

            # get property fields

            get_pffc_opts = {
                'class_name': name_class,
                'property_name': name_property,
                'class_version': name_class_version
            }
            self.property_fields = ccda.get_property_fields_from_class(**get_pffc_opts)
        else:
            self.property_fields = self._input_struct

        # coloring
        strcolor = None
        color = None
        palette_line = None
        if ".B1" in self.name_device:
            strcolor = Colors.STR_COLOR_H
            color = Colors.COLOR_H
            palette_line = "ocean"
        elif ".B2" in self.name_device:
            strcolor = Colors.STR_COLOR_V
            color = Colors.COLOR_V
            palette_line = "bbq"

        # calculate auto width

        i = 0
        size_str_max = 0
        for propertyField in self.property_fields:
            if name_fields is not None:
                if propertyField.name not in name_fields:
                    continue
            if size_str_max < len(propertyField.name):
                size_str_max = len(propertyField.name)
        size_str_max = int(size_str_max * 8.2)

        # get property definition

        if self._input_struct is not None:
            property_definition = {}
            property_definition["writable"] = False
            property_definition["readable"] = False
            property_definition["monitorable"] = False
            property_definition["cycle_bound"] = False
        else:
            property_definition = ccda.get_property_definition(**get_pffc_opts)

        index_definition = 3

        try:
            if not property_definition["readable"]:
                self._get_button.hide()
                index_definition = index_definition - 1
        except:
            self._get_button.hide()
            index_definition = index_definition - 1

        try:

            if "writable" in property_definition:
                edit = property_definition["writable"]
            else:
                edit = False

            if not edit:
                self._set_button.hide()
                self._checkbox_all.hide()
                index_definition = index_definition - 1
        except:

            edit = False

            self._set_button.hide()
            self._checkbox_all.hide()
            index_definition = index_definition - 1

        try:
            if not property_definition["monitorable"]:
                self._subscribe_button.hide()
                index_definition = index_definition - 1
        except:
            self._subscribe_button.hide()
            index_definition = index_definition - 1

        if index_definition == 0:
            self.panel_buttons.hide()

        self._cycle_bound = False

        try:
            if property_definition["is_multiplexed"]:
                self._cycle_bound = True
        except:
            pass

        try:
            if property_definition["cycle_bound"]:
                self._cycle_bound = True
        except:
            pass

        # add field widgets

        col = 0
        row = 0

        self._function_expression = {}

        pfield_time = PropertyFesaWidget.dotdict({'array_dim1': None,
                 'array_dim2': None,
                 'data_type': 'scalar',
                 'description': None,
                 'field_direction': 'OUT',
                 'max_value': None,
                 'min_value': None,
                 'name': '_time',
                 'primitive_data_type': 'INT',
                 'property_field_boolean_meaning': None,
                 'property_field_enums': [],
                 'unit': None,
                 'unit_exponent': None})

        pfield_cycle = PropertyFesaWidget.dotdict({'array_dim1': 256,
                       'array_dim2': None,
                       'data_type': 'scalar',
                       'description': None,
                       'field_direction': 'OUT',
                       'max_value': None,
                       'min_value': None,
                       'name': '_cycle',
                       'primitive_data_type': 'STRING',
                       'property_field_boolean_meaning': None,
                       'property_field_enums': [],
                       'unit': None,
                       'unit_exponent': None})

        add_extra = True

        for propertyField in self.property_fields:
            if propertyField.name == "_time" or propertyField.name == "_cycle":
                add_extra = False
                break

        if add_extra:
            self.property_fields.append(pfield_time)
            self.property_fields.append(pfield_cycle)

        for propertyField in self.property_fields:

            if self._max_row != 0:
                if row == self._max_row:
                    col = col + 3
                    row = 0

            if name_fields is not None:
                if propertyField.name not in name_fields:
                    continue

            # if propertyField.name == "acqStamp" or propertyField.name == "cycleName" or propertyField.name == "cycleStamp" or propertyField.name == "updateFlags":
            #     continue

            f_expression = None
            if self._function_expression is not None:
                try:
                    f_expression = self._function_expression[propertyField['name']]
                except:
                    f_expression = None

            # SINGLE

            if propertyField.data_type != "array2D" and propertyField.data_type != "array" and propertyField.data_type != "custom_type_array" and \
                    propertyField.data_type != "enum_array" and propertyField.array_dim1 is None and propertyField.data_type != "bit_enum_array":

                # SCALAR

                if propertyField.data_type == "scalar":

                    # BOOL

                    if propertyField.primitive_data_type == "BOOL":
                        bool_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'history': history,
                            "color": strcolor,
                            "palette_line": palette_line
                        }

                        if display == "split":
                            bool_opts['name'] = False

                        self.bool_widget = boolpanelwidget.BoolPanelWidget(self, **bool_opts)
                        self.bool_widget._label_name.setMinimumWidth(size_str_max)
                        self.layout.addWidget(self.bool_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.bool_widget

                    # OTHER

                    else:
                        type_format = "str"
                        if type_fields is not None:
                            if propertyField.name in type_fields:
                                type_format = type_fields[propertyField.name]
                        if f_expression is not None and f_expression != "" and f_expression != "x":
                            type_format = "fun"
                        number_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'history': history,
                            "type_format": type_format,
                            "function_expression": f_expression,
                            "color": strcolor,
                            "palette_line": palette_line
                        }

                        if display == "split":
                            number_opts['name'] = False

                        self.number_widget = numberpanelwidget.NumberPanelWidget(self, **number_opts)
                        self.number_widget._label_name.setMinimumWidth(size_str_max)
                        self.number_widget.update_function_signal.connect(self.clear_data)
                        self.layout.addWidget(self.number_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.number_widget

                # ENUM

                elif propertyField.data_type == "enum":
                    le = len(propertyField.property_field_enums)
                    labels = {}
                    for pfe in propertyField.property_field_enums:
                        labels[str(pfe.value)] = str(pfe.enum_symbol)
                    enum_opts = {
                        'title': None,
                        'label_name': propertyField.name,
                        'labels': labels,
                        'edit': edit,
                        'history': history,
                        "color": strcolor,
                        "palette_line": palette_line
                    }

                    if display == "split":
                        enum_opts['name'] = False

                    self.enum_widget = enumpanelwidget.EnumPanelWidget(self, **enum_opts)
                    self.enum_widget._label_name.setMinimumWidth(size_str_max)
                    self.layout.addWidget(self.enum_widget, row, col + 1)
                    if ("writable" in property_definition) and property_definition["writable"]:
                        checkbox = QCheckBox("")
                        checkbox.setChecked(True)
                        self._components_check[propertyField.name] = checkbox
                        if self._partial:
                            self.layout.addWidget(checkbox, row, col)
                    self.layout.setRowStretch(row, 0)
                    self._components[propertyField.name] = self.enum_widget

                # BITENUM

                elif propertyField.data_type == "bit_enum":
                    le = len(propertyField.property_field_enums)
                    labels = {}
                    for pfe in propertyField.property_field_enums:
                        labels[str(pfe.value)] = str(pfe.enum_symbol)
                    type_format = "str"
                    if type_fields is not None:
                        if propertyField.name in type_fields:
                            type_format = type_fields[propertyField.name]
                    bitenum_opts = {
                        'title': None,
                        'label_name': propertyField.name,
                        'labels': labels,
                        'size_bit': le,
                        'edit': edit,
                        'history': history,
                        "type_format": type_format,
                        "color": strcolor,
                        "palette_line": palette_line
                    }

                    if display == "split":
                        bitenum_opts['name'] = False

                    self.bitenum_widget = bitenumpanelwidget.BitEnumPanelWidget(self, **bitenum_opts)
                    self.bitenum_widget._label_name.setMinimumWidth(size_str_max)
                    self.layout.addWidget(self.bitenum_widget, row, col + 1)

                    if ("writable" in property_definition) and property_definition["writable"]:
                        checkbox = QCheckBox("")
                        checkbox.setChecked(True)
                        self._components_check[propertyField.name] = checkbox
                        if self._partial:
                            self.layout.addWidget(checkbox, row, col)
                    self.layout.setRowStretch(row, 0)
                    self._components[propertyField.name] = self.bitenum_widget

                # STRING (used?)

                else:
                    string_opts = {
                        'title': None,
                        'label_name': propertyField.name,
                        'edit': edit,
                        'history': history,
                        "color": strcolor,
                        "palette_line": palette_line
                    }

                    if display == "split":
                        string_opts['name'] = False

                    self.string_widget = stringpanelwidget.StringPanelWidget(self, **string_opts)
                    self.string_widget._label_name.setMinimumWidth(size_str_max)
                    self.layout.addWidget(self.string_widget, row, col + 1)
                    if ("writable" in property_definition) and property_definition["writable"]:
                        checkbox = QCheckBox("")
                        checkbox.setChecked(True)
                        self._components_check[propertyField.name] = checkbox
                        if self._partial:
                            self.layout.addWidget(checkbox, row, col)
                    self.layout.setRowStretch(row, 0)
                    self._components[propertyField.name] = self.string_widget

            # ARRAY

            else:

                # BYTE ARRAY -> STRING

                if propertyField.data_type == "scalar" and propertyField.primitive_data_type == "STRING":
                    string_opts = {
                        'title': None,
                        'label_name': propertyField.name,
                        'edit': edit,
                        'history': history,
                        "color": strcolor,
                        "palette_line": palette_line
                    }

                    if display == "split":
                        string_opts['name'] = False

                    self.string_widget = stringpanelwidget.StringPanelWidget(self, **string_opts)
                    self.string_widget._label_name.setMinimumWidth(size_str_max)
                    self.layout.addWidget(self.string_widget, row, col + 1)
                    if ("writable" in property_definition) and property_definition["writable"]:
                        checkbox = QCheckBox("")
                        checkbox.setChecked(True)
                        self._components_check[propertyField.name] = checkbox
                        if self._partial:
                            self.layout.addWidget(checkbox, row, col)
                    self.layout.setRowStretch(row, 0)
                    self._components[propertyField.name] = self.string_widget

                # ARRAY

                elif propertyField.data_type == "array" or propertyField.data_type == "custom_type_array" or propertyField.data_type == "enum_array" or propertyField.data_type == "bit_enum_array":

                    # STRING

                    if propertyField.primitive_data_type == "STRING":
                        array_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'string': True,
                            "color": color
                        }

                        if display == "split":
                            array_opts['name'] = False

                        self.string_widget = arraypanelwidget.ArrayPanelWidget(self, **array_opts)
                        self.string_widget._label_name.setMinimumWidth(size_str_max)
                        self.layout.addWidget(self.string_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.string_widget

                    # OTHER

                    else:
                        if f_expression is not None and f_expression != "" and f_expression != "x":
                            type_format = "fun"
                        array_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'history': history,
                            "function_expression": f_expression,
                            'twod': False,
                            "color": color
                        }

                        if display == "split":
                            array_opts['name'] = False

                        self.table_widget = arraypanelwidget.ArrayPanelWidget(self, **array_opts)
                        self.table_widget._label_name.setMinimumWidth(size_str_max)
                        self.table_widget.update_function_signal.connect(self.clear_data)
                        self.layout.addWidget(self.table_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.table_widget

                # ARRAY2D

                elif propertyField.data_type == "array2D":

                    # STRING

                    if propertyField.primitive_data_type == "STRING":
                        array_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'history': history,
                            'twod': True,
                            'string': True,
                            "color": color
                        }

                        if display == "split":
                            array_opts['name'] = False

                        self.string_widget = stringpanelwidget.ArrayPanelWidget(self, **array_opts)
                        self.string_widget._label_name.setMinimumWidth(size_str_max)
                        self.layout.addWidget(self.string_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.string_widget

                    # OTHER

                    else:
                        if f_expression is not None and f_expression != "" and f_expression != "x":
                            type_format = "fun"
                        array_opts = {
                            'title': None,
                            'label_name': propertyField.name,
                            'edit': edit,
                            'history': False,
                            "function_expression": f_expression,
                            'twod': True,
                            "color": color
                        }

                        if display == "split":
                            array_opts['name'] = False

                        self.table_widget = arraypanelwidget.ArrayPanelWidget(self, **array_opts)
                        self.table_widget._label_name.setMinimumWidth(size_str_max)
                        self.table_widget.update_function_signal.connect(self.clear_data)
                        self.layout.addWidget(self.table_widget, row, col + 1)
                        if ("writable" in property_definition) and property_definition["writable"]:
                            checkbox = QCheckBox("")
                            checkbox.setChecked(True)
                            self._components_check[propertyField.name] = checkbox
                            if self._partial:
                                self.layout.addWidget(checkbox, row, col)
                        self.layout.setRowStretch(row, 0)
                        self._components[propertyField.name] = self.table_widget

                # OTHER (used?)

                else:
                    if f_expression is not None and f_expression != "" and f_expression != "x":
                        type_format = "fun"
                    array_opts = {
                        'title': None,
                        'label_name': propertyField.name,
                        'edit': edit,
                        'history': False,
                        "function_expression": f_expression,
                        'twod': True,
                        "color": color
                    }

                    if display == "split":
                        array_opts['name'] = False

                    self.table_widget = arraypanelwidget.ArrayPanelWidget(self, **array_opts)
                    self.table_widget._label_name.setMinimumWidth(size_str_max)
                    self.table_widget.update_function_signal.connect(self.clear_data)
                    self.layout.addWidget(self.table_widget, row, col + 1)
                    if ("writable" in property_definition) and property_definition["writable"]:
                        checkbox = QCheckBox("")
                        checkbox.setChecked(True)
                        self._components_check[propertyField.name] = checkbox
                        if self._partial:
                            self.layout.addWidget(checkbox, row, col)
                    self.layout.setRowStretch(row, 0)
                    self._components[propertyField.name] = self.table_widget

            # info

            info_button = QPushButton(fa.icons["info-circle"])
            font_cell = QFont()
            font_cell.setFamily("FontAwesome")
            info_button.setFont(font_cell)
            self._components_info[propertyField.name] = info_button
            if self._info:
                self.layout.addWidget(info_button, row, col + 2)
            info_button.setMaximumWidth(20)
            info_button.setToolTip(str(propertyField))
            info_button.setStyleSheet("padding-right:5px;border:none;color:" + Colors.STR_COLOR_BLUE + ";")

            # incr

            i = i + 1
            row = row + 1

            # add last label stretch 1

        self.bottomline = QLabel("")
        self.layout.addWidget(self.bottomline, row, col)
        self.layout.setRowStretch(row, 1)

    def handle_event(self, name, value):
        """
        Handle event dispatcher.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """

        if value['_time'] - self._update_time > 50:
            if self._header is not None:
                if self._header.is_pause():
                    return
                try:
                    self._header.recv(value['_cycle'], time=value['_time'])
                except:
                    return
            for name_field in value.keys():
                try:
                    if self.name_fields is not None:
                        if name_field not in self.name_fields:
                            continue
                    if name_field == "_time":
                        self._components[name_field].set_data(value[name_field]/1000, cycle=value['_cycle'])
                    else:
                        self._components[name_field].set_data(value[name_field], cycle=value['_cycle'])
                except Exception as e:
                    print(e)
                    print("error! " + name_field)
            self._update_time = value['_time']

    def dark_(self):
        """
        Set dark theme.
        """
        self._light = False

        self._get_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        self._set_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        if not self.is_subscribing:
            self._subscribe_button.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        if self._filter:
            self._text_filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;")
        self._clear_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        for component in self._components.values():
            component.dark_()
        if self._header is not None:
            self._header.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._light = True

        self._get_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        self._set_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        if not self.is_subscribing:
            self._subscribe_button.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        if self._filter:
            self._text_filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;")
        self._clear_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        for component in self._components.values():
            component.light_()
        if self._header is not None:
            self._header.light_()

    def set_font_size(self, val=8):
        """
        Change font size.

        :param val: Font size value (default=8).
        :type val: int, optional
        """
        font = self._get_button.font()
        font.setPointSize(val)
        self._get_button.setFont(font)
        self._set_button.setFont(font)
        self._subscribe_button.setFont(font)
        self._clear_button.setFont(font)
        for component in self._components.values():
            component.set_font_size(val)


class _VirtualTiming(QWidget):
    """
    Class to pass timing info (manual) to propertyfesawidget.

    :param name_cycle: Cycle name.
    :type name_cycle: str
    """

    def __init__(self, name_cycle=""):
        """
        Initialize class.
        """
        super().__init__()
        self.name_cycle = name_cycle

    def get_cycle(self):
        """
        Return cycle name.
        """
        return self.name_cycle


class _Example(QMainWindow):
    """
    Example class to test.
    """

    def __init__(self, dark):
        super().__init__()
        self.init_ui(dark)
        self._update_time = 0

    def init_ui(self, dark):
        """
        Init user interface.
        """

        # font_size = 8
        # settings = PropertyFesaWidget(None,
        #                                         title="bsra_core settings",
        #                                         # name_device = "BSRA_TEST1",
        #                                         # name_property = "Setting_HW_Bsra_Core",
        #                                         name_device = "BR1.BWSACQ.V",
        #                                         name_property = "Setting",
        #                                         name_fields=None,
        #                                         max_row=0,
        #                                         history=True,
        #                                         header=False,
        #                                         # type_fields={"alphaConstant":"fxp131"},
        #                                         subscribe=False)

        instances = []

        # instances.append(  ["Setting_HW_Bsra_Core",
        #              "BSRA_TEST1",
        # ""])

        # instances.append(  ["WSIntegrationAcquisition",
        #              "BR1.BWSACQ.H",
        #              ""])

        # instances.append( ["Acquisition",
        #              "BISWRef2",
        #              ""])
        #
        # # instances.append(  ["Acquisition_HW_ScopeMemory",
        # #              "BSRA_TEST1",
        # #              ""])
        #
        #
        # # # # instances.append(  ["Acquisition",
        # # #              "PR.BCT",
        # # #              "CPS.USER.TOF"])
        #
        # instances.append(  ["AcquisitionTrajectoryBBB",
        #              "PR.BPM",
        #              "CPS.USER.ALL"])
        #
        # instances.append(  ["_DeviceInfo",
        #              "GD0000000000000000000000007559",
        #              ""])

        # instances.append(  ["SPSSetting",
        #              "SPS.BWS.41677.V",
        #              "SPS.USER.LHCMD2"])

        # instances.append(  ["CaptureLogging",
        #              "LHC.BWSACQ.B2H2",
        #              ""])

        # instances.append(["EnableControl",
        #                   "YMD3.FC30",
        #                   ""])

        # instances.append(["ExpertSettings",
        #                   "SPS.BCSPILLSPS.DEV",
        #                   ""])
        #
        instances.append(["StartWSIntegration",
                          "PR.BWSACQ.54H",
                          ""])
        #

        # instances.append(["SettingsArrays",
        #                   "BISWRefSB",
        #                   ""])
        #
        # instances.append(["SettingsScalars",
        #                   "BISWRefSB",
        #                   ""])
        #
        # instances.append(["Acquisition",
        #                   "BISWRefSB",
        #                   ""])
        #
        # instances.append(  ["Cmp_Setting",
        #              "PR.BCT",
        #              "CPS.USER.ZERO"])

        # instances.append( ["Acquisition",
        #              "BR1.H0HM",
        #              "PSB.USER.SFTPRO1"])

        # # instances.append( ["Acquisition",
        # #             "LHC.BCTDC24.A",
        # #              "LHC.USER.ALL"])

        # instances.append(  ["AcquisitionTrajectoryBBB",
        #              "PR.BPM",
        #              "CPS.USER.ALL"])

        # instances.append(  ["AcquireBoard",
        #              "TST.BWSLIUEXP.LIN1",
        #              ""])

        # # instances.append(  ["Setting",
        # #              "HV204L.865.CH1",
        # #              ""])

        # instances.append( ["Settings",
        #              "BISWRef1",
        #              ""])

        # instances.append( ["Acquisition",
        #              "BR1.BWS.4L1.H",
        #              "PSB.USER.SFTPRO1"])

        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)
        ind = 0

        self._tabs = QTabWidget()
        self.layout.addWidget(self._tabs, 0, 0)

        for instance in instances:
            timingpanel = _VirtualTiming(instance[2])
            # name_fields = ["anArray"]
            # type_fields = {"alphaConstant":"fxp131"}

            # get_pffc_opts = {
            #     'class_name': "BWSACQ",
            #     'property_name': "Logging",
            #     'class_version': None-
            # }
            # property_fields = ccda.get_property_fields_from_class(**get_pffc_opts)

            # print(property_fields)

            property_fesa_widget = PropertyFesaWidget(
                                                    timingpanel,
                                                      # title="Title",
                                                      name_device=instance[1],
                                                      name_property=instance[0],
                                                      name_fields={"!harmonic"},
                                                      # type_fields=type_fieldss,
                                                      max_row=20,
                                                      # history=True,
                                                      filter=True,
                                                      header=True,
                                                      # info=False,
                                                      # partial=False,
                                                      input_struct=None)  # property_fields)

            # property_fesa_widget = PropertyFesaWidget( None,
            #                                     title="bsra_core status",
            #                                     name_device = "BSRA_TEST1",
            #                                     name_property = "Acquisition_HW_Bsra_Core",
            #                                     name_fields=None,
            #                                     max_row=0,
            #                                     history=True)
            if dark:
                Colors.COLOR_LIGHT = False
                property_fesa_widget.dark_()
            else:
                Colors.COLOR_LIGHT = True
                property_fesa_widget.light_()
            #
            # property_fesa_widget.set_font_size(font_size)
            # font = self._tabs.font()
            # font.setPointSize(font_size)
            # self._tabs.tabBar().setFont(font)

            self._tabs.addTab(property_fesa_widget, instance[1] + "/" + instance[0])

            # property_fesa_widget._components["harmonic"].set_data([1,1])
            ind = ind + 1

        self.setCentralWidget(central_widget)

        # if dark:
        #     central_widget.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        # else:
        #     central_widget.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")

        # self._tabs.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        # self._tabs.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")

        self.resize(800, 600)

class tt(QMainWindow):
    def __init__(self):
        super().__init__()
        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        # self.layout.setContentsMargins(0, 0, 0, 0)
        # self.layout.setSpacing(5)
        self._label = QLabel("Test")
        self._lineedit = QLineEdit("This is a test")
        self._button = QPushButton("Subscribe")

        from PyQt5.QtWidgets import   QListView, QComboBox

        self._combobox = QComboBox()
        self._listview = QListView(self._combobox)
        self._combobox.setView(self._listview)


        self.layout.addWidget(self._label, 0, 0)
        self.layout.addWidget(self._lineedit, 0, 1)
        self.layout.addWidget(self._button, 1, 0)
        self.layout.addWidget(self._combobox, 1, 1)

        self.setCentralWidget(central_widget)
        self.resize(800, 600)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    dark = True

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    # darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)

    lpalette = QPalette()
    lpalette.setColor(QPalette.Window, Colors.COLOR_DWHITE)
    lpalette.setColor(QPalette.WindowText, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.Base, Colors.COLOR_DWHITE)
    lpalette.setColor(QPalette.AlternateBase, Colors.COLOR_DWHITE)
    lpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    lpalette.setColor(QPalette.ToolTipText, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.Text, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.Button, Colors.COLOR_LIGHT6GRAY)
    lpalette.setColor(QPalette.ButtonText, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.BrightText, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    lpalette.setColor(QPalette.HighlightedText, Colors.COLOR_BLACK)
    lpalette.setColor(QPalette.Background, Colors.COLOR_DWHITE)

    qssblack = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
        }
        QMenuBar::item:selected {    
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QMenuBar::item:pressed {
            background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }              
        QScrollArea {
            background-color: transparent;
        }
        QCheckBox {
            color: """ + Colors.STR_COLOR_WHITE + """;
        }
        QCheckBox::indicator {
            border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            background-color: """ + Colors.STR_COLOR_L3BLACK + """;            
        }    
        QCheckBox::indicator:checked {
            max-width:14;
            max-height:14;
            border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
            background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
        } 
        QLineEdit {
            background-color:""" + Colors.STR_COLOR_L2BLACK + """;
            color:""" + Colors.STR_COLOR_WHITE + """;
            text-align:left;
            padding-left:2px;
            padding-top:2px;
            padding-bottom:2px;
            border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
        }         
        QComboBox {
            border:none;
            background-color:""" + Colors.STR_COLOR_L2BLACK + """;
            selection-color:""" + Colors.STR_COLOR_WHITE + """;
            color:""" + Colors.STR_COLOR_WHITE + """;
            selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
        }                
        QListView {
            background-color:""" + Colors.STR_COLOR_L2BLACK + """;
            selection-color:""" + Colors.STR_COLOR_WHITE + """;
            color:""" + Colors.STR_COLOR_WHITE + """;
            selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
        }
    """

    qss = """
        QScrollArea {
            background-color: transparent;
        }
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
        }
        QMenuBar::item:selected {    
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QMenuBar::item:pressed {
            background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QCheckBox::indicator {
            border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;
            background-color: """ + Colors.STR_COLOR_WHITE + """;            
        }       
        QCheckBox::indicator:checked {
            max-width:14;
            max-height:14;
            border:0px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
            background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
        }
        QCheckBox {            
            color: """ + Colors.STR_COLOR_BLACK + """;                        
        }
        QLineEdit {
            color:""" + Colors.STR_COLOR_BLACK + """;
            text-align:left;
            padding-left:2px;
            padding-top:2px;
            padding-bottom:2px;
            border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            background-color:""" + Colors.STR_COLOR_WHITE + """;
        }
        QComboBox {
            border:0px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
            background-color:""" + Colors.STR_COLOR_WHITE + """;
            selection-color:""" + Colors.STR_COLOR_BLACK + """;
            color:""" + Colors.STR_COLOR_BLACK + """;
            selection-background-color:""" + Colors.STR_COLOR_DWHITE + """;
        }
        QListView {
            background-color:""" + Colors.STR_COLOR_DWHITE + """;
            selection-color:""" + Colors.STR_COLOR_WHITE + """;
            color:""" + Colors.STR_COLOR_BLACK + """;
            selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
        }
    """

    if dark:
        pass
        app.setPalette(darkpalette)
        app.setStyleSheet(qssblack)
    else:
        app.setPalette(lpalette)
        app.setStyleSheet(qss)

    ex = _Example(dark)
    ex.show()

    sys.exit(app.exec_())