"""
This file contains a set of PyQt5 widgets and associated tools designed to interact with
FESA (Front-End Software Architecture) devices at CERN.

The primary classes include:

- `DevicesFesaModel`: A data model class that fetches valid FESA class devices and their associated information.
- `DevicesFesaWidget`: A widget for displaying FESA devices with names, colors, and texts in a user-friendly interface.
- `DeviceClassPanelWidget`: A panel widget that displays devices in QButtonWidgets.

These tools are built to facilitate the creation of expert-level graphical user interfaces (GUIs) that manage and
monitor FESA devices. The classes provide methods to filter, sort, and interact with devices based on their class,
FEC (Front-End Controller), and other criteria.

Usage:

1. `DevicesFesaModel`:
    - Initialize with a list of class names to retrieve FESA devices and their information.
    - Example: `model = DevicesFesaModel(class_names=['Class1', 'Class2'])`

2. `DevicesFesaWidget`:
    - This widget can be embedded in your PyQt5 application to display and interact with FESA devices.
    - Example: `widget = DevicesFesaWidget(parent=self, class_names=['Class1', 'Class2'], title='Device Overview')`

3. `DeviceClassPanelWidget`:
    - Use this panel widget within the `DevicesFesaWidget` to manage devices within a specific class.
    - Example: `DeviceClassPanelWidget(parent=widget, class_names=['Class1'])`
"""

import sys
import logging

from typing import List, Dict, Type, TypeVar
from functools import partial
from enum import Enum
from collections import defaultdict

from PyQt5.QtCore import pyqtSignal,Qt,pyqtSlot,QEvent,QObject
from PyQt5.QtGui import QPalette,QColor
from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QGridLayout, QHBoxLayout, \
    QMainWindow,QTabWidget, QScrollArea,QLabel,QPushButton,QLineEdit

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.treewidget import TreeWidget
from expert_gui_core.comm.devicequery import DeviceQuery, DeviceQueryFilter, GroupingType

DeviceButton = TypeVar("DeviceButton", bound=QPushButton)
PanelWidget = TypeVar("DeviceClassPanelWidget", bound=QWidget)


class DevicesFesaModel:
    """
    Data Model Class that fetches valid FESA class devices and their FESA information.

    :param query: A `DeviceQuery` object that interacts with the CCDA API to fetch Device objects.
    :type query: :class:`expert_gui_core.comm.devicequery.DeviceQuery`

    :raises ValueError: If the `query` object's `group_by` attribute is not set to `GroupingType.CLASSES`.

    Attributes:
        query (DeviceQuery): The `DeviceQuery` object used to retrieve device data.
        devices (dict): A nested dictionary mapping class names to device information.
            The structure is: {<Class Name>: {<ACC>: {<FEC Name>: [<Device Name 1>, <Device Name 2>, ...]}}}
        name_devices (list): A list of all device names retrieved from the query.
    """

    def __init__(self, query: DeviceQuery):
        self.logger = logging.getLogger(self.__class__.__name__)

        if query.__getattribute__('_group_by') != GroupingType.CLASSES:
            raise ValueError('The DeviceQuery group_by attribute must be set to GroupingType.CLASSES for it to be '
                             'used in this widget.')
        self._query = query

        self.devices = {}
        self.name_devices = []

        result = self._query.dispatch()
        self.devices = self.remove_labels(result)
        self.name_devices = self._get_device_names(self.devices)

    @staticmethod
    def remove_labels(data: Dict) -> Dict[str, Dict[str, Dict[str, List[str]]]]:
        """
        Removes the 'CLASSES', 'ACCELERATORS', 'FECS' and 'DEVICES' labels from the raw device data.

        The resulting structure is: {<Class Name>: {<ACC>: {<FEC Name>: [<Device Name 1>, <Device Name 2>, ...]}}}

        :param data: Raw data from the `DeviceQuery` object.
        :type data: dict
        :return: A dictionary mapping class names to their respective devices.
        :rtype: dict
        """
        transformed_data = {}

        for class_name, class_data in data.get('CLASSES', {}).items():
            new_class_data = {}
            for accelerator_name, accelerator_data in class_data.get('ACCELERATORS', {}).items():
                new_accelerator_data = {}
                for fecs_key, fecs_data in accelerator_data.get('FECS', {}).items():
                    devices_data = fecs_data.get('DEVICES', {})
                    new_fecs_data = {device_key: devices_data[device_key] for device_key in devices_data.keys()}
                    new_accelerator_data[fecs_key] = new_fecs_data
                new_class_data[accelerator_name] = new_accelerator_data
            transformed_data[class_name] = new_class_data

        return transformed_data

    @staticmethod
    def _get_device_names(data: Dict) -> List[str]:
        """
        Extracts all device names from the transformed data.

        :param data: The transformed device data.
        :type data: dict
        :return: A list of all device names.
        :rtype: list
        """
        result = []
        for accs_sub_dict in data.values():
            for fecs_sub_dict in accs_sub_dict.values():
                for devices_dict in fecs_sub_dict.values():
                    result.extend(list(devices_dict.keys()))
        return result


class TabGrouping(Enum):
    """Enumeration for grouping options in tabs."""
    CLASS = 'class'
    ACCELERATOR = 'accelerator'
    FEC = 'fec'


class Layout(Enum):
    """Enumeration for layout options."""
    GRID = "grid"
    TREE = "tree"


class DevicesFesaWidget(QWidget):
    """
    A widget for displaying FESA (Front-End Software Architecture) devices based on a specified query.

    This widget supports displaying FESA devices in either a grid or tree layout, with options for
    grouping devices by FEC (Front-End Controller) or class name. It includes filtering capabilities
    and allows customization of the buttons representing the devices.

    :param parent: The parent widget.
    :type parent: QObject
    :param layout: The layout type, either grid or tree.
    :type layout: Layout
    :param title: Optional title for the widget.
    :type title: str, optional
    :param max_col: Maximum number of columns in the grid layout.
    :type max_col: int
    :param tab_grouping: Option for grouping devices in tabs.
    :type tab_grouping: TabGrouping, optional
    :param group_by_fec: Flag to group devices by FEC.
    :type group_by_fec: bool
    :param widget: The widget type for displaying devices.
    :type widget: Type[DeviceButton], optional
    :param class_names: List of class names to query for FESA devices.
    :type class_names: List[str], optional
    :param fesa_query: Query object for fetching FESA devices.
    :type fesa_query: DeviceQuery, optional

    :attr filter_changed: Signal emitted when the filter text changes.
    :type filter_changed: pyqtSignal
    """

    filter_changed = pyqtSignal()

    def __init__(self,
                 parent: QObject,
                 layout: Layout = Layout.GRID,
                 title: str = None,
                 max_col: int = 5,
                 tab_grouping: TabGrouping = None,
                 group_by_fec: bool = False,
                 widget: Type[DeviceButton] = None,
                 class_names: List[str] = None,
                 fesa_query: DeviceQuery = None):
        super().__init__(parent)

        self._tree_widget = None
        self.scroll_area = None
        self.scroll_area_widget_contents = None
        self.grid_layout = None
        self.label_title = None
        self.panel_filter = None
        self.layout_filter = None
        self.text_filter = None
        self.tabs = None

        self._show_tree = True if layout == Layout.TREE else False

        if class_names and fesa_query:
            raise ValueError("You cannot pass both class_names and fesa_query as arguments. Choose one.")
        elif class_names:
            fesa_query = DeviceQuery(classes=class_names, group_by=GroupingType.CLASSES)
        self.device_fesa_model = DevicesFesaModel(query=fesa_query)

        self._device_buttons = {}
        self.device_button_widget = widget
        self._devices = self.device_fesa_model.devices if self.device_fesa_model.devices else None
        self._panel_class_devices = defaultdict(list)
        self._panels = []

        # Initialize UI components
        self._init_ui(title, tab_grouping, max_col, group_by_fec)

    def _init_ui(self, title: str, tab_grouping: TabGrouping, max_col: int, group_by_fec: bool):
        self._init_layout()
        self._add_title(title)
        if self._devices:
            if self._show_tree:
                self._tree_widget = TreeWidget(data=self._devices, header_hidden=True, include_filter=False)
                self.grid_layout.addWidget(self._tree_widget, 2, 0)

            else:
                self._add_devices(tab_grouping, max_col, group_by_fec)
        self._add_filter()

    def _init_layout(self):
        """Initialize the main layout and scroll area."""
        self.layout = QHBoxLayout(self)
        self.scroll_area = QScrollArea(self)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area_widget_contents = QWidget()
        self.grid_layout = QGridLayout(self.scroll_area_widget_contents)
        self.scroll_area.setWidget(self.scroll_area_widget_contents)
        self.layout.addWidget(self.scroll_area)

    def _add_title(self, title: str):
        """
        Initialize the title label.

        :param title: The title text.
        :type title: str
        """
        if title:
            self.label_title = QLabel(title)
            self.label_title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_title.setAlignment(Qt.AlignCenter)
            self.grid_layout.addWidget(self.label_title, 0, 0)
            self.grid_layout.setRowStretch(0, 0)

    def _add_filter(self):
        """Initialize the filter components."""
        self.panel_filter = QWidget()
        self.layout_filter = QGridLayout(self.panel_filter)
        self.layout_filter.setContentsMargins(10, 5, 0, 0)
        self.layout_filter.setSpacing(5)
        self.layout_filter.addWidget(QLabel("Filter: "), 0, 2, alignment=Qt.AlignRight)
        self.text_filter = QLineEdit("")
        self.text_filter.installEventFilter(self)
        self.filter_changed.connect(self._filter_fields)
        self.layout_filter.addWidget(self.text_filter, 0, 3, alignment=Qt.AlignLeft)
        self.layout_filter.addWidget(QLabel(" "), 0, 1)
        self.layout_filter.setColumnStretch(1, 1)
        self.grid_layout.addWidget(self.panel_filter, 1, 0)
        self.grid_layout.setRowStretch(1, 0)

    def _add_devices(self, use_tab_grouping: TabGrouping, max_col: int, group_by_fec: bool):
        """
        Initialize the device panels or tabs.

        :param use_tab_grouping: Option for grouping devices in tabs.
        :type use_tab_grouping: TabGrouping
        :param max_col: Maximum number of columns in the grid layout.
        :type max_col: int
        :param group_by_fec: Flag to group devices by FEC.
        :type group_by_fec: bool
        """
        ind = 2
        if use_tab_grouping:
            self.tabs = QTabWidget()
            self.tabs.setTabsClosable(False)
            self.grid_layout.addWidget(self.tabs, ind, 0)
            self.grid_layout.setRowStretch(ind, 1)

        if use_tab_grouping == TabGrouping.CLASS:
            self._group_by_class(max_col, group_by_fec)
        elif use_tab_grouping == TabGrouping.ACCELERATOR:
            self._group_by_accelerator(max_col, group_by_fec)
        elif use_tab_grouping == TabGrouping.FEC:
            self._group_by_fec(max_col, group_by_fec)
        else:
            self._no_grouping(max_col, group_by_fec)

        self._panels = [panel for panels in self._panel_class_devices.values() for panel in panels]
        for panel in self._panels:
            self._device_buttons.update(panel.get_device_buttons(mapped=True))

    def _no_grouping(self, max_col: int, group_by_fec: bool):
        """
        Add all devices into a single panel without any grouping (no tabs).

        :param max_col: Maximum number of columns in the grid layout.
        :type max_col: int
        :param group_by_fec: Flag to group devices by FEC.
        :type group_by_fec: bool
        """
        flattened_fecs = {}

        for class_device, accelerators in self._devices.items():
            for accelerator, fecs in accelerators.items():
                for fec_name, devices in fecs.items():
                    if fec_name not in flattened_fecs:
                        flattened_fecs[fec_name] = []
                    flattened_fecs[fec_name].extend(devices.keys())

        panel = DeviceClassPanelWidget(device_widget=self.device_button_widget,
                                       fecs=flattened_fecs, max_col=max_col,
                                       group_by_fec=group_by_fec, show_title=True)
        for class_device in self._devices.keys():
            self._panel_class_devices[class_device].append(panel)

        self.grid_layout.addWidget(panel, 2, 0)
        self.grid_layout.setRowStretch(2, 1)

    def _group_by_class(self, max_col: int, group_by_fec: bool):
        """
        Group devices by class name.

        :param max_col: Maximum number of columns in the grid layout.
        :type max_col: int
        :param group_by_fec: Flag to group devices by FEC.
        :type group_by_fec: bool
        """
        # Flatten the FECs and devices for this class across all accelerators
        for class_device, accelerators in self._devices.items():
            flattened_fecs = {}
            for accelerator, fecs in accelerators.items():
                for fec_name, devices in fecs.items():
                    if fec_name not in flattened_fecs:
                        flattened_fecs[fec_name] = []
                    flattened_fecs[fec_name].extend(devices.keys())

            panel = DeviceClassPanelWidget(device_widget=self.device_button_widget,
                                           class_name=class_device,
                                           fecs=flattened_fecs, max_col=max_col,
                                           group_by_fec=group_by_fec, show_title=False)
            self._panel_class_devices[class_device].append(panel)
            self.tabs.addTab(panel, class_device)

    def _group_by_accelerator(self, max_col: int, group_by_fec: bool):
        """
        Group devices by accelerator name.

        :param max_col: Maximum number of columns in the grid layout.
        :type max_col: int
        :param group_by_fec: Flag to group devices by FEC.
        :type group_by_fec: bool
        """
        accelerator_data = defaultdict(lambda: defaultdict(list))

        # Flatten the data by grouping FECs and devices under each accelerator
        for class_device, accelerators in self._devices.items():
            for accelerator, fecs in accelerators.items():
                for fec_name, devices in fecs.items():
                    accelerator_data[accelerator][fec_name].extend(devices.keys())
        panel = None
        for accelerator, fecs in accelerator_data.items():
            for class_name in self._devices.keys():
                if accelerator in self._devices[class_name]:
                    panel = DeviceClassPanelWidget(device_widget=self.device_button_widget,
                                                   class_name=class_name,
                                                   fecs=fecs, max_col=max_col,
                                                   group_by_fec=group_by_fec, show_title=False)
                    self._panel_class_devices[class_name].append(panel)
            self.tabs.addTab(panel, accelerator)

    def _group_by_fec(self, max_col: int, group_by_fec: bool):
        """
        Group devices by FEC name.

        :param max_col: Maximum number of columns in the grid layout.
        :type max_col: int
        :param group_by_fec: Flag to group devices by FEC.
        :type group_by_fec: bool
        """
        fec_data = defaultdict(list)

        # Flatten the data by grouping devices under each FEC
        for class_device, accelerators in self._devices.items():
            for accelerator, fecs in accelerators.items():
                for fec_name, devices in fecs.items():
                    fec_data[fec_name].extend(devices.keys())
        panel = None
        for fec_name, devices in fec_data.items():
            for class_device, accelerators in self._devices.items():
                for accelerator, fecs in accelerators.items():
                    if fec_name in fecs:
                        panel = DeviceClassPanelWidget(device_widget=self.device_button_widget,
                                                       class_name=class_device,
                                                       fecs={fec_name: devices}, max_col=max_col,
                                                       group_by_fec=group_by_fec, fec_tab=True, show_title=False)
                        self._panel_class_devices[class_device].append(panel)

            self.tabs.addTab(panel, fec_name)

    def eventFilter(self, widget: QObject, event: QEvent) -> bool:
        """
        Handle keyboard events for the filter.

        :param widget: The widget being filtered.
        :type widget: QObject
        :param event: The event being processed.
        :type event: QEvent
        :return: Whether the event was handled.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self.text_filter and \
                event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    @pyqtSlot()
    def _filter_fields(self):
        """Filter visible field panels using their names."""
        filter_text = self.text_filter.text()
        if self._show_tree:
            self._tree_widget.filter_fields(filter_text)
        for panel in self._panels:
            panel.filter_fields(filter_text)

    def get_device_class_panel_widget(self, class_name: str) -> List[PanelWidget]:
        """
        Get list of DeviceClassPanelWidget objects.

        :param class_name: The class name for which to retrieve panels.
        :type class_name: str
        :return: List of panel widgets.
        :rtype: List[PanelWidget]
        """
        return self._panel_class_devices.get(class_name)

    def get_devices(self) -> List:
        """
        Get the list of devices.

        :return: List of devices.
        :rtype: List
        """
        return self.device_fesa_model.name_devices

    def get_device_buttons(self, class_name: str) -> List[DeviceButton]:
        """
        Get the list of device buttons.

        :param class_name: The class name for which to retrieve device buttons.
        :type class_name: str
        :return: List of device buttons.
        :rtype: List[DeviceButton]
        """
        if not self._panel_class_devices or self._show_tree:
            return []
        btn_list = [panel.get_device_buttons() for panel in self._panel_class_devices[class_name]]
        flat_btn_list = [btn for btns in btn_list for btn in btns]
        return flat_btn_list

    def get_device_button(self, class_name: str, device: str) -> QWidget | None:
        """
        Get a specific device button.

        :param class_name: The class name of the device.
        :type class_name: str
        :param device: The name of the specific device.
        :type device: str
        :return: The device button widget, or None if not found.
        :rtype: QWidget | None
        """
        if not self._panel_class_devices or self._show_tree:
            return None
        for panel in self._panel_class_devices[class_name]:
            btn = panel.get_device_button(device)
            if btn:
                return btn
        return None

    def set_title(self, device: str, title: str):
        """
        Set the title in a device button.

        :param device: The name of the device.
        :type device: str
        :param title: The title to set.
        :type title: str
        """
        if not self._show_tree and device in self._device_buttons:
            self._device_buttons[device].set_title(title)

    def get_text(self, device: str) -> str:
        """
        Get the text in a device button.

        :param device: The name of the device.
        :type device: str
        :return: The text in the device button.
        :rtype: str
        """
        if not self._panel_class_devices:
            return ""
        return self._device_buttons.get(device).get_text() if device in self._device_buttons else None

    def set_text(self, device: str, text: str):
        """
        Set the text in a device button.

        :param device: The name of the device.
        :type device: str
        :param text: The text to set.
        :type text: str
        """
        if not self._show_tree and device in self._device_buttons:
            self._device_buttons[device].set_text(text)

    def set_tooltip(self, device: str, text: str):
        """
        Set the tooltip in a device button.

        :param device: The name of the device.
        :type device: str
        :param text: The tooltip text.
        :type text: str
        """
        if not self._show_tree and device in self._device_buttons:
            self._device_buttons[device].setToolTip(text)

    def set_color(self, device: str, color: str):
        """
        Set the background color in a device button.

        :param device: The name of the device.
        :type device: str
        :param color: The color to set.
        :type color: str
        """
        if not self._show_tree and device in self._device_buttons:
            self._device_buttons[device].set_color(color)

    def set_text_color(self, device: str, color: str):
        """
        Set the text color in a device button.

        :param device: The name of the device.
        :type device: str
        :param color: The text color to set.
        :type color: str
        """
        if not self._show_tree and device in self._device_buttons:
            self._device_buttons[device].set_text_color(color)

    def add_signal(self, signal: pyqtSignal):
        """
        Add a custom signal to the widget.

        :param signal: The signal to add.
        :type signal: pyqtSignal
        """
        if not self._show_tree:
            for panel in self._panels:
                panel.add_signal(signal)
        else:
            self._tree_widget.add_signal(signal)

    def dark_(self):
        """Set the widget to dark theme."""
        if self.text_filter:
            self.text_filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align"
                                                                                                      ":right;border"
                                                                                                      ":1px solid " +
                Colors.STR_COLOR_LIGHT1GRAY + ";")

        if self.label_title:
            self.label_title.setStyleSheet("color: blue; font-weight: bold;")
        for panel in self._panels:
            panel.dark_()

    def light_(self):
        """Set the widget to light theme."""
        if self.text_filter:
            self.text_filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align"
                                                                                                     ":right;border"
                                                                                                     ":1px solid " +
                Colors.STR_COLOR_LIGHT4GRAY + ";")

        if self.label_title:
            self.label_title.setStyleSheet("color: blue; font-weight: bold;")
        for panel in self._panels:
            panel.light_()


class DeviceClassPanelWidget(QWidget):
    """
    A panel widget for displaying all devices associated with a specific FESA class.

    This widget displays devices either in a grid layout or grouped by FEC. It also supports optional
    customization of the device buttons and can show the class name as a title.

    :param device_widget: Optional widget to display device information.
    :type device_widget: QWidget, optional
    :param show_title: Flag indicating whether to show the class name as a title (default=True).
    :type show_title: bool
    :param class_name: The name of the device class.
    :type class_name: str
    :param fecs: Dictionary mapping FEC names to their associated devices.
    :type fecs: Dict[str, List[str]], optional
    :param max_col: Maximum number of columns in the grid layout (default=5).
    :type max_col: int
    :param group_by_fec: If True, devices belonging to the same FEC are grouped together in rows.
    :type group_by_fec: bool
    :param fec_tab: If True, the name of the FEC will be shown in the parent's widget tab.
    :type fec_tab: bool
    """

    def __init__(self,
                 device_widget: QWidget = None,
                 show_title: bool = True,
                 class_name: str = "",
                 fecs: Dict[str, List[str]] = None,
                 max_col: int = 5,
                 group_by_fec: bool = False,
                 fec_tab: bool = False):
        super().__init__()

        self._signal = None
        self._label_title = show_title
        self._max_col = max_col
        self._group_by_fec = group_by_fec
        self._fec_tab = fec_tab
        self._fecs = fecs if fecs else {}
        self._name_devices = [device for devices in self._fecs.values() for device in devices]
        self._class_name = class_name

        self._device_buttons = {}
        self._device_buttons_show = {}
        self._label_fecs = {}

        # Adjust max columns based on the number of devices
        self._max_col = min(self._max_col, len(self._name_devices))
        self._init_layout()

        if self._label_title:
            self._init_title_label()
        self._init_device_buttons(device_widget)

    def _init_layout(self):
        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)

    def _init_title_label(self):
        title_text = self._class_name
        self._label_title = QLabel(title_text)
        self._label_title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.gridLayout.addWidget(self._label_title, 0, 0)
        self.gridLayout.setRowStretch(0, 0)

    def _init_device_buttons(self, device_widget: Type[DeviceButton] | None):
        """
        Initialize device buttons based on FECs and their devices.

        :param device_widget: The widget type for displaying devices.
        :type device_widget: Type[DeviceButton], optional
        """
        row, col = 2, 0
        for fec, devices in self._fecs.items():
            if self._group_by_fec:
                col = 0
                row += 1
                self._label_fecs[fec] = QLabel(fec)
                self.gridLayout.addWidget(self._label_fecs[fec], row, 0)
                row += 1

            for device in devices:
                kwargs = {'title': device} if self._group_by_fec or self._fec_tab else {'title': fec,
                                                                                        'subtitle': device}
                device_button = device_widget(**kwargs) if device_widget else DeviceButtonWidget(**kwargs)
                self._device_buttons[device] = device_button
                self._device_buttons_show[device] = True
                device_button.mousePressEvent = partial(self.press, device)

                if col >= self._max_col:
                    col = 0
                    row += 1

                self.gridLayout.addWidget(device_button, row, col)
                self.gridLayout.setColumnStretch(col, 1)
                self.gridLayout.setRowStretch(row, 1)

                col += 1

        self.gridLayout.setRowStretch(row + 1, 1)

    def update_panel_devices(self):
        """Redraw the device panel to reflect the current device visibility."""
        row = 1
        col = 0
        for label in self._label_fecs.values():
            self.gridLayout.removeWidget(label)

        for fec, devices in self._fecs.items():
            col = 0 if self._group_by_fec else col
            should_show_fec_label = any(self._device_buttons_show[device] for device in devices) and self._group_by_fec

            if self._group_by_fec and should_show_fec_label:
                row += 1
                self.gridLayout.addWidget(self._label_fecs[fec], row, 0)
                row += 1

            for device in devices:
                self.gridLayout.removeWidget(self._device_buttons[device])
                if self._device_buttons_show[device]:
                    if col >= self._max_col:
                        col = 0
                        row += 1
                    self.gridLayout.addWidget(self._device_buttons[device], row, col)
                    self.gridLayout.setColumnStretch(col, 1)
                    self.gridLayout.setRowStretch(row, 1)
                    col += 1

        self.gridLayout.setRowStretch(row + 1, 1)

    def filter_fields(self, text: str):
        """
        Filter visible field panels using their names.

        :param text: Filter string.
        :type text: str
        """
        for device in self._name_devices:
            self._device_buttons[device].hide()
            self._device_buttons_show[device] = False

        device_matches = [d for d in self._name_devices if text in d]

        # List of devices whose FEC matches the search term
        fecs_matches = [item for sublist in (self._fecs[f] for f in self._fecs if text in f) for item in sublist]

        all_matches = device_matches + fecs_matches
        for device in all_matches:
            self._device_buttons[device].show()
            self._device_buttons_show[device] = True

        if self._group_by_fec:
            for fec, _ in self._fecs.items():
                self._label_fecs[fec].hide()

            for fec, devices in self._fecs.items():
                should_show_fec_label = any(self._device_buttons_show[device] for device in devices)
                if should_show_fec_label:
                    self._label_fecs[fec].show()

        self.update_panel_devices()

    def get_devices(self) -> List[str]:
        """
        Get the list of devices.

        :return: List of device names.
        :rtype: List[str]
        """
        return list(self._device_buttons.keys())

    def get_devices_visibility(self) -> Dict[str, bool]:
        """
        Get a dictionary representing the visibility status of each device.

        :return: Dictionary mapping device names to their visibility status.
        :rtype: Dict[str, bool]
        """
        return self._device_buttons_show

    def get_device_buttons(self, mapped: bool = False) -> List[DeviceButton] | Dict[str, DeviceButton]:
        """
        Get collection of DeviceButtonWidgets. Returns dictionary of {'Device Name': <DeviceButton>} if mapped is True,
        Otherwise, returns list of all DeviceButton instances.

        :param mapped: If True, returns a dictionary mapping device names to their buttons.
        :type mapped: bool
        :return: List or dictionary of device buttons.
        :rtype: List[DeviceButton] | Dict[str, DeviceButton]
        """
        return self._device_buttons if mapped else list(self._device_buttons.values())

    def get_device_button(self, device: str) -> QWidget | None:
        """
        Get a specific device button.

        :param device: The name of the device.
        :type device: str
        :return: The device button widget, or None if not found.
        :rtype: QWidget | None
        """
        return self._device_buttons[device] if device in self._device_buttons else None

    def set_title(self, device: str, title: str):
        """
        Change the title in a device button.

        :param device: The name of the device.
        :type device: str
        :param title: The title to be displayed on the device button.
        :type title: str
        """
        self._device_buttons[device].set_title(title)

    def set_text(self, device: str, text: str):
        """
        Change the text in a device button.

        :param device: The name of the device.
        :type device: str
        :param text: The text to be displayed on the device button.
        :type text: str
        """
        self._device_buttons[device].set_text(text)

    def get_text(self, device: str) -> str:
        """
        Get the text from a device button.

        :param device: The name of the device.
        :type device: str
        :return: The text displayed on the device button.
        :rtype: str
        """
        return self._device_buttons[device].get_text_label().text()

    def set_subtitle_color(self, device: str, color: str):
        """
        Change the subtitle color of a device button.

        :param device: The name of the device.
        :type device: str
        :param color: The background color to be applied to the device button.
        :type color: QColor
        """
        if device in self._device_buttons:
            self._device_buttons[device].set_subtitle_color(color)

    def set_color(self, device: str, color: str):
        """
        Change the background color of a device button.

        :param device: The name of the device.
        :type device: str
        :param color: The background color to be applied to the device button.
        :type color: QColor
        """
        if device in self._device_buttons:
            self._device_buttons[device].set_color(color)

    def add_signal(self, signal: pyqtSignal):
        """
        Add a PyQt signal to be triggered when a device button is pressed.

        :param signal: The signal to be added/connected.
        :type signal: pyqtSignal
        """
        self._signal = signal

    def press(self, device: str, e: QObject):
        """
        Handle the event when a device button is pressed.

        :param device: The name of the device.
        :type device: str
        :param e: The event object.
        :type e: QObject
        """
        if self._signal and e:
            self._signal.emit(device)

    def dark_(self):
        """Set the widget to a dark theme."""
        if self._label_title:
            self._label_title.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;")
        for label in self._label_fecs.values():
            label.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:italic;")

    def light_(self):
        """Set the widget to a light theme."""
        if self._label_title:
            self._label_title.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;")
        for label in self._label_fecs.values():
            label.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:italic;")


class DeviceButtonWidget(QPushButton):
    """
    A custom QPushButton for displaying device information with optional title, subtitle, text, and color.

    :param title: The title to be shown on the button.
    :type title: str, optional
    :param subtitle: The subtitle to be shown on the button.
    :type subtitle: str, optional
    :param text: The text to be shown below the subtitle.
    :type text: str, optional
    :param color: The background color of the button.
    :type color: str, optional
    """

    update_signal = pyqtSignal()

    def __init__(self, title: str = "", subtitle: str = "", text: str = "", color: str = None):
        super().__init__()
        self._subtitle_color = None
        self._title_color = None
        self._text_color = None
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self._title = title
        self._subtitle = subtitle
        self._text = text
        self._color = color

        self._title_label = self._create_label(self._title, bold=True)
        self._subtitle_label = self._create_label(self._subtitle)
        self._text_label = self._create_label(self._text)

        self._init_ui()
        self.update_signal.connect(self.update_button)
        self.update_signal.emit()

    @staticmethod
    def _create_label(text: str, bold: bool = False) -> QLabel:
        """
        Create a QLabel with optional bold styling.

        :param text: The text to display in the label.
        :type text: str
        :param bold: Whether the label text should be bold.
        :type bold: bool, optional
        :return: The created QLabel.
        :rtype: QLabel
        """
        label = QLabel(text)
        label.setTextFormat(Qt.RichText)
        label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        label.setAttribute(Qt.WA_TranslucentBackground)
        label.setAttribute(Qt.WA_TransparentForMouseEvents)
        label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        if bold:
            label.setStyleSheet("font-weight:bold;")
        return label

    def _init_ui(self):
        self.setFixedHeight(80)

        layout = QGridLayout()
        layout.setContentsMargins(2, 2, 2, 2)
        layout.setSpacing(0)
        self.setLayout(layout)

        if not self._subtitle_label.text() and not self._text_label.text():
            layout.addWidget(self._subtitle_label, 0, 0)
            layout.addWidget(self._title_label, 1, 0)
            layout.addWidget(self._text_label, 2, 0)
        else:
            layout.addWidget(self._title_label, 0, 0)
            layout.addWidget(self._subtitle_label, 1, 0)
            layout.addWidget(self._text_label, 2, 0)

    def set_title(self, title: str, color: str = None):
        """
        Set the title of the button and optionally change its color.

        :param title: The title to set.
        :type title: str
        :param color: The color to set for the title.
        :type color: str, optional
        """
        self._title = title
        if color is not None:
            self._title_color = color
        self.update_signal.emit()

    def set_subtitle(self, subtitle: str, color: str = None):
        """
        Set the subtitle of the button and optionally change its color.

        :param subtitle: The subtitle to set.
        :type subtitle: str
        :param color: The color to set for the subtitle.
        :type color: str, optional
        """
        self._subtitle = subtitle
        if color is not None:
            self._subtitle_color = color
        self.update_signal.emit()

    def set_text(self, text: str, color: str = None):
        """
        Set the text of the button and optionally change its color.

        :param text: The text to set.
        :type text: str
        :param color: The color to set for the text.
        :type color: str, optional
        """
        self._text = text
        if color is not None:
            self._text_color = color
        self.update_signal.emit()

    def set_text_color(self, color: str):
        if color is not None:
            self._text_color = color
            self.update_signal.emit()

    def set_subtitle_color(self, color: str):
        if color is not None:
            self._subtitle_color = color
            self.update_signal.emit()

    def set_title_color(self, color: str):
        if color is not None:
            self._title_color = color
            self.update_signal.emit()

    def set_color(self, color: str):
        """
        Set the background color of the button.

        :param color: The background color to set.
        :type color: str
        """
        if color is not None:
            self._color = color
            self.update_signal.emit()

    def get_title_label(self) -> QLabel:
        """
        Get the QLabel displaying the title.

        :return: The QLabel displaying the title.
        :rtype: QLabel
        """
        return self._title_label

    def get_subtitle_label(self) -> QLabel:
        """
        Get the QLabel displaying the subtitle.

        :return: The QLabel displaying the subtitle.
        :rtype: QLabel
        """
        return self._subtitle_label

    def get_text_label(self) -> QLabel:
        """
        Get the QLabel displaying the text.

        :return: The QLabel displaying the text.
        :rtype: QLabel
        """
        return self._text_label

    def get_layout(self) -> QGridLayout | None:
        """
        Get the layout of the button.

        :return: The layout of the button, or None if not set.
        :rtype: QGridLayout | None
        """
        return self.layout()

    @pyqtSlot()
    def update_button(self):
        """
        Update the UI elements of the button based on the current state.
        """
        self._title_label.setText(self._title)
        if self._title_color:
            color_str = self._title_color.name() if isinstance(self._title_color, QColor) else self._title_color
            self._title_label.setStyleSheet(f"font-weight:bold; color: {color_str};")
        else:
            self._title_label.setStyleSheet("font-weight:bold;")

        self._subtitle_label.setText(self._subtitle)
        if self._subtitle_color:
            color_str = self._subtitle_color.name() if isinstance(self._subtitle_color,
                                                                  QColor) else self._subtitle_color
            self._subtitle_label.setStyleSheet(f"color: {color_str};")

        self._text_label.setText(self._text)
        if self._text_color:
            color_str = self._text_color.name() if isinstance(self._text_color, QColor) else self._text_color
            self._text_label.setStyleSheet(f"color: {color_str};")

        if self._color is not None:
            color_str = self._color.name() if isinstance(self._color, QColor) else self._color
            self.setStyleSheet(f"background-color: {color_str};")

        self.updateGeometry()


class AlternativeDeviceButtonWidget(QPushButton):
    """
    Example of how to create an alternative Device Button (QPushButton)
    """

    def __init__(self, title="", subtitle="", text="", color=None):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self._title = title
        self._subtitle = subtitle
        self._text = text
        self._color = color

        self._title_label = self._create_label(self._title, bold=True)
        self._subtitle_label = self._create_label(self._subtitle)
        self._text_label = self._create_label(self._text)

        self._init_ui()

    @staticmethod
    def _create_label(text: str, bold: bool = False) -> QLabel:
        label = QLabel(text)
        label.setTextFormat(Qt.RichText)
        label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        label.setAttribute(Qt.WA_TranslucentBackground)
        label.setAttribute(Qt.WA_TransparentForMouseEvents)
        label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        if bold:
            label.setStyleSheet("font-weight:bold;")
        return label

    def _init_ui(self):
        self.setFixedHeight(200)
        self.setFixedWidth(200)

        layout = QGridLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(5)
        self.setLayout(layout)

        layout.addWidget(self._title_label, 0, 0, 1, 2)
        layout.addWidget(self._subtitle_label, 1, 0, 1, 2)
        layout.addWidget(self._text_label, 2, 0, 1, 2)

        # Make the button circular
        self.setStyleSheet("border-radius: 100; border: 1px solid black")


class BaseExample(QMainWindow):
    """
    Base class for demonstrating different configurations of DevicesFesaWidget.
    """
    device_selected_signal = pyqtSignal(str)

    def __init__(self, example_title="Devices Fesa Widget Example"):
        """
        Initialize the class and set up the user interface.
        """
        super().__init__()
        self.dfw = None
        self.layout = None

        self.setWindowTitle(example_title)
        self._init_ui()
        self.device_selected_signal.connect(self.open_device)

    def _init_ui(self):
        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)

        # Add DevicesFesaWidget with example parameters
        self.dfw = self._add_device_widget_example()

        self.dfw.add_signal(self.device_selected_signal)

        self.layout.addWidget(self.dfw)
        self.setCentralWidget(central_widget)
        self.layout.setRowStretch(0, 1)
        self.resize(1200, 1000)

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        """
        Create and add a DevicesFesaWidget instance to the layout with the given parameters.
        This should be overridden by subclasses to provide specific configuration.
        """
        raise NotImplementedError("Subclasses should implement this method.")

    def open_device(self, name: str):
        """Handle device selection."""
        print(f"Device selected: {name} in {self.dfw.__class__}")


class Example1a(BaseExample):
    """
    Example 1: No tabs
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        query = DeviceQuery(classes=["BSISO", "BOBR", "BSTLHC"], group_by=GroupingType.CLASSES)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.GRID,
                                tab_grouping=None, group_by_fec=True)
        btn = dfw.get_device_buttons("BSISO")[0]
        btn.set_color("red")
        dfw.dark_()
        return dfw


class Example1b(BaseExample):
    """
    Example 1.2: Class Tabs.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        query = DeviceQuery(classes=["BSISO", "BOBR", "BSTLHC"], group_by=GroupingType.CLASSES)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.GRID,
                                tab_grouping=TabGrouping.CLASS, group_by_fec=True)
        return dfw


class Example1c(BaseExample):
    """
    Example 1.2: Accelerator Tabs.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        query = DeviceQuery(classes=["BSISO", "BOBR", "BSTLHC"], group_by=GroupingType.CLASSES)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.GRID,
                                tab_grouping=TabGrouping.ACCELERATOR, group_by_fec=True)
        return dfw


class Example1d(BaseExample):
    """
    Example 1.2: Fec Tabs.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        query = DeviceQuery(classes=["BSISO"], group_by=GroupingType.CLASSES)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.GRID,
                                tab_grouping=TabGrouping.FEC)
        # Retrieving the 4th button for the "BSISO" device class
        btn = dfw.get_device_buttons("BSISO")[3]
        # Setting all the attributes for the button
        btn.set_title("Device Title", color="blue")
        btn.set_subtitle("Device Subtitle", color="green")
        btn.set_text("Device Description", color="orange")
        btn.set_color("red")

        return dfw


class Example2(BaseExample):
    """
    Example 2: Non-Grouped BOBRs from LHC.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        dfw = DevicesFesaWidget(self, class_names=["BOBR"], layout=Layout.GRID, max_col=3, group_by_fec=False)
        return dfw


class Example3(BaseExample):
    """
    Example 3: Removing a device.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        filter3 = DeviceQueryFilter(devices_exclude=["SPARE_HOOD_SC1"])

        query = DeviceQuery(classes=["BSISO"], group_by=GroupingType.CLASSES,
                            query_filter=filter3)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.GRID, group_by_fec=True,
                                title="BSISO but SPARE_HOOD_SC1 is removed")
        return dfw


class Example4(BaseExample):
    """
    Example 4: Using devices_include
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        filter_4 = DeviceQueryFilter(devices_include=['BOBR_cfv-3524-cmsbcm2', 'BOBR_cfv-774-cttstbst',
                                                      'BOBR_cfv-865-blm8', 'BOBR_cfv-865-bsrlhcdev',
                                                      'BOBR_cfv-865-bsrspsdev', 'SPARE_HOOD_SC2', 'SPARE_HOOD_SC3',
                                                      'FAKE_SPARE_HOOD_1', 'FAKE_SPARE_HOOD_2', 'SPARE_HOOD_SC4',
                                                      'COLLECT.SC', 'FAKE_COLLECT.SC'])

        query = DeviceQuery(classes=["BOBR", "BSISO"], group_by=GroupingType.CLASSES, query_filter=filter_4)
        dfw = DevicesFesaWidget(self, fesa_query=query, layout=Layout.TREE, tab_grouping=TabGrouping.CLASS)
        return dfw


class Example5(BaseExample):
    """
    Example 5: Using custom device widget.
    """

    def _add_device_widget_example(self) -> DevicesFesaWidget:
        dfw = DevicesFesaWidget(self, class_names=["BSISO"], layout=Layout.GRID, max_col=3,
                                widget=AlternativeDeviceButtonWidget, tab_grouping=TabGrouping.CLASS, group_by_fec=True)
        return dfw


def init_app_style(q_app: QApplication, dark_mode: bool = False, qss: str = None):
    # Set application-wide font size
    user_font_text = q_app.font()
    user_font_text.setPointSize(10)
    q_app.setFont(user_font_text)

    # Define and set a dark palette
    dark_palette = QPalette()
    dark_palette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    dark_palette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    dark_palette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    dark_palette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    dark_palette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    dark_palette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    dark_palette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    dark_palette.setColor(QPalette.Background, Colors.COLOR_LBLACK)

    if dark_mode:
        q_app.setPalette(dark_palette)

    if not qss:
        background_color = Colors.STR_COLOR_WHITE if not dark_mode else Colors.STR_COLOR_LIGHT1GRAY

        qss = f"""
            QMenuBar::item {{
                spacing: 2px;
                padding: 2px 10px;
                background-color: {Colors.STR_COLOR_TRANSPARENT};
            }}
            QMenuBar::item:selected {{
                background-color: {Colors.STR_COLOR_LIGHT1GRAY};
            }}
            QMenuBar::item:pressed {{
                background: {Colors.STR_COLOR_LIGHT1GRAY};
            }}
            QLineEdit {{
                background-color: {background_color};
            }}
        """
    q_app.setStyleSheet(qss)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    init_app_style(q_app=app, dark_mode=True)

    # Create instances of each example window
    example1a = Example1a("No Tabs")
    example1b = Example1b("Class Tabs")
    example1c = Example1c("Accelerator Tabs")
    example1d = Example1d("FEC Tabs")
    example2 = Example2("Non-Grouped BOBRs from LHC")
    example3 = Example3("Using devices_to_hide and a title")
    example4 = Example4("Tree View Using devices_include")
    example5 = Example5("Using custom Device Widget")

    # Show all example windows
    example1a.show()
    example1b.show()
    example1c.show()
    example1d.show()
    example2.show()
    example3.show()
    example4.show()
    example5.show()

    sys.exit(app.exec_())
