import sys
from ast import literal_eval
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QEvent
from PyQt5.QtGui import QColor, QFont, QPixmap, QIcon
from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QMenu, QGridLayout, QMdiArea, QComboBox, QCompleter, \
    QCheckBox, QMainWindow, QVBoxLayout,QScrollArea, QLabel, QPushButton, QLineEdit, QListView, QMdiSubWindow

from expert_gui_core.gui.common.colors import Colors

from expert_gui_core.comm import edgecomm

from expert_gui_core.gui.widgets.common import togglepanelwidget
from expert_gui_core.gui.widgets.combiner import propertyedgewidget

class MetaPropertyEdgeWidget(QWidget):
    """
    Class widget to display automatic Multiple Edge block instance field(s) panels.
    Possibility to define the module and lun.

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent):
        """
        Initialize class.
        """

        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._properties = {}
        self._settings = {}

        layout = QGridLayout()
        layout.setContentsMargins(4, 2, 4, 2)
        layout.setSpacing(10)
        self.setLayout(layout)

        # search panel

        self._edge_search = _EdgeSearch(self)
        self._edge_search.setMinimumWidth(600)

        self.toggle_panel_search = togglepanelwidget.TogglePanelWidget(self._edge_search, iconshow="sliders-h",
                                                                       align="topright")
        layout.addWidget(self.toggle_panel_search, 0, 0, 1, 1)

        # frames panel

        self.mdi = _MdiArea(self)
        layout.addWidget(self.mdi, 0, 1)

        layout.setRowStretch(0, 1)

        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

        self.set_font_size(9)

    def set_settings(self, settings):
        """
        Set settings.
        """
        try:
            self._settings = dict(literal_eval(settings))
        except Exception as xcp:
            print(xcp)
        self.load_settings()

    def get_settings(self):
        """
        Get settings.
        """
        for setting in self._settings:
            self._settings[setting]["functions"] = {}
            expressions = self._properties[setting].get_expressions()
            if expressions is not None:
                self._settings[setting]["functions"] = expressions
        return self._settings

    def close(self):
        """
        Close the widget.
        """
        for p in self._properties.values():
            try:
                p.deleteLater()
                p.subscribe(None)
            except:
                pass
        self._edge_search._names = []

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        self._edge_search.set_font_size(size)
        for pfw in self._properties.values():
            pfw.set_font_size(size)

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self.mdi.dark_()
        self._edge_search.dark_()
        self.toggle_panel_search.dark_()
        for pfw in self._properties.values():
            try:
                pfw.dark_()
            except:
                pass

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self.mdi.light_()
        self._edge_search.light_()
        self.toggle_panel_search.light_()
        for pfw in self._properties.values():
            try:
                pfw.light_()
            except:
                pass

    def add_pew(self, property_edge_widget, name):
        """
        Add property edge widget.

        :param property_fesa: Property FESA name.
        :type property_fesa: str
        :param name: Tab name.
        :type name: str
        """
        self._properties[name] = property_edge_widget
        sub = _SubWindow(self, property_edge_widget, self.mdi, name)
        sub.setWindowTitle(name)
        self.mdi.addWindow(sub, name)
        property_edge_widget.show()
        sub.show()

    def load_settings(self):
        """
        Load list of settings.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.close()
        try:
            for setting in self._settings:
                info = setting.split("/")
                module = info[0]
                lun = int(info[1])
                block = info[2]
                registers = self._settings[setting]["registers"]
                functions = None
                try:
                    functions = self._settings[setting]["functions"]
                except:
                    pass
                self._edge_search.load_settings(module, lun, block, registers, functions=functions)
        except Exception as xcp:
            print(xcp)
        QApplication.restoreOverrideCursor()

    def force_history(self, history):
        """
        Force the history flag.
        """
        self._edge_search.force_history(history)


class _MdiArea(QMdiArea):

    def __init__(self, parent):
        super(_MdiArea, self).__init__(parent)

        self._subs = {}
        self._actions = {}
        self._parent = parent
        self._selname = ""

        self.context_menu = QMenu(self)
        cascade = self.context_menu.addAction("Cascade")
        tiled = self.context_menu.addAction("Tiled")

        cascade.triggered.connect(self.cascade_triggered)
        tiled.triggered.connect(self.tiled_triggered)

    def addWindow(self, sub, name):
        self.addSubWindow(sub)
        self._subs[name] = sub
        sub.set_edge_search(self._parent._edge_search)
        show_sub = self.context_menu.addAction(name)
        show_sub.triggered.connect(lambda x: self.show_sub(name))
        self._actions[name] = show_sub

    def show_sub(self, name):
        self._subs[name].showMaximized()

    def delete_sub(self, name):
        try:
            self._parent._edge_search.close_pew(name)
            self._parent._edge_search._names.remove(name)
            self._subs.pop(name)
            self.context_menu.removeAction(self._actions[name])
            self._parent._properties.pop(name)
            super.delete_sub(name)
        except:
            pass

    def contextMenuEvent(self, event):
        self.context_menu.exec(event.globalPos())

    def cascade_triggered(self):
        self.cascadeSubWindows()

    def tiled_triggered(self):
        self.tileSubWindows()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setBackground(Colors.COLOR_LIGHT0GRAY)
        qss = """ 
            QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QLabel {
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                margin:5px;
            }
        """

        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)

    def light_(self):
        """
        Set light theme.
        """
        self.setBackground(Colors.COLOR_WHITE)
        qss = """
                QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
                QLabel {
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                margin: 5px;
            }
        """
        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)


class _SubWindow(QMdiSubWindow):

    def __init__(self, parent, widget, mdi, name):
        super(_SubWindow, self).__init__(parent)
        self._edge_search = None
        self._widget = widget
        self._name = name
        self.mdi = mdi
        self.windowStateChanged.connect(self.delayActivated)
        self.setWidget(widget)
        self.setWindowIcon(self.create_icon_by_color(QColor("transparent")))
        self.resize(500, 600)

    def set_edge_search(self, es):
        self._edge_search = es

    def closeEvent(self, event):
        """
        Close a sub window.
        """
        self.mdi.delete_sub(self._name)
        self._widget.deleteLater()
        self._widget.subscribe(None)
        event.accept()

    def create_icon_by_color(self, color):
        pixmap = QPixmap(512, 512)
        pixmap.fill(color)
        return QIcon(pixmap)

    def delayActivated(self, oldState, newState):
        if self.mdi._selname == self._name:
            return
        self.mdi._selname = self._name
        if newState & Qt.WindowActive:
            self._edge_search.load_registers(self._name, self._widget.name_fields)


class _FilterFields(QWidget):
    """
    Widget panel to filter field/registers

    :param parent: Parent object.
    :type parent: object
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        self._parent = parent

        self._field_entities = []

        self._components = {}
        self._text_filter = QLineEdit("")

        self._add_text_filter = False

        self.layout = QGridLayout(self)

        self.scroll = QScrollArea()
        self.scroll.setStyleSheet("border:none;")
        self.widget = QWidget()
        self.vbox = QVBoxLayout()

        self.widget.setLayout(self.vbox)

        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.layout.addWidget(self.scroll, 0, 0)
        self.show()

    def clear_layout(self):
        for fe in self._field_entities:
            fe.deleteLater()
        self._field_entities = []

    def set_list_fields(self, listfields, version_edge="", version_driver="", name_driver="", name_lun=0):
        self.clear_layout()
        height = (4 + len(listfields)) * 30
        if height > 600:
            height = 600
        self.setMinimumHeight(height)

        self._components = {}

        # add filter
        if self._add_text_filter == False:
            panel_filter = QWidget()
            layout_filter = QGridLayout(panel_filter)
            layout_filter.setContentsMargins(10, 5, 0, 0)
            layout_filter.setSpacing(5)
            layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)
            self._text_filter.installEventFilter(self)
            self.filter_changed.connect(self.filter_fields)
            layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
            layout_filter.addWidget(QLabel(" "), 0, 1)
            layout_filter.setColumnStretch(1, 1)
            self.vbox.addWidget(panel_filter)
            self._add_text_filter = True

        fe_all = _FieldEntity(self, "All")
        fe_all.checkbox_plot.toggled.connect(self.pressall)
        self._field_entities.append(fe_all)
        for lf in listfields:
            _field_entity = _FieldEntity(self, lf['name'], field=lf)
            self._components[lf['name']] = _field_entity
            self._field_entities.append(_field_entity)

        for fe in self._field_entities:
            self.vbox.addWidget(fe)

        completer = QCompleter(self._components)
        self._text_filter.setCompleter(completer)

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                else:
                    self._components[component_field].hide()
            except:
                self._components[component_field].hide()

    def pressall(self, e):
        """
        Press one checkbox.
        """
        for fe in self._field_entities:
            fe.setChecked(self._field_entities[0].isChecked())

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        for fe in self._field_entities:
            fe.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        for fe in self._field_entities:
            fe.light_()

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        for fe in self._field_entities:
            fe.set_font_size(size)


class _FieldEntity(QWidget):
    """
    Field widget panel

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent, name, field=None):
        """
        Initialize Class.
        """

        super(QWidget, self).__init__(parent)

        self._parent = parent
        self.name = name
        self.setMaximumHeight(30)
        self.layout = QGridLayout()
        self.layout.setContentsMargins(5, 0, 5, 5)
        self.layout.setSpacing(5)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)
        # name
        label_ = QLabel(name + "  ")
        label_.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        label_.setAlignment(Qt.AlignRight)
        label_.setStyleSheet("font-weight:bold;")
        self.layout.addWidget(label_, 0, 0)

        self._field = None
        self.label_rwmode = None
        self.label_type = None
        self.label_dwith = None
        self.label_size = None

        # checkboxsplit
        self.checkbox_plotsplit = QCheckBox("")
        self.checkbox_plotsplit.setToolTip("Split array")
        self.checkbox_plotsplit.setChecked(False)
        self.checkbox_plotsplit.setMaximumWidth(16)

        if field is not None:

            self._field = field

            # rwmode
            self.label_rwmode = QLabel(" " + field['rwmode'] + " ")
            self.label_rwmode.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_rwmode.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_rwmode, 0, 1)
            # type
            self.label_type = QLabel(field['type'])
            self.label_type.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_type.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_type, 0, 2)
            # dwidth
            self.label_dwith = QLabel(str(field['dwith']))
            self.label_dwith.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_dwith.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_dwith, 0, 3)
            # size
            if field['depth'] > 1:
                self.label_size = QLabel(" " + str(field['depth']) + " ")
                self.label_size.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
                self.label_size.setAlignment(Qt.AlignCenter)
                self.layout.addWidget(self.label_size, 0, 4)
                if field['depth'] < 128:
                    self.layout.addWidget(self.checkbox_plotsplit, 0, 5)

                    # checkbox
        self.checkbox_plot = QCheckBox("")
        self.checkbox_plot.setChecked(False)
        self.checkbox_plot.setMaximumWidth(16)
        self.layout.addWidget(self.checkbox_plot, 0, 6)
        self.layout.setRowStretch(0, 0)
        self.layout.setColumnStretch(0, 1)
        self.layout.setColumnStretch(1, 0)
        self.layout.setColumnStretch(2, 0)
        self.layout.setColumnStretch(3, 0)
        self.layout.setColumnStretch(4, 0)
        self.layout.setColumnStretch(5, 0)
        self.layout.setColumnStretch(6, 0)

    def isChecked(self):
        return self.checkbox_plot.isChecked()

    def isCheckedSplit(self):
        return self.checkbox_plotsplit.isChecked()

    def setChecked(self, val):
        self.checkbox_plot.setChecked(val)

    def setCheckedSplit(self, val):
        self.checkbox_plotsplit.setChecked(val)

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLUE + ";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT0GREEN + ";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT3GRAY + ";color:white;")

        self.checkbox_plot.setStyleSheet(
            "color:" + Colors.STR_COLOR_WHITE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";")
        self.checkbox_plotsplit.setStyleSheet(
            "color:" + Colors.STR_COLOR_L2RED + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";" + "border:1px solid " + Colors.STR_COLOR_L2RED + ";")

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLUE + ";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT0GREEN + ";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT3GRAY + ";color:white;")

        self.checkbox_plot.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLACK + ";background-color:" + Colors.STR_COLOR_WHITE + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        self.checkbox_plotsplit.setStyleSheet(
            "color:" + Colors.STR_COLOR_L2RED + ";background-color:" + Colors.STR_COLOR_WHITE + ";" + "border:1px solid " + Colors.STR_COLOR_L2RED + ";")

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        # """
        pass


class _EdgeSearch(QWidget):
    """
    Class widget panel to search module/lun/block_instance

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent):
        """
        Initialize Class.
        """

        super(QWidget, self).__init__(parent)

        self._parent = parent
        self.layout = QGridLayout()
        self.layout.setContentsMargins(1, 1, 1, 10)
        self.layout.setSpacing(0)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        self._properties = []
        self._timing_domain = ""
        self._dark = False
        self._force_history = False

        self.mod_dict = edgecomm.EdgeComm.get_modules()

        self._names = []

        modules = []

        for module in self.mod_dict:
            modules.append(module)

        modules.sort()

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self.font_text = QFont()
        self.font_text_small = QFont()

        self._label_module = QLabel("Module : ")

        self._enum_module = QComboBox()
        self._list_module = QListView(self._enum_module)
        self._enum_module.setView(self._list_module)
        self._enum_module.setEditable(True)
        self._enum_module.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_module.currentTextChanged.connect(self.get_lun)
        self._enum_module.setFont(self.font_text)

        self._get_lun = QPushButton(fa.icons["search"])

        self._refresh_lun = QPushButton(fa.icons["redo"])
        self._label_lun = QLabel("Lun : ")
        self._enum_lun = QComboBox()
        self._label_blockinstance = QLabel("Block instance : ")
        self._get_blockinstance = QPushButton(fa.icons["search"])
        self._enum_blockinstance = QComboBox()
        self._plus = QPushButton(fa.icons["plus"])

        self.filter_fields = _FilterFields(self)

        self._get_lun.setFont(self.font_fa)
        self._get_lun.setMaximumWidth(25)
        self._get_lun.setMinimumHeight(25)
        self._get_lun.mousePressEvent = self.get_lun

        self._refresh_lun.setFont(self.font_fa)
        self._refresh_lun.setMaximumWidth(25)
        self._refresh_lun.setMinimumHeight(25)
        self._refresh_lun.mousePressEvent = self.refresh_lun

        self._get_blockinstance.setFont(self.font_fa)
        self._get_blockinstance.setMaximumWidth(25)
        self._get_blockinstance.setMinimumHeight(25)
        self._get_blockinstance.mousePressEvent = self.get_blockinstance

        self._plus.setFont(self.font_fa)
        self._plus.setMaximumWidth(30)
        self._plus.setMinimumHeight(30)
        self._plus.mousePressEvent = self.plus

        self._enum_lun = QComboBox()
        self._list_lun = QListView(self._enum_lun)
        self._enum_lun.setView(self._list_lun)
        self._enum_lun.setEditable(True)
        self._enum_lun.currentTextChanged.connect(self.get_blockinstance)
        self._enum_lun.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_lun.setFont(self.font_text)

        self._enum_blockinstance = QComboBox()
        self._list_blockinstance = QListView(self._enum_blockinstance)
        self._enum_blockinstance.setView(self._list_blockinstance)
        self._enum_blockinstance.setEditable(True)
        self._enum_blockinstance.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_blockinstance.currentTextChanged.connect(self.get_fields)
        self._enum_blockinstance.setFont(self.font_text)

        self.layout.setSpacing(5)
        self.layout.addWidget(self._label_module, 0, 0)
        self.layout.addWidget(self._enum_module, 0, 1)
        self.layout.addWidget(self._get_lun, 0, 2)
        self.layout.addWidget(self._refresh_lun, 0, 3)

        self.layout.addWidget(self._label_lun, 1, 0)
        self.layout.addWidget(self._enum_lun, 1, 1)

        self.layout.addWidget(self._label_blockinstance, 2, 0)
        self.layout.addWidget(self._enum_blockinstance, 2, 1)

        self.layout.addWidget(self._plus, 4, 2)

        self.layout.addWidget(self.filter_fields, 3, 0, 1, 2)

        self.filter_fields.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 0)

        self.layout.setRowStretch(0, 0)
        self.layout.setRowStretch(1, 0)
        self.layout.setRowStretch(2, 0)
        self.layout.setRowStretch(3, 1)

        for module in modules:
            self._enum_module.addItem(module)

        self.set_font_size(9)

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        self._font_size = size
        self.font_text.setPointSize(size)
        self.font_text_small.setPointSize(size + 1)
        self.font_fa.setPointSize(size)

        self._label_module.setFont(self.font_text)
        self._enum_module.setFont(self.font_text_small)
        self._get_lun.setFont(self.font_fa)
        self._refresh_lun.setFont(self.font_fa)
        self._plus.setFont(self.font_fa)
        self._label_lun.setFont(self.font_text)
        self._enum_lun.setFont(self.font_text_small)
        self._get_blockinstance.setFont(self.font_fa)
        self._label_blockinstance.setFont(self.font_text)
        self._enum_blockinstance.setFont(self.font_text_small)
        self.filter_fields.set_font_size(size)

    def dark_(self):
        """
        Set dark theme.
        """
        self._dark = True
        self._label_module.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_lun.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_blockinstance.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._enum_module.setStyleSheet(
            "border-radius:4px ;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._get_lun.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._refresh_lun.setStyleSheet(
            "color:" + Colors.STR_COLOR_LIGHTRED + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._plus.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._get_blockinstance.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";")
        self._enum_blockinstance.setStyleSheet(
            "border-radius:4px ;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_blockinstance.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self._enum_lun.setStyleSheet(
            "border-radius:4px ;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_lun.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self.filter_fields.dark_()

    def light_(self):
        """
        Set lght theme.
        """
        self._dark = False
        self._label_module.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_lun.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_blockinstance.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._enum_module.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")
        self._get_lun.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._refresh_lun.setStyleSheet(
            "color:" + Colors.STR_COLOR_LIGHTRED + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._plus.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";" + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._get_blockinstance.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";")
        self._enum_blockinstance.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")
        self._list_blockinstance.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._enum_lun.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")
        self._list_lun.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self.filter_fields.light_()

    def plus(self, e):
        """
        Add property(ies) panels to parent tab.
        """

        QApplication.setOverrideCursor(Qt.WaitCursor)
        instances = []
        lun = int(self._enum_lun.currentText())
        module = self._enum_module.currentText()

        list_fields = None
        list_fe = None

        history = True

        if self._enum_blockinstance.currentText() == "All":
            for blockinstance in self._blockinstance:
                instances.append([
                    blockinstance,
                    lun])
        else:
            list_fields = []
            for fe in self.filter_fields._field_entities:
                if fe.isChecked():
                    if fe.isCheckedSplit():
                        list_fields.append(fe.name)
                        for i in range(0, self.filter_fields._components[fe.name]._field['depth']):
                            list_fields.append(fe.name + "#" + str(i))
                    else:
                        list_fields.append(fe.name)
                        # history
                        try:
                            if self.filter_fields._components[fe.name]._field['depth'] > 1024:
                                history = False
                        except:
                            pass

            instances.append([
                self._enum_blockinstance.currentText(),
                lun])

        history = history or self._force_history

        for instance in instances:

            name = module + "/" + str(instance[1]) + "/" + instance[0]

            if name in self._names:
                self._parent.mdi.delete_sub(name)

            self._names.append(name)

            try:
                list_fields.remove("All")
            except:
                pass

            property_edge_widget = propertyedgewidget.PropertyEdgeWidget(
                self._parent,
                title=instance[0],
                name_lun=instance[1],
                name_blockinstance=instance[0],
                name_module=module,
                name_fields=list_fields,
                max_row=0,
                history=history)
            property_edge_widget.set_font_size(self._font_size)
            if self._dark:
                property_edge_widget.dark_()
            else:
                property_edge_widget.light_()

            self._parent.add_pew(property_edge_widget, module + "/" + str(instance[1]) + "/" + instance[0])

            self._parent._settings[module + "/" + str(instance[1]) + "/" + instance[0]] = {}
            self._parent._settings[module + "/" + str(instance[1]) + "/" + instance[0]]["registers"] = list_fields
            self._parent._settings[module + "/" + str(instance[1]) + "/" + instance[0]]["functions"] = {}

        QApplication.restoreOverrideCursor()

    def force_history(self, history):
        """
        Force the history flag.
        """
        self._force_history = history

    def load_settings(self, module, lun, block, registers, functions=None):
        """
        Load settings.
        """

        # Check everything ok
        if module not in self.mod_dict:
            return
        if lun not in self.mod_dict[module]:
            return
        edgec = edgecomm.EdgeComm(module, lun)
        blockinstances = edgec.get_hwdesc().keys()
        if block not in blockinstances:
            return
        listfields = edgec.get_hwdesc()[block]
        listfields_to_use = []
        listfunctions_to_use = {}
        history = True
        for field in listfields:
            if field['name'] in registers:
                listfields_to_use.append(field['name'])
                try:
                    if field['depth'] > 1024:
                        history = False
                except:
                    pass
                if functions is not None:
                    try:
                        funct = functions[field['name']]
                        listfunctions_to_use[field['name']] = funct
                    except:
                        pass
        history = history or self._force_history
        # Add pew
        property_edge_widget = propertyedgewidget.PropertyEdgeWidget(
            self._parent,
            title=block,
            name_lun=lun,
            name_blockinstance=block,
            name_module=module,
            name_fields=listfields_to_use,
            function_expression=listfunctions_to_use,
            max_row=0,
            history=history)
        property_edge_widget.set_font_size(self._font_size)
        if self._dark:
            property_edge_widget.dark_()
        else:
            property_edge_widget.light_()
        self._parent.add_pew(property_edge_widget, module + "/" + str(lun) + "/" + block)

    def load_registers(self, name, list_fields=None):
        """
        Load registers from its config key.
        """
        try:
            info = name.split("/")
            module = info[0]
            lun = int(info[1])
            block = info[2]
            self._enum_module.setCurrentText(module)
            self._enum_lun.setCurrentText(str(lun))
            self._enum_blockinstance.setCurrentText(block)
            self.get_fields()
            self.select_fields(list_fields)
        except:
            pass

    def close_pew(self, name):
        """
        One property_edge_widget is being closed...

        :param name: Name of the pew.
        :type parent: str
        """
        try:
            self._settings.pop(name)
        except:
            return

    def refresh_lun(self, e):
        """
        Refresh list drivers
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        self._enum_module.currentTextChanged.disconnect(self.get_lun)
        self._enum_module.clear()
        self._enum_module.currentTextChanged.connect(self.get_lun)
        self.mod_dict = edgecomm.EdgeComm.get_modules()
        self._names = []
        modules = []
        for module in self.mod_dict:
            modules.append(module)
        modules.sort()
        for module in modules:
            self._enum_module.addItem(module)
        QApplication.restoreOverrideCursor()

    def get_lun(self, e):
        """
        Get list luns
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        try:
            luns = self.mod_dict[self._enum_module.currentText()]
        except:
            return
        luns.sort()
        ind = 0
        self._enum_lun.clear()
        if len(luns) == 0:
            QApplication.restoreOverrideCursor()
            return
        for lun in luns:
            slun = str(lun)
            self._enum_lun.addItem(slun)
        QApplication.restoreOverrideCursor()

    def get_blockinstance(self, e):
        """
        Get list blockinstance
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        try:
            lun = int(self._enum_lun.currentText())
            if lun == "":
                QApplication.restoreOverrideCursor()
                return
        except:
            QApplication.restoreOverrideCursor()
            return
        edgec = edgecomm.EdgeComm(self._enum_module.currentText(), lun)
        blockinstances = edgec.get_hwdesc().keys()

        self._enum_blockinstance.clear()

        self._blockinstance = []
        for blockinstance in blockinstances:
            self._enum_blockinstance.addItem(blockinstance)
            self._blockinstance.append(blockinstance)

        QApplication.restoreOverrideCursor()

    def select_fields(self, list_fields=None):
        """
        Select or not checbox fields.
        """
        if list_fields is None:
            return

        for field in list_fields:
            self.filter_fields._components[field].setChecked(True)

    def get_fields(self):
        """
        Get list fields
        """

        QApplication.setOverrideCursor(Qt.WaitCursor)
        module = self._enum_module.currentText()
        try:
            lun = int(self._enum_lun.currentText())
        except:
            QApplication.restoreOverrideCursor()
            return
        block_instance = self._enum_blockinstance.currentText()

        if block_instance == "All" or block_instance == "":
            QApplication.restoreOverrideCursor()
            return

        if lun == "":
            QApplication.restoreOverrideCursor()
            return

        edgec = edgecomm.EdgeComm(module, lun)
        listfields = edgec.get_hwdesc()[block_instance]

        self.filter_fields.set_list_fields(listfields,
                                           version_edge=edgec._get_edge_version(),
                                           version_driver=edgec._get_module_version(),
                                           name_driver=module,
                                           name_lun=lun)

        if self._dark:
            self.filter_fields.dark_()
        else:
            self.filter_fields.light_()

        QApplication.restoreOverrideCursor()


class _Example(QMainWindow):
    """
    Example class to test.
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()
        self._update_time = 0

    def init_ui(self):
        """
        Init user interface.
        """
        dark = True

        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)
        ind = 0

        self._meta = MetaPropertyEdgeWidget(central_widget)
        self.layout.addWidget(self._meta, 0, 0)

        self.setCentralWidget(central_widget)

        if dark:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        else:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        self.resize(1300, 900)
        self.move(100, 100)

        if dark:
            self._meta.dark_()
        else:
            self._meta.light_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
