import sys

from time import sleep

from ast import literal_eval

import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QEvent, QTimer, QThread, QObject, QCoreApplication
from PyQt5.QtGui import QPalette, QColor, QFont, QPixmap, QIcon, QContextMenuEvent
from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QMenu, QGridLayout, \
    QCheckBox, QMainWindow, QScrollArea, QLabel, QPushButton, QLineEdit, \
    QWidgetAction, QMdiArea, QMdiSubWindow, QComboBox, QCompleter, QListView, QFileDialog

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import ccda
from expert_gui_core.gui.widgets.common import togglepanelwidget
from expert_gui_core.gui.widgets.combiner import propertyfesawidget
from expert_gui_core.gui.widgets.timing import timingwidget


class MetaPropertyFesaWidget(QWidget):
    """
    Class widget to display automatic Multiple FESA property field(s) panels.
    Possibility to define the device and property.

    :param parent: Parent object.
    :type parent: object
    :param timing_panel: Timing panel linked
    :type timing_panel: class:'expert_gui_core.gui.timing.timingwidget.TimingPanel', optional
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent, timing_panel=None):
        """
        Initialize class.
        """

        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._properties = {}
        self._settings = {}

        layout = QGridLayout()
        layout.setContentsMargins(4, 2, 4, 2)
        self.setLayout(layout)

        # timing panel

        self._timing_panel = None

        # search panel

        self._fesa_search = _FesaSearch(self)
        self._fesa_search.setMinimumWidth(500)
        self.toggle_panel_search = togglepanelwidget.TogglePanelWidget(self._fesa_search, iconshow="sliders-h",
                                                                       align="topright")
        layout.addWidget(self.toggle_panel_search, 0, 0, 4, 1)

        # filter

        panel_filter = QWidget()
        panel_filter.setStyleSheet("background-color:transparent;")
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(0, 5, 0, 0)
        layout_filter.setSpacing(0)
        layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)
        self._text_filter = QLineEdit("")
        self._text_filter.installEventFilter(self)
        self.filter_changed.connect(self.filter_fields)
        layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
        layout_filter.addWidget(QLabel(" "), 0, 1)
        layout_filter.setColumnStretch(1, 0)
        layout.addWidget(panel_filter, 2, 1)

        # frames panel

        self.mdi = _MdiArea(self)
        layout.addWidget(self.mdi, 3, 1)

        layout.setRowStretch(0, 0)
        layout.setRowStretch(1, 0)
        layout.setRowStretch(2, 0)
        layout.setRowStretch(3, 1)

        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

        # self.set_font_size(9)

        self._contextMenu = QMenu(self)

        self.l_open = QLabel('Open')
        self.l_open.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_open.setAlignment(Qt.AlignLeft)
        self.openAct = QWidgetAction(self._contextMenu)
        self.openAct.setDefaultWidget(self.l_open)
        self._contextMenu.addAction(self.openAct)

        self.l_save = QLabel('Save')
        self.l_save.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_save.setAlignment(Qt.AlignLeft)
        self.saveAct = QWidgetAction(self._contextMenu)
        self.saveAct.setDefaultWidget(self.l_save)
        self._contextMenu.addAction(self.saveAct)

        if timing_panel is None:

            self.thread = QThread()
            self.worker = MetaPropertyFesaWidget.Worker()
            self.worker.moveToThread(self.thread)
            self.worker.update_gui.connect(self.single)
            self.thread.started.connect(self.worker.run)
            self.worker.finished.connect(self.thread.quit)
            self.thread.finished.connect(self.worker.deleteLater)
            QCoreApplication.processEvents()
            self.thread.start()
        else:
            self._timing_panel = timing_panel

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        for component_field in self.mdi._subs.keys():
            if filtertxt is None or filtertxt == "":
                self.mdi._subs[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self.mdi._subs[component_field].show()
                else:
                    self.mdi._subs[component_field].hide()
            except:
                self.mdi._subs[component_field].hide()

        self._fesa_search._parent.mdi.tiled_signal.emit(True)

    @pyqtSlot()
    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    class Worker(QObject):
        finished = pyqtSignal()
        update_gui = pyqtSignal()

        def run(self):
            sleep(0.1)
            self.update_gui.emit()
            self.finished.emit()

    @property
    def fesa_search(self):
        return self._fesa_search

    @pyqtSlot()
    def single(self):
        domain = ["PSB", "LEI", "CPS", "SPS", "LNA", "ADE"]
        self._timing_panel = timingwidget.TimingPanel(domain=domain, listener=None)
        self.layout().addWidget(self._timing_panel, 1, 1)

    def set_settings(self, settings):
        """
        Set settings.
        """
        try:
            self._settings = dict(literal_eval(settings))
        except Exception as xcp:
            print(xcp)
        self.load_settings()

    def get_settings(self):
        """
        Get settings.
        """
        for setting in self._settings:
            self._settings[setting]["functions"] = {}
            expressions = self._properties[setting].get_expressions()
            if expressions is not None:
                self._settings[setting]["functions"] = expressions
        return self._settings

    def close(self):
        """
        Close the widget.
        """
        for p in self._properties.values():
            try:
                p.deleteLater()
                p.subscribe(None)
            except:
                pass
        self._fesa_search._names = []

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        self._fesa_search.set_font_size(size)
        for pfw in self._properties.values():
            pfw.set_font_size(size)

    def dark_(self):
        """
        Set dark theme.
        """

        self.mdi.dark_()
        self._fesa_search.dark_()
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        if self._timing_panel is not None:
            self._timing_panel.dark_()
        self.toggle_panel_search.dark_()
        for pfw in self._properties.values():
            try:
                pfw.dark_()
            except:
                pass

        qss = """ 
            QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QLabel {
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                margin:5px;
            }
        """

        if self._contextMenu != None:
            self._contextMenu.setStyleSheet(qss)

    def light_(self):
        """
        Set light theme.
        """
        self.mdi.light_()
        self._fesa_search.light_()
        if self._timing_panel is not None:
            self._timing_panel.light_()
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        self.toggle_panel_search.light_()
        for pfw in self._properties.values():
            try:
                pfw.light_()
            except:
                pass

        qss = """
                QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
                QLabel {
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                margin: 5px;
            }
        """
        if self._contextMenu != None:
            self._contextMenu.setStyleSheet(qss)

    def add_pfw(self, property_fesa_widget, name, display="normal"):
        """
        Add property fesa widget.

        :param property_fesa: Property FESA name.
        :type property_fesa: str
        :param name: Tab name.
        :type name: str
        """
        sub = _SubWindow(self, property_fesa_widget, self.mdi, name, display=display)
        if not sub.valid:
            sub.deleteLater()
            return
        sub.setWindowTitle(name)
        sub.setToolTip(name)
        self._properties[name] = property_fesa_widget
        # sub.setWindowFlags(sub.windowFlags() | Qt.FramelessWindowHint)
        self.mdi.addWindow(sub, name)
        if display == "normal":
            property_fesa_widget.show()
        sub.show()

    def get_cycle(self):
        """
        Return selected cycle.

        :return: Selected cycle fron the timing panel and for the specified timing domain
        :rtype: str
        """
        return self._timing_panel.get_cycle_per_dom(self._fesa_search._timing_domain)

    def contextMenuEvent(self, event):
        """
        Click right menu options to format the string.
        """
        action = self._contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == self.openAct:
            self.open_setting()
        elif action == self.saveAct:
            self.save_setting()

        event.accept()

    def open_setting(self):
        """
        Open setting file.
        """
        try:
            name = QFileDialog.getOpenFileName(self, 'Open File')
            with open(name[0]) as configfile:
                self.set_settings(configfile.read())
        except:
            return

    def save_setting(self):
        """
        Save setting file.
        """
        try:
            name = QFileDialog.getSaveFileName(self, 'Save File')
            with open(name[0], 'w') as configfile:
                configfile.write(str(self.get_settings()))
        except Exception as xcp:
            print(xcp)

    def load_settings(self):
        """
        Load list of settings.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        try:

            for setting in self._settings:
                info = setting.split("/")
                class_name = info[0]
                device = info[1]
                property_name = info[2]
                fields = self._settings[setting]["fields"]
                functions = None
                try:
                    functions = self._settings[setting]["functions"]
                except:
                    pass

                if "display" in self._settings[setting].keys():
                    self._fesa_search.load_settings(class_name, device, property_name, fields, functions=functions,
                                                    display=self._settings[setting]["display"])
                else:
                    self._fesa_search.load_settings(class_name, device, property_name, fields, functions=functions)

            self._fesa_search._parent.mdi.tiled_signal.emit(True)

        except Exception as xcp:
            print("e load_settings", device, xcp)
        QApplication.restoreOverrideCursor()


class _MdiArea(QMdiArea):

    tiled_signal = pyqtSignal(bool)

    def __init__(self, parent):
        super(_MdiArea, self).__init__(parent)

        self._subs = {}
        self._actions = {}
        self._parent = parent
        self._selname = ""


        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.context_menu = QMenu(self)

        l_cascade = QLabel('Cascade')
        l_cascade.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        l_cascade.setAlignment(Qt.AlignLeft)
        self.cascade = QWidgetAction(self.context_menu)
        self.cascade.setDefaultWidget(l_cascade)
        self.cascade.triggered.connect(self.cascade_triggered)
        self.context_menu.addAction(self.cascade)

        l_tiled = QLabel('Tiled')
        l_tiled.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        l_tiled.setAlignment(Qt.AlignLeft)
        self.tiled = QWidgetAction(self.context_menu)
        self.tiled.setDefaultWidget(l_tiled)
        self.tiled.triggered.connect(self.tiled_pfw)
        self.context_menu.addAction(self.tiled)

        self.tiled_signal.connect(self.tiled_pfw)

    def addWindow(self, sub, name):
        self.addSubWindow(sub)
        self._subs[name] = sub
        sub.set_fesa_search(self._parent._fesa_search)

        l_ss = QLabel(name)
        l_ss.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        l_ss.setAlignment(Qt.AlignLeft)
        show_sub = QWidgetAction(self.context_menu)
        show_sub.setDefaultWidget(l_ss)
        show_sub.triggered.connect(lambda x: self.show_sub(name))
        self.context_menu.addAction(show_sub)

        self._actions[name] = show_sub

    def delete_sub(self, name):
        try:
            self._parent._fesa_search.close_pfw(name)
            try:
                self._parent._fesa_search._names.remove(name)
            except:
                pass
            self._subs.pop(name)
            self.context_menu.removeAction(self._actions[name])
            self._parent._properties.pop(name)
        except Exception as e:
            print("e delete_sub", e)

    def show_sub(self, name):
        self._subs[name].showMaximized()

    def contextMenuEvent(self, event:QContextMenuEvent):
        show = True
        for sub in self._subs:
            pos = self._subs[sub].pos()
            size = self._subs[sub].size()
            x = event.pos().x()
            y = event.pos().y()
            if x > pos.x() and (x < pos.x()+size.width()):
                if y > pos.y() and (y < pos.y() + size.height()):
                    show = False
                    break
        if show:
            self.context_menu.exec(event.globalPos())
        event.accept()

    def cascade_triggered(self):
        self.cascadeSubWindows()

    @pyqtSlot(bool)
    def tiled_pfw(self, auto=True):
        QTimer.singleShot(1000,lambda:self.tiled_pfw_process(auto))

    def tiled_pfw_process(self, auto=True):

        auto=True

        count = 0
        for sub in self._subs.values():
            if sub.isVisible():
                count = count + 1

        if count == 0:
            return

        if auto:
            if count <= 4:
                if count == 2:
                    h = int((self.height() - 20))
                    w = int((self.width() - 20) / 2)
                elif count == 1:
                    h = int((self.height() - 20))
                    w = int((self.width() - 20))
                else:
                    h = int((self.height() - 20) / 2)
                    w = int((self.width() - 20) / 2)
            elif count <= 6:
                h = int((self.height() - 20) / 2)
                w = int((self.width() - 20) / 3)
            else:
                h = int((self.height() - 20) / 3)
                w = int((self.width() - 20) / 3)
        else:
            w = int((self.width() - 20))
            h = int((self.height() - 20))

        if w < 250:
            w = 250
        if h < 150:
            h = 150

        ind = 0

        for sub in self._subs.values():
            if sub.isVisible():
                sub.resize(w, h)
                if count > 6:
                    sub.move((ind % 3) * w, (int(ind / 3)) * h)
                elif count == 6:
                    sub.move((ind % 3) * w, (int(ind / 3)) * h)
                elif count == 5:
                    sub.move((ind % 3) * w, (int(ind / 3)) * h)
                elif count == 4:
                    sub.move((ind % 2) * w, (int(ind / 2)) * h)
                elif count == 3:
                    sub.move((ind % 2) * w, (int(ind / 2)) * h)
                elif count == 2:
                    sub.move((ind % 2) * w, 0)
                elif count == 1:
                    sub.move(0, 0)
                ind = ind + 1

    def dark_(self):
        """
        Set dark theme.
        """
        self.setBackground(Colors.COLOR_LIGHT0GRAY)
        qss = """ 
            QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QLabel {
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                margin:5px;
            }
        """

        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)

    def light_(self):
        """
        Set light theme.
        """
        self.setBackground(Colors.COLOR_WHITE)
        qss = """
                QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
                QLabel {
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                margin: 5px;
            }
        """
        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)


class _SubWindow(QMdiSubWindow):

    def __init__(self, parent, widget, mdi, name, display="normal"):
        super(_SubWindow, self).__init__(parent)
        self._fesa_search = None
        self.parent = parent
        self._widget = widget
        self._name = name
        self.mdi = mdi
        self._display = display
        self.windowStateChanged.connect(self.delayActivated)
        self.display = display
        self.valid = True
        if display == "split":
            names = name.split("/")
            try:
                self.setWidget(widget._components[names[-2]]._data_widget)
            except:
                # print("NOT FOUND", names, widget._components)
                self.valid = False
                return
        else:
            self.setWidget(widget)
        self.setWindowIcon(self.create_icon_by_color(QColor("transparent")))
        self.resize(600, 600)

    def set_fesa_search(self, fs):
        self._fesa_search = fs

    def closeEvent(self, event):
        """
        Close a sub window.
        """
        self.mdi.delete_sub(self._name)
        self._widget.deleteLater()
        self._widget.unsubscribe()
        if event is not None:
            event.accept()

    def create_icon_by_color(self, color):
        pixmap = QPixmap(512, 512)
        pixmap.fill(color)
        return QIcon(pixmap)

    def delayActivated(self, oldState, newState):
        self.mdi._selname = self._name
        if self.display == "split":
            return
        if newState & Qt.WindowActive:
            if isinstance(self._widget, propertyfesawidget.PropertyFesaWidget):
                self._fesa_search.load_fields(self._name, self._widget.name_fields)


class _FilterFields(QWidget):
    """
    Widget panel to filter field/registers

    :param parent: Parent object.
    :type parent: object
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        self._parent = parent

        self._field_entities = []

        self._components = {}
        self._text_filter = QLineEdit("")

        self._add_text_filter = False

        self.layout = QGridLayout(self)

        self.scroll = QScrollArea()
        self.scroll.setStyleSheet("border:none;")
        self.widget = QWidget()
        self.vbox = QGridLayout()

        self.widget.setLayout(self.vbox)

        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.layout.addWidget(self.scroll, 0, 0)
        self.show()

    def clear_layout(self):
        for fe in self._field_entities:
            fe.deleteLater()
        self._field_entities = []

    def set_list_fields(self, listfields):
        self.clear_layout()
        height = (4 + len(listfields)) * 30
        if height > 600:
            height = 600
        self.setMinimumHeight(height)

        self._components = {}

        # add filter
        if self._add_text_filter == False:
            panel_filter = QWidget()
            layout_filter = QGridLayout(panel_filter)
            layout_filter.setContentsMargins(10, 5, 0, 0)
            layout_filter.setSpacing(5)
            layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)
            self._text_filter.installEventFilter(self)
            self.filter_changed.connect(self.filter_fields)
            layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
            layout_filter.addWidget(QLabel(" "), 0, 1)
            layout_filter.setColumnStretch(1, 1)
            self.vbox.addWidget(panel_filter, 0, 0)
            self.vbox.setRowStretch(0, 0)
            self._add_text_filter = True

        fe_all = _FieldEntity(self, "All")
        fe_all.checkbox_plot.toggled.connect(self.pressall)
        self._field_entities.append(fe_all)
        for lf in listfields:
            _field_entity = _FieldEntity(self, lf)
            self._components[lf] = _field_entity
            self._field_entities.append(_field_entity)

        ind = 1
        for fe in self._field_entities:
            self.vbox.addWidget(fe, ind, 0)
            self.vbox.setRowStretch(ind, 0)
            ind = ind + 1
        self.vbox.setRowStretch(ind, 1)

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    def pressall(self, e):
        """
        Press one checkbox.
        """
        for fe in self._field_entities:
            fe.setChecked(self._field_entities[0].isChecked())

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                else:
                    self._components[component_field].hide()
            except:
                self._components[component_field].hide()

    def dark_(self):
        """
        Set dark theme.
        """
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        for fe in self._field_entities:
            fe.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        for fe in self._field_entities:
            fe.light_()

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        for fe in self._field_entities:
            fe.set_font_size(size)


class _FieldEntity(QWidget):
    """
    Field widget panel

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent, name):
        """
        Initialize Class.
        """

        super(QWidget, self).__init__(parent)

        self._parent = parent
        self.name = name
        self.setMaximumHeight(30)
        self.layout = QGridLayout()
        self.layout.setContentsMargins(5, 0, 5, 5)
        self.layout.setSpacing(5)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        label_ = QLabel(name)
        label_.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        label_.setStyleSheet("font-weight:bold;")
        label_.setAlignment(Qt.AlignRight)
        self.layout.addWidget(label_, 0, 0)
        self.checkbox_plot = QCheckBox("")
        self.checkbox_plot.setChecked(True)
        self.checkbox_plot.setMaximumWidth(16)
        self.layout.addWidget(self.checkbox_plot, 0, 1)
        self.layout.setRowStretch(0, 0)

    def isChecked(self):
        return self.checkbox_plot.isChecked()

    def setChecked(self, val):
        self.checkbox_plot.setChecked(val)

    def dark_(self):
        """
        Set dark theme.
        """
        pass

    def light_(self):
        """
        Set light theme.
        """
        pass


class _FesaSearch(QWidget):
    """
    Class widget panel to search class/device/property

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent):
        """
        Initialize class.
        """

        super(QWidget, self).__init__(parent)

        self._parent = parent
        self.layout = QGridLayout()
        self.layout.setContentsMargins(10, 10, 10, 10)
        self.layout.setSpacing(2)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        self._names = []

        self._properties_archived = {}

        self._properties = []
        self._timing_domain = ""
        self._dark = False

        self._auto = True
        self._history = True

        # Get all classes from CCDA

        self.classes = ccda.get_all_classes()
        self.classes.sort()

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self.font_text = QFont()
        self.font_text_small = QFont()

        self._label_class = QLabel("Class : ")
        self._le_classes = QLineEdit()
        self._le_classes.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self._get_devices = QPushButton(fa.icons["search"])
        self._label_device = QLabel("Device : ")
        self._label_property = QLabel("Property : ")
        self._get_properties = QPushButton(fa.icons["search"])
        self._plus = QPushButton(fa.icons["plus"])

        self._get_devices.setFont(self.font_fa)
        self._get_devices.setMaximumWidth(25)
        self._get_devices.setMinimumHeight(25)
        self._get_devices.mousePressEvent = self.get_devices

        self._get_properties.setFont(self.font_fa)
        self._get_properties.setMaximumWidth(25)
        self._get_properties.setMinimumHeight(25)
        self._get_properties.mousePressEvent = self.get_properties

        self._plus.setFont(self.font_fa)
        self._plus.setMaximumWidth(30)
        self._plus.setMinimumHeight(30)
        self._plus.mousePressEvent = self.plus

        self._enum_devices = QComboBox()
        self._list_devices = QListView(self._enum_devices)
        self._enum_devices.setView(self._list_devices)
        self._enum_devices.setEditable(True)
        self._enum_devices.currentTextChanged.connect(self.get_auto_properties)
        self._enum_devices.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_devices.setFont(self.font_text)

        self._enum_properties = QComboBox()
        self._list_properties = QListView(self._enum_properties)
        self._enum_properties.setView(self._list_properties)
        self._enum_properties.currentTextChanged.connect(self.get_auto_fields)
        self._enum_properties.setEditable(True)
        self._enum_properties.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_properties.setFont(self.font_text)

        self.layout.setSpacing(5)
        self.layout.addWidget(self._label_class, 0, 0)
        self.layout.addWidget(self._le_classes, 0, 1)
        self.layout.addWidget(self._get_devices, 0, 2)

        self.layout.addWidget(self._label_device, 1, 0)
        self.layout.addWidget(self._enum_devices, 1, 1)

        self.layout.addWidget(self._label_property, 2, 0)
        self.layout.addWidget(self._enum_properties, 2, 1)

        # history and split
        w = QWidget()
        layout = QGridLayout(w)
        layout.setContentsMargins(10, 10, 10, 10)
        layout.setSpacing(2)

        self.cb_history = QCheckBox("History")
        self.cb_history.setChecked(True)
        layout.addWidget(self.cb_history, 0, 1)

        self.cb_split = QCheckBox("Split fields")
        self.cb_split.setChecked(False)
        layout.addWidget(self.cb_split, 0, 2)

        self.cb_flat = QCheckBox("Flat")
        self.cb_flat.setChecked(False)
        layout.addWidget(self.cb_flat, 0, 3)

        self.layout.addWidget(w, 3, 1)

        self.filter_fields = _FilterFields(self)
        self.filter_fields.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.layout.addWidget(self.filter_fields, 4, 0, 1, 2)

        self.layout.addWidget(self._plus, 4, 2)

        self.layout.setRowStretch(0, 0)
        self.layout.setRowStretch(1, 0)
        self.layout.setRowStretch(2, 0)
        self.layout.setRowStretch(3, 0)
        self.layout.setRowStretch(4, 1)

        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 0)

        completer = QCompleter(self.classes)
        self._le_classes.setCompleter(completer)
        self._le_classes.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        if len(self.classes) > 1:
            self._le_classes.setText(self.classes[1])

        self.set_font_size(9)

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        self._font_size = size
        self.font_text.setPointSize(size)
        self.font_text_small.setPointSize(size + 1)
        self.font_fa.setPointSize(size)

        self._label_class.setFont(self.font_text)
        self._le_classes.setFont(self.font_text_small)
        self._get_devices.setFont(self.font_fa)
        self._plus.setFont(self.font_fa)
        self._label_device.setFont(self.font_text)
        self._enum_devices.setFont(self.font_text_small)
        self._get_properties.setFont(self.font_fa)
        self._label_property.setFont(self.font_text)
        self._enum_properties.setFont(self.font_text_small)
        self.filter_fields.set_font_size(size)

    def dark_(self):
        """
        Set dark theme.
        """
        self._dark = True
        self._label_class.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_device.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_property.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._le_classes.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";")
        self._get_devices.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._plus.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._get_properties.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._enum_properties.setStyleSheet(
            "border-radius:4px ;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self._enum_devices.setStyleSheet(
            "border-radius:4px ;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self.filter_fields.dark_()

    def set_history(self, history):
        self._history = history
        self.cb_history.setChecked(self._history)

    @property
    def history(self) -> bool:
        return self._history

    def light_(self):
        """
        Set light theme.
        """
        self._dark = False
        self._label_class.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_device.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_property.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._le_classes.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        self._get_devices.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._plus.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";" + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._get_properties.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLUE + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")

        self._enum_properties.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")
        self._list_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")

        self._enum_devices.setStyleSheet(
            "border-radius:4px ;padding:3px;background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")
        self._list_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_L2BLUE + ";")

        self.filter_fields.light_()

    def plus(self, e):
        """
        Add property(ies) panels to parent tab.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)

        self._history = self.cb_history.isChecked()

        instances = []
        class_name = self._le_classes.text()
        device = self._enum_devices.currentText()

        list_fields = None

        if self._enum_properties.currentText() == "All":
            for propertyfesa in self._properties:
                instances.append([
                    class_name,
                    device,
                    propertyfesa])
        else:
            list_fields = []
            for fe in self.filter_fields._field_entities:
                if fe.isChecked():
                    if fe.name != "All":
                        list_fields.append(fe.name)
            instances.append([
                class_name,
                device,
                self._enum_properties.currentText()])

        if self.cb_flat.isChecked():

            for instance in instances:

                device = instance[1]
                property_name = instance[2]

                # scalar flat
                name = class_name + "/" + device + "/" + property_name + "/#/flat"

                if name in self._names:
                    self._parent.mdi.delete_sub(name)
                else:
                    self._names.append(name)

                try:
                    list_fields.remove("All")
                except:
                    pass

                from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget
                psw = PropertySubscriptionWidget(
                    device_name=device,
                    property_name=property_name,
                    fields=list_fields
                )

                if len(psw._fields) != 0:
                    if self._dark:
                        psw.dark_()
                    else:
                        psw.light_()

                    self._parent.add_pfw(psw, name, display="flat")

                # arrays flat

                name = class_name + "/" + device + "/" + property_name + "/#2/flat"

                if name in self._names:
                    self._parent.mdi.delete_sub(name)
                else:
                    self._names.append(name)

                from expert_gui_core.gui.widgets.common.subscriptionarraywidget import PropertySubscriptionArrayWidget
                psw = PropertySubscriptionArrayWidget(
                    self,
                    device_names=[device],
                    property_name=property_name,
                    fields=list_fields
                )

                if len(psw._fields) != 0:

                    if self._dark:
                        psw.dark_()
                    else:
                        psw.light_()

                    self._parent.add_pfw(psw, name, display="flat")

                self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]] = {}
                self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["fields"] = list_fields
                self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["functions"] = {}
                self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["display"] = "flat"
                self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["history"] = True

            self._parent.mdi.tiled_signal.emit(True)

        else:
            if self.cb_split.isChecked():

                for instance in instances:

                    try:
                        list_fields.remove("All")
                    except:
                        pass

                    device = instance[1]
                    property_name = instance[2]

                    listfields_to_use = list_fields

                    listfunctions_to_use = {}

                    for field in listfields_to_use:

                        fieldtouse = []
                        fieldtouse.append(field)
                        fonctiontouse = {}
                        if field in listfunctions_to_use:
                            fonctiontouse[field] = listfunctions_to_use[field]

                        name = class_name + "/" + device + "/" + property_name + "/" + fieldtouse[0] + "/split"

                        if name in self._names:
                            self._parent.mdi.delete_sub(name)
                        else:
                            self._names.append(name)

                        property_fesa_widget = propertyfesawidget.PropertyFesaWidget(
                            self._parent,
                            title=None,
                            name_device=device,
                            name_property=property_name,
                            name_fields=fieldtouse,
                            function_expression=fonctiontouse,
                            max_row=0,
                            header=False,
                            button=False,
                            filter=False,
                            input_struct=None,
                            info=False,
                            partial=False,
                            history=True,
                            display="split")

                        if self._dark:
                            property_fesa_widget.dark_()
                        else:
                            property_fesa_widget.light_()

                        self._parent.add_pfw(property_fesa_widget, name, display="split")

                        property_fesa_widget.set_font_size(self._font_size)

                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]] = {}
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["fields"] = list_fields
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["functions"] = {}
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["display"] = "split"
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["history"] = True

                self._parent.mdi.tiled_signal.emit(True)

            else:

                for instance in instances:

                    name = instance[0] + "/" + instance[1] + "/" + instance[2] + "/#/normal"

                    if name in self._names:
                        self._parent.mdi.delete_sub(name)
                    else:
                        self._names.append(name)

                    try:
                        list_fields.remove("All")
                    except:
                        pass

                    property_fesa_widget = propertyfesawidget.PropertyFesaWidget(
                        self._parent,
                        title=instance[2],
                        name_device=instance[1],
                        name_property=instance[2],
                        name_fields=list_fields,
                        max_row=0,
                        history=self._history,
                        display="normal")

                    property_fesa_widget.set_font_size(self._font_size)

                    if self._dark:
                        property_fesa_widget.dark_()
                    else:
                        property_fesa_widget.light_()

                    self._parent.add_pfw(property_fesa_widget, name)

                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]] = {}
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["fields"] = list_fields
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["functions"] = {}
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["display"] = "normal"
                    self._parent._settings[instance[0] + "/" + instance[1] + "/" + instance[2]]["history"] = self._history

        QApplication.restoreOverrideCursor()

    def load_settings(self, class_name, device, property_name, fields, functions=None, display="normal"):
        """
        Load settings.
        """

        self._auto = False

        classes_uc = self.classes.copy()
        classes_uc = [element.upper() for element in classes_uc];

        self._le_classes.setText(class_name)

        devices = self._get_devicesb()

        # if devices is not None or device not in devices:
        #     self._auto = True
        #     return

        self._enum_devices.setCurrentText(device)
        self.get_properties(None)
        self._enum_properties.setCurrentText(property_name)

        listfields = self.get_fields()

        self.select_fields(listfields)
        listfields_to_use = []
        listfunctions_to_use = {}

        try:
            if fields[0] == "All":
                fields = listfields.copy()
        except:
            pass

        for field in listfields:
            if field in fields:
                listfields_to_use.append(field)
                if functions is not None:
                    try:
                        funct = functions[field]
                        listfunctions_to_use[field] = funct
                    except:
                        pass

        if display == "split":

            for field in listfields_to_use:

                fieldtouse = []
                fieldtouse.append(field)
                fonctiontouse = {}
                if field in listfunctions_to_use:
                    fonctiontouse[field] = listfunctions_to_use[field]

                # Add pfw
                property_fesa_widget = propertyfesawidget.PropertyFesaWidget(
                    self._parent,
                    title=None,
                    name_device=device,
                    name_property=property_name,
                    name_fields=fieldtouse,
                    function_expression=fonctiontouse,
                    max_row=0,
                    header=False,
                    button=False,
                    filter=False,
                    input_struct=None,
                    info=False,
                    partial=False,
                    history=True,
                    display="split")

                if self._dark:
                    property_fesa_widget.dark_()
                else:
                    property_fesa_widget.light_()

                property_fesa_widget.set_font_size(self._font_size)

                self._parent.add_pfw(property_fesa_widget,
                                     class_name + "/" + device + "/" + property_name + "/" + fieldtouse[0] + "/split",
                                     display="split")

            # self._parent.mdi.tiled_signal.emit(True)

        elif display == "flat":

            name = class_name + "/" + device + "/" + property_name + "/#/flat"

            if listfields_to_use is not None:
                if "_time" not in listfields_to_use:
                    listfields_to_use.append("_time")
                    listfields_to_use.append("_cycle")
            try:
                listfields_to_use.remove("All")
            except:
                pass

            from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget
            psw = PropertySubscriptionWidget(
                device_name=device,
                property_name=property_name,
                fields=listfields_to_use
            )

            if len(psw._fields) != 0:
                if self._dark:
                    psw.dark_()
                else:
                    psw.light_()

                self._parent.add_pfw(psw, name, display="flat")

            # arrays flat

            name = class_name + "/" + device + "/" + property_name + "/#2/flat"

            if name in self._names:
                self._parent.mdi.delete_sub(name)
            else:
                self._names.append(name)

            from expert_gui_core.gui.widgets.common.subscriptionarraywidget import PropertySubscriptionArrayWidget
            psw = PropertySubscriptionArrayWidget(
                self,
                device_names=[device],
                property_name=property_name,
                fields=listfields_to_use
            )

            if len(psw._fields) != 0:

                if self._dark:
                    psw.dark_()
                else:
                    psw.light_()

                self._parent.add_pfw(psw, name, display="flat")

            # self._parent.mdi.tiled_signal.emit(True)

        else:

            if listfields_to_use is not None:
                if "_time" not in listfields_to_use:
                    listfields_to_use.append("_time")
                    listfields_to_use.append("_cycle")

            # Add pfw
            property_fesa_widget = propertyfesawidget.PropertyFesaWidget(
                self._parent,
                title=property_name,
                name_device=device,
                name_property=property_name,
                name_fields=listfields_to_use,
                function_expression=listfunctions_to_use,
                max_row=0,
                history=self._history)

            property_fesa_widget.set_font_size(self._font_size)

            if self._dark:
                property_fesa_widget.dark_()
            else:
                property_fesa_widget.light_()

            self._parent.add_pfw(property_fesa_widget, class_name + "/" + device + "/" + property_name + "/#/normal")

        self._auto = True

    def load_fields(self, name, list_fields=None):
        """
        Load registers from its config key.
        """
        try:
            info = name.split("/")
            class_name = info[0]
            device = info[1]
            prop = info[2]
            self._enum_properties.setCurrentText(prop)
            self._enum_devices.setCurrentText(device)
            self.get_fields()
            self.select_fields(list_fields)
        except:
            pass

    def get_devices(self, e):
        self._get_devicesb()

    def _get_devicesb(self):
        """
        Get list devices from CCDA and add it to the combobox.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self._le_classes.text() == "LTIM" or self._le_classes.text() == "AnalogueSignal":
            QApplication.restoreOverrideCursor()
            return
        devices = ccda.get_devices_from_class(self._le_classes.text())
        devices.sort()
        ind = 0
        self._enum_devices.clear()
        if len(devices) == 0:
            QApplication.restoreOverrideCursor()
            return
        for device in devices:
            if ind < 1000:
                self._enum_devices.addItem(device)
            ind = ind + 1
        for i in range(0, self._enum_devices.count()):
            self._enum_devices.setItemData(i, Qt.AlignRight, Qt.TextAlignmentRole)
        QApplication.restoreOverrideCursor()
        return devices

    def get_auto_properties(self):
        if self._auto:
            self.get_properties(None)

    def get_auto_fields(self):
        if self._auto:
            self.get_fields()

    def get_properties(self, e):
        """
        Get list properties from CCDA and add it to the combobox.
        """
        device = self._enum_devices.currentText()

        if device == "" or device is None:
            QApplication.restoreOverrideCursor()
            return
        properties = ccda.get_properties_from_device(self._enum_devices.currentText())
        properties.sort()

        previous_property = self._enum_properties.currentText()

        self._enum_properties.clear()

        self._properties = []
        for propertyfesa in properties:
            self._enum_properties.addItem(propertyfesa)
            self._properties.append(propertyfesa)
        for i in range(0, self._enum_properties.count()):
            self._enum_properties.setItemData(i, Qt.AlignRight, Qt.TextAlignmentRole)
        try:
            if previous_property != "":
                self._enum_properties.setCurrentText(previous_property)
            else:
                if len(self._properties) > 0:
                    self._enum_properties.setCurrentText(self._properties[0])
        except:
            if len(self._properties) > 0:
                self._enum_properties.setCurrentText(self._properties[0])

        self._timing_domain = ccda.get_timingdomain_from_device(self._enum_devices.currentText())

        QApplication.restoreOverrideCursor()

    def get_fields(self):
        """
        Get list fields
        """

        QApplication.setOverrideCursor(Qt.WaitCursor)
        name_class = self._le_classes.text()
        name_property = self._enum_properties.currentText()
        name_class_version = None

        if name_property == "All":
            QApplication.restoreOverrideCursor()
            return None

        if name_property == "":
            QApplication.restoreOverrideCursor()
            return None

        get_pffc_opts = {
            'class_name': name_class,
            'property_name': name_property,
            'class_version': None
        }

        key = name_class + "/" + name_property
        if key in self._properties_archived:
            propertyfields = self._properties_archived[key]
        else:
            propertyfields = ccda.get_property_fields_from_class(**get_pffc_opts)
            self._properties_archived[key] = propertyfields

        listfields = []

        for propertyField in propertyfields:
            listfields.append(propertyField.name)

        self.filter_fields.set_list_fields(listfields)

        if self._dark:
            self.filter_fields.dark_()
        else:
            self.filter_fields.light_()

        QApplication.restoreOverrideCursor()

        return listfields

    def close_pfw(self, name):
        """
        One property_fesa_widget is being closed...

        :param name: Name of the pew.
        :type parent: str
        """
        try:
            self._settings.pop(name)
        except:
            return

    def select_fields(self, list_fields=None):
        """
        Select or not checbox fields.
        """
        if list_fields is None:
            return

        for field in list_fields:
            self.filter_fields._components[field].setChecked(True)


class _Example(QMainWindow):
    """
    Example class to test.
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()

        self.init_ui()
        self._update_time = 0

    def init_ui(self):
        """
        Init user interface.
        """
        dark = True

        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)
        ind = 0

        self._meta = MetaPropertyFesaWidget(central_widget)
        self.layout.addWidget(self._meta, 0, 0)

        self.setCentralWidget(central_widget)

        if dark:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        else:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        self.resize(1300, 900)
        self.move(100, 100)

        if dark:
            self._meta.dark_()
        else:
            self._meta.light_()


if __name__ == '__main__':
    Colors.init_font()
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    qssblack = """
            QMenuBar::item {
                spacing: 2px;
                padding: 2px 10px;
                background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
            }
            QMenuBar::item:selected {
                background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QMenuBar::item:pressed {
                background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QScrollArea {
                background-color: transparent;
            }
            QCheckBox {
                max-width:14;
                max-height:14;
                color: """ + Colors.STR_COLOR_WHITE + """;
                background-color: """ + Colors.STR_COLOR_L2BLACK + """;
            }
            QCheckBox::indicator {
                border:1px solid """ + Colors.STR_COLOR_LIGHT2GRAY + """;    
                background-color: """ + Colors.STR_COLOR_L2BLACK + """;           
            }       
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """ + Colors.STR_COLOR_L1BLACK + """;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;
            }
            QLineEdit {
                background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QComboBox {
                border:none;
                background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
            }
            QListView {
                background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_WHITE + """;
                selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
            }
        """

    qss = """
            QScrollArea {
                background-color: transparent;
            }
            QMenuBar::item {
                spacing: 2px;           
                padding: 2px 10px;
                background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
            }
            QMenuBar::item:selected {    
                background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QMenuBar::item:pressed {
                background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QCheckBox::indicator {
                border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;    
                background-color: """ + Colors.STR_COLOR_WHITE + """;           
            }       
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """ + Colors.STR_COLOR_LIGHT4GRAY + """;
                background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
            }             
            QCheckBox {
                max-width:14;
                max-height:14;
                color: """ + Colors.STR_COLOR_BLACK + """;
                background-color: """ + Colors.STR_COLOR_WHITE + """;            
            }
            QLineEdit {
                color:""" + Colors.STR_COLOR_BLACK + """;
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                background-color:""" + Colors.STR_COLOR_WHITE + """;
            }
            QComboBox {
                border:1px solid """ + Colors.STR_COLOR_LIGHT6GRAY + """;
                background-color:""" + Colors.STR_COLOR_WHITE + """;
                selection-color:""" + Colors.STR_COLOR_BLACK + """;
                color:""" + Colors.STR_COLOR_BLACK + """;
                selection-background-color:""" + Colors.STR_COLOR_L2BLUE + """;
            }
            QListView {
                background-color:""" + Colors.STR_COLOR_WHITE + """;
                selection-color:""" + Colors.STR_COLOR_WHITE + """;
                color:""" + Colors.STR_COLOR_BLACK + """;
                selection-background-color:""" + Colors.STR_COLOR_L2BLUE + """;
            }
        """

    app.setStyleSheet(qss)

    ex = _Example()

    ex.show()
    sys.exit(app.exec_())
