import sys
import numpy as np
import logging
from ast import literal_eval
from datetime import datetime

import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QDateTime, QEvent, QObject, QThread, QCoreApplication

from PyQt5.QtGui import QPainter, QPalette, QPen, QBrush, QColor, QFont, QPixmap, QIcon

from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QMenu, QGridLayout, \
    QRadioButton, QCheckBox, QMainWindow, QVBoxLayout, QScrollArea, QLabel, QPushButton, QLineEdit, \
    QMdiArea, QMdiSubWindow, QComboBox, QCompleter, QDateTimeEdit, QListView, QFileDialog, \
    qApp, QListWidget, QPlainTextEdit, QListWidgetItem

from expert_gui_core.tools import formatting

from expert_gui_core.gui.common.colors import Colors

from expert_gui_core.comm import ccda
from expert_gui_core.comm import nxcalscomm

from expert_gui_core.gui.widgets.common import togglepanelwidget, titledborderwidget
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.widgets.combiner import nxcalswidget


class MetaNXCALSWidget(QWidget):
    """

    Class widget to display automatic Multiple NXCALS variable) panels

    """

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._settings = {}

        self._datawidgets = []
        self._nxcals = {}

        layout = QGridLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(10)
        self.setLayout(layout)

        self._nxcals_search = _NXCALSSearch(self)
        self._nxcals_search.setMinimumWidth(700)

        self.toggle_panel_search = togglepanelwidget.TogglePanelWidget(self._nxcals_search, iconshow="sliders-h",
                                                                       align="topright")
        layout.addWidget(self.toggle_panel_search, 0, 0)

        self.mdi = _MdiArea(self)
        layout.addWidget(self.mdi, 0, 1)

        layout.setRowStretch(0, 1)

        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

    def close(self):
        """
        Close the widget.
        """
        pass

    def open_settings(self, e):
        """
        Open setting file.
        """
        try:
            name = QFileDialog.getOpenFileName(self, 'Open File')
            with open(name[0]) as configfile:
                self.set_settings(configfile.read())
        except:
            return

    def save_settings(self, e):
        """
        Save setting file.
        """
        try:
            name = QFileDialog.getSaveFileName(self, 'Save File')
            with open(name[0], 'w') as configfile:
                configfile.write(str(self.get_settings()))
        except Exception as xcp:
            print(xcp)

    def set_settings(self, settings):
        """
        Set settings.
        """
        try:
            self._settings = dict(literal_eval(settings))
        except Exception as xcp:
            print(xcp)
            return
        self.load_settings()

    def get_settings(self):
        """
        Get settings.
        """
        return self._settings

    def load_settings(self):
        """
        Load list of settings.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.close()

        settings = dict(self._settings)
        self._settings = {}

        try:
            for setting in settings:

                info = setting.split("$")

                class_name = info[1]
                device_name = info[2]
                var_name = info[3]
                user_name = info[4]
                from_name = info[5]
                to_name = info[6]

                try:
                    functions = settings[setting]["functions"]
                except:
                    function = None

                try:
                    list_time = settings[setting]["times"]
                except:
                    list_time = None

                # print(class_name)
                # print(device_name)
                # print(var_name)
                # print(user_name)
                # print(from_name)
                # print(to_name)

                self._nxcals_search.load_settings(class_name,
                                                  device_name,
                                                  var_name,
                                                  user_name,
                                                  from_name,
                                                  to_name,
                                                  functions=functions,
                                                  list_time=list_time)
        except Exception as xcp:
            print(xcp)
        QApplication.restoreOverrideCursor()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self.mdi.dark_()
        self._nxcals_search.dark_()
        self.toggle_panel_search.dark_()
        for dtw in self._datawidgets:
            dtw.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self.mdi.light_()
        self._nxcals_search.light_()
        self.toggle_panel_search.light_()
        for dtw in self._datawidgets:
            dtw.light_()

    def add_dtw(self, data_widget, name, name_full=""):
        """
        Add property data widget.

        :param property_fesa: Property FESA name.
        :type property_fesa: str
        :param name: Tab name.
        :type name: str
        """
        if name not in self._nxcals:
            self._datawidgets.append(data_widget)
            self._nxcals[name] = data_widget
            sub = _SubWindow(self, data_widget, self.mdi, name, name_full)
            sub.setWindowTitle(name)
            self.mdi.addWindow(sub, name, name_full)
            sub.setMaximumWidth(self.mdi.width() - 10)
            data_widget.show()
            sub.show()


class _MdiArea(QMdiArea):

    def __init__(self, parent):
        super(_MdiArea, self).__init__(parent)

        self._subs = {}
        self._actions = {}
        self._parent = parent
        self._selname = ""
        self.activated = True

        self.context_menu = QMenu(self)
        cascade = self.context_menu.addAction("Cascade")
        tiled = self.context_menu.addAction("Tiled")

        cascade.triggered.connect(self.cascade_triggered)
        tiled.triggered.connect(self.tiled_triggered)

    def addWindow(self, sub, name, name_full=""):
        self.addSubWindow(sub)
        self._subs[name] = sub
        self._selname = name_full
        sub.set_nxcals_search(self._parent._nxcals_search)
        show_sub = self.context_menu.addAction(name)
        show_sub.triggered.connect(lambda x: self.show_sub(name))
        self._actions[name] = show_sub

    def show_sub(self, name):
        self._subs[name].showMaximized()

    def delete_sub(self, name):
        try:
            self._parent._nxcals_search.close_dtw(name)
            self._parent._nxcals_search._names.remove(name)
            self._subs[name].hide()
            self.removeSubWindow(self._subs[name])
            del self._subs[name]
            self.context_menu.removeAction(self._actions[name])
            del self._parent._nxcals[name]
        except Exception as e:
            print(e)
            pass

    def mousePressEvent(self, e):
        self.activated = not self.activated

    def contextMenuEvent(self, event):
        if self.activated:
            self.context_menu.exec(event.globalPos())

    def cascade_triggered(self):
        self.cascadeSubWindows()

    def tiled_triggered(self):
        self.tileSubWindows()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setBackground(Colors.COLOR_LIGHT0GRAY)
        qss = """ 
            QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QLabel {
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_L3BLACK + """;   
                margin:5px;
            }
        """

        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)

    def light_(self):
        """
        Set light theme.
        """
        self.setBackground(Colors.COLOR_WHITE)
        qss = """
                QLabel:hover {
                    color: """ + Colors.STR_COLOR_LBLUE + """;   
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
                QLabel {
                    background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                }
            QMenu {
                background-color: """ + Colors.STR_COLOR_LIGHT6GRAY + """;   
                margin: 5px;
            }
        """
        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)

    def resizeEvent(self, e):
        for sub in self._subs.values():
            sub.setMaximumSize(self.width() - 0, self.height() - 0)
            sub.resize(int(self.width() / 2) - 0, int(self.height() / 2) - 0)


class _ClkWidget(QWidget):

    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self._parent = parent

    def mouseDoubleClickEvent(self, e):
        if e.button() == Qt.RightButton:
            self._parent.select_window()


class _SubWindow(QMdiSubWindow):

    def __init__(self, parent, widget, mdi, name, name_full=""):
        super(_SubWindow, self).__init__(parent)
        self.setToolTip(name)
        self._nxcals_search = None
        self._widget = widget
        self._name = name
        self._name_full = name_full
        self.mdi = mdi

        self.w = _ClkWidget(self)
        self.layout = QGridLayout(self.w)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)
        self.layout.addWidget(self._widget, 0, 0, 1, 1)
        self.setWidget(self.w)
        self.setWindowIcon(self.create_icon_by_color(QColor("transparent")))
        self.resize(400, 500)

    def set_nxcals_search(self, es):
        self._nxcals_search = es

    def closeEvent(self, event):
        """
        Close a sub window.
        """
        try:
            self.mdi.delete_sub(self._name)
            self._widget.deleteLater()
        except Exception as e:
            print(e)
            pass
        event.accept()

    def create_icon_by_color(self, color):
        pixmap = QPixmap(512, 512)
        pixmap.fill(color)
        return QIcon(pixmap)

    def select_window(self):
        if self.mdi._selname == self._name_full:
            return
        self.mdi._selname = self._name_full
        self._nxcals_search.load_config(self._name_full)


class _PeriodSelection(QWidget):
    """

    Class widget panel to select either tfom tto or LHC fill number

    """

    def __init__(self, parent, nxcals_comm=None):
        super(QWidget, self).__init__(parent)

        last_lhcfill_number = 0
        if nxcals_comm is not None:
            try:
                last_lhcfill_number = nxcals_comm.get_last_fill_number()
            except:
                pass

        self._parent = parent
        self.layout = QGridLayout()
        self.layout.setSpacing(2)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        self.font_fa.setPointSize(12)

        self._now = QPushButton("now")
        self._now.setToolTip("Set time now")
        self._now.setMaximumWidth(50)
        self._now.setMaximumHeight(20)

        self._left = QPushButton(fa.icons["chevron-circle-left"])
        self._left.setToolTip("Move to previous time period")
        self._left.setFont(self.font_fa)
        self._left.setMaximumWidth(25)
        self._left.setMaximumHeight(25)
        self._left.mousePressEvent = self.prev

        self._right = QPushButton(fa.icons["chevron-circle-right"])
        self._right.setToolTip("Move to next time period")
        self._right.setFont(self.font_fa)
        self._right.setMaximumWidth(25)
        self._right.setMaximumHeight(25)
        self._right.mousePressEvent = self.next

        now = QDateTime.currentMSecsSinceEpoch()

        time_to = QDateTime()
        time_to.setMSecsSinceEpoch(now)

        time_from = QDateTime()
        time_from.setMSecsSinceEpoch(now - 600000)

        self.layout.addWidget(self._left, 0, 1, 2, 1)

        self.rb_time = QRadioButton()
        self.rb_time.setChecked(True)
        self.rb_time.setStyleSheet(''' 
                        QRadioButton {
                            color: #888888;
                        }
                        QRadioButton::checked {
                            color: rgb(84,190,230);
                        }
                        QRadioButton::indicator {
                            width: 8px;
                            height: 8px;
                            border-radius: 4px;                            
                        }
                        QRadioButton::indicator::unchecked{ 
                            border: 1px solid; 
                            border-color: #888888;
                            border-radius: 4px;
                            background-color: #888888; 
                            width: 8px; 
                            height: 8px; 
                        }

                        QRadioButton::indicator::checked{ 
                            border: 1px solid; 
                            border-color: rgb(84,190,230);
                            border-radius: 4px;
                            background-color: rgb(84,190,230); 
                            width: 8px; 
                            height: 8px; 
                        }
            ''')
        self.layout.addWidget(self.rb_time, 0, 0, 2, 1)

        self._label_from = QLabel("  From : ")
        self.layout.addWidget(self._label_from, 0, 2)
        self._datetime_from = QDateTimeEdit(self, calendarPopup=True)
        self._datetime_from.setDateTime(time_from)
        self._datetime_from.setDisplayFormat('yyyy-MM-dd hh:mm:ss.zzz')
        self.layout.addWidget(self._datetime_from, 0, 3)

        self.checkbox_rt = QCheckBox("")
        self.checkbox_rt.setChecked(False)
        self.checkbox_rt.setMaximumWidth(16)
        self.checkbox_rt.setToolTip("Live!")

        self._label_to = QLabel("  To : ")
        self.layout.addWidget(self._label_to, 1, 2)
        self._datetime_to = QDateTimeEdit(self, calendarPopup=True)
        self._datetime_to.setDateTime(time_to)
        self._datetime_to.setDisplayFormat('yyyy-MM-dd hh:mm:ss.zzz')
        self.layout.addWidget(self._datetime_to, 1, 3)

        self.layout.addWidget(self._now, 1, 4)
        self._now.mousePressEvent = self.now

        self.layout.addWidget(self._right, 0, 5, 2, 3)

        self.layout.setColumnStretch(1, 0)
        self.layout.setColumnStretch(3, 1)

        self.lhcfill = numberpanelwidget.NumberPanelWidget(self,
                                                           title=None,
                                                           label_name=" LHC fill : ",
                                                           type_format="spi",
                                                           history=False,
                                                           display_format="4.0")
        self.lhcfill.set_data(last_lhcfill_number)

        self.rb_lhc = QRadioButton()
        self.rb_lhc.setStyleSheet(''' 
                        QRadioButton {
                            color: #888888;
                        }
                        QRadioButton::checked {
                            color: rgb(84,190,230);
                        }
                        QRadioButton::indicator {
                            width: 8px;
                            height: 8px;
                            border-radius: 4px;                            
                        }
                        QRadioButton::indicator::unchecked{ 
                            border: 1px solid; 
                            border-color: #888888;
                            border-radius: 4px;
                            background-color: #888888; 
                            width: 8px; 
                            height: 8px; 
                        }

                        QRadioButton::indicator::checked{ 
                            border: 1px solid; 
                            border-color: rgb(84,190,230);
                            border-radius: 4px;
                            background-color: rgb(84,190,230); 
                            width: 8px; 
                            height: 8px; 
                        }
            ''')
        self.rb_lhc.setChecked(False)
        self.layout.addWidget(self.rb_lhc, 3, 0, 2, 1)

        self.layout.addWidget(self.lhcfill, 3, 2, 2, 2)

        self._dark = False

    def now(self, e):
        time_to = QDateTime()
        now = QDateTime.currentMSecsSinceEpoch()
        time_to.setMSecsSinceEpoch(now)
        self._datetime_to.setDateTime(time_to)

    def prev(self, e):
        tfrom = self._datetime_from.dateTime()
        ltfrom = tfrom.toMSecsSinceEpoch()
        tto = self._datetime_to.dateTime()
        ltto = tto.toMSecsSinceEpoch()
        dt = ltto - ltfrom
        if dt > 0:
            ltto = ltto - dt
            ltfrom = ltfrom - dt
        tfrom.setMSecsSinceEpoch(ltfrom)
        tto.setMSecsSinceEpoch(ltto)
        self._datetime_from.setDateTime(tfrom)
        self._datetime_to.setDateTime(tto)

    def next(self, e):
        tfrom = self._datetime_from.dateTime()
        ltfrom = tfrom.toMSecsSinceEpoch()
        tto = self._datetime_to.dateTime()
        ltto = tto.toMSecsSinceEpoch()
        dt = ltto - ltfrom
        if dt > 0:
            ltto = ltto + dt
            ltfrom = ltfrom + dt
        tfrom.setMSecsSinceEpoch(ltfrom)
        tto.setMSecsSinceEpoch(ltto)
        self._datetime_from.setDateTime(tfrom)
        self._datetime_to.setDateTime(tto)

    def get_fill(self):
        return self.lhcfill.get_data()

    def is_fill(self):
        return self.rb_lhc.isChecked()

    def get_from(self):
        tfrom = self._datetime_from.dateTime()
        return tfrom.toString(self._datetime_from.displayFormat())

    def get_to(self):
        tto = self._datetime_to.dateTime()
        return tto.toString(self._datetime_to.displayFormat())

    def get_from_utc(self):
        tfrom = self._datetime_from.dateTime()
        tfromutc = tfrom.toUTC()
        return tfromutc.toString(self._datetime_from.displayFormat())

    def get_to_utc(self):
        tto = self._datetime_to.dateTime()
        ttoutc = tto.toUTC()
        return ttoutc.toString(self._datetime_to.displayFormat())

    def set_tfrom(self, tfrom):
        syl = tfrom.split(".")
        tfrom = syl[0] + "." + syl[-1][0:3]
        self._datetime_from.setDateTime(QDateTime.fromString(tfrom, 'yyyy-MM-dd hh:mm:ss.zzz'))

    def set_tto(self, tto):
        syl = tto.split(".")
        tto = syl[0] + "." + syl[-1][0:3]
        self._datetime_to.setDateTime(QDateTime.fromString(tto, 'yyyy-MM-dd hh:mm:ss.zzz'))

    def dark_(self):
        """Set dark theme"""
        self._dark = True
        self._label_from.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_to.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._now.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._left.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._right.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._datetime_from.setStyleSheet(
            "QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";}")
        self._datetime_to.setStyleSheet(
            "QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";}")
        self.lhcfill.dark_()
        self.checkbox_rt.setStyleSheet(
            "color:" + Colors.STR_COLOR_WHITE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")

    def light_(self):
        """Set light theme"""
        self._dark = False
        self._label_from.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_to.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._now.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._left.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._right.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._datetime_from.setStyleSheet(
            "QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";}")
        self._datetime_to.setStyleSheet(
            "QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";}")
        self.lhcfill.light_()
        self.checkbox_rt.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLACK + ";background-color:" + Colors.STR_COLOR_WHITE + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")


class _QScrollWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ph = 0.
        self.pi = 0.

    def new_count(self, sizetot, index, sizedelta):
        if sizetot == 0:
            return

        self.ph = sizedelta / sizetot
        self.pi = index / sizetot

        self.update()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        size = self.size()

        self.w = size.width()
        self.h = size.height()

        hr = int((self.h - 8) * self.ph)
        ir = int((self.h - 6) * self.pi)

        qp.setRenderHint(QPainter.Antialiasing)

        pen = QPen(QColor(150, 150, 150), 1)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)

        qp.drawLine(1, 1, self.w - 3, 1)
        qp.drawLine(self.w - 3, 1, self.w - 3, self.h - 3)
        qp.drawLine(self.w - 3, self.h - 3, 1, self.h - 3)
        qp.drawLine(1, self.h - 3, 1, 1)

        qp.setBrush(QBrush(Colors.COLOR_BLUE))
        qp.drawRect(3, 3 + ir, self.w - 8, hr)


class _NXCALSSearch(QWidget):
    """

    Class widget panel to search NXCALS variables

    """

    nxcalscomm = None

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        self._parent = parent

        self.layout = QGridLayout()
        self.layout.setContentsMargins(10, 10, 10, 10)
        self.layout.setSpacing(10)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        # self.nxcalscomm = nxcalscomm.NXCALSComm()
        # self.nxcalscomm = None


        self._variables = []
        self._properties = []
        self._names = []

        self._lcb_pu = []
        self._enum_count = None

        # Get all classes from CCDA
        #
        # self.classes = ccda.get_all_classes()
        # self.classes.sort()

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self.font_text = QFont()
        self.font_text_small = QFont()

        # variable

        self._variable_panel = self.create_variable_panel()
        self.layout.addWidget(self._variable_panel, 0, 0, 1, 2)

        # search

        self._search_panel = self.create_search_panel()
        self.layout.addWidget(self._search_panel, 1, 0, 1, 2)

        # config

        self._config_panel = self.create_config_panel()
        self.layout.addWidget(self._config_panel, 2, 0, 1, 1)

        # result

        self._result_panel = self.create_result_panel()
        self.layout.addWidget(self._result_panel, 3, 0, 1, 1)

        # log

        self._log_panel = self.create_log_panel()
        self.layout.addWidget(self._log_panel, 4, 0, 1, 1)

        # nav

        self._nav_panel = self.create_nav_panel()
        self.layout.addWidget(self._nav_panel, 2, 1, 3, 1)

        self.layout.setRowStretch(0, 0)
        self.layout.setRowStretch(1, 0)
        self.layout.setRowStretch(2, 0)
        self.layout.setRowStretch(3, 0)
        self.layout.setRowStretch(4, 1)

        # completer = QCompleter(self.classes)
        # self._le_classes.setCompleter(completer)
        # if len(self.classes) > 1:
        #     self._le_classes.setText(self.classes[1])
        self.classes = None
        self.thread = QThread()
        self.worker = _NXCALSSearch.Worker()
        self.worker.moveToThread(self.thread)
        self.worker.update_gui.connect(self.load_classes)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.thread.finished.connect(self.worker.deleteLater)
        QCoreApplication.processEvents()
        self.thread.start()

        self._dark = False

    class Worker(QObject):
        finished = pyqtSignal()
        update_gui = pyqtSignal()

        def run(self):
            import time
            time.sleep(1)
            self.update_gui.emit()
            _NXCALSSearch.nxcalscomm = nxcalscomm.NXCALSComm()
            self.finished.emit()

    @pyqtSlot()
    def load_classes(self):

        self.classes = ccda.get_all_classes()
        self.classes.sort()

        completer = QCompleter(self.classes)
        self._le_classes.setCompleter(completer)
        if len(self.classes) > 1:
            self._le_classes.setText(self.classes[1])

    def create_variable_panel(self):

        panel = titledborderwidget.TitledBorderWidget("NXCALS Variable")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 15, 10)
        layout.setSpacing(5)

        # class

        self._label_class = QLabel("Class : ")
        self._le_classes = QLineEdit()
        self._le_classes.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

        # device

        self._get_devices = QPushButton(fa.icons["search"])
        self._get_devices.setToolTip("Get device list")
        self._label_device = QLabel("Device : ")
        self._enum_devices = QComboBox()

        # property

        self._label_property = QLabel("Property : ")
        self._get_properties = QPushButton(fa.icons["search"])
        self._get_properties.setToolTip("Get property list")
        self._enum_properties = QComboBox()

        # variable

        self._label_variable = QLabel("Variable : ")
        self._enum_variables = QComboBox()
        self._get_variables = QPushButton(fa.icons["search"])
        self._get_variables.setToolTip("Get variable list")

        # user

        self._label_user = QLabel("User : ")
        self._enum_users = QComboBox()

        self._get_devices.setFont(self.font_fa)
        self._get_devices.setMaximumWidth(25)
        self._get_devices.setMinimumWidth(25)
        self._get_devices.setMinimumHeight(25)
        self._get_devices.setMaximumHeight(25)
        self._get_devices.mousePressEvent = self.get_devices

        self._get_properties.setFont(self.font_fa)
        self._get_properties.setMaximumWidth(25)
        self._get_properties.setMinimumWidth(25)
        self._get_properties.setMaximumHeight(25)
        self._get_properties.setMinimumHeight(25)
        self._get_properties.mousePressEvent = self.get_properties

        self._get_variables.setFont(self.font_fa)
        self._get_variables.setMaximumWidth(25)
        self._get_variables.setMinimumWidth(25)
        self._get_variables.setMinimumHeight(25)
        self._get_variables.setMaximumHeight(25)
        self._get_variables.mousePressEvent = self.get_variables

        self._enum_devices = QComboBox()
        self._list_devices = QListView(self._enum_devices)
        self._enum_devices.setView(self._list_devices)
        self._enum_devices.setEditable(True)
        self._enum_devices.currentTextChanged.connect(self.get_properties)
        self._enum_devices.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_devices.setFont(self.font_text)

        self._enum_properties = QComboBox()
        self._list_properties = QListView(self._enum_properties)
        self._enum_properties.setView(self._list_properties)
        self._enum_properties.setEditable(True)
        self._enum_properties.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_properties.setFont(self.font_text)

        self._enum_variables = QComboBox()
        self._list_variables = QListView(self._enum_variables)
        self._enum_variables.setView(self._list_variables)
        self._enum_variables.setEditable(True)
        self._enum_variables.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_variables.setFont(self.font_text)

        self._enum_users = QComboBox()
        self._list_users = QListView(self._enum_users)
        self._enum_users.setView(self._list_users)
        self._enum_users.setEditable(True)
        self._enum_users.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_users.setFont(self.font_text)

        layout.addWidget(self._label_class, 0, 0)
        layout.addWidget(self._le_classes, 0, 1)
        layout.addWidget(self._get_devices, 0, 2)

        layout.addWidget(self._label_device, 1, 0)
        layout.addWidget(self._enum_devices, 1, 1)
        layout.addWidget(self._get_variables, 1, 2)

        layout.addWidget(self._label_variable, 2, 0)
        layout.addWidget(self._enum_variables, 2, 1)

        layout.addWidget(self._label_user, 3, 0)
        layout.addWidget(self._enum_users, 3, 1)

        self._users = []

        self._users.append("None")
        self._users.append("CPS.USER.ALL")

        from expert_gui_core.gui.widgets.timing import timingwidget
        listcps = timingwidget.TimingPanel.list_cps
        listcps.sort()
        for user in listcps:
            self._users.append("CPS.USER." + user)
        self._users.append("PSB.USER.ALL")
        listpsb = timingwidget.TimingPanel.list_psb
        listpsb.sort()
        for user in listpsb:
            self._users.append("PSB.USER." + user)
        self._users.append("SPS.USER.ALL")
        listsps = timingwidget.TimingPanel.list_sps
        listsps.sort()
        for user in listsps:
            self._users.append("SPS.USER." + user)
        self._users.append("LEI.USER.ALL")
        listlei = timingwidget.TimingPanel.list_lei
        listlei.sort()
        for user in listlei:
            self._users.append("LEI.USER." + user)
        self._users.append("ADE.USER.ALL")
        listade = timingwidget.TimingPanel.list_ade
        listade.sort()
        for user in listade:
            self._users.append("ADE.USER." + user)
        self._users.append("LNA.USER.ALL")
        listlna = timingwidget.TimingPanel.list_lna
        listlna.sort()
        for user in listlna:
            self._users.append("LNA.USER." + user)
        self._users.append("LHC.USER.ALL")

        for user in self._users:
            self._enum_users.addItem(user)

        return panel

    def create_config_panel(self):

        panel = titledborderwidget.TitledBorderWidget("Configuration")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 15, 10)
        layout.setSpacing(5)

        self.open_button = QPushButton("open")
        self.open_button.setMaximumWidth(50)
        self.open_button.mousePressEvent = self._parent.open_settings
        layout.addWidget(self.open_button, 0, 0, Qt.AlignRight)

        self.save_button = QPushButton("save")
        self.save_button.setMaximumWidth(50)
        self.save_button.mousePressEvent = self._parent.save_settings
        layout.addWidget(self.save_button, 0, 1, Qt.AlignLeft)

        panel.setMinimumHeight(90)

        return panel

    def create_search_panel(self):

        panel = titledborderwidget.TitledBorderWidget("NXCALS Search")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 15, 10)
        layout.setSpacing(5)

        self._meta = _PeriodSelection(self, self.nxcalscomm)
        self._meta.setMaximumHeight(120)

        layout.addWidget(self._meta, 0, 0, 1, 2)

        self._plus = QPushButton(fa.icons["search"])
        self._plus.setToolTip("Find selected NXCALS data")

        self._plus.setFont(self.font_fa)
        self._plus.setMaximumWidth(40)
        self._plus.setMinimumHeight(40)
        self._plus.mousePressEvent = self.search
        layout.addWidget(self._plus, 0, 3)

        return panel

    def create_result_panel(self):

        panel = titledborderwidget.TitledBorderWidget("NXCALS Result")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 15, 10)
        layout.setSpacing(5)

        self.infoTextBox = _PlainTextEditInfo(self)
        font_logging = self.infoTextBox.font()
        font_logging.setPointSize(8)
        self.infoTextBox.setFont(font_logging)
        layout.addWidget(self.infoTextBox, 0, 0, 1, 1)

        panel.setMaximumHeight(90)

        return panel

    def create_log_panel(self):

        panel = titledborderwidget.TitledBorderWidget("NXCALS Log")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 15, 10)
        layout.setSpacing(5)

        self.logTextBox = _QTextEditLogger(self)
        font_logging = self.logTextBox.widget.font()
        font_logging.setPointSize(8)
        self.logTextBox.widget.setFont(font_logging)
        formatting.logger_nxcals().addHandler(self.logTextBox)

        layout.addWidget(self.logTextBox.widget, 0, 0, 1, 1)

        return panel

    def create_nav_panel(self):

        panel = titledborderwidget.TitledBorderWidget("Data Selection")

        layout = QGridLayout(panel)
        layout.setContentsMargins(15, 25, 10, 10)
        layout.setSpacing(5)

        self.listWidget = QListWidget()

        font_info = self.listWidget.font()
        font_info.setPointSize(8)
        self.listWidget.setFont(font_info)

        layout.addWidget(self.listWidget, 0, 0, 1, 2)

        panel.setMaximumWidth(300)

        w = QWidget()
        layoutsub = QGridLayout(w)
        layoutsub.setContentsMargins(0, 0, 0, 0)
        layoutsub.setSpacing(5)

        layout.addWidget(w, 1, 0)

        self._scroll = _QScrollWidget()
        self._scroll.setMinimumWidth(20)
        layout.addWidget(self._scroll, 0, 3)

        self._up = QPushButton(fa.icons["arrow-up"])
        self._up.setToolTip("Previous list")
        self._up.setFont(self.font_fa)
        self._up.setMaximumWidth(25)
        self._up.setMinimumWidth(25)
        self._up.setMinimumHeight(25)
        self._up.setMaximumHeight(25)
        self._up.mousePressEvent = self.up_list

        self._down = QPushButton(fa.icons["arrow-down"])
        self._down.setToolTip("Next list")
        self._down.setFont(self.font_fa)
        self._down.setMaximumWidth(25)
        self._down.setMinimumWidth(25)
        self._down.setMinimumHeight(25)
        self._down.setMaximumHeight(25)
        self._down.mousePressEvent = self.down_list

        layoutsub.addWidget(self._up, 0, 1, Qt.AlignRight)
        layoutsub.addWidget(self._down, 0, 2, Qt.AlignLeft)

        self._snr = QCheckBox("SNR")
        self._snr.setToolTip("SNR filter")

        layoutsub.addWidget(self._snr, 0, 3)

        self._add = QPushButton(fa.icons["arrow-right"])
        self._add.setToolTip("Add to data browser")

        self._add.setFont(self.font_fa)
        self._add.setMaximumWidth(40)
        self._add.setMinimumHeight(40)
        self._add.mousePressEvent = self.add

        layoutsub.addWidget(self._add, 0, 4, Qt.AlignRight)

        layout.setRowStretch(0, 1)
        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(3, 0)
        layout.setRowStretch(1, 0)

        self.fill_nav([])

        return panel

    def change_count(self):
        """
        Change count selection.
        :return:
        """
        if self._enum_count.currentText() == "all":
            count = 0
        elif self._enum_count.currentText() == "none":
            count = -1
        else:
            count = int(self._enum_count.currentText())
        ind = 0
        for cb in self._cb_pu:
            if count > 0:
                if ind < count:
                    cb.setChecked(True)
                else:
                    cb.setChecked(False)
            else:
                if count == 0:
                    cb.setChecked(True)
                else:
                    cb.setChecked(False)
            ind = ind + 1

        self.check_scroll()

    def check_scroll(self):

        ind_last_true = -1
        ind_first_true = -1

        for i, cb in enumerate(self._cb_pu):
            if cb.isChecked():
                ind_last_true = i + 1

        for i, cb in enumerate(self._cb_pu):
            if cb.isChecked():
                ind_first_true = i
                break

        self._scroll.new_count(sizetot=len(self._cb_pu),
                               index=ind_first_true,
                               sizedelta=(ind_last_true - ind_first_true))

    def up_list(self, e):

        if not self._add.isEnabled():
            return

        boul = []
        boul_tmp = 2 * [False] * len(self._cb_pu)

        ind_last_true = -1
        ind_first_true = -1

        for i, cb in enumerate(self._cb_pu):
            boul.append(cb.isChecked())
            if cb.isChecked():
                ind_last_true = i + 1

        for i, cb in enumerate(self._cb_pu):
            if cb.isChecked():
                ind_first_true = i
                break

        if ind_last_true == -1:
            return

        if ind_first_true == -1:
            return

        if ind_first_true == 0:
            return

        for cb in self._cb_pu:
            cb.setChecked(False)

        delta = ind_last_true - ind_first_true

        boul_tmp[:] = boul[delta:]

        for i, b in enumerate(boul_tmp):
            if i < len(self._cb_pu):
                if b:
                    self._cb_pu[i].setChecked(True)

        # self.add(None)

        self.check_scroll()

    def down_list(self, e):

        if not self._add.isEnabled():
            return

        boul = []
        boul_tmp = 2 * [False] * len(self._cb_pu)

        ind_last_true = -1
        ind_first_true = -1

        for i, cb in enumerate(self._cb_pu):
            boul.append(cb.isChecked())
            if cb.isChecked():
                ind_last_true = i + 1

        for i, cb in enumerate(self._cb_pu):
            if cb.isChecked():
                ind_first_true = i
                break

        if ind_last_true == -1:
            return

        if ind_first_true == -1:
            return

        if ind_last_true == len(self._cb_pu):
            return

        for cb in self._cb_pu:
            cb.setChecked(False)

        delta = ind_last_true - ind_first_true

        boul_tmp[ind_first_true + delta:] = boul[ind_first_true:]

        for i, b in enumerate(boul_tmp):
            if i < len(self._cb_pu):
                if b:
                    self._cb_pu[i].setChecked(True)

        self.check_scroll()

    def fill_nav(self, list_time):

        self.listWidget.clear()

        font_info = self.listWidget.font()
        font_info.setPointSize(8)

        self._cb_pu = []
        self._icb_pu = {}
        self._lcb_pu = []

        ind = 1

        # top labels

        layoutWidget = QWidget()

        itemtop = QListWidgetItem()

        layoutC = QGridLayout(layoutWidget)
        layoutC.setContentsMargins(2, 2, 2, 2)
        layoutC.setSpacing(2)

        count_label = QLabel("")
        count_label.setFont(font_info)
        count_label.setAlignment(Qt.AlignRight)

        if self._enum_count is None:
            self._enum_count = QComboBox()
            self._enum_count.currentTextChanged.connect(self.change_count)
            self._enum_count.addItem("all")
            self._enum_count.addItem("none")
            self._enum_count.addItem("300")
            self._enum_count.addItem("100")
            self._enum_count.addItem("50")
            self._enum_count.addItem("20")
            self._enum_count.addItem("10")
            self._enum_count.addItem("5")
            self._enum_count.addItem("1")
            self._list_count = QListView(self._enum_count)
            self._enum_count.setView(self._list_count)
            self._enum_count.setEditable(False)
            self._enum_count.setFont(font_info)

        layoutC.addWidget(self._enum_count, 0, 0)

        layoutC.addWidget(count_label, 0, 1)

        itemtop.setSizeHint(layoutWidget.sizeHint())

        self.listWidget.addItem(itemtop)
        self.listWidget.setItemWidget(itemtop, layoutWidget)

        # checkboxes
        count = 0

        if self._enum_count.currentText() == "all":
            count = 0
        elif self._enum_count.currentText() == "none":
            count = -1
        else:
            count = int(self._enum_count.currentText())

        for ti in list_time:

            item = QListWidgetItem()

            try:
                str_ti = formatting.format_value_to_string(ti, type_format="date")
            except Exception as e:
                str_ti = str(ti)
                print(e)

            checkbox = QCheckBox(str_ti)
            checkbox.setFont(font_info)
            if count > 0:
                if ind < (int(self._enum_count.currentText()) + 1):
                    checkbox.setChecked(True)
            else:
                if count == 0:
                    checkbox.setChecked(True)
                else:
                    checkbox.setChecked(False)

            ind_label = QLabel("[" + str(ind) + "]")
            ind_label.setFont(font_info)

            self._icb_pu[str_ti] = ti
            self._cb_pu.append(checkbox)
            self._lcb_pu.append(ind_label)

            layoutWidget = QWidget()

            layoutC = QGridLayout(layoutWidget)
            layoutC.setContentsMargins(2, 2, 2, 2)
            layoutC.setSpacing(2)

            layoutC.addWidget(checkbox, ind, 0)
            layoutC.addWidget(ind_label, ind, 1)

            item.setSizeHint(layoutWidget.sizeHint())

            self.listWidget.addItem(item)
            self.listWidget.setItemWidget(item, layoutWidget)

            ind = ind + 1

            layoutC.setColumnStretch(0, 1)
            layoutC.setColumnStretch(1, 0)

        count_label.setText("[" + str(len(list_time)) + "]")

        self.check_scroll()

    def dark_(self):
        """
        Set dark theme
        """
        self._dark = True

        qss = """
              color: """ + Colors.STR_COLOR_WHITE + """;
              QCheckBox::indicator {
                  border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;            
              }       
              QCheckBox::indicator:checked {
                  max-width:14;
                  max-height:14;
                  border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                  background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
              }
              QCheckBox {
                  max-width:14;
                  max-height:14;
                  color: """ + Colors.STR_COLOR_WHITE + """;
                  background-color: """ + Colors.STR_COLOR_BLACK + """;            
              }
              """
        self._snr.setStyleSheet(qss)

        self.logTextBox.widget.setStyleSheet("background-color:" + Colors.STR_COLOR_BLACK + ";")
        self.infoTextBox.setStyleSheet("background-color:" + Colors.STR_COLOR_BLACK + ";")

        self._label_class.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_device.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_property.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_variable.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_user.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")

        for l in self._lcb_pu:
            l.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")

        self._le_classes.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        self._get_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._get_variables.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._up.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._down.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._plus.setStyleSheet(
            "background-color:orange;color:" + Colors.STR_COLOR_WHITE + ";")
        self._add.setStyleSheet(
            "background-color:orange;color:" + Colors.STR_COLOR_WHITE + ";")
        self._get_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._enum_properties.setStyleSheet(
            "border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self._enum_devices.setStyleSheet(
            "border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self._enum_variables.setStyleSheet(
            "border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._list_variables.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")
        self._enum_users.setStyleSheet(
            "border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_L2BLACK + ";")

        self.save_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        self.open_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

        self._meta.dark_()

    def light_(self):
        """
        Set light theme
        """
        self._dark = False

        self._meta.light_()

        qss = """
               color: """ + Colors.STR_COLOR_BLACK + """;
               QCheckBox::indicator {
                   border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;            
               }       
               QCheckBox::indicator:checked {
                   max-width:14;
                   max-height:14;
                   border:0px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;
                   background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
               }          
               QCheckBox {
                   max-width:14;
                   max-height:14;
                   color: """ + Colors.STR_COLOR_BLACK + """;
                   background-color: """ + Colors.STR_COLOR_WHITE + """;            
               }
               """
        self._snr.setStyleSheet(qss)

        self.logTextBox.widget.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")
        self.infoTextBox.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")

        self._label_class.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_device.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_property.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_variable.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_user.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")

        for l in self._lcb_pu:
            l.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")

        self._le_classes.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        self._get_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._get_variables.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._plus.setStyleSheet(
            "background-color:orange;color:" + Colors.STR_COLOR_WHITE + ";border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        self._add.setStyleSheet(
            "background-color:orange;color:" + Colors.STR_COLOR_WHITE + ";border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        self._get_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._up.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._down.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")
        self._enum_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._list_properties.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._enum_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._list_devices.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._enum_variables.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._list_variables.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._enum_users.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")

        self.save_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        self.open_button.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

    def search(self, e):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        try:

            formatting.print_nxcals("Search!", level=logging.DEBUG)

            self.fill_nav([])

            instances = []

            if self._enum_variables.currentText() == "*":
                for variable in self._variables:
                    instances.append(variable)
            else:
                instances.append(self._enum_variables.currentText())

            for instance in instances:

                user = self._enum_users.currentText()

                if self._meta.is_fill():
                    fillnumber = self._meta.get_fill()
                    start, end = self.nxcalscomm.get_time_window_for_fill(fillnumber)

                    self._meta.set_tfrom(start)
                    self._meta.set_tto(end)

                stfrom = self._meta.get_from_utc()
                stto = self._meta.get_to_utc()

                is_vector = False
                instance_sc = instance

                if (":IMAGE" in instance) and self._le_classes.text() == "BTVDC":
                    instance_sc = instance.replace(":IMAGE", ":imagePositionSet1")
                elif (":IMAGE" in instance) and self._le_classes.text() == "BTVI":
                    instance_sc = instance.replace(":IMAGE", ":nbPtsInSet1")
                elif (":IMAGE" in instance) and self._le_classes.text() == "BSISO":
                    instance_sc = instance.replace(":IMAGE", ":projDataSet2")
                elif (":IMAGE" in instance) and self._le_classes.text() == "BEMCPS":
                    instance_sc = instance.replace(":IMAGE", ":projDataSet1")

                if (user is not None) and (user != "None"):
                    stat = self.nxcalscomm.get_stat(instance_sc, tfrom=stfrom, tto=stto, selector=user)
                else:
                    stat = self.nxcalscomm.get_stat(instance_sc, tfrom=stfrom, tto=stto)

                if stat is not None and stat["ValueCount"] is not None and stat["ValueCount"] > 0:

                    is_vector = stat["vector"]

                    if (":IMAGE" in instance) and self._le_classes.text() == "BTVDC":
                        is_vector = True
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BTVI":
                        is_vector = True
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BSISO":
                        is_vector = True
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BEMCPS":
                        is_vector = True

                    self.infoTextBox.append_info("Search :" + instance, stat["ValueCount"])

                    if is_vector:
                        if stat["ValueCount"] > 1500:
                            formatting.print_nxcals("Count too big (max 1500) ! ", stat["ValueCount"],
                                                    level=logging.ERROR)
                            formatting.print_nxcals("Finished!", level=logging.DEBUG)
                            QApplication.restoreOverrideCursor()
                            return
                    else:
                        if stat["ValueCount"] > 3000000:
                            formatting.print_nxcals("Count too big (max 3000000) ! ", stat["ValueCount"],
                                                    level=logging.ERROR)
                            formatting.print_nxcals("Finished!", level=logging.DEBUG)
                            QApplication.restoreOverrideCursor()
                            return
                else:
                    formatting.print_nxcals("No data ! ", level=logging.ERROR)
                    formatting.print_nxcals("Finished!", level=logging.DEBUG)
                    QApplication.restoreOverrideCursor()
                    return

                if is_vector:

                    if instance_sc is None:

                        result = self.nxcalscomm.get_time(name=instance,
                                                          tfrom=stfrom,
                                                          tto=stto,
                                                          selector=user)

                    else:

                        result = self.nxcalscomm.get_time(name=instance_sc,
                                                          tfrom=stfrom,
                                                          tto=stto,
                                                          selector=user)

                    self.infoTextBox.append_info("Search : " + instance, len(result["data"][0]))

                    self.fill_nav(result["data"][0])

                else:

                    if stat is not None or stat["ValueCount"] is not None:
                        self.add_scalar()
                    else:
                        formatting.print_nxcals("Nothing found", level=logging.ERROR)

            formatting.print_nxcals("Finished!", level=logging.DEBUG)

        except:
            pass

        QApplication.restoreOverrideCursor()

    def add_scalar(self):

        QApplication.setOverrideCursor(Qt.WaitCursor)

        try:

            formatting.print_nxcals("Go!", level=logging.CRITICAL)

            instances = []

            if self._enum_variables.currentText() == "*":
                for variable in self._variables:
                    instances.append(variable)
            else:
                instances.append(self._enum_variables.currentText())

            user = self._enum_users.currentText()

            for instance in instances:

                result = None

                if self._meta.is_fill():
                    fillnumber = self._meta.get_fill()
                    start, end = self.nxcalscomm.get_time_window_for_fill(fillnumber)

                    self._meta.set_tfrom(start)
                    self._meta.set_tto(end)

                stfrom = self._meta.get_from_utc()
                stto = self._meta.get_to_utc()

                formatting.print_nxcals("Get", instance, level=logging.DEBUG)

                result = self.nxcalscomm.get_data(name=instance,
                                                  tfrom=stfrom,
                                                  tto=stto,
                                                  selector=user)

                if result is None:
                    formatting.print_nxcals("No data!", level=logging.ERROR)
                    self._add.setDisabled(False)
                    QApplication.restoreOverrideCursor()
                    return

                try:
                    len_result = len(result["data"][0])
                except:
                    formatting.print_nxcals("No data!", level=logging.ERROR)
                    self._add.setDisabled(False)
                    QApplication.restoreOverrideCursor()
                    return

                suser = user

                if (user is None) or (user == "None"):
                    user = None
                    suser = "None"

                name = instance + "/" + self._meta.get_from() + "/" + self._meta.get_to() + "/" + suser + "/" + str(
                    len(result["data"][0]))

                key = "nxcals" + "$" + \
                      self._le_classes.text() + "$" + \
                      self._enum_devices.currentText() + "$" + \
                      instance + "$" + \
                      suser + "$" + \
                      self._meta.get_from() + "$" + \
                      self._meta.get_to()

                formatting.print_nxcals(name, level=logging.WARNING)

                if key in self._names:
                    self._parent.mdi.delete_sub(key)

                self._names.append(key)

                if (result is not None) and (result["split"][2] >= 1) and (len(result["data"]) > 0):
                    pass
                else:
                    if (result is None) or len(result["data"]) == 0:
                        formatting.print_nxcals("No data (case 2)!", level=logging.ERROR)
                        QApplication.restoreOverrideCursor()
                        self._add.setDisabled(False)
                        return

                if (result["data"][0] is None) or (len(result["data"][0]) == 0):
                    formatting.print_nxcals("No data (case 3)!", level=logging.ERROR)
                    QApplication.restoreOverrideCursor()
                    self._add.setDisabled(False)
                    return

                nxcals_widget = nxcalswidget.NXCALSWidget(self,
                                                          title="",
                                                          name_device=self._enum_devices.currentText(),
                                                          name_variable=instance,
                                                          name_cycle=user,
                                                          tfrom=result["split"][0],
                                                          tto=result["split"][1],
                                                          vector=False)

                self._parent.add_dtw(nxcals_widget, key, name_full=key)

                nxcals_widget.set_data(result["data"][0],
                                       valuex=result["data"][1],
                                       name=instance,
                                       name_parent=instance)

                if self._dark:
                    nxcals_widget.dark_()
                else:
                    nxcals_widget.light_()

                self.infoTextBox.append_info(name, len(result["data"][0]))

                add_key = True

                if add_key:
                    self._parent._settings[key] = {}
                    self._parent._settings[key]["value"] = key
                    self._parent._settings[key]["functions"] = {}
                    self._parent._settings[key]["times"] = []

            formatting.print_nxcals("Finished!", level=logging.CRITICAL)

        except:
            pass

        QApplication.restoreOverrideCursor()

    def add(self, e):
        """
        Add property(ies) panels to parent tab
        """

        QApplication.setOverrideCursor(Qt.WaitCursor)

        try:

            formatting.print_nxcals("Go!", level=logging.DEBUG)

            instances = []

            if self._enum_variables.currentText() == "*":
                for variable in self._variables:
                    instances.append(variable)
            else:
                instances.append(self._enum_variables.currentText())

            for instance in instances:

                user = self._enum_users.currentText()

                list_time = []

                result_width = None
                result_p2 = None
                result_n = None

                count = 0

                try:

                    hole = False

                    for cb in self._cb_pu:
                        if cb.isChecked():
                            time_cb = float(formatting.format_value_to_string(cb.text(), type_format="idate"))
                            date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ', timespec='milliseconds')
                            if len(list_time) < 300:
                                list_time.append(date_cb)
                            else:
                                cb.setChecked(False)
                                hole = True
                        else:
                            hole = True

                    if len(list_time) == len(self._cb_pu):
                        list_time = []
                        time_cb = float(formatting.format_value_to_string(self._cb_pu[0].text(), type_format="idate"))
                        date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ', timespec='milliseconds')
                        list_time.append(date_cb)
                        list_time.append("all")
                        time_cb = float(formatting.format_value_to_string(self._cb_pu[-1].text(), type_format="idate"))
                        date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ', timespec='milliseconds')
                        list_time.append(date_cb)

                    if hole:
                        list_time = []
                        add_hole = True
                        cb_prev = None
                        for cb in self._cb_pu:
                            if cb.isChecked() and add_hole:
                                add_hole = False
                                time_cb = float(formatting.format_value_to_string(cb.text(), type_format="idate"))
                                date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ', timespec='milliseconds')
                                list_time.append(date_cb)
                                list_time.append("all")
                            elif not cb.isChecked() and not add_hole:
                                if cb_prev is not None:
                                    time_cb = float(
                                        formatting.format_value_to_string(cb_prev.text(), type_format="idate"))
                                    date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ',
                                                                                           timespec='milliseconds')
                                    list_time.append(date_cb)
                                    add_hole = True
                            cb_prev = cb

                        if not add_hole:
                            time_cb = float(
                                formatting.format_value_to_string(self._cb_pu[-1].text(), type_format="idate"))
                            date_cb = datetime.utcfromtimestamp(time_cb).isoformat(sep=' ', timespec='milliseconds')
                            list_time.append(date_cb)

                except Exception as e:
                    print(e)

                if len(list_time) > 0:

                    suser = user

                    if user == "None":
                        user = None
                        suser = "None"

                    stfrom = str(list_time[0])
                    stto = str(list_time[len(list_time) - 1])

                    # SPECIFC CASES #

                    if (":IMAGE" in instance) and self._le_classes.text() == "BTVDC":

                        formatting.print_nxcals("Get BTVDC imagePositionSet1")
                        instance_p1 = instance.replace(":IMAGE", ":imagePositionSet1")
                        result_width = self.nxcalscomm.get_data(name=instance_p1,
                                                                tfrom=list_time[0],
                                                                tto=list_time[len(list_time) - 1],
                                                                selector=user,
                                                                list_time=list_time)

                        formatting.print_nxcals("Get BTVDC imagePositionSet1 length", str(len(result_width["data"][0])))

                        if (result_width is not None) and (len(result_width["data"]) > 0):

                            formatting.print_nxcals("Get BTVDC imagePositionSet2")
                            instance_p2 = instance.replace(":IMAGE", ":imagePositionSet2")
                            result_p2 = self.nxcalscomm.get_data(name=instance_p2,
                                                                 tfrom=list_time[0],
                                                                 tto=list_time[len(list_time) - 1],
                                                                 selector=user,
                                                                 list_time=list_time)

                            formatting.print_nxcals("Get BTVDC imagePositionSet2 length",
                                                    str(len(result_p2["data"][0])))

                            if (result_width is not None) and (len(result_width["data"]) > 0):
                                instance_real = instance.replace(":IMAGE", ":image2D")
                                formatting.print_nxcals("Get BTVDC", instance_real)
                                result = self.nxcalscomm.get_data(name=instance_real,
                                                                  tfrom=list_time[0],
                                                                  tto=list_time[len(list_time) - 1],
                                                                  selector=user,
                                                                  list_time=list_time)
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BTVI":

                        formatting.print_nxcals("Get BTVI width")
                        instance_width = instance.replace(":IMAGE", ":nbPtsInSet1")
                        result_width = self.nxcalscomm.get_data(name=instance_width,
                                                                tfrom=list_time[0],
                                                                tto=list_time[len(list_time) - 1],
                                                                selector=user,
                                                                list_time=list_time)

                        if (result_width is not None) and (len(result_width["data"]) > 0):
                            formatting.print_nxcals("Get BTVI imagePositionSet1")
                            instance_p1 = instance.replace(":IMAGE", ":imagePositionSet1")
                            result_p1 = self.nxcalscomm.get_data(name=instance_p1,
                                                                 tfrom=list_time[0],
                                                                 tto=list_time[len(list_time) - 1],
                                                                 selector=user,
                                                                 list_time=list_time)

                            formatting.print_nxcals("Get BTVI imagePositionSet2")
                            instance_p2 = instance.replace(":IMAGE", ":imagePositionSet2")
                            result_p2 = self.nxcalscomm.get_data(name=instance_p2,
                                                                 tfrom=list_time[0],
                                                                 tto=list_time[len(list_time) - 1],
                                                                 selector=user,
                                                                 list_time=list_time)

                            formatting.print_nxcals("Get BTVI nbimages")
                            instance_n = instance.replace(":IMAGE", ":nbImages")
                            result_n = self.nxcalscomm.get_data(name=instance_n,
                                                                tfrom=list_time[0],
                                                                tto=list_time[len(list_time) - 1],
                                                                selector=user,
                                                                list_time=list_time)

                            instance_real = instance.replace(":IMAGE", ":imageSet")
                            formatting.print_nxcals("Get BTVI", instance_real)
                            result = self.nxcalscomm.get_data(name=instance_real,
                                                              tfrom=list_time[0],
                                                              tto=list_time[len(list_time) - 1],
                                                              selector=user,
                                                              list_time=list_time)
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BSISO":

                        formatting.print_nxcals("Get BSISO projDataSet2")

                        instance_width = instance.replace(":IMAGE", ":projDataSet2")

                        result_width = self.nxcalscomm.get_data(name=instance_width,
                                                                tfrom=list_time[0],
                                                                tto=list_time[len(list_time) - 1],
                                                                selector=user,
                                                                list_time=list_time)

                        if (result_width is not None) and (len(result_width["data"][0]) > 0):
                            instance_real = instance.replace(":IMAGE", ":projDataSet1")

                            formatting.print_nxcals("Get BSISO", instance_real)

                            result = self.nxcalscomm.get_data(name=instance_real,
                                                              tfrom=list_time[0],
                                                              tto=list_time[len(list_time) - 1],
                                                              selector=user,
                                                              list_time=list_time)
                    elif (":IMAGE" in instance) and self._le_classes.text() == "BEMCPS":

                        formatting.print_nxcals("Get BEMCPS projDataSet1")

                        instance_1 = instance.replace(":IMAGE", ":projDataSet1")

                        if "H:" in instance:
                            instance_width = instance_1.replace("H:", "V:")
                        else:
                            instance_width = instance_1.replace("V:", "H:")

                        result_width = self.nxcalscomm.get_data(name=instance_width,
                                                                tfrom=list_time[0],
                                                                tto=list_time[len(list_time) - 1],
                                                                selector=user,
                                                                list_time=list_time)

                        if (result_width is not None) and (len(result_width["data"][0]) > 0):
                            instance_2 = instance_1.replace(":IMAGE", ":projDataSet1")

                            formatting.print_nxcals("Get BEMCPS", instance_2)

                            result = self.nxcalscomm.get_data(name=instance_2,
                                                              tfrom=list_time[0],
                                                              tto=list_time[len(list_time) - 1],
                                                              selector=user,
                                                              list_time=list_time)

                    # END SPECIFC CASES #

                    else:

                        result = self.nxcalscomm.get_data(name=instance,
                                                          tfrom=list_time[0],
                                                          tto=list_time[len(list_time) - 1],
                                                          selector=user,
                                                          list_time=list_time)

                    if (result is None) or (len(result["data"][0]) == 0):
                        formatting.print_nxcals("No data!", level=logging.ERROR)
                        formatting.print_nxcals("Finished!", level=logging.CRITICAL)
                        self._add.setDisabled(False)
                        QApplication.restoreOverrideCursor()
                        return

                    count = len(result["data"][0])

                    suser = user

                    if (user is None) or user == "None":
                        suser = "None"

                    name = instance + "/" + self._meta.get_from() + "/" + self._meta.get_to() + "/" + suser + "/" + str(
                        count)

                    key = "nxcals" + "$" + \
                          self._le_classes.text() + "$" + \
                          self._enum_devices.currentText() + "$" + \
                          instance + "$" + \
                          suser + "$" + \
                          self._meta.get_from() + "$" + \
                          self._meta.get_to()

                    formatting.print_nxcals(name, level=logging.WARNING)

                    if key in self._names:
                        self._parent.mdi.delete_sub(key)

                    self._names.append(key)

                    if result_width is not None:

                        if self._le_classes.text() == "BSISO":

                            if len(result_width["data"][0][0]) > 0:
                                nwidth_mosaic = 10
                                if len(result["data"][0]) < 30:
                                    nwidth_mosaic = 5
                                elif len(result["data"][0]) < 10:
                                    nwidth_mosaic = 2

                                data_to_see = []
                                time_to_see = []

                                for i in range(0, len(result["data"][0])):
                                    time_1 = result["data"][1][i]
                                    try:
                                        index_2 = result_width["data"][1].index(time_1)
                                    except:
                                        continue
                                    data1 = np.array(result_width["data"][0][index_2])
                                    data2 = np.array(result["data"][0][i])

                                    snr1 = self.signaltonoise_dB(data1)
                                    snr2 = self.signaltonoise_dB(data2)

                                    if (not self._snr.isChecked()) or (snr1 < -5 and snr2 < -5):
                                        data_to_see.append(np.dot(data1[:, None], data2[None, :]))
                                        time_to_see.append(time_1)

                                if len(data_to_see) == 0:
                                    self._add.setDisabled(False)
                                    QApplication.restoreOverrideCursor()
                                    return

                                from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
                                nxcals_widget = profilewidget.ProfileWidget(self,
                                                                            profile=False,
                                                                            multi=True,
                                                                            mosaic=True,
                                                                            maxcount_mosaic=len(data_to_see),
                                                                            nwidth_mosaic=nwidth_mosaic,
                                                                            bascule_tab=False,
                                                                            positionH=Qt.AlignBottom,
                                                                            positionV=Qt.AlignTop
                                                                            )
                                self._parent.add_dtw(nxcals_widget, key, name_full=key)

                                for i in range(len(data_to_see)):
                                    nxcals_widget.set_data(data_to_see[i], time=time_to_see[i])

                            else:

                                nxcals_widget = nxcalswidget.NXCALSWidget(self,
                                                                          title="",
                                                                          name_device=self._enum_devices.currentText(),
                                                                          name_variable=instance,
                                                                          name_cycle=user,
                                                                          tfrom=result["split"][0],
                                                                          tto=result["split"][1],
                                                                          vector=True)

                                self._parent.add_dtw(nxcals_widget, key, name_full=key)

                                nxcals_widget.set_data(result["data"][0],
                                                       valuex=result["data"][1],
                                                       name=instance,
                                                       name_parent=instance)

                        elif self._le_classes.text() == "BEMCPS":

                            if len(result_width["data"][0][0]) > 0:
                                nwidth_mosaic = 10
                                if len(result["data"][0]) < 30:
                                    nwidth_mosaic = 5
                                elif len(result["data"][0]) < 10:
                                    nwidth_mosaic = 2

                                data_to_see = []
                                time_to_see = []

                                for i in range(0, len(result["data"][0])):
                                    time_1 = result["data"][1][i]
                                    try:
                                        index_2 = result_width["data"][1].index(time_1)
                                    except:
                                        continue

                                    data1 = np.array(result_width["data"][0][index_2])
                                    data2 = np.array(result["data"][0][i])

                                    snr1 = self.signaltonoise_dB(data1)
                                    snr2 = self.signaltonoise_dB(data2)

                                    if (not self._snr.isChecked()) or (snr1 < -5 and snr2 < -5):
                                        data_to_see.append(np.dot(data1[:, None], data2[None, :]))
                                        time_to_see.append(time_1)

                                if len(data_to_see) == 0:
                                    self._add.setDisabled(False)
                                    QApplication.restoreOverrideCursor()
                                    return

                                from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
                                nxcals_widget = profilewidget.ProfileWidget(self,
                                                                            profile=False,
                                                                            multi=True,
                                                                            mosaic=True,
                                                                            maxcount_mosaic=len(data_to_see),
                                                                            nwidth_mosaic=nwidth_mosaic,
                                                                            bascule_tab=False,
                                                                            positionH=Qt.AlignBottom,
                                                                            positionV=Qt.AlignTop
                                                                            )

                                self._parent.add_dtw(nxcals_widget, key, name_full=key)

                                for i in range(len(data_to_see)):
                                    nxcals_widget.set_data(data_to_see[i], time=time_to_see[i])

                            else:

                                nxcals_widget = nxcalswidget.NXCALSWidget(self,
                                                                          title="",
                                                                          name_device=self._enum_devices.currentText(),
                                                                          name_variable=instance,
                                                                          name_cycle=user,
                                                                          tfrom=result["split"][0],
                                                                          tto=result["split"][1],
                                                                          vector=True)

                                self._parent.add_dtw(nxcals_widget, key, name_full=key)

                                nxcals_widget.set_data(result["data"][0],
                                                       valuex=result["data"][1],
                                                       name=instance,
                                                       name_parent=instance)

                        elif self._le_classes.text() == "BTVDC":

                            nwidth_mosaic = 5
                            if len(result["data"][0]) < 10:
                                nwidth_mosaic = 2

                            data_to_see = []
                            time_to_see = []

                            for i in range(0, len(result["data"][0])):
                                time_1 = result["data"][1][i]

                                width = int(len(result_width["data"][0][0]))
                                height = int(len(result["data"][0][i]) / (width))
                                data = np.array(result["data"][0][i]).reshape(height, width)

                                snr = self.signaltonoise_dB(data)

                                if (not self._snr.isChecked()) or (snr < -5):
                                    data_to_see.append(data)
                                    time_to_see.append(time_1)

                            if len(data_to_see) == 0:
                                self._add.setDisabled(False)
                                QApplication.restoreOverrideCursor()
                                return

                            from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
                            nxcals_widget = profilewidget.ProfileWidget(self,
                                                                        profile=False,
                                                                        multi=True,
                                                                        mosaic=True,
                                                                        maxcount_mosaic=len(data_to_see),
                                                                        nwidth_mosaic=nwidth_mosaic,
                                                                        bascule_tab=False,
                                                                        positionH=Qt.AlignBottom,
                                                                        positionV=Qt.AlignTop
                                                                        )

                            self._parent.add_dtw(nxcals_widget, key, name_full=key)

                            for i in range(len(data_to_see)):
                                nxcals_widget.set_data(data_to_see[i], time=time_to_see[i])

                            nxcals_widget._mosaic_widget.handle_selection_changed([0])

                            # x axis

                            offset1 = 0
                            offset2 = 0
                            scfactor1 = 1
                            scfactor2 = 1

                            try:
                                if result_width["data"][0][-1][1] > result_width["data"][0][-1][0]:
                                    offset1 = -result_width["data"][0][-1][0]
                                    scfactor1 = result_width["data"][0][-1][1] - result_width["data"][0][-1][0]
                                else:
                                    offset1 = -result_width["data"][0][-1][-1]
                                    scfactor1 = result_width["data"][0][-1][-2] - result_width["data"][0][-1][-1]

                                # y axis

                                if result_p2["data"][0][-1][1] > result_p2["data"][0][-1][0]:
                                    offset2 = -result_p2["data"][0][-1][0]
                                    scfactor2 = result_p2["data"][0][-1][1] - result_p2["data"][0][-1][0]
                                else:
                                    offset2 = -result_p2["data"][0][-1][-1]
                                    scfactor2 = result_p2["data"][0][-1][-2] - result_p2["data"][0][-1][-1]
                            except:
                                pass

                            nxcals_widget._mosaic_widget._dialog.profile_widget.change_scale(
                                xscale=scfactor1,
                                yscale=scfactor2,
                                xoffset=offset1,
                                yoffset=offset2)

                            nxcals_widget._mosaic_widget._dialog.hide()

                        elif self._le_classes.text() == "BTVI":
                            nwidth_mosaic = 5
                            if len(result["data"][0]) < 10:
                                nwidth_mosaic = 2

                            data_to_see = []
                            time_to_see = []

                            for i in range(0, len(result["data"][0])):
                                nbimg = int(result_n["data"][0][i])
                                if nbimg > 0:

                                    width = int(result_width["data"][0][i])
                                    height = int(len(result["data"][0][i]) / (width * nbimg))

                                    add = False

                                    for j in range(0, nbimg):
                                        data = np.array(
                                            result["data"][0][i][j * width * height:(j + 1) * width * height]).reshape(
                                            height, width)
                                        snr = self.signaltonoise_dB(data)
                                        if (not self._snr.isChecked()) or (snr < -5):
                                            add = True

                                    if add:
                                        for j in range(0, nbimg):
                                            data = np.array(result["data"][0][i][
                                                            j * width * height:(j + 1) * width * height]).reshape(
                                                height, width)
                                            data_to_see.append(data)
                                            time_to_see.append(result["data"][1][i] + j)

                            if len(data_to_see) == 0:
                                self._add.setDisabled(False)
                                QApplication.restoreOverrideCursor()
                                return

                            from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
                            nxcals_widget = profilewidget.ProfileWidget(self,
                                                                        profile=False,
                                                                        multi=True,
                                                                        mosaic=True,
                                                                        maxcount_mosaic=len(data_to_see),
                                                                        nwidth_mosaic=nwidth_mosaic,
                                                                        bascule_tab=False,
                                                                        positionH=Qt.AlignBottom,
                                                                        positionV=Qt.AlignTop
                                                                        )
                            self._parent.add_dtw(nxcals_widget, key, name_full=key)

                            for i in range(len(data_to_see)):
                                nxcals_widget.set_data(data_to_see[i], time=time_to_see[i])

                            nxcals_widget._mosaic_widget.handle_selection_changed([0])

                            # x axis

                            if result_p1["data"][0][-1][1] > result_p1["data"][0][-1][0]:
                                offset1 = -result_p1["data"][0][-1][0]
                                scfactor1 = result_p1["data"][0][-1][1] - result_p1["data"][0][-1][0]
                            else:
                                offset1 = -result_p1["data"][0][-1][-1]
                                scfactor1 = result_p1["data"][0][-1][-2] - result_p1["data"][0][-1][-1]

                            # y axis

                            if result_p2["data"][0][-1][1] > result_p2["data"][0][-1][0]:
                                offset2 = -result_p2["data"][0][-1][0]
                                scfactor2 = result_p2["data"][0][-1][1] - result_p2["data"][0][-1][0]
                            else:
                                offset2 = -result_p2["data"][0][-1][-1]
                                scfactor2 = result_p2["data"][0][-1][-2] - result_p2["data"][0][-1][-1]

                            nxcals_widget._mosaic_widget._dialog.profile_widget.change_scale(
                                xscale=scfactor1,
                                yscale=scfactor2,
                                xoffset=offset1,
                                yoffset=offset2)

                            nxcals_widget._mosaic_widget._dialog.hide()

                    # END SPECIFIC CASES #

                    else:

                        nxcals_widget = nxcalswidget.NXCALSWidget(self,
                                                                  title="",
                                                                  name_device=self._enum_devices.currentText(),
                                                                  name_variable=instance,
                                                                  name_cycle=user,
                                                                  tfrom=list_time[0],
                                                                  tto=list_time[len(list_time) - 1],
                                                                  vector=True)

                        self._parent.add_dtw(nxcals_widget, key, name_full=key)

                        nxcals_widget.set_data(result["data"][0],
                                               valuex=result["data"][1],
                                               name=instance,
                                               name_parent=instance)

                    if self._dark:
                        nxcals_widget.dark_()
                    else:
                        nxcals_widget.light_()

                    self.infoTextBox.append_info(name, len(result["data"][0]))

                    add_key = True

                    if add_key:
                        self._parent._settings[key] = {}
                        self._parent._settings[key]["value"] = key
                        self._parent._settings[key]["functions"] = {}

                        list_time = []
                        for cb in self._cb_pu:
                            if cb.isChecked():
                                list_time.append(cb.text())
                        self._parent._settings[key]["times"] = list_time

        except Exception as e:
            print(e)
            pass

        formatting.print_nxcals("Finished!", level=logging.CRITICAL)

        self._add.setDisabled(False)
        QApplication.restoreOverrideCursor()

    def signaltonoise_dB(self, a):

        a = np.asanyarray(a)
        m = a.mean()
        sd = a.std()
        if sd == 0:
            return 0
        return 20 * np.log10(abs(m / sd))

    def get_devices(self, e):
        """
        Get list devices from CCDA
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self._le_classes.text() == "LTIM" or self._le_classes.text() == "AnalogueSignal":
            QApplication.restoreOverrideCursor()
            return
        formatting.print_nxcals("Get devices", self._le_classes.text())
        devices = ccda.get_devices_from_class(self._le_classes.text())
        devices.sort()
        ind = 0
        self._enum_devices.clear()
        if len(devices) == 0:
            QApplication.restoreOverrideCursor()
            return
        for device in devices:
            if ind < 1000:
                self._enum_devices.addItem(device)
            ind = ind + 1
        QApplication.restoreOverrideCursor()

    def get_properties(self, e):
        """
        Get list properties from CCDA
        """
        device = self._enum_devices.currentText()
        if device == "":
            QApplication.restoreOverrideCursor()
            return
        formatting.print_nxcals("Get properties", self._enum_devices.currentText())
        properties = self.nxcalscomm.get_properties(self._enum_devices.currentText())
        properties.sort()
        self._enum_properties.clear()

        self._properties = []
        for propertyfesa in properties:
            self._enum_properties.addItem(propertyfesa)
            self._properties.append(propertyfesa)

        QApplication.restoreOverrideCursor()

    def get_variables(self, e):
        """
        Get list properties from CCDA
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        device_name = self._enum_devices.currentText()
        property_name = "*"
        formatting.print_nxcals("Get variables", device_name, property_name)
        variables = self.nxcalscomm.get_variables(device_name=device_name, property_name=property_name, field_name="*")
        if variables is None:
            QApplication.restoreOverrideCursor()
            return
        variables.sort()
        self._enum_variables.clear()
        self._variables = []

        # SPECIFC CASES #
        for variable in variables:
            if (":image2D" in variable) and self._le_classes.text() == "BTVDC":
                new_variable = variable.replace(":image2D", ":IMAGE")
                self._enum_variables.addItem(new_variable)
                self._properties.append(new_variable)
                break
            elif (":imageSet" in variable) and self._le_classes.text() == "BTVI":
                new_variable = variable.replace(":imageSet", ":IMAGE")
                self._enum_variables.addItem(new_variable)
                self._properties.append(new_variable)
                break
            elif (":projDataSet1" in variable) and self._le_classes.text() == "BSISO":
                new_variable = variable.replace(":projDataSet1", ":IMAGE")
                self._enum_variables.addItem(new_variable)
                self._properties.append(new_variable)
                break
            elif (":projDataSet1" in variable) and \
                    self._le_classes.text() == "BEMCPS" and \
                    (("H:" in variable) or ("V:" in variable)):
                new_variable = variable.replace(":projDataSet1", ":IMAGE")
                self._enum_variables.addItem(new_variable)
                self._properties.append(new_variable)
                break
        # END SPECIFC CASES #

        for variable in variables:
            self._enum_variables.addItem(variable)
            self._properties.append(variable)
        QApplication.restoreOverrideCursor()

    def close_dtw(self, name):
        """
        One datawidget is being closed...

        :param name: Name of the dtw.
        :type parent: str
        """
        try:
            del self._parent._settings[name]
        except Exception as e:
            print("PROBLEM")
            print(e)

    def load_settings(self, class_name, device_name, var_name, user_name, from_name, to_name, functions=None, open=True,
                      list_time=None):
        """
        Load settings.
        """

        # print(class_name)
        # print(device_name)
        # print(var_name)
        # print(user_name)
        # print(from_name)
        # print(to_name)

        # class

        class_name = class_name.upper()

        if class_name not in self.classes:
            return

        self._le_classes.setText(class_name)

        # device

        self.get_devices(None)
        self._enum_devices.setCurrentText(device_name)

        # variable

        self.get_variables(None)
        self._enum_variables.setCurrentText(var_name)

        # user

        if user_name is None:
            self._enum_users.setCurrentText("None")
        else:
            self._enum_users.setCurrentText(user_name)

        # lhc fill or not

        if from_name == "-1":
            self._meta.rb_lhc.setChecked(True)
            self._meta.lhcfill.set_data(int(to_name))
        else:
            self._meta.rb_time.setChecked(True)
            self._meta.set_tfrom(from_name)
            self._meta.set_tto(to_name)

        # search

        if open:

            # search times

            self.search(None)

            # times in cb list

            if list_time is not None and len(list_time) > 0:

                for cb in self._cb_pu:
                    cb.setChecked(False)

                for ti in list_time:
                    for cb in self._cb_pu:
                        if ti in cb.text():
                            cb.setChecked(True)

                # open

                self.add(None)

    def load_config(self, name):
        """
        Load registers from its config key.
        """
        try:
            if open:
                info = name.split("$")
            else:
                info = name.split("/")

            class_name = info[1]
            device_name = info[2]
            var_name = info[3]
            user_name = info[4]
            if user_name == "":
                user_name = None
            from_name = info[5]
            to_name = info[6]

            self.load_settings(class_name, device_name, var_name, user_name, from_name, to_name, open=False)

        except Exception as e:
            print(e)
            pass


class _FilterFields(QWidget):
    """
    Widget panel to filter field/registers

    :param parent: Parent object.
    :type parent: object
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        self._parent = parent

        self._field_entities = []

        self._components = {}
        self._text_filter = QLineEdit("")

        self._add_text_filter = False

        self.layout = QGridLayout(self)

        self.scroll = QScrollArea()
        self.scroll.setStyleSheet("border:none;")
        self.widget = QWidget()
        self.vbox = QVBoxLayout()

        self.widget.setLayout(self.vbox)

        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.layout.addWidget(self.scroll, 0, 0)
        self.show()

    def clear_layout(self):
        for fe in self._field_entities:
            fe.deleteLater()
        self._field_entities = []

    def add_field(self, measname, t1, t2, classname="", devicename="", propertyname=""):
        pass
        # self.clear_layout()
        # height = (4+len(listfields))*30
        # if height > 600:
        #     height=600
        # self.setMinimumHeight(height)
        #
        # self._components = {}
        #
        # # add filter
        # if self._add_text_filter == False:
        #     panel_filter = QWidget()
        #     layout_filter = QGridLayout(panel_filter)
        #     layout_filter.setContentsMargins(10, 5, 0, 0)
        #     layout_filter.setSpacing(5)
        #     layout_filter.addWidget(QLabel("Filter : "), 0,2,alignment=Qt.AlignRight)
        #     self._text_filter.installEventFilter(self)
        #     # self.filter_changed.connect(self.filter_fields)
        #     layout_filter.addWidget(self._text_filter, 0,3,alignment=Qt.AlignLeft)
        #     layout_filter.addWidget(QLabel(" "), 0,1)
        #     layout_filter.setColumnStretch(1,1)
        #     self.vbox.addWidget(panel_filter)
        #     self._add_text_filter = True
        #
        # fe_all = _FieldEntity(self,"All")
        # fe_all.checkbox_plot.toggled.connect(self.pressall)
        # self._field_entities.append(fe_all)
        # for lf in listfields:
        #     _field_entity = _FieldEntity(self,lf['name'],field=lf)
        #     self._components[lf['name']] = _field_entity
        #     self._field_entities.append(_field_entity)
        #
        # for fe in self._field_entities:
        #     self.vbox.addWidget(fe)
        #
        # completer = QCompleter(self._components)
        # self._text_filter.setCompleter(completer)

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                else:
                    self._components[component_field].hide()
            except:
                self._components[component_field].hide()

    def pressall(self, e):
        """
        Press one checkbox.
        """
        for fe in self._field_entities:
            fe.setChecked(self._field_entities[0].isChecked())

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        for fe in self._field_entities:
            fe.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        for fe in self._field_entities:
            fe.light_()


class _FieldEntity(QWidget):
    """
    Field widget panel

    :param parent: Parent object.
    :type parent: object
    """

    def __init__(self, parent, name, field=None):
        """
        Initialize Class.
        """

        super(QWidget, self).__init__(parent)

        self._parent = parent
        self.name = name
        self.setMaximumHeight(30)
        self.layout = QGridLayout()
        self.layout.setContentsMargins(5, 0, 5, 5)
        self.layout.setSpacing(5)
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)
        # name
        label_ = QLabel(name + "  ")
        label_.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        label_.setAlignment(Qt.AlignRight)
        label_.setStyleSheet("font-weight:bold;")
        self.layout.addWidget(label_, 0, 0)

        self._field = None
        self.label_rwmode = None
        self.label_type = None
        self.label_dwith = None
        self.label_size = None

        if field is not None:

            self._field = field

            # rwmode
            self.label_rwmode = QLabel(" " + field['rwmode'] + " ")
            self.label_rwmode.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_rwmode.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_rwmode, 0, 1)
            # type
            self.label_type = QLabel(field['type'])
            self.label_type.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_type.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_type, 0, 2)
            # dwidth
            self.label_dwith = QLabel(str(field['dwith']))
            self.label_dwith.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.label_dwith.setAlignment(Qt.AlignCenter)
            self.layout.addWidget(self.label_dwith, 0, 3)
            # size
            if field['depth'] > 1:
                self.label_size = QLabel(" " + str(field['depth']) + " ")
                self.label_size.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
                self.label_size.setAlignment(Qt.AlignCenter)
                self.layout.addWidget(self.label_size, 0, 4)
        # checkbox
        self.checkbox_plot = QCheckBox("")
        self.checkbox_plot.setChecked(False)
        self.checkbox_plot.setMaximumWidth(16)
        self.layout.addWidget(self.checkbox_plot, 0, 5)
        self.layout.setRowStretch(0, 0)
        self.layout.setColumnStretch(0, 1)
        self.layout.setColumnStretch(1, 0)
        self.layout.setColumnStretch(2, 0)
        self.layout.setColumnStretch(3, 0)
        self.layout.setColumnStretch(4, 0)
        self.layout.setColumnStretch(5, 0)

    def isChecked(self):
        return self.checkbox_plot.isChecked()

    def setChecked(self, val):
        self.checkbox_plot.setChecked(val)

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_L2BLACK + ";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLUE + ";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT0GREEN + ";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT3GRAY + ";color:white;")

        self.checkbox_plot.setStyleSheet(
            "color:" + Colors.STR_COLOR_WHITE + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";")

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE + ";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHTRED + ";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLUE + ";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT0GREEN + ";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:" + Colors.STR_COLOR_LIGHT3GRAY + ";color:white;")

        self.checkbox_plot.setStyleSheet(
            "color:" + Colors.STR_COLOR_BLACK + ";background-color:" + Colors.STR_COLOR_WHITE + ";" + "border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")


class _PlainTextEditInfo(QPlainTextEdit):
    msg_signal = pyqtSignal(str, str, str)

    def __init__(self, parent):
        super().__init__(parent)
        self.msg_signal.connect(self.refresh)

    def append_info(self, message, str_count=0, color=None):
        if color is None:
            colort = 'white'
            if Colors.COLOR_LIGHT:
                colort = 'black'
        else:
            colort = color
        self.msg_signal.emit(message, str(str_count), colort)

    @pyqtSlot(str, str, str)
    def refresh(self, msg, msg_count, color):
        self.appendHtml(f'<font color="{color}">{msg}</font><font color="orange"> --> <b>{msg_count}</b></font><br>')
        qApp.processEvents()


class _PlainTextEditLogging(QPlainTextEdit):
    msg_signal = pyqtSignal(str, str)

    def __init__(self, parent):
        super().__init__(parent)
        self.msg_signal.connect(self.refresh)

    def append_log(self, message, color='#888'):
        self.msg_signal.emit(message, color)

    @pyqtSlot(str, str)
    def refresh(self, msg, color):
        self.appendHtml(f'<font color="{color}">{msg}</font><br>')
        qApp.processEvents()


class _QTextEditLogger(logging.Handler):

    def __init__(self, parent):
        super().__init__()
        self.widget = _PlainTextEditLogging(parent)
        self.widget.setReadOnly(True)

    def emit(self, record):
        msg = self.format(record)
        colorw = 'white'
        colorg = '#666'
        if Colors.COLOR_LIGHT:
            colorw = 'black'
            colorg = "#aaa"

        color = {
            logging.DEBUG: '#3AADFF',
            logging.INFO: colorg,
            logging.WARNING: colorw,
            logging.ERROR: 'red',
            logging.CRITICAL: '#FF00DC'
        }.get(record.levelno, colorg)
        self.widget.append_log(msg, color)


class _Example(QMainWindow):
    """

    Example class to test

    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()
        self._update_time = 0

    def init_ui(self):
        """
        Init user interface.
        """
        dark = True

        central_widget = QWidget()

        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)

        ind = 0

        self._meta = MetaNXCALSWidget(central_widget)
        self.layout.addWidget(self._meta, 0, 0)

        self.setCentralWidget(central_widget)

        if dark:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        else:
            central_widget.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        self.resize(1300, 900)
        self.move(100, 100)

        if dark:
            Colors.COLOR_LIGHT = False
            self._meta.dark_()
        else:
            Colors.COLOR_LIGHT = True
            self._meta.light_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
        }
        QMenuBar::item:selected {    
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QMenuBar::item:pressed {
            background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }

        QMenu {
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;   
            margin: 2px;
        }
        QMenu::item {
            background-color: transparent;
        }
        QMenu::item:selected { 
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;                
        }         
        QLineEdit:{background-color: black;}            
    """
    app.setStyleSheet(qss)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
