import sys
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QMainWindow, QVBoxLayout, QScrollArea, QLabel, QGroupBox

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import headerwidget
from expert_gui_core.gui.widgets.pyqt import datawidget


class NXCALSWidget(QWidget):
    """
    Class widget to display automaic NXCALS data.
    
    :param parent: Parent object.
    :type parent: object
    :param title: Title to be shown in the top center.
    :type title: str, optional
    :param name_device: Device name.
    :type name_device: str
    :param name_variable: NXCALS variable name.
    :type name_variable: str
    :param name_cycle: Cycle (PPM filtering) name
    :type name_cycle: str
    
    :param header: Show or not the header bar (default=True).
    :type header: bool, optional
    """

    filter_changed = pyqtSignal()

    def __init__(self, parent, title=None, name_device="", name_variable="", name_cycle="", tfrom="", tto="",
                 vector=False, header=True):
        """
        Initialize class.        
        """
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._parent = parent

        # layout construction

        self.layout = QGridLayout()
        self.layout.setContentsMargins(5, 2, 5, 2)
        self.layout.setSpacing(0)

        groupBox = QGroupBox()
        groupBox.setLayout(self.layout)

        # scroll area   

        self.scroll = QScrollArea()
        self.scroll.setWidget(groupBox)
        self.scroll.setWidgetResizable(True)

        layout = QVBoxLayout(self)

        # header

        if header:
            self._header = headerwidget.HeaderWidget()
            layout.addWidget(self._header)
        else:
            self._header = None

        # title

        if title is not None:
            self._label_title = QLabel(title)
            self._label_title.setAlignment(Qt.AlignCenter)
            # layout.addWidget(self._label_title)         

        if vector:
            data_widget_opts = {
                "auto": True,
                "show_chart": True,
                "show_table": True,
                "show_image": True,
                "show_chartgl_3d": True,
                "show_chartgl_surface": True,
                "show_chartgl": True,
                "history": False,
                "show_chart_value_axis": True,
                "show_image_value_axis": True,
                "show_chart_grid": True,
                "statistics": True,
                "fitting": True,
                "color_amplitude": True
            }

            self._data_widget = datawidget.DataWidget(None,
                                                      title=None,
                                                      name=[name_variable],
                                                      header=False,
                                                      **data_widget_opts)
        else:

            data_widget_opts = {
                "auto": True,
                "show_chart": True,
                "show_table": True,
                "show_chartgl_3d": False,
                "show_chartgl_surface": False,
                "show_chartgl": False,
                "history": False,
                "show_chart_value_axis": True,
                "show_chart_grid": True,
                "time_in_chart": True,
                "statistics": True,
                "fitting": True
            }

            self._data_widget = datawidget.DataWidget(None,
                                                      title=None,
                                                      name=[name_variable],
                                                      header=False,
                                                      **data_widget_opts)

        layout.addWidget(self._data_widget)

    def set_data(self, value, valuex=None, name="Default", name_parent="Default"):
        """
        Inject data in data widget.

        :param value: Input data
        :type value: scalar or arrays
        ::param valuex: Input data x
        :type valuex: scalar or arrays,optional
        ::param name: name variable
        :type name: str,optional
        ::param name_parent: name variable
        :type name_parent: str,optional        
        """
        if self._header is not None:
            if self._header.is_pause():
                return
            try:
                self._header.recv(None)
            except:
                return
        self._data_widget.set_data(value, valuex=valuex, name=name, name_parent=name_parent)

    def dark_(self):
        """
        Set dark theme.
        """
        self._light = False
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLACK + ";")
        self._data_widget.dark_()
        if self._header is not None:
            self._header.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._light = True
        self._data_widget.light_()
        if self._header is not None:
            self._header.light_()

    def set_font_size(self, val=8):
        """
        Change font size.

        :param val: Font size value (default=8).
        :type val: int, optional
        """
        font = self._get_button.font()
        font.setPointSize(val)


class _Example(QMainWindow):
    """
    Example class to test.    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """
        Init user interface.
        """

        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)

        nxcals_widget = NXCALSWidget(None,
                                     title="Title",
                                     name_device="A",
                                     name_variable="B",
                                     tfrom="TFROM",
                                     tto="TTO",
                                     name_cycle="C")

        self.layout.addWidget(nxcals_widget, 0, 0)

        self.setCentralWidget(central_widget)

        self.resize(500, 600)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
        }
        QMenuBar::item:selected {    
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QMenuBar::item:pressed {
            background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }              
        QLineEdit:{background-color: black;}
    """
    app.setStyleSheet(qss)

    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
