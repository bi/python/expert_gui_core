import sys
from time import sleep
import numpy as np
import pandas as pd
from queue import Queue
import logging
from threading import Thread
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt,  pyqtSlot
from PyQt5.QtGui import  QPalette, QColor
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QHeaderView, QMainWindow, QLineEdit

from expert_gui_core.gui.common.colors import Colors

from expert_gui_core.comm import ccda
from expert_gui_core.comm import fesadispatcher

from expert_gui_core.gui.widgets.common import headerwidget, togglepanelwidget, titledborderwidget
from expert_gui_core.gui.widgets.pyqt import tablewidget

from ._version import __version__  # noqa


class DevicesTableWidget(QWidget):
    table_changed = pyqtSignal()
    data_event = pyqtSignal(str, str, int, str)

    def __init__(self,
                 parent,
                 header=True,
                 filter=True,
                 devices=[],
                 fecs={},
                 properties={},
                 status_colors=None,
                 struct=None,
                 timer=1000):
        """
        Initialize class.
        """
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self.hide()

        self._parent = parent

        self.queue = Queue()

        self.index = []
        self.index_count = []
        self.dispatcher = []
        self.devices_per_class = {}
        self.classes_used = []
        self._show_row = None
        self._show_col = None

        # create the table

        self.table = tablewidget.TableWidget(parent=self,
                                             include_status_list=True,
                                             enable_column_options=True,
                                             status_colors=status_colors,
                                             auto_resizing=True,
                                             cells_editable=True)

        if struct is None:

            self.struct = {}

            for class_name in properties:

                # create devices list from class names

                self.devices_per_class_red = []

                array_stars = []
                array_notstars = []

                if class_name in fecs:
                    devices_per_class = []
                    dev_fec = ccda.get_devices_from_class_per_fec(class_name)
                    for fec in fecs[class_name]:
                        for dev in dev_fec[fec]:
                            if dev not in devices:
                                devices.append(dev)
                                devices_per_class.append(dev)
                else:
                    devices_per_class = ccda.get_devices_from_class(class_name)

                for dev in devices:
                    if "*" in dev and "!" not in dev:
                        array_stars.append(dev.split("*"))
                    elif "*" in dev and "!" in dev:
                        array_notstars.append(dev.split("*"))

                for device_name in devices_per_class:
                    if len(devices) > 0:
                        find_star_filter = True
                        if len(array_stars) > 0:
                            check = True
                            for astar in array_stars:
                                for star in astar:
                                    if star not in device_name and star != '':
                                        check = False
                                        break
                            find_star_filter = check

                        if len(array_notstars) > 0:
                            check = True
                            for astar in array_notstars:
                                for star in astar:
                                    star_filter = star.replace("!", "")
                                    if star_filter not in device_name and star_filter != '':
                                        check = False
                                        break
                            find_star_filter = not check

                        if (device_name in devices or str("!" + device_name) not in devices) and find_star_filter:
                            self.devices_per_class_red.append(device_name)
                            self.devices_per_class[device_name] = class_name
                            if class_name not in self.classes_used:
                                self.classes_used.append(class_name)
                    else:
                        self.devices_per_class_red.append(device_name)
                        self.devices_per_class[device_name] = class_name
                        if class_name not in self.classes_used:
                            self.classes_used.append(class_name)

                # create row names using device list

                for device_name in self.devices_per_class_red:
                    self.index.append(device_name)
                    self.index_count.append(len(self.index))

                time_sub = {}
                cycle_sub = {}

                for prop in properties[class_name]:

                    if "#" in prop:
                        if "#Time" in prop:
                            info = prop.split("#")
                            time_sub[info[0]] = properties[class_name][prop]
                        if "#Cycle" in prop:
                            info = prop.split("#")
                            cycle_sub[info[0]] = properties[class_name][prop]

                # create structure from configuration

                self._function_expression = {}
                type_fields = None

                for prop in properties[class_name]:

                    if "#" in prop:
                        continue

                    # create dispatcher list

                    info = prop.split("#")

                    # min time between 2 notifications

                    if info[0] in time_sub:
                        min_time_between_event = time_sub[info[0]]
                    else:
                        min_time_between_event = 200

                    # cycle name

                    if info[0] in cycle_sub:
                        cycle_subscription = cycle_sub[info[0]]
                    else:
                        cycle_subscription = ""

                    for device_name in self.devices_per_class_red:
                        self.dispatcher.append(
                            fesadispatcher.FesaDispatcher(self, device_name, {prop: self.handle_event},
                                                          cycle=cycle_subscription,
                                                          min_time_between_event=min_time_between_event))

                    prop_fields = ccda.get_property_fields_from_class(class_name=class_name, property_name=prop)
                    conf_fields = properties[class_name][prop]

                    for prop_field in prop_fields:

                        add = True
                        if conf_fields is not None:
                            add = False
                            if prop_field.name in conf_fields:
                                add = True
                        if add:
                            if len(properties) > 1:
                                self.struct[class_name + "#" + prop_field.name] = None
                            else:
                                if class_name in self.classes_used:
                                    self.struct[prop_field.name] = None

            structtmp = {}

            structtmp["DEVICES"] = self.index
            for name in self.struct:
                structtmp[name] = np.full(
                    shape=len(self.index),
                    fill_value=np.nan,
                    dtype=np.float128
                )

            self.struct = structtmp

        # create dataframe

        self.df = pd.DataFrame(self.struct, index=self.index_count)

        # filter text

        if filter:
            self.filter = QLineEdit()
            self.filter.setMaximumWidth(400)
        else:
            self.filter = None

        # create the table

        self.table.set_data(self.df)

        if status_colors is not None:
            status_list = np.full(
                shape=len(self.index),
                fill_value=0,
                dtype=np.int32
            )
            self.df["status_list"] = status_list

        # resize device column

        self.table.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)

        self.table.verticalHeader().setMinimumSectionSize(40)

        # add component in main window

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(5, 2, 5, 2)
        self.layout.setSpacing(2)

        # header

        if header:
            self.header = headerwidget.HeaderWidget()
            self.layout.addWidget(self.header, 0, 0)
        else:
            self.header = None

        if filter:
            self.layout.addWidget(self.filter, 1, 0, alignment=Qt.AlignRight)

        self.layout.addWidget(self.table, 2, 0)

        self.layout.setColumnStretch(0, 1)
        self.layout.setRowStretch(0, 0)
        self.layout.setRowStretch(1, 0)
        self.layout.setRowStretch(2, 1)

        # mutex used in timer

        self.update = False

        # connect signal

        self.table_changed.connect(self.update_table)

        # Turn-on the worker thread.

        Thread(target=self.worker, daemon=True).start()

        # timer

        self.timer = _Timer_Table(timer / 1000, self.check_update)
        self.timer.start()

        for disp in self.dispatcher:
            try:
                disp.start()
            except:
                pass

    def worker(self):
        while True:

            item = self.queue.get()

            name = item[0]
            value = item[1]

            info = name.split("/")
            device = info[0]

            if len(self.classes_used) > 1:
                class_name = self.devices_per_class[device] + "#"
            else:
                class_name = ""

            ind = self.index.index(device) + 1

            update = False
            for val in value:
                col_name = class_name + val
                if col_name in self.df and val != "_time" and val != "_cycle":
                    try:
                        self.df[col_name][ind] = str(value[val])
                        self.data_event.emit(name, col_name, ind, str(value[val]))
                    except:
                        self.df[col_name][ind] = ""
                    update = True

            self.update = update

            self.queue.task_done()

    def check_update(self):
        """
        Check whether the table should be updated.
        """
        if self.update:
            self.update = False

            self._show_col = None
            for col in self.df:
                try:
                    dd = self.df[col].isin(np.nan)
                except:
                    pass

            if self.filter is not None:
                self._show_row = None
                filter = self.filter.text()
                if filter != "" and "#" not in filter:
                    self._show_row = []
                    show_row = self.df.apply(
                        lambda row: row.astype(str).str.contains(self.filter.text(), case=False).any(), axis=1)
                    for i in range(len(show_row)):
                        self._show_row.append(not show_row.iloc[i])
                elif filter != "" and "#" in filter:
                    if "status_list" in self.df:
                        show_row = self.df["status_list"]
                        self._show_row = []
                        status_filter = 0
                        try:
                            status_filter = int(filter.replace("#", ""))
                        except:
                            pass
                        for i in range(len(show_row)):
                            if show_row.iloc[i] == status_filter:
                                self._show_row.append(False)
                            else:
                                self._show_row.append(True)
            else:
                self._show_row = None
            self.table_changed.emit()

    @pyqtSlot()
    def update_table(self):
        """
        Update table.
        """
        if self._show_row is not None and self.filter is not None and self.filter.text() != "":
            for i in range(len(self._show_row)):
                self.table.setRowHidden(i, self._show_row[i])
        else:
            for i in range(self.table._data_model.rowCount()):
                self.table.setRowHidden(i, False)

        if self._show_col is not None:
            for i in range(len(self._show_col)):
                self.table.setColHidden(i, self._show_col[i])

        self.table.refresh_table()

        self.header.recv()

    def set_status(self, name, value):
        """
        Set status row
        """
        if name in self.index:
            ind = self.index.index(name)
            self.df["status_list"][ind] = int(value)

    def handle_event(self, name, value):
        """
        Received notification.
        """

        if self.header is not None:
            if self.header.is_pause():
                return

        self.queue.put([name, value])

    def close(self):
        """
        Close widget.
        """
        self.timer.stop()
        for disp in self.dispatcher:
            disp.stop()

    def dark_(self):
        """
        Set to dark theme.
        """
        self.table.dark_()
        if self.filter is not None:
            self.filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:left;padding-left:2px;border:1px solid " + Colors.STR_COLOR_L3BLACK + ";")
        if self.header is not None:
            self.header.dark_()

    def light_(self):
        """
        Set to light theme.
        """
        self.table.light_()
        if self.filter is not None:
            self.filter.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";text-align:left;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        if self.header is not None:
            self.header.light_()


class _Timer_Table:
    """
    Class timer.
    """

    def __init__(self, interval, target_function):
        self.interval = interval
        self.target_function = target_function
        self.is_running = False
        self.thread = None

    def _run(self):
        while self.is_running:
            self.target_function()
            sleep(self.interval)

    def start(self):
        if not self.is_running:
            self.is_running = True
            self.thread = Thread(target=self._run)
            self.thread.start()

    def stop(self):
        if self.is_running:
            self.is_running = False
            self.thread.join()


class _Example(QMainWindow):
    """
    Example class to test.    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):

        logging.disable(logging.WARNING)
        logging.disable(logging.ERROR)
        logging.disable(logging.DEBUG)
        logging.disable(logging.INFO)
        logging.disable(logging.CRITICAL)

        central_widget = QWidget()
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5)

        status_colors = {
            "1": [QColor(255, 0, 0), QColor(255, 0, 0, 20)],
            "2": [QColor(0, 150, 255), QColor(0, 0, 255, 20)],
            "3": [QColor(255, 106, 0), QColor(255, 106, 0, 20)]
        }

        # self.dtw = DevicesTableWidget(self,
        #                                 devices = ["YCB0.BSC0800", "YRC0.BSC0150"],
        #                                 # devices = ["!FCTEST865", "!YMD3*"],
        #                                 # devices = ["Y*"],
        #                                 properties =
        #                                 {
        #                                     # "BSISO":{
        #                                     #     "Status":{"channel_status"},
        #                                     #     "ExpertSetting":{"sSpeed"},
        #                                         # "Status#Cycle":{"PSB.USER.ALL"}
        #                                     # },
        #                                     "BSISO": {
        #                                         "Status": None,
        #                                         "Status#Time": 500,
        #                                     },
        #                                     # "BCTDC24SPS": {
        #                                     #     "Acquisition": None,
        #                                     #     "Acquisition#Time": 500,
        #                                     #     "Acquisition#Cycle": "SPS.USER.LHCION4"
        #                                     # },
        #                                     "BOPFCREX":{
        #                                         "Acquisition":None
        #                                     }
        #                                     # "BISWRef":{
        #                                     #     "Acquisition":None
        #                                     # }
        #                                 },
        #                                 timer=1000,
        #                                 status_colors = status_colors
        #                             )

        # self.dtw.set_status("YCB0.BSC0800", 2)

        # self.dtw.set_status("YRC0.BSC0150", 3)
        # self.dtw.set_status("YOFFL1.BSC330", 1)
        # self.dtw.set_status("YOFFL2.BSC70", 2)
        # self.dtw.set_status("YOFFL2.BSC170", 3)

        self.dtw = DevicesTableWidget(self,
                                      # devices=["YCB0.BSC0800", "YRC0.BSC0150"],
                                      # devices = ["!FCTEST865", "!YMD3*"],
                                      # devices = ["Y*"],
                                      # fecs={
                                      #       "BSISO":{
                                      #           "cfv-197-bsiso"
                                      #       }
                                      # },
                                      properties={
                                          # "BSISO":{
                                          #     "Status":{"channel_status"},
                                          #     "ExpertSetting":{"sSpeed"},
                                          # "Status#Cycle":{"PSB.USER.ALL"}
                                          # },
                                          "BSISO": {
                                              "Status": None,
                                              "Status#Time": 500,
                                          },
                                          # "BCTDC24SPS": {
                                          #     "Acquisition": None,
                                          #     "Acquisition#Time": 500,
                                          #     "Acquisition#Cycle": "SPS.USER.LHCION4"
                                          # },
                                          # "BOPFCREX": {
                                          #     "Acquisition": None
                                          # }
                                          # "BISWRef":{
                                          #     "Acquisition":None
                                          # }
                                      },
                                      timer=1000,
                                      status_colors=status_colors
                                      )

        self.dtw.set_status("YCB0.BSC0800", 2)

        self.dtw.set_status("YRC0.BSC0150", 3)
        self.dtw.set_status("YOFFL1.BSC330", 1)
        self.dtw.set_status("YOFFL2.BSC70", 2)
        self.dtw.set_status("YOFFL2.BSC170", 3)
        self.dtw.data_event.connect(self.update_data)
        self.dtw.table.clicked.connect(self.click_table)
        self.layout.addWidget(self.dtw, 0, 0)
        self.setCentralWidget(central_widget)

        self.dtw.dark_()
        # self.dtw.light_()
        self.dtw.show()

        self.resize(1800, 800)

    def click_table(self, item):
        print(item.column(), item.row(), item.data())

    def update_data(self, name: str, col_name: str, ind: int, value: str):
        # print(name, col_name, ind, value)
        if col_name == "indexExpertLog":
            device = name.split("/")[0]
            if int(value) > 50:
                self.dtw.set_status(device, 3)
            else:
                self.dtw.set_status(device, 2)

    def closeEvent(self, event):
        self.dtw.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    # darkpalette = QPalette()
    # darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    # darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    # darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    # darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    # app.setPalette(darkpalette)
    #
    # qss = """
    #     QMenuBar::item {
    #         spacing: 2px;
    #         padding: 2px 10px;
    #         background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";
    #     }
    #     QMenuBar::item:selected {
    #         background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";
    #     }
    #     QMenuBar::item:pressed {
    #         background: """+Colors.STR_COLOR_LIGHT1GRAY+""";
    #     }
    #     QLineEdit:{background-color: black;}
    # """
    # app.setStyleSheet(qss)

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    # self.setStyleSheet("background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
    # self._tabs.setStyleSheet(
    #     "background-color: " + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

    qss = """
                QMenuBar::item {
                    spacing: 2px;           
                    padding: 2px 10px;
                    background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
                }
                QMenuBar::item:selected {    
                    background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }
                QMenuBar::item:pressed {
                    background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }              
                QScrollArea {
                    background-color: transparent;
                }
                QRadioButton::checked {
                    color: """ + Colors.STR_COLOR_L2BLUE + """;  
                }
                QRadioButton::indicator {
                    border-radius: 6px;                            
                }
                QRadioButton::indicator::unchecked{ 
                    border-radius: 6px;
                    border:2px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;            
                }

                QRadioButton::indicator::checked{ 
                    border: 2px solid; 
                    border-color: """ + Colors.STR_COLOR_L2BLUE + """;  
                    border-radius: 6px;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;   
                }
                QCheckBox {
                    color: """ + Colors.STR_COLOR_WHITE + """;
                }
                QCheckBox::indicator {
                    max-width:14;
                    max-height:14;
                    border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;            
                }    
                QCheckBox::indicator:checked {
                    max-width:14;
                    max-height:14;
                    border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
                }          
                QLineEdit {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    text-align:left;
                    padding-left:2px;
                    padding-top:2px;
                    padding-bottom:2px;
                    border:1px solid """ + Colors.STR_COLOR_L3BLACK + """;
                }         
                QComboBox {
                    border:none;
                    background-color:""" + Colors.STR_COLOR_L1BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }                
                QListView {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }
            """
    app.setStyleSheet(qss)

    ex = _Example()
    ex.show()

    sys.exit(app.exec_())
