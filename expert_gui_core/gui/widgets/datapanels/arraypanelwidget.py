import sys
import numpy as np
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot

from PyQt5.QtGui import QPalette, QGuiApplication, QFont

from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QMenu, QGridLayout, \
    QMainWindow, QScrollArea, QLabel, QPushButton, QWidgetAction, QInputDialog

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import expander
from expert_gui_core.gui.widgets.pyqt import datawidget


class ArrayPanelWidget(QWidget):
    """
    Low-level table widget setting/displaying (only) array/array2d data.
    
    :param parent: Parent object
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param edit: Edit or not (not used, default=True)
    :type edit: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional
    :param size_history: Size history (default=20)
    :type size_history: int, optional
    :param twod: Specify whether it is a 2D array (default=False)
    :type twod: bool, optional
    :param string: Specify whether it is a string type array (default=False)
    :type string: bool, optional
    """

    update_signal = pyqtSignal()
    update_function_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    _signal = pyqtSignal()

    def __init__(self, parent, title=None, name=True, label_name="", edit=True,
                 history=False, size_history=None, twod=False, string=False, statistics=False, fitting=False,
                 type_format="raw", function_expression=None, color=None):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._edit = edit

        self.data = None
        self.data_type = None
        self.cycle = None
        self._type_format = type_format
        self._contextMenu = None
        self._color = color

        self.default_palette = QGuiApplication.palette()
        self._size_max_row_table = 10000
        self._size_max_col_table = 1000
        self._maxheight = 38
        self._nrow = 3

        self.font_text = QFont()
        self.font_textbig = QFont()
        self.font_textsmall = QFont()
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self._function_expression = function_expression

        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        if not name:
            self._main_widget.hide()

        self.mainlayout.setContentsMargins(0, 0, 0, 0)
        self.mainlayout.setSpacing(0)

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)

        self.mainlayout.addWidget(self._main_widget, 0, 0)

        self._label_title = QLabel(title)
        self._name = label_name
        self._label_name = QLabel(label_name + " ")

        self._label_table = QLabel("")
        self._label_table.setWordWrap(False)

        self._chart_button = QPushButton(fa.icons["chart-line"])

        self.font = self._label_title.font()
        self._label_title.setAlignment(Qt.AlignCenter)
        self._label_title.setStyleSheet("color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;")

        if title is not None and not name:
            self.layout.addWidget(self._label_title, 0, 0, 1, 5)
            self._maxheight = self._maxheight + 18

        self.setMinimumHeight(self._maxheight)

        self.layout.addWidget(self._chart_button, 1, 0)
        self._chart_button.setFont(self.font_fa)
        self._chart_button.setMaximumWidth(20)
        self._chart_button.mousePressEvent = self.show_chart

        if name and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name, 1, 1)
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.layout.addWidget(self._label_table, 1, 2)
        self._label_table.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

        self._label_type = QPushButton()
        self._label_type.setFont(self.font_fa)
        self._label_type.setText(fa.icons["circle"])
        self._label_type.setMaximumWidth(20)
        self._label_type.setStyleSheet("padding:0;margin:0;border:none;color:transparent;")

        self.layout.addWidget(self._label_type, 1, 3)
        self.layout.setColumnStretch(3, 0)

        self.layout.setColumnStretch(2, 3)

        self.twod = twod

        palette = None
        if color == Colors.COLOR_H:
            palette = "ocean"
        elif color == Colors.COLOR_V:
            palette = "bbq"

        data_widget_opts = {
            "auto": True,
            "show_singlechart": not string and not twod,
            "show_chart": not string and (twod or history),
            "show_singletable": not twod,
            "show_table": twod,
            "show_image": not string and (twod or history),
            "show_chartgl_3d": False,
            "show_chartgl_surface": False,
            "show_chartgl": False,
            "history": history,
            "show_chart_value_axis": True,
            "show_singlechart_value_axis": True,
            "show_image_value_axis": True,
            "show_chart_grid":True,
            "show_singlechart_grid": True,
            "optimization": "modulo",
            "statistics": statistics,
            "fitting": fitting,
            "chart_palette": palette,
            "color_single_chart": color
        }

        self._data_widget = datawidget.DataWidget(None,
                                                  title=None,
                                                  name=[self._name],
                                                  header=True,
                                                  max=size_history,
                                                  **data_widget_opts)

        try:
            # here
            self._data_widget._data_widget._singletable.model.dataChanged.connect(self.change_table)
        except Exception as e:
            print(e)
            pass

        if title is not None:
            self._frame = expander._DataWidgetExpander(self, name=title)
        else:
            self._frame = expander._DataWidgetExpander(self, name=label_name)

        self.update_signal.connect(self.update_data)
        self.update_signal_chart.connect(self.update_chart)

        self.set_led_type(self._type_format)

        if not history:
            self.set_data([0])

    def change_table(self, item=None):

        self.data = self._data_widget.get_table().model.dataframe

        if len(self.data.columns) == 1:
            self.data = np.ravel(self.data)
        else:
            self.data = self.data.to_numpy().T

        self.update_signal.emit()
        self.update_signal_chart.emit()
        self._data_widget._data_widget.update_signal.emit()

    def get_label_component(self):
        return self._label_name

    def get_main_component(self):
        return self._label_table

    def contextMenuEvent(self, event):
        """
        Click right menu options to format the string.
        """
        self._contextMenu = QMenu(self)

        self.l_raw = QLabel('Raw')
        self.l_raw.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_raw.setAlignment(Qt.AlignLeft)
        rawAct = QWidgetAction(self._contextMenu)
        rawAct.setDefaultWidget(self.l_raw)
        self._contextMenu.addAction(rawAct)

        self.l_sig = QLabel('Signed')
        self.l_sig.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_sig.setAlignment(Qt.AlignLeft)
        sigAct = QWidgetAction(self._contextMenu)
        sigAct.setDefaultWidget(self.l_sig)
        self._contextMenu.addAction(sigAct)

        self.l_fun = QLabel('function')
        self.l_fun.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_fun.setAlignment(Qt.AlignLeft)
        funAct = QWidgetAction(self._contextMenu)
        funAct.setDefaultWidget(self.l_fun)
        self._contextMenu.addAction(funAct)

        self.l_raw.setStyleSheet("padding:3 10 3 10;")
        self.l_sig.setStyleSheet("padding:3 10 3 10;color:" + Colors.STR_COLOR_LIGHTRED + ";")
        self.l_fun.setStyleSheet("padding:3 10 3 10;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";")

        self.style_menu()

        action = self._contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == rawAct:
            self._type_format = "raw"
        elif action == sigAct:
            self._type_format = "sig"
        elif action == funAct:
            self._type_format = "fun"
            expression = self._function_expression
            if expression is None:
                expression = "x"

            text, ok = QInputDialog.getMultiLineText(self, 'Function Editor',
                                                     ' Only use x as variable\n ex:\n np.array(x,dtype=np.int32)\n fftpack.fft(x)\n x.real\n x[1:]\n x != xprev\n cycle == "CPS.USER.TOF"\n',
                                                     expression)
            if ok:
                self.update_function_signal.emit()
                self._function_expression = text
        self.update_signal.emit()

    def style_menu(self):
        """
        Change style menu.
        """
        if self._contextMenu is not None:
            self.l_raw.setStyleSheet("padding:3 10 3 10;")
            self.l_sig.setStyleSheet("padding:3 10 3 10;color:" + Colors.STR_COLOR_LIGHTRED + ";")
            self.l_fun.setStyleSheet("padding:3 10 3 10;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";")

    def set_led_type(self, type):
        """
        Set led color per type.

        :param type: Change format data display.
        :type type: str
        """
        if self._type_format == "raw":
            self._label_type.setStyleSheet("border:0;color:transparent;")
        elif self._type_format == "sig":
            self._label_type.setStyleSheet("border:0;color:" + Colors.STR_COLOR_LIGHTRED + ";")
        elif self._type_format == "fun":
            self._label_type.setStyleSheet("border:0;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";")

    def set_minimum_width_name(self, width):
        """
        Resize label name with a specific (fixed) size.
        
        :param width: Minimum widht of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def set_font_size(self, size=8):
        """
        Change font size (not used).
        
        :param size: Font size (default=8).
        :type size: int, optional
        """
        pass

    def show_chart(self, e):
        """
        Show chart plot.
        """
        if self._frame.isVisible():
            self._frame.hide()
        else:
            self._frame.show()
            self._data_widget.update_all_components()

    def dark_(self):
        """
        Set dark theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_WHITE + ";")
        self._label_table.setStyleSheet(
            "border-radius:4px ;background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:left;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";")
        self._chart_button.setStyleSheet("padding-right:5px;border:none;color:" + Colors.STR_COLOR_BLUE + ";")
        self._data_widget.dark_()
        self.setStyleSheet("QInputDialog {background-color:" + Colors.STR_COLOR_LBLACK + ";}")
        self.style_menu()

    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:" + Colors.STR_COLOR_BLACK + ";")
        self._label_table.setStyleSheet(
            "border-radius:4px ;background-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + ";text-align:left;padding:3px;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";")
        self._chart_button.setStyleSheet("padding-right:5px;border:none;color:" + Colors.STR_COLOR_BLUE + ";")
        self.setStyleSheet("QInputDialog {background-color:" + Colors.STR_COLOR_LIGHT5GRAY + ";}")
        self._data_widget.light_()
        self.style_menu()

    def set_data(self, data, cycle=None):
        """
        Set data in a plot item and table.

        :param data: Data structure.
        :type data: expert_gui_core.comm.data.DataObject
        :param cycle: Specify cycle name
        :type cycle: str, optional
        """

        if self.twod:
            if "list" not in str(type(data)) and "array" not in str(type(data)):
                if "int" in str(type(data)):
                    datanp = np.zeros(shape=(1, 1), dtype=np.intc)
                elif "short" in str(type(data)):
                    datanp = np.zeros(shape=(1, 1), dtype=np.short)
                elif "long" in str(type(data)):
                    datanp = np.zeros(shape=(1, 1), dtype=np.int_)
                elif "float" in str(type(data)):
                    datanp = np.zeros(shape=(1, 1), dtype=np.single)
                elif "double" in str(type(data)):
                    datanp = np.zeros(shape=(1, 1), dtype=np.double)
                datanp[0][0] = data
                data = datanp
        elif "list" not in str(type(data)) and "array" not in str(type(data)):
            if "list" not in str(type(data)) and "array" not in str(type(data)):
                if "int" in str(type(data)):
                    datanp = np.zeros(shape=(1), dtype=np.intc)
                elif "short" in str(type(data)):
                    datanp = np.zeros(shape=(1), dtype=np.short)
                elif "long" in str(type(data)):
                    datanp = np.zeros(shape=(1), dtype=np.int_)
                elif "float" in str(type(data)):
                    datanp = np.zeros(shape=(1), dtype=np.single)
                elif "double" in str(type(data)):
                    datanp = np.zeros(shape=(1), dtype=np.double)
                datanp[0] = data
                data = datanp
        if self._type_format == "sig":
            self.data = np.array(data, dtype=np.float128)
        elif self._type_format == "fun" and self._function_expression != None:
            try:
                xprev = self.data
                x = data
                functions = self._function_expression.split("\n")
                for function in functions:
                    xold = x
                    x = eval(function)
                    try:
                        if type(x) == type(True):
                            if x == False:
                                x = None
                            elif x == True:
                                x = xold
                            if x is None:
                                return
                    except:
                        pass
                data = x
            except Exception as e2:
                self._function_expression = None
            if data is None:
                return
            self.data = data
        else:
            self.data = data

        try:
            self.data_type = self.data.dtype
        except:
            self.data_type = "object"

        self.cycle = cycle
        self.update_signal.emit()
        self.update_signal_chart.emit()

    @pyqtSlot()
    def update_chart(self):
        """
        Update chart component.
        """
        self._data_widget.set_data(self.data, cycle=self.cycle, name=self._name, name_parent=self._name)

    @pyqtSlot()
    def update_data(self):
        """
        Update component table.
        """
        self.set_data_set(self.data)
        self.set_led_type(self._type_format)

    def set_data_set(self, data):
        """
        Set data in a plot item.

        :param data: Set data in the simple label component
        :type data: expert_gui_core.comm.data.DataObject
        """
        str_data = str(data).replace("\n", "")
        if len(str_data) > 30:
            self._label_table.setText(str_data[0:30])
        else:
            self._label_table.setText(str_data[0:len(str(str_data))])

    def get_data(self):
        """
        Get data.

        :return: Data array
        :rtype: NumPy array
        """
        data = self.data
        try:
            data = data.astype(self.data_type)
        except:
            pass
        return data

    def changeValue(self):
        """
        Change data event.
        """
        if self._signal is not None:
            self._signal.emit()

    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression

    def addSignal(self, signal):
        """
        Add PyQtSignal to be triggered if data changed.

        :param signal: Set specific signal
        ;type: PyQtSignal
        """
        self._signal = signal

    def get_signal(self):
        return self._signal


class _Example(QMainWindow):
    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0

    def init_ui(self):
        """Init user interface"""
        central_widget = QWidget()

        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)

        for i in range(1):
            self.array_widget = ArrayPanelWidget(self, title=None, label_name="name", twod=True, history=False)
            data = [
                [420, 380, 390, 120, 300, 100, 600],
                [50, 40, 45, 10, 10, 20, 35],
                [500, 400, 450, 100, 100, 200, 350]
            ]
            # data = [420, 380, 390, 120, 300, 100, 600]
            self.layout.addWidget(self.array_widget, i, 0)
            self.array_widget.set_data(data)
            self.array_widget.dark_()

        self.setCentralWidget(scroll)

        self.resize(500, 400)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(12)
    app.setFont(font_text_user)
    default_palette = QGuiApplication.palette()
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    app.setPalette(darkpalette)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
