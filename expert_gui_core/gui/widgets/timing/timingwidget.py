import sys

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QPoint

from PyQt5.QtGui import QPainter, QPalette, QBrush, QFont

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, \
    QHeaderView, QMainWindow, QVBoxLayout, QTabWidget, QLabel, \
    QPushButton, QGroupBox, QSplitter, QTableWidgetItem, QTableWidget, qApp

from pyjapc import PyJapc

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import fesacomm


class TimingPanel(QWidget):
    """
    
    Timing panel multi domain
    
    """

    list_cps = list({
        "AD", "EAST1", "EAST2", "EAST3", "EAST4", "ION1", "ION2", "ION3",
        "LHC1", "LHC2", "LHC3", "LHC4", "LHC5", "LHCIND1", "LHCIND2", "LHCIND3",
        "LHCPILOT", "MD1", "MD2", "MD3", "MD4", "MD5", "MD6", "MD7",
        "MD8", "MD9", "MD10", "SFTPRO1", "SFTPRO2", "SFTPRO3", "TOF", "ZERO"
    })
    list_psb = list({
        "AD", "EAST1", "EAST2", "EAST3", "LHC1A", "LHC1B", "LHC2A", "LHC2B",
        "LHC3", "LHC4", "LHC5", "LHCIND1", "LHCIND2", "LHCIND3", "LHCPILOT", "MD1",
        "MD2", "MD3", "MD4", "MD5", "MD6", "MD7", "MD8", "MD9",
        "MD10", "NORMGPS", "NORMHRS", "SFTPRO1", "SFTPRO2", "STAGISO", "TOF", "ZERO"
    })
    list_sps = list({
        "AWAKE1", "HIRADMT1", "HIRADMT2", "LHC1", "LHC2", "LHC25NS", "LHC3", "LHC4",
        "LHC50NS", "LHCINDIV", "LHCION1", "LHCION2", "LHCION3", "LHCION4", "LHCMD1", "LHCMD2",
        "LHCMD3", "LHCMD4", "LHCPILOT", "MD1", "MD2", "MD3", "MD4", "MD5",
        "SFTION1", "SFTION2", "SFTION3", "SFTION4", "SFTPRO1", "SFTPRO2", "SFTSHIP", "ZERO"
    })
    list_lei = list({
        "AMD", "AMDEC", "AMDNOM", "AMDOPTIC", "AMDRF", "ANOMINAL", "BIOMD", "EARLY",
        "FL_IN_MD", "FL_IN_SU", "FL_NO_MD", "LIN3MEAS", "MD1", "MD2", "MD3", "MD4",
        "MD5", "MD6", "MD7", "MD8", "MDEARLY", "MDEC", "MDNOM", "MDOPTIC",
        "MDRF", "MD_100NS", "L3_MONIT", "MD_75NS", "NOMINAL", "NOM_75NS", "POLARITY", "ZERO"
    })
    list_ade = list({
        "ADE"
    })
    list_lna = list({
        "HMMD1", "HMMD2", "HMMDEC", "HMMDOPT", "HMMDRF", "HMPROD1", "HMPROD2", "PBMD1",
        "PBMD2", "PBMD3", "PBMDEC", "PBMDOPT", "PBMDRF", "PBPRD1", "PBPRD2", "PMD1",
        "PMD2", "PMD3", "MD1", "MD2", "MD3", "MD4", "MD5", "MD6",
        "MD7", "MD8", "PMDEC", "PMDOPT", "PMDRF", "PPROD1", "PPROD2", "ZERO"
    })

    _signal_cycle_selected = pyqtSignal(str)

    def __init__(self, domain=[], parent=None, listener=None, minimized=True, subscribe=False):
        QWidget.__init__(self, parent)
        self.domain = domain
        self.listener = listener
        self._subscribe = subscribe
        self.init_ui()
        if minimized:
            for subtimingpanel in self.sub_panels:
                subtimingpanel.hide_tables()
            self.resize(self.minimumSizeHint())
        qApp.processEvents()

    def init_ui(self):
        """Init UI function"""
        mainLayout = QGridLayout()
        mainLayout.setContentsMargins(0, 0, 0, 10)
        mainLayout.setSpacing(0)
        self._tabs = QTabWidget()
        self.sub_panels = []

        if len(self.domain) > 1:
            single = False
        else:
            single = True
        for dom in self.domain:

            biswsubtimingpanel = _BiswSubTimingPanel(self,
                                                     dom,
                                                     listener=self.listener,
                                                     single=single,
                                                     subscribe=self._subscribe)
            self.sub_panels.append(biswsubtimingpanel)
            biswsubtimingpanel.set_signal_cycle_selected(self._signal_cycle_selected)
            if len(self.domain) > 1:
                self._tabs.addTab(biswsubtimingpanel, dom)
            else:
                mainLayout.addWidget(biswsubtimingpanel, 1, 0)

        self._signal_cycle_selected.connect(self.cycle_selected)

        if len(self.domain) > 1:
            mainLayout.addWidget(self._tabs, 1, 0)
        self.setLayout(mainLayout)

        self.set_font_size(9)

    @pyqtSlot(str)
    def cycle_selected(self, str):
        pass

    def expand(self):
        """Expand sub panels, make them visible"""
        for subpanel in self.sub_panels:
            subpanel.show_tables()

    def set_font_size(self, size=8):
        """Change font size"""
        font_tab = QFont()
        font_tab.setPointSize(size - 1)
        self._tabs.tabBar().setFont(font_tab)

    def dark_(self):
        """Change GUI skin to dark theme"""
        self._tabs.setStyleSheet(
            "background-color: " + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        for subpanel in self.sub_panels:
            subpanel.dark_()

    def light_(self):
        """Change GUI skin to light theme"""
        self._tabs.setStyleSheet(
            "background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        for subpanel in self.sub_panels:
            subpanel.light_()

    def get_cycle(self):
        """Return selected cycle"""
        if len(self.domain) > 1:
            return self.sub_panels[self._tabs.currentIndex()].get_cycle()
        else:
            return self.sub_panels[0].get_cycle()

    def get_cycle_per_dom(self, domain):
        """Get cycle for a specific domain"""
        if domain == "LHC":
            return "LHC.USER.ALL"
        else:
            index = self.domain.index(domain)
            return self.sub_panels[index].get_cycle()

    @property
    def signal_cycle_selected(self):
        return self._signal_cycle_selected


class _BiswSubTimingPanel(QWidget):
    """
    
    Timing panel for one specific domain
    
    """

    update_signal = pyqtSignal(str, object)

    def __init__(self, bi_timing_panel=None, subdomain="", parent=None, listener=None, single=False, subscribe=False):
        QWidget.__init__(self, parent)
        self.bi_timing_panel = bi_timing_panel
        self.listener = listener
        self.subdomain = subdomain
        self._subscribe = subscribe
        self._table_visible = False

        self.init_ui()
        self.size_tot_user = 0
        self.super_cycle_old = 1
        self.i_user = 0
        self.j_user = 0
        self.light = True

        self._single = single
        self.update_signal.connect(self.on_sub_update_e)

        if fesacomm.FesaComm.JAPC is None:
            fesacomm.FesaComm.JAPC = PyJapc(incaAcceleratorName=None)
        self.comms = fesacomm.FesaComm.JAPC

        self.start_monitoring()

    def set_signal_cycle_selected(self, signal):
        self.timing_tables.set_signal_cycle_selected(signal)

    def start_monitoring(self):
        if self.subdomain == "CPS":
            self.comms.setSelector('CPS.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.PX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)
        elif self.subdomain == "PSB":
            self.comms.setSelector('PSB.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.BX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)
        elif self.subdomain == "SPS":
            self.comms.setSelector('SPS.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.SX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)
        elif self.subdomain == "LEI":
            self.comms.setSelector('LEI.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.EX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)
        elif self.subdomain == "LNA":
            self.comms.setSelector('LNA.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.AX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)
        elif self.subdomain == "ADE":
            self.comms.setSelector('ADE.USER.ALL')
            self.subs = self.comms.subscribeParam("XTIM.DX.SCY-CT/Acquisition", self.on_sub_update,
                                                  onException=self.dev_null)

        self.subs.startMonitoring()

    def dev_null(*args, **kwargs):
        pass

    def exception(parameterName, description, exception):
        """
        Exception.
        
        :param parameterName: Param name (not used).
        :type param: str
        :param description: Exception desc (not used).
        :type desc: str
        :param exception: Exception.
        :type ex: exception
        """
        # pass
        msg = str(exception)
        if "cern.japc.core.NoDataAvailableException" not in msg:
            print(parameterName, exception)

    def init_ui(self):
        """Init user interface"""
        self.setAutoFillBackground(True)
        mainLayout = QVBoxLayout()
        mainLayout.addStretch(1)
        mainLayout.setSpacing(0)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.timing_line = _TimingLine(self)
        self.timing_line.setMinimumHeight(22)
        self.timing_line.setMaximumHeight(22)
        self.timing_tables = _TimingTables(
            self, self.subdomain, listener=self.listener, subscribe=self._subscribe)
        self.panel = QWidget()
        self.panel.setAutoFillBackground(True)
        layout = QGridLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.timing_tables, 1, 0)
        layout.addWidget(self.timing_line, 2, 0)
        layout.setContentsMargins(2, 2, 2, 2)
        self.panel.setLayout(layout)
        mainLayout.addWidget(self.panel)
        self.setLayout(mainLayout)

    def dark_(self):
        """Change GUI skin to dark theme"""
        self.light = False
        self.timing_line.dark_()
        self.timing_tables.dark_()

    def light_(self):
        """Change GUI skin to light theme"""
        self.light = True
        self.timing_line.light_()
        self.timing_tables.light_()

    def hide_tables(self):
        """Hide tables on top"""
        self._table_visible = False
        self.timing_tables.hide()
        if self._single:
            self.bi_timing_panel.setMaximumHeight(30)
            self.bi_timing_panel.setMinimumHeight(30)
        else:
            self.bi_timing_panel.setMaximumHeight(75)
            self.bi_timing_panel.setMinimumHeight(75)

    def show_tables(self):
        """Show table or not"""
        if self._table_visible:
            self.hide_tables()
        else:
            self._table_visible = True
            self.timing_tables.show()
            if self._single:
                self.bi_timing_panel.setMaximumHeight(227)
                self.bi_timing_panel.setMinimumHeight(227)
            else:
                self.bi_timing_panel.setMaximumHeight(250)
                self.bi_timing_panel.setMinimumHeight(250)
        self.resize(self.minimumSizeHint())

    def resizeEvent(self, e):
        """Resize component"""
        w = self.width()
        h = self.height()
        if self.timing_tables.fieldTableI is not None:
            if w < 1100:
                self.timing_tables.fieldTableI.hide()
            else:
                self.timing_tables.fieldTableI.show()

    def on_sub_update(self, name, val):
        """Update received"""
        self.update_signal.emit(name, val)

    @pyqtSlot(str, object)
    def on_sub_update_e(self, name, val):
        """Update tables/line per domain"""
        user = ""
        cycle_duration = 0
        basic_period_nb = val['BASIC_PERIOD_NB']
        cycle_duration = val['CYCLE_DURATION_MS'] / 1200.
        user = val['USER']
        try:
            super_cycle_nb = val['SUPERCYCLE_NB']
        except:
            if self.super_cycle_old == 1:
                super_cycle_nb = 0
            else:
                super_cycle_nb = 1
        size_items = len(val)
        if self.timing_tables.fieldTableR.item(self.j_user, self.i_user) is not None:
            try:
                self.timing_tables.fieldTableR.item(self.j_user, self.i_user).setBackground(Qt.transparent)
                if self.light == True:
                    self.timing_tables.fieldTableR.item(self.j_user, self.i_user).setForeground(
                        QBrush(Colors.COLOR_LIGHT1GRAY))
                else:
                    self.timing_tables.fieldTableR.item(self.j_user, self.i_user).setForeground(
                        QBrush(Colors.COLOR_LIGHT3GRAY))
            except:
                pass
        try:
            cycle_nb = val['CYCLE_NB']
        except:
            cycle_nb = 1
        if cycle_nb == 1:
            self.super_cycle_old = super_cycle_nb
            self.size_tot_user = 0
        try:
            lsa_cycle_name = val['lsaCycleName']
        except:
            lsa_cycle_name = ""
        self.i_user = int(self.size_tot_user % 6)
        self.j_user = int(self.size_tot_user / 6.)
        self.timing_tables.fieldTableR.setItem(self.j_user, self.i_user, QTableWidgetItem(str(user)))
        try:
            self.timing_tables.fieldTableR.item(self.j_user, self.i_user).setBackground(
                QBrush(Colors.COLOR_LIGHTORANGE))
            self.timing_tables.fieldTableR.item(self.j_user, self.i_user).setForeground(QBrush(Colors.COLOR_BLACK))
        except:
            pass
        self.size_tot_user = self.size_tot_user + 1
        self.timing_tables.fieldTableI.setColumnCount(2)
        self.timing_tables.fieldTableI.setRowCount(size_items)
        self.timing_tables.fieldTableI.setItem(0, 0, QTableWidgetItem('BASIC_PERIOD_NB'))
        self.timing_tables.fieldTableI.setItem(0, 1, QTableWidgetItem(str(val['BASIC_PERIOD_NB'])))
        self.timing_tables.fieldTableI.setItem(1, 0, QTableWidgetItem('BEAM_ID'))
        self.timing_tables.fieldTableI.setItem(1, 1, QTableWidgetItem(str(val['BEAM_ID'])))
        self.timing_tables.fieldTableI.setItem(2, 0, QTableWidgetItem('BP_DURATION_MS'))
        self.timing_tables.fieldTableI.setItem(2, 1, QTableWidgetItem(str(val['BP_DURATION_MS'])))
        self.timing_tables.fieldTableI.setItem(3, 0, QTableWidgetItem('CYCLE_DURATION_MS'))
        self.timing_tables.fieldTableI.setItem(3, 1, QTableWidgetItem(str(val['CYCLE_DURATION_MS'])))
        self.timing_tables.fieldTableI.setItem(4, 0, QTableWidgetItem('CYCLE_NB'))
        self.timing_tables.fieldTableI.setItem(4, 1, QTableWidgetItem(str(cycle_nb)))
        self.timing_tables.fieldTableI.setItem(5, 0, QTableWidgetItem('CYCLE_TAG'))
        try:
            cycle_tag = val['CYCLE_TAG']
        except:
            cycle_tag = ""
        self.timing_tables.fieldTableI.setItem(5, 1, QTableWidgetItem(str(cycle_tag)))
        self.timing_tables.fieldTableI.setItem(6, 0, QTableWidgetItem('PARTICLE'))
        self.timing_tables.fieldTableI.setItem(6, 1, QTableWidgetItem(str(val['PARTICLE'])))
        self.timing_tables.fieldTableI.setItem(7, 0, QTableWidgetItem('USER'))
        self.timing_tables.fieldTableI.setItem(7, 1, QTableWidgetItem(str(val['USER'])))
        self.timing_tables.fieldTableI.setItem(8, 0, QTableWidgetItem('acqC'))
        self.timing_tables.fieldTableI.setItem(8, 1, QTableWidgetItem(str(val['acqC'])))
        self.timing_tables.fieldTableI.setItem(9, 0, QTableWidgetItem('lsaCycleName'))
        self.timing_tables.fieldTableI.setItem(9, 1, QTableWidgetItem(str(lsa_cycle_name)))
        self.timing_tables.fieldTableI.setItem(10, 0, QTableWidgetItem('oCounter'))
        self.timing_tables.fieldTableI.setItem(10, 1, QTableWidgetItem(str(val['oCounter'])))
        if self.subdomain == "CPS":
            self.timing_tables.fieldTableI.setItem(11, 0, QTableWidgetItem('DYN_DEST_EAST'))
            self.timing_tables.fieldTableI.setItem(11, 1, QTableWidgetItem(str(val['DYN_DEST_EAST'])))
            self.timing_tables.fieldTableI.setItem(12, 0, QTableWidgetItem('DYN_DEST_FTS'))
            self.timing_tables.fieldTableI.setItem(12, 1, QTableWidgetItem(str(val['DYN_DEST_FTS'])))
            self.timing_tables.fieldTableI.setItem(13, 0, QTableWidgetItem('DYN_DEST_TOF'))
            self.timing_tables.fieldTableI.setItem(13, 1, QTableWidgetItem(str(val['DYN_DEST_TOF'])))
            self.timing_tables.fieldTableI.setItem(14, 0, QTableWidgetItem('PROG_DEST'))
            self.timing_tables.fieldTableI.setItem(14, 1, QTableWidgetItem(str(val['PROG_DEST'])))
            self.timing_tables.fieldTableI.setItem(15, 0, QTableWidgetItem('PROG_DEST2'))
            self.timing_tables.fieldTableI.setItem(15, 1, QTableWidgetItem(str(val['PROG_DEST2'])))
        elif self.subdomain == "PSB":
            self.timing_tables.fieldTableI.setItem(11, 0, QTableWidgetItem('CONSEQ_DISO_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(11, 1, QTableWidgetItem(str(val['CONSEQ_DISO_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(12, 0, QTableWidgetItem('CONSEQ_HRS_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(12, 1, QTableWidgetItem(str(val['CONSEQ_HRS_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(13, 0, QTableWidgetItem('CONSEQ_ISO_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(13, 1, QTableWidgetItem(str(val['CONSEQ_ISO_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(14, 0, QTableWidgetItem('CONSEQ_PS_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(14, 1, QTableWidgetItem(str(val['CONSEQ_PS_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(15, 0, QTableWidgetItem('DYN_LINAC_DEST'))
            self.timing_tables.fieldTableI.setItem(15, 1, QTableWidgetItem(str(val['DYN_LINAC_DEST'])))
            self.timing_tables.fieldTableI.setItem(16, 0, QTableWidgetItem('END_BEAM'))
            self.timing_tables.fieldTableI.setItem(16, 1, QTableWidgetItem(str(val['END_BEAM'])))
            self.timing_tables.fieldTableI.setItem(17, 0, QTableWidgetItem('PROG_LINAC_DEST'))
            self.timing_tables.fieldTableI.setItem(17, 1, QTableWidgetItem(str(val['PROG_LINAC_DEST'])))
            self.timing_tables.fieldTableI.setItem(18, 0, QTableWidgetItem('PROG_PSB_DEST'))
            self.timing_tables.fieldTableI.setItem(18, 1, QTableWidgetItem(str(val['PROG_PSB_DEST'])))
            self.timing_tables.fieldTableI.setItem(19, 0, QTableWidgetItem('PSB_NO_RING_MASK'))
            self.timing_tables.fieldTableI.setItem(19, 1, QTableWidgetItem(str(val['PSB_NO_RING_MASK'])))
            self.timing_tables.fieldTableI.setItem(20, 0, QTableWidgetItem('SPS_BATCH_NUMBER'))
            self.timing_tables.fieldTableI.setItem(20, 1, QTableWidgetItem(str(val['SPS_BATCH_NUMBER'])))
            self.timing_tables.fieldTableI.setItem(21, 0, QTableWidgetItem('START_BEAM'))
            self.timing_tables.fieldTableI.setItem(21, 1, QTableWidgetItem(str(val['START_BEAM'])))
            self.timing_tables.fieldTableI.setItem(22, 0, QTableWidgetItem('START_DISO_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(22, 1, QTableWidgetItem(str(val['START_DISO_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(23, 0, QTableWidgetItem('START_HRS_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(23, 1, QTableWidgetItem(str(val['START_HRS_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(24, 0, QTableWidgetItem('START_ISO_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(24, 1, QTableWidgetItem(str(val['START_ISO_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(25, 0, QTableWidgetItem('START_PS_SEQUENCE'))
            self.timing_tables.fieldTableI.setItem(25, 1, QTableWidgetItem(str(val['START_PS_SEQUENCE'])))
            self.timing_tables.fieldTableI.setItem(26, 0, QTableWidgetItem('TAIL_CLIPPER'))
            self.timing_tables.fieldTableI.setItem(26, 1, QTableWidgetItem(str(val['TAIL_CLIPPER'])))

            # Update timing line

        self.timing_line.set_bp(basic_period_nb - 1 + cycle_duration, user, cycle_nb, val['CYCLE_DURATION_MS'],
                                str(lsa_cycle_name))

    def get_cycle(self):
        """Return selected cycle"""
        return self.timing_tables.cycle_selected.text()


class _TimingTables(QWidget):
    """
    
    Timing tables widget
    
    """

    def __init__(self, bi_sub_timing_panel, subdomain="", parent=None, listener=None, subscribe=False):
        QWidget.__init__(self, parent)
        self.subdomain = subdomain
        self._subscribe = subscribe
        self.bi_sub_timing_panel = bi_sub_timing_panel
        self.listener = listener
        self._signal_selected = None
        self.init_ui()

    def init_ui(self):
        """Init user interface"""
        mainLayout = QGridLayout()
        splitter = QSplitter()

        self.fieldTable = QTableWidget()
        self.fieldTable.setRowCount(5)
        self.fieldTable.setColumnCount(7)
        self.fieldTable.horizontalHeader().hide()
        self.fieldTable.verticalHeader().hide()
        self.fieldTable.verticalHeader().setDefaultSectionSize(10)
        self.fieldTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.palette_table_user_to_select = QPalette()
        self.palette_table_user_to_select.setColor(QPalette.Highlight, Colors.COLOR_BLUE)
        self.palette_table_user_to_select.setColor(QPalette.HighlightedText, Colors.COLOR_BLACK)
        self.fieldTable.setPalette(self.palette_table_user_to_select)
        self.fieldTable.cellClicked.connect(self.fieldTable_clicked)

        TimingPanel.list_cps.sort()
        TimingPanel.list_psb.sort()
        TimingPanel.list_sps.sort()
        TimingPanel.list_lei.sort()
        TimingPanel.list_lna.sort()
        TimingPanel.list_ade.sort()

        for i in range(0, 5):
            for j in range(0, 7):
                if (i * 7 + j) < 32:
                    if self.subdomain == "CPS":
                        user_name = TimingPanel.list_cps[i * 7 + j]
                    elif self.subdomain == "PSB":
                        user_name = TimingPanel.list_psb[i * 7 + j]
                    elif self.subdomain == "SPS":
                        user_name = TimingPanel.list_sps[i * 7 + j]
                    elif self.subdomain == "LEI":
                        user_name = TimingPanel.list_lei[i * 7 + j]
                    elif self.subdomain == "LNA":
                        user_name = TimingPanel.list_lna[i * 7 + j]
                    elif self.subdomain == "ADE":
                        if i == 0 and j == 0:
                            user_name = TimingPanel.list_ade[0]
                            self.fieldTable.setItem(i, j, QTableWidgetItem(user_name))
                    if self.subdomain != "ADE":
                        self.fieldTable.setItem(i, j, QTableWidgetItem(user_name))

        p = QPalette()
        p.setColor(QPalette.Highlight, Colors.COLOR_BLUE)
        p.setColor(QPalette.HighlightedText, Colors.COLOR_BLACK)

        self.fieldTableR = QTableWidget()
        self.fieldTableR.setRowCount(40)
        self.fieldTableR.setColumnCount(6)
        self.fieldTableR.horizontalHeader().hide()
        self.fieldTableR.verticalHeader().hide()
        self.fieldTableR.verticalHeader().setDefaultSectionSize(10)
        self.fieldTableR.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.fieldTableR.setPalette(p)

        self.fieldTableI = QTableWidget()
        self.fieldTableI.setRowCount(4)
        self.fieldTableI.setColumnCount(2)
        self.fieldTableI.horizontalHeader().hide()
        self.fieldTableI.verticalHeader().hide()
        self.fieldTableI.verticalHeader().setDefaultSectionSize(10)
        self.fieldTableI.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.fieldTableI.setPalette(p)

        self._size_font = 8
        self.font_painter = QFont()
        self.font_painter.setPointSize(self._size_font)

        self.font_painter_small = QFont()
        self.font_painter_small.setPointSize(self._size_font)

        self.fieldTable.setFont(self.font_painter)
        self.fieldTableR.setFont(self.font_painter)
        self.fieldTableI.setFont(self.font_painter_small)

        splitter.setContentsMargins(0, 0, 0, 0)

        splitter.addWidget(self.fieldTable)
        splitter.addWidget(self.fieldTableR)
        splitter.addWidget(self.fieldTableI)

        splitter.setOrientation(Qt.Horizontal)
        splitter.setStretchFactor(0, 2)
        splitter.setStretchFactor(1, 2)
        splitter.setStretchFactor(2, 1)

        self.panel = QWidget()
        self.panel.setAutoFillBackground(True)
        layout = QVBoxLayout()

        layout.setSpacing(5)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(splitter)

        layout_h = QGridLayout(self)
        layout_h.setContentsMargins(5, 5, 5, 5)
        layout_h.setSpacing(5)

        self.horizontalGroupBox = QGroupBox()
        self.horizontalGroupBox.setLayout(layout_h)

        self.cycle_selected = QLabel(self.subdomain + ".USER.ALL")
        self.cycle_selected.setStyleSheet("color: " + Colors.STR_COLOR_BLACK + ";")
        font = QFont()
        font.setPointSize(self._size_font + 1)
        font.setBold(True)
        self.cycle_selected.setFont(font)
        layout_h.addWidget(self.cycle_selected, 0, 2)

        button_none = QPushButton()
        button_none.setText("\u03A6")
        button_none.setStyleSheet("padding: 3 8 3 8px;");
        button_none.clicked.connect(self.button_none_clicked)
        layout_h.addWidget(button_none, 0, 0)

        button_all = QPushButton()
        button_all.setText("All")
        button_all.setStyleSheet("padding: 3 8 3 8px;");
        button_all.clicked.connect(self.button_all_clicked)
        layout_h.addWidget(button_all, 0, 1)

        self.button_start = QPushButton()
        self.button_start.setText("Subscribe")
        self.button_start.setStyleSheet("padding: 3 8 3 8px;");
        self.button_start.clicked.connect(self.action)
        if self._subscribe:
            layout_h.addWidget(self.button_start, 0, 3)

        layout_h.setColumnStretch(0, 0)
        layout_h.setColumnStretch(1, 0)
        layout_h.setColumnStretch(2, 1)
        layout_h.setColumnStretch(3, 0)

        layout.addWidget(self.horizontalGroupBox)

        self.panel.setLayout(layout)
        mainLayout.setSpacing(0)
        mainLayout.addWidget(self.panel)

        self.setLayout(mainLayout)

    def action(self):
        """START or STOP action to listener"""
        if self.button_start.text() == "Subscribe":
            if self.listener is not None:
                self.listener.start(self.cycle_selected.text())
            self.button_start.setText("Stop")
            self.button_start.setStyleSheet("padding: 3 8 3 8px;background-color:" + Colors.STR_COLOR_LIGHTRED + ";")
        else:
            if self.listener is not None:
                self.listener.stop()
            self.button_start.setText("Subscribe")
            self.button_start.setStyleSheet("padding: 3 8 3 8px;background-color:none;")

    def dark_(self):
        """Set dark theme"""
        self.palette_table_user_to_select.setColor(QPalette.Highlight, Colors.COLOR_BLUE)
        self.fieldTable.setPalette(self.palette_table_user_to_select)
        self.cycle_selected.setStyleSheet("color: " + Colors.STR_COLOR_WHITE + ";")

    def light_(self):
        """Set light theme"""
        self.palette_table_user_to_select.setColor(QPalette.Highlight, Colors.COLOR_BLUE)
        self.fieldTable.setPalette(self.palette_table_user_to_select)
        self.cycle_selected.setStyleSheet("color: " + Colors.STR_COLOR_BLACK + ";")

    def button_none_clicked(self):
        """Select ALL cycles"""
        self.cycle_selected.setText("None")
        if self._signal_selected is not None:
            self._signal_selected.emit("None")

    def button_all_clicked(self):
        """Select ALL cycles"""
        self.cycle_selected.setText(self.subdomain + ".USER.ALL")
        if self._signal_selected is not None:
            self._signal_selected.emit(self.subdomain + ".USER.ALL")

    def fieldTable_clicked(self, row, col):
        """Select cycle from table"""
        item = self.fieldTable.item(row, col)
        try:
            if item is not None:
                self.cycle_selected.setText(self.subdomain + ".USER." + item.text())
                if self._signal_selected is not None:
                    self._signal_selected.emit(self.subdomain + ".USER." + item.text())
        except:
            pass

    def set_signal_cycle_selected(self, signal):
        self._signal_selected = signal


class _TimingLine(QWidget):
    """
    
    Timing line shown at the bottom
    
    """

    def __init__(self, bi_sub_timing_panel, parent=None):
        QWidget.__init__(self, parent)
        self._font_size = 8
        self.bi_sub_timing_panel = bi_sub_timing_panel
        self.listepoints = []
        self.bp_last = 0
        self.end_of_scycle = 0
        self.users = []
        self.cycle_nb = []
        self.cycle_duration = []
        self.size_tot_user = 0
        self.lsa_cycle_name = ""
        self.font_text_user = QFont()
        self.font_text_user.setBold(True)
        self.font_text_user.setPointSize(self._font_size)
        self.dark_()

    def dark_(self):
        """Set dark theme"""
        self.color_bg = Colors.COLOR_BLACK
        self.color_inactive = Colors.COLOR_L3BLUE
        self.color_inactive_2 = Colors.COLOR_BLUE
        self.color_inactive_low = Colors.COLOR_LIGHT0GRAY
        self.color_active = Colors.COLOR_LIGHTORANGE
        self.color_pink = Colors.COLOR_PINK
        self.color_user_active = Colors.COLOR_WHITE

    def light_(self):
        """Set light theme"""
        self.color_bg = Colors.COLOR_WHITE
        self.color_inactive = Colors.COLOR_L3BLUE
        self.color_inactive_2 = Colors.COLOR_BLUE
        self.color_inactive_low = Colors.COLOR_LIGHT4GRAY
        self.color_active = Colors.COLOR_LIGHTORANGE
        self.color_pink = Colors.COLOR_PINK
        self.color_user_active = Colors.COLOR_BLACK

    def set_bp(self, bp, user, cycle_nb, cycle_duration, lsa_cycle_name):
        """Set BP update"""

        # Is first

        if cycle_nb == 1:
            self.users = []
            self.cycle_nb = []
            self.cycle_duration = []
            self.end_of_scycle = self.bp_last

            # Assign data

        self.bp_last = bp
        self.users.append(user)
        self.cycle_nb.append(cycle_nb)
        self.cycle_duration.append(int(cycle_duration / 1200))
        self.lsa_cycle_name = lsa_cycle_name

        # Update paint

        self.update()

    def mousePressEvent(self, e):
        """Mouse pressed in table"""
        self.bi_sub_timing_panel.bi_timing_panel.expand()

    def paintEvent(self, e):
        """Paint event handling"""
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        gaph = 8
        gapv = 10
        inter_gap = 5

        w_rect = self.frameGeometry().width()
        h_rect = self.frameGeometry().height() - 10

        bp_prev = 0
        bascule = True
        NTOT_SQUARE = 120

        w_wagon = (w_rect - 2. * gapv - (NTOT_SQUARE - 1.) * inter_gap) / (NTOT_SQUARE)

        painter.setBrush(self.color_inactive_low)
        painter.setPen(self.color_inactive_low)
        for i in range(0, NTOT_SQUARE):
            painter.drawRect(int(gapv + i * (w_wagon + inter_gap)), int(11 + gaph), int(w_wagon),
                             int(h_rect - 2 * gaph))

        if len(self.users) == 0:
            return

        painter.setFont(self.font_text_user)

        for i in range(0, len(self.users)):

            if bascule:
                painter.setPen(self.color_inactive)
                painter.setBrush(self.color_inactive)
            else:
                painter.setPen(self.color_inactive_2)
                painter.setBrush(self.color_inactive_2)

            if i == 0 or self.cycle_nb[i] != self.cycle_nb[i - 1]:
                bascule = not bascule

            if i == len(self.users) - 1:
                painter.setPen(self.color_user_active)
                painter.drawText(QPoint(int(gapv + (bp_prev * 0) * (w_wagon + inter_gap)), 10),
                                 "[" + str(self.bi_sub_timing_panel.subdomain) + "] " + str(self.cycle_nb[i]) + " " +
                                 self.users[i] + " | " + self.lsa_cycle_name)
                painter.setPen(self.color_active)
                painter.setBrush(self.color_active)

            for j in range(bp_prev, bp_prev + self.cycle_duration[i]):
                painter.drawRect(int(gapv + j * (w_wagon + inter_gap)), int(11 + gaph), int(w_wagon),
                                 int(h_rect - 2 * gaph))

            bp_prev = bp_prev + self.cycle_duration[i]

        painter.end()


class _Example(QMainWindow):
    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """Init user interface"""
        w = QWidget()
        domain = ["CPS", "PSB", "SPS", "LEI", "LNA", "ADE"]
        # domain = ["CPS"]
        self.timing_panel = TimingPanel(domain=domain, listener=None)
        self.setWindowTitle('Timing Panel')
        mainLayout = QGridLayout()
        mainLayout.addWidget(self.timing_panel, 1, 0)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(1200, 210)

        self.timing_panel.dark_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
        }
        QMenuBar::item:selected {    
            background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }
        QMenuBar::item:pressed {
            background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
        }              
        QLineEdit:{background-color: black;}
    """
    app.setStyleSheet(qss)

    ex = _Example()

    ex.show()
    sys.exit(app.exec_())
