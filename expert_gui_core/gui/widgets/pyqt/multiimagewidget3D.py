import sys
from time import sleep
import numpy as np
import pyqtgraph as pg
from enum import Enum
import pyqtgraph.opengl as gl
from traceback import print_exception
import scipy.ndimage as ndi
from typing import List, Dict, NamedTuple, Any, Type, Tuple, TypeVar, Union, Optional
from pyqtgraph import makeRGBA, colormap
from pyqtgraph.opengl import GLImageItem, GLViewWidget, GLGridItem, GLAxisItem, GLMeshItem


from PyQt5.QtGui import QPixmap, QVector3D, QKeyEvent, QWheelEvent, QFocusEvent, QMouseEvent, QImage, QFontMetrics, QKeySequence

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QThread

from PyQt5.QtWidgets import QWidget, QMainWindow, QApplication, QVBoxLayout, QGridLayout, QLabel, QCheckBox, \
    QButtonGroup, QRadioButton, QSpacerItem, QSizePolicy, QSplitter, QScrollArea, QHBoxLayout, QPushButton

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm.data import DataObject
from expert_gui_core.gui.widgets.pyqt import imagewidget



class _MultiImageIdType(Enum):
    ImageIndex = 1


class _CameraPosition(Enum):
    Center = 1
    Corner = 2


class _VignetteUpdate:

    def __init__(self, vignette, grab=False, clear_previous=True, clear_selected=False):
        self._grab = grab
        self._clear_previous = clear_previous
        self._vignette = vignette
        self._clear_selected = clear_selected

    def clear_selected(self):
        return self._clear_selected

    def vignette(self):
        return self._vignette

    def grab(self):
        return self._grab

    def clear_previous(self):
        return self._clear_previous


class _Vignette(QWidget):
    signal_clicked = pyqtSignal(_VignetteUpdate)
    signal_mouse_enter = pyqtSignal()
    signal_mouse_leave = pyqtSignal()
    signal_selected_changed = pyqtSignal()

    def __init__(self, parent, object_id: str, pixmap: QPixmap, max_width=250):
        super(QWidget, self).__init__(parent)

        self._parent = parent
        self._object_id = object_id
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(1, 1, 1, 1)

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        self._pixmap_label = QLabel()
        self.layout().addWidget(self._pixmap_label)
        self._pixmap_scaled = pixmap.scaledToWidth(max_width)

        self._pixmap = pixmap
        self._pixmap_label.setPixmap(self._pixmap_scaled)
        self._selection_active = False

    def get_pixmap(self):
        return self._pixmap

    def mouseReleaseEvent(self, event):
        grab = bool((QApplication.keyboardModifiers() & Qt.ShiftModifier))
        clear_previous = not grab and event.button() != Qt.MiddleButton
        self.signal_clicked.emit(
            _VignetteUpdate(self, clear_selected=self._selection_active, clear_previous=clear_previous, grab=grab))

    def keyPressEvent(self, event):
        if event.key() == Qt.ShiftModifier:
            pass

    def mousePressEvent(self, event):
        self._selection_active = True

    def mouseMoveEvent(self, event):
        if self._selection_active:
            pass

    def set_active_styles(self):
        self.setStyleSheet('border: 2px solid red;')

    def set_inactive_styles(self):
        self.setStyleSheet('border: none;')

    def object_id(self):
        return self._object_id


class _Vignettes(QWidget):
    selection_changed = pyqtSignal(object)

    def __init__(self, parent, orientation, max_width=250):
        super(QWidget, self).__init__()
        self._parent = parent
        self.setFocusPolicy(Qt.StrongFocus)

        self._active_vignettes = []
        if orientation == Qt.Horizontal:
            self.setLayout(QHBoxLayout())
        else:
            self.setLayout(QVBoxLayout())

        self.layout().setSpacing(3)
        self.layout().setContentsMargins(3, 3, 0, 3)
        self._max_vignette_width = max_width
        self._spacer = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self._vignettes = {}
        self._vignettes_ordered = []

        self.layout().addItem(self._spacer)

    def clear_data(self):
        self._vignettes = {}
        self._vignettes_ordered = []
        self._active_vignettes = []

    def add(self, object_id: str, rgba_images):
        if not isinstance(rgba_images, List):
            rgba_images = [rgba_images]
        for image in rgba_images:
            pixmap = QPixmap(QImage(image, len(rgba_images[0][0]), len(rgba_images[0]), QImage.Format_RGBA8888))
            vignette = _Vignette(self, object_id, pixmap, max_width=100)
            self.layout().addWidget(vignette)
            vignette.signal_clicked.connect(self.update_selection)
            self._vignettes[object_id] = vignette
            self._vignettes_ordered.append(object_id)

    def dark_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLACK + ";")

    def light_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")

    def update_selection(self, update):
        if update.grab() and len(self._active_vignettes) == 1:
            try:
                index_1 = self._vignettes_ordered.index(self._active_vignettes[0].object_id())
                index_2 = index_1
            except:
                self.set_none_active()
                return
            start = index_1 if index_1 < index_2 else index_2
            end = index_2 if index_2 > index_1 else index_1
            new_active_vignettes = []
            for object_id in self._vignettes_ordered[start:end + 1]:
                new_active_vignettes.append(self._vignettes[object_id])
            self.set_active_vignettes(new_active_vignettes)
        elif update.clear_previous():
            self.set_active_vignettes([update.vignette()])
        elif update.clear_selected():
            if update.vignette() in self._active_vignettes:
                self._active_vignettes.remove(update.vignette())
            else:
                self._active_vignettes.append(update.vignette())
            self.set_active_vignettes(self._active_vignettes)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Up or key == Qt.Key_Left or key == Qt.Key_Backtab:
            self.set_previous_vignette_active()
        elif key == Qt.Key_Down or key == Qt.Key_Right or key == Qt.Key_Tab:
            self.set_next_vignette_active()
        elif key == Qt.Key_Escape:
            self.set_none_active()

    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            self.set_previous_vignette_active()
        elif event.angleDelta().y() < 0:
            self.set_next_vignette_active()

    def set_previous_vignette_active(self):
        if len(self._active_vignettes) == 1:
            try:
                index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) - 1
                if index < 0:
                    index = len(self._vignettes_ordered) - 1
                self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])
            except:
                pass

    def set_next_vignette_active(self):
        if len(self._active_vignettes) == 1:
            try:
                index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) + 1
                if index > len(self._vignettes_ordered) - 1:
                    index = 0
                self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])
            except:
                pass

    def set_none_active(self):
        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()
        self._active_vignettes = []
        self.selection_changed.emit(self._vignettes_ordered)

    def get_object_ids(self, vignettes):
        object_ids = []
        for object_id, vignette in self._vignettes.items():
            if vignette in vignettes:
                object_ids.append(object_id)
        return object_ids

    def set_active_vignettes(self, vignettes):
        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()
        for vignette in vignettes:
            self._vignettes[vignette.object_id()].set_active_styles()
        self._active_vignettes = vignettes
        self.selection_changed.emit(self.get_object_ids(vignettes))


class MyGLV(gl.GLViewWidget):
    _position_1 = None

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.RightButton:
            pos = event.position()
            dx = self._position_1.x() - pos.x()
            dy = self._position_1.y() - pos.y()
            pos_cam = self.cameraParams()['center']
            pos_cam.setZ(pos_cam.z() - dy)
            pos_cam.setY(pos_cam.y() + dx)
            kwds = {'center': pos_cam}
            self.setCameraParams(**kwds)
        super().mouseReleaseEvent(event)

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton:
            self._position_1 = event.position()
        super().mousePressEvent(event)


class _MultiImage3D(QWidget):
    update_signal = pyqtSignal()

    def __init__(self, parent, max_width=300, max_count=10, id_type=_MultiImageIdType.ImageIndex):

        super().__init__()

        self.parent = parent

        self._line_max = None
        self._line_max2 = None
        self._line_max3 = None

        self._selection_active = False
        self._gl_view: MyGLV = None
        self._gl_images = {}

        self._translation: int = 0

        self._np_images = []
        self._data = {}

        self._last_shown_ids = set([])

        self._max_width = max_width

        self._opacity = 100
        self._opacity_2 = 255
        self._opacity_threshold = 128

        self._max_count = max_count
        self._count = 0

        self.init_layout()

        self._position_center = None
        self._position_center_azi = 0
        self._distance = 1400

        self._gl_view = MyGLV()

        self.set_camera(_CameraPosition.Center)

        self.layout().addWidget(self._gl_view)

        self.layout().addWidget(self.create_settings_panel())

        self._translation = 800 / self._max_count
        self._id_type = id_type

        self._gl_images = {}
        self._gl_images_ordered = []

        self._lines_grid = []

    def create_settings_panel(self):

        panel = QWidget(self)

        layout = QGridLayout(panel)
        layout.setContentsMargins(4, 4, 4, 4)
        layout.setSpacing(4)

        layout.addWidget(QLabel("max:"), 0, 0)

        self._max_line = QCheckBox()
        layout.addWidget(self._max_line, 0, 1)
        self._max_line.stateChanged.connect(self.change_max_line)

        self.button_group = QButtonGroup()
        radiobuttons_opacity = ["10%", "40%", "70%", "100%", "signal", "normal"]

        for n, c in enumerate(radiobuttons_opacity):
            b = QRadioButton(c)
            if n == 5:
                b.setChecked(True)
            self.button_group.addButton(b, n)
            layout.addWidget(b, int(n / 9), 2 + n % 9)

        self.button_group.buttonClicked[int].connect(self.change_rb)

        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 0)
        layout.setColumnStretch(2, 1)
        layout.setColumnStretch(3, 1)
        layout.setColumnStretch(4, 1)
        layout.setColumnStretch(5, 1)
        layout.setColumnStretch(6, 1)
        layout.setColumnStretch(7, 1)

        panel.setMaximumHeight(30)

        return panel

    def change_max_line(self):

        if self._line_max is not None:
            self._gl_view.removeItem(self._line_max)
            self._gl_view.removeItem(self._line_max2)
            self._gl_view.removeItem(self._line_max3)
            self._line_max = None

        if self._max_line.isChecked():
            points_max_array = np.array(self.parent._max_points)
            self._line_max = gl.GLLinePlotItem(pos=points_max_array,
                                               width=1,
                                               antialias=True,
                                               color=(0., 1., 1., 0.9))
            self._gl_view.addItem(self._line_max)

            points_max_arrayx = np.array(self.parent._max_pointsx)
            self._line_max2 = gl.GLLinePlotItem(pos=points_max_arrayx,
                                                width=3,
                                                antialias=True,
                                                color=(1., 0., 0.95, 1.))
            self._gl_view.addItem(self._line_max2)

            points_max_arrayy = np.array(self.parent._max_pointsy)
            self._line_max3 = gl.GLLinePlotItem(pos=points_max_arrayy,
                                                width=3,
                                                antialias=True,
                                                color=(1., 0., 0.95, 1.))
            self._line_max3.translate(0, 400, 0)
            self._gl_view.addItem(self._line_max3)

    def change_rb(self, id):
        if id == 0:
            self._opacity = 40
            self._opacity_2 = 2
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)
        elif id == 1:
            self._opacity = 60
            self._opacity_2 = 10
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)
        elif id == 2:
            self._opacity = 80
            self._opacity_2 = 30
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)
        elif id == 3:
            self._opacity = 100
            self._opacity_2 = 70
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)
        elif id == 4:
            self._opacity = 100
            self._opacity_2 = 0
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)
        elif id == 5:
            self._opacity = 100
            self._opacity_2 = 255
            self.opacity(self._opacity, value2=self._opacity_2, threshold=self._opacity_threshold)

    def clear_data(self):
        self._gl_images = {}
        self._gl_images_ordered = []
        self._gl_images = {}
        self._translation = 0
        self._np_images = []
        self._data = {}

    def to_grayscale(self, rgb):
        r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
        gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
        return gray

    def width(self):
        return self._max_width

    def images(self):
        return self._gl_images

    def add(self, object_id, rgba_image, factor=1., w=1., h=1.):
        if not self._selection_active:
            self._last_shown_ids.add(object_id)

        if len(self._gl_images_ordered) == 0:
            nw = h * factor
            nh = w * factor
            points = [
                [(0, 0, 0), (720, 0, 0)],
                [(0, nw, 0), (720, nw, 0)],
                [(0, 0, -nh), (720, 0, -nh)],
                [(0, nw, -nh), (720, nw, -nh)]
            ]
            for pts in points:
                points_array = np.array(pts)
                drawing_variable = gl.GLLinePlotItem(pos=points_array, width=3, antialias=True,
                                                     color=(0.5, 0.5, 0.5, 0.2))
                self._lines_grid.append(drawing_variable)
                self._gl_view.addItem(drawing_variable)

        if self._line_max is not None:
            self._gl_view.removeItem(self._line_max)
            self._gl_view.removeItem(self._line_max2)
            self._gl_view.removeItem(self._line_max3)
            self._line_max = None

        # if self._max_line.isChecked():
        #
        #     points_max_array = np.array(self.parent._max_points)
        #     self._line_max = gl.GLLinePlotItem(pos=points_max_array, width=1, antialias=True, color=(0., 1., 1.,  0.9))
        #     self._gl_view.addItem(self._line_max)
        #
        #     points_max_arrayx = np.array(self.parent._max_pointsx)
        #     self._line_max2 = gl.GLLinePlotItem(pos=points_max_arrayx, width=3, antialias=True, color=(1., 0., 0.95, 1.))
        #     # self._line_max2.translate(0,0,0)
        #     self._gl_view.addItem(self._line_max2)
        #
        #     points_max_arrayy = np.array(self.parent._max_pointsy)
        #     self._line_max3 = gl.GLLinePlotItem(pos=points_max_arrayy, width=3, antialias=True, color=(1., 0., 0.95, 1.))
        #     self._line_max3.translate(0, 400, 0)
        #     self._gl_view.addItem(self._line_max3)

        gl_image = GLImageItem(rgba_image)
        gl_image.scale(factor, factor, 1)
        gl_image.rotate(90, 0, 1, 0)

        if self._count >= self._max_count:
            self._count = self._max_count - 1

        gl_image.translate(self._count * self._translation, 0, 0)

        self._count = self._count + 1

        data = gl_image.data
        data[:, :, 3] = 256 * (self._opacity / 100) - 1
        data[(data[:, :, 0] < self._opacity_threshold), 3] = int(self._opacity_2)
        gl_image.setData(data)

        self._gl_images[object_id] = gl_image
        self._gl_view.addItem(gl_image)

        self._gl_images_ordered.append(object_id)

        if self._max_line.isChecked():
            points_max_array = np.array(self.parent._max_points)
            self._line_max = gl.GLLinePlotItem(pos=points_max_array,
                                               width=1,
                                               antialias=True,
                                               color=(0., 1., 1., 0.9))
            self._gl_view.addItem(self._line_max)

            points_max_arrayx = np.array(self.parent._max_pointsx)
            self._line_max2 = gl.GLLinePlotItem(pos=points_max_arrayx,
                                                width=3,
                                                antialias=True,
                                                color=(1., 0., 0.95, 1.))
            self._gl_view.addItem(self._line_max2)

            points_max_arrayy = np.array(self.parent._max_pointsy)
            self._line_max3 = gl.GLLinePlotItem(pos=points_max_arrayy,
                                                width=3,
                                                antialias=True,
                                                color=(1., 0., 0.95, 1.))
            self._line_max3.translate(0, 400, 0)
            self._gl_view.addItem(self._line_max3)

    def opacity(self, value, value2=255, threshold=128):
        self._opacity = value
        self._opacity_2 = value2
        self._opacity_threshold = threshold
        for key, gl_image in self._gl_images.items():
            data = gl_image.data
            data[:, :, 3] = 256 * (self._opacity / 100) - 1
            data[(data[:, :, 0] < threshold), 3] = int(self._opacity_2)
            gl_image.setData(data)

    def set_gl_image(self, indices):
        to_remove = [i for i in self._last_shown_ids if i not in indices]
        to_add = [i for i in indices if i not in self._last_shown_ids]
        for obj_id in to_remove:
            if obj_id in self._gl_images:
                self._gl_view.removeItem(self._gl_images[obj_id])
        for obj_id in to_add:
            if obj_id in self._gl_images:
                self._gl_view.addItem(self._gl_images[obj_id])
        self._last_shown_ids = set(indices)

    def init_layout(self):
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

    def light_(self):
        self._gl_view.setBackgroundColor(Colors.COLOR_WHITE)
        qss = """
                        QCheckBox::indicator {
                            border:1px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;            
                        }       
                        QCheckBox::indicator:checked {
                            max-width:14;
                            max-height:14;
                            border:0px solid """ + Colors.STR_COLOR_LIGHT5GRAY + """;
                            background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
                        }             
                        QCheckBox {
                            max-width:14;
                            max-height:14;
                            color: """ + Colors.STR_COLOR_BLACK + """;
                            background-color: """ + Colors.STR_COLOR_WHITE + """;            
                        }
                        """
        self._max_line.setStyleSheet(qss)
        for line in self._lines_grid:
            line.color = (0.9, 0.9, 0.9, 0.5)
            line.update()

    def dark_(self):
        self._gl_view.setBackgroundColor(Colors.COLOR_LBLACK)
        self._max_line.setStyleSheet(
            "max-width:14;max-height:14;color: " + Colors.STR_COLOR_WHITE + ";border:1px solid " + Colors.STR_COLOR_L3BLACK + ";background-color: " + Colors.STR_COLOR_L2BLACK + ";")
        for line in self._lines_grid:
            line.color = (0.3, 0.3, 0.3, 0.5)
            line.update()

    def set_camera(self, position):
        if position == _CameraPosition.Center:
            if self._position_center is None:
                self._position_center = QVector3D(0, 200, -280)
                azimuth = 0
                elevation = 17
                self._gl_view.setCameraPosition(pos=self._position_center,
                                                elevation=elevation,
                                                azimuth=azimuth,
                                                distance=self._distance)


class MultiImageWidget3D(QWidget):
    update_signal = pyqtSignal(str, bool, tuple, dict)

    def multi_image(self) -> _MultiImage3D:
        return self._multi_image

    def __init__(self, parent, bar_position=Qt.BottomEdge, transpose=False, max_count=10):
        super(QWidget, self).__init__(parent)

        self._count = 0

        self._update_time = 0
        self._min_refresh_time = 0
        self._mutex = False
        self._transpose = False
        self._filter = None

        self._cmap = None
        self._lut = None

        self._rgba_images = {}
        self._image_items = {}
        self._image_nparrays = []
        self._np_images_keys = []
        self._np_images = {}

        self._max_points = []
        self._max_pointsx = []
        self._max_pointsy = []

        orientation = Qt.Horizontal if bar_position == Qt.TopEdge or bar_position == Qt.BottomEdge else Qt.Vertical
        self._transpose = transpose
        self._max_count = max_count - 1
        self._multi_image = _MultiImage3D(self, max_count=max_count)
        self._vignettes = _Vignettes(parent, orientation, max_width=100)

        self.init_layout()

        self._container.layout().addWidget(self._multi_image)

        self._container.layout().addWidget(self._imagewidget)
        self._imagewidget.hide()

        self.init_splitter(orientation)
        self._data = DataObject(history=True, max=max_count)

        self._cmap = pg.colormap.getFromMatplotlib('jet')
        self._lut = self._cmap.getLookupTable(alpha=True)

        if bar_position == Qt.TopEdge or bar_position == Qt.LeftEdge:
            scrollArea = QScrollArea()
            scrollArea.setWidgetResizable(True)
            scrollArea.setMinimumWidth(125)
            scrollArea.setWidget(self._vignettes)
            self._splitter.addWidget(scrollArea)
            self._splitter.addWidget(self._container)
            self._splitter.setStretchFactor(0, 0)
            self._splitter.setStretchFactor(1, 1)
        else:
            self._splitter.addWidget(self._container)
            scrollArea = QScrollArea()
            scrollArea.setWidgetResizable(True)
            scrollArea.setMinimumWidth(125)
            scrollArea.setWidget(self._vignettes)
            self._splitter.addWidget(scrollArea)
            self._splitter.setStretchFactor(0, 1)
            self._splitter.setStretchFactor(1, 0)

        self.update_signal.connect(self.update)

        self._vignettes.selection_changed.connect(self.handle_selection_changed)

        self.dark_()

    def get_size_array(arr):
        try:
            size = arr.shape[0] * arr.shape[1]
            return [size, arr.shape[0], arr.shape[1], 1]
        except:
            try:
                size = len(arr) * len(arr[0])
                return [size, len(arr[0]), len(arr), 2]
            except:
                return [-1, 0, 0]

    def set_data(self, data, update=True, name="default", *args, **kwargs):
        self._rgba_images = {}
        self._image_items = {}
        self._image_nparrays = []
        si = MultiImageWidget3D.get_size_array(data)
        # if si[0] > 100000:
        #     try:
        #         w = si[1]
        #         h = si[2]
        #         fact = w/h
        #         fw = 1
        #         fh = 1
        #         if w > 300:
        #             fw = int(w / 300)
        #         if h > 300:
        #             fh = int(h*fact / 300)
        #         self._data.add(data[::fw, ::fh])
        #     except:
        #         return
        # else:
        self._data.add(data)
        if update and self._mutex == False:
            self.update_signal.emit(name, update, args, kwargs)

    def np_to_rgba(self, np_image):
        levels = [np_image.min(), np_image.max()]
        lut = self._lut
        return pg.makeRGBA(np_image, levels=levels, lut=lut)[0]

    def init_layout(self):
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

        self._imagewidget = imagewidget.ImageWidget(self)
        self._imagewidget.add_plot()

        self._imagewidget.setContentsMargins(0, 0, 0, 0)
        self._container = QWidget()
        self._container.setContentsMargins(0, 0, 0, 0)
        self._container.setLayout(QVBoxLayout())
        self._container.layout().setContentsMargins(0, 0, 0, 0)
        self._container.layout().setSpacing(0)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def init_splitter(self, bar_position: Qt.Edge):
        self._splitter = QSplitter()
        self._splitter.setHandleWidth(5)
        self.layout().addWidget(self._splitter)
        if bar_position == Qt.BottomEdge or bar_position == Qt.TopEdge:
            self._splitter.setOrientation(Qt.Vertical)
        else:
            self._splitter.setOrientation(Qt.Horizontal)

    @pyqtSlot(str, bool, tuple, dict)
    def update(self, name='default', update=True, args=None, kwargs=None):
        """ Updates the component, with the current contents of the data object.
            The data object contains all the images to be rendered.

            We don't know if we need the last 3, the last one, or if all the images are already drawn.
        """
        self._mutex = True

        try:
            key_image = None

            raw_image = self._data.get_last(name)
            raw_image_median = ndi.median_filter(raw_image, 1)
            max_index = np.argmax(raw_image_median)
            max_position = np.unravel_index(max_index, raw_image.shape)

            try:
                object_id = self._count
                self._count = self._count + 1

                if len(self._np_images) > self._max_count:

                    key_image = self._np_images_keys[0]
                    try:

                        self._vignettes._vignettes_ordered.pop(0)

                        obj = self._multi_image._gl_images[key_image]
                        self._multi_image._gl_view.removeItem(obj)
                        self._multi_image._gl_images.pop(key_image)
                        self._multi_image._gl_images_ordered.pop(0)

                        self._max_points.pop(0)
                        self._max_pointsx.pop(0)
                        self._max_pointsy.pop(0)

                        self._np_images_keys.pop(0)
                        self._np_images.pop(key_image)

                        ind = 0
                        for key in self._np_images_keys:
                            obj = self._multi_image._gl_images[key]
                            obj.translate(-self._multi_image._translation, 0, 0)
                            self._max_points[ind][0] = self._max_points[ind][0] - self._multi_image._translation
                            self._max_pointsx[ind][0] = self._max_pointsx[ind][0] - self._multi_image._translation
                            self._max_pointsy[ind][0] = self._max_pointsy[ind][0] - self._multi_image._translation
                            ind = ind + 1

                    except:
                        pass
                if object_id not in self._np_images:
                    np_image = np.array(raw_image)
                    total_size = np_image.shape[0] * np_image.shape[1]
                    w = np_image.shape[0]
                    h = np_image.shape[1]
                    if w > h:
                        factor = 400. / w
                    else:
                        factor = 400. / h
                    if total_size > 100000:
                        mod = int(total_size / 50000)
                        np_image_vignette = np_image[::mod, ::mod]
                    else:
                        np_image_vignette = np.array(raw_image)
                    if key_image is not None:
                        try:
                            vignette = self._vignettes._vignettes.pop(key_image)
                            self._vignettes.layout().removeWidget(vignette)
                        except:
                            pass
                    self._np_images_keys.append(object_id)

                    z = (self.multi_image()._count) * self.multi_image()._translation

                    pt = [z, max_position[1] * factor, -max_position[0] * factor]
                    self._max_points.append(pt)
                    ptx = [z, max_position[1] * factor, 0]
                    self._max_pointsx.append(ptx)

                    pty = [z, 0, -max_position[0] * factor]
                    self._max_pointsy.append(pty)

                    self._np_images[object_id] = np_image

                    self._vignettes.add(object_id, self.np_to_rgba(np_image_vignette))

                    self._multi_image.add(object_id, self.np_to_rgba(np_image), factor=factor, w=w, h=h)

                if self._count > 10000000000:
                    self._count = 0
            except:
                pass

        except Exception as ex:
            print_exception(type(ex), ex, ex.__traceback__)
            self._mutex = False

        self._mutex = False

    def handle_selection_changed(self, object_ids):
        if len(object_ids) == 1:
            return self.show_imagewidget(self._np_images[object_ids[0]])
        self._multi_image.set_gl_image(object_ids)
        self.show_multi_images()

    def show_imagewidget(self, np_image):
        np_image = np.rot90(np_image, -1)
        self._multi_image.hide()
        self._imagewidget.show()
        self._imagewidget.set_data(np_image)

    def set_opacity(self, value, value2=255, threshold=128):
        """
        Image operation. As the internal data must be modified, the image must be redrawn.
        :param value:
        :return:
        """
        self._multi_image.opacity(self._opacity, value2=value2, threshold=threshold)

    def change_rb(self, index):
        """
        Select radio button
        :param index: rb number 0-5 ["10%", "40%", "70%", "100%", "signal", "normal"]
        :return:
        """
        self.multi_image().change_rb(index)

    def center_camera(self, distance=None):
        self._multi_image.set_camera(_CameraPosition.Center)

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + "; }"
                                                                                                           "QSplitter::handle { background-color: " + Colors.STR_COLOR_DWHITE + "; }"
        )
        self._multi_image.light_()
        self._vignettes.light_()
        self._imagewidget.light_()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + "; }"
                                                                                                            "QSplitter::handle { background-color: " + Colors.STR_COLOR_L2BLACK + "; }")
        self._multi_image.dark_()
        self._vignettes.dark_()
        self._imagewidget.dark_()

    def show_multi_images(self):
        self._multi_image.show()
        self._imagewidget.hide()

    def file_to_image_array(self, filename):
        image = Image.open(filename)
        if image.mode == "RGB":
            image.putalpha(Image.new('L', image.size, 255))
        return np.asarray(image)

    def data_object(self):
        return self._data

    def images(self, name="default"):
        return self._data.get(name)

    def get_image_arrays(self, images):
        np_arrays = []
        for image in images:
            if isinstance(image, str) and os.path.isfile(image):
                np_arrays.append(self.file_to_image_array(image))
            elif isinstance(image, np.ndarray):
                if image.shape[2] == 3:
                    image = np.insert(image, 3, 255, axis=2)
                np_arrays.append(image)
            else:
                raise Exception("Invalid image format supplied to MultiImage3dsWidget.")
        return np_arrays


class _UpdateThread(QThread):
    new_image = pyqtSignal(np.ndarray)

    @staticmethod
    def gaussian(sigma=1., xoffset=0):
        x_grid = 900
        y_grid = 500
        muu = 0
        x, y = np.meshgrid(np.linspace(-5, 5, x_grid), np.linspace(-5, 5, y_grid))
        dst = np.sqrt((x - xoffset) ** 2 + y ** 2)
        return np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))

    def run(self):
        count = 0
        while count < 200:
            time.sleep(0.1)
            count += 1
            self.new_image.emit(_UpdateThread.gaussian(xoffset=(-5 + 0.01 * count)))


class _Example(QMainWindow):

    def __init__(self):
        super().__init__()

        w = QWidget()
        w.setLayout(QVBoxLayout())

        self._multi_image_widget = MultiImageWidget3D(None, bar_position=Qt.LeftEdge, max_count=10)

        updater = _UpdateThread()
        updater.new_image.connect(self.add_image)
        updater.start()

        w.layout().addWidget(self._multi_image_widget)

        controls = QWidget()
        controls.setLayout(QHBoxLayout())

        col2 = QWidget()
        col2.setLayout(QVBoxLayout())
        dark = QPushButton("Dark")
        dark.clicked.connect(self._multi_image_widget.dark_)
        col2.layout().addWidget(dark)

        light = QPushButton("Light")
        col2.layout().addWidget(light)
        light.clicked.connect(self._multi_image_widget.light_)
        controls.layout().addWidget(col2)

        w.layout().addWidget(controls)

        self.setCentralWidget(w)
        self.show()
        self.resize(1100, 600)
        self.move(100, 300)

    @pyqtSlot(np.ndarray)
    def add_image(self, raw_image):
        self._multi_image_widget.set_data(raw_image)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.resize(600, 600)
    ex.show()
    sys.exit(app.exec_())
