import sys
from time import sleep
import numpy as np
import pyqtgraph as pg
import pandas as pd
from PIL import Image
import pyqtgraph.functions as fn
import scipy.ndimage as ndi
from scipy.optimize import curve_fit
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, QRect, pyqtSlot, QPoint, QPointF, QRectF

from PyQt5.QtGui import QPainter, QPalette, QPen, QBrush, QColor

from PyQt5.QtWidgets import QApplication, QSlider, QStyle, QStyleOptionSlider, QSizePolicy, QWidget, QMenu, QGridLayout, \
    QHBoxLayout, QRadioButton, QCheckBox, QHeaderView, QMainWindow, QVBoxLayout, QTabWidget, QLabel, QPushButton, QTableWidget

from expert_gui_core.tools import formatting
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import fesacommDA
from expert_gui_core.comm import fesacomm
from expert_gui_core.comm import simucomm
from expert_gui_core.gui.widgets.common import headerwidget, togglepanelwidget, titledborderwidget
from expert_gui_core.gui.widgets.pyqt.basicimageitem import BasicImageItem
from expert_gui_core.gui.widgets.pyqt.basicplotitem import BasicPlotItem
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.pyqt import multiimagewidget3D
from expert_gui_core.gui.widgets.pyqt import mosaicwidget
from expert_gui_core.gui.widgets.pyqt.tablewidget import TableWidget



def calculate_distance(pos1, pos2):
    """
    Calculation of the distance (cartesian)
    :param pos1:
    :param pos2:
    :return:
    """
    return np.sqrt((pos2[0] - pos1[0]) ** 2 + (pos2[1] - pos1[1]) ** 2)


def fo_gauss(x, a, mu, sig, c):
    """
    Gauss formula.
    """
    gauss = a * np.exp(-(x - mu) ** 2 / (2 * sig ** 2)) + c
    return gauss


class MyRangeSlider(QSlider):
    sliderMoved = pyqtSignal(int, int)

    def __init__(self, *args):
        super(MyRangeSlider, self).__init__(*args)

        self._low = self.minimum()
        self._high = self.maximum()

        self.setStyleSheet("""
            MyRangeSlider {
                width:25px;
            }
        """)

        self.pressed_control = QStyle.SC_None
        self.tick_interval = 0
        self.tick_position = QSlider.NoTicks
        self.hover_control = QStyle.SC_None
        self.click_offset = 0

        self.active_slider = 0

    def low(self):
        return self._low

    def setLow(self, low: int):
        self._low = low
        self.update()

    def high(self):
        return self._high

    def setHigh(self, high):
        self._high = high
        self.update()

    def paintEvent(self, event):

        painter = QPainter(self)
        style = QApplication.style()

        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        opt.siderValue = 0
        opt.sliderPosition = 0
        opt.subControls = QStyle.SC_SliderGroove
        if self.tickPosition() != self.NoTicks:
            opt.subControls |= QStyle.SC_SliderTickmarks
        style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)
        groove = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderGroove, self)

        self.initStyleOption(opt)
        opt.subControls = QStyle.SC_SliderGroove

        opt.siderValue = 0

        opt.sliderPosition = self._low
        low_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        opt.sliderPosition = self._high

        high_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)
        low_pos = self.__pick(low_rect.center())
        high_pos = self.__pick(high_rect.center())

        min_pos = min(low_pos, high_pos)
        max_pos = max(low_pos, high_pos)

        c = QRect(low_rect.center(), high_rect.center()).center()

        if opt.orientation == Qt.Horizontal:
            span_rect = QRect(QPoint(min_pos, c.y() - 2), QPoint(max_pos, c.y() + 1))
        else:
            span_rect = QRect(QPoint(c.x() - 2, min_pos), QPoint(c.x() + 1, max_pos))

        if opt.orientation == Qt.Horizontal:
            groove.adjust(0, 0, -1, 0)
        else:
            groove.adjust(0, 0, 0, -1)

        if True:
            highlight = self.palette().color(QPalette.Highlight)
            painter.setBrush(QBrush(highlight))
            painter.setPen(QPen(highlight, 0))
            '''
            if opt.orientation == Qt.Horizontal:
                self.setupPainter(painter, opt.orientation, groove.center().x(), groove.top(), groove.center().x(), groove.bottom())
            else:
                self.setupPainter(painter, opt.orientation, groove.left(), groove.center().y(), groove.right(), groove.center().y())
            '''
            painter.drawRect(span_rect.intersected(groove))

        for i, value in enumerate([self._low, self._high]):

            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            if i == 0:
                opt.subControls = QStyle.SC_SliderHandle
            else:
                opt.subControls = QStyle.SC_SliderHandle

            if self.tickPosition() != self.NoTicks:
                opt.subControls |= QStyle.SC_SliderTickmarks

            if self.pressed_control:
                opt.activeSubControls = self.pressed_control
            else:
                opt.activeSubControls = self.hover_control

            opt.sliderPosition = value
            opt.sliderValue = value

            style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)

    def mousePressEvent(self, event):
        event.accept()

        style = QApplication.style()
        button = event.button()

        if button:
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            self.active_slider = -1

            if self._high - self._low <= 2:
                self._high = self._high + 2
            if self._high > self.maximum():
                self._high = self._high - 2
                self._low = self._low - 2

            for i, value in enumerate([self._low, self._high]):
                opt.sliderPosition = value

                hit = style.hitTestComplexControl(style.CC_Slider, opt, event.pos(), self)
                if hit == style.SC_SliderHandle:
                    self.active_slider = i
                    self.pressed_control = hit

                    self.triggerAction(self.SliderMove)
                    self.setRepeatAction(self.SliderNoAction)
                    self.setSliderDown(True)
                    break

            if self.active_slider < 0:
                self.pressed_control = QStyle.SC_SliderHandle
                self.click_offset = self.__pixelPosToRangeValue(self.__pick(event.pos()))
                self.triggerAction(self.SliderMove)
                self.setRepeatAction(self.SliderNoAction)
        else:
            event.ignore()

    def mouseMoveEvent(self, event):
        if self.pressed_control != QStyle.SC_SliderHandle:
            event.ignore()
            return

        event.accept()
        new_pos = self.__pixelPosToRangeValue(self.__pick(event.pos()))
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)

        if self.active_slider < 0:
            offset = new_pos - self.click_offset
            self._high += offset
            self._low += offset
            if self._low < self.minimum():
                diff = self.minimum() - self._low
                self._low += diff
                self._high += diff
            if self._high > self.maximum():
                diff = self.maximum() - self._high
                self._low += diff
                self._high += diff
            if new_pos >= self._high:
                new_pos = self._high - 1
            if new_pos <= self._low:
                new_pos = self._low + 1
        elif self.active_slider == 0:
            if new_pos >= self._high:
                new_pos = self._high - 1
            self._low = new_pos
        else:
            if new_pos <= self._low:
                new_pos = self._low + 1
            self._high = new_pos

        self.click_offset = new_pos

        self.update()

        self.sliderMoved.emit(self._low, self._high)

    def __pick(self, pt):
        if self.orientation() == Qt.Horizontal:
            return pt.x()
        else:
            return pt.y()

    def __pixelPosToRangeValue(self, pos):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        style = QApplication.style()

        gr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderGroove, self)
        sr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderHandle, self)

        if self.orientation() == Qt.Horizontal:
            slider_length = sr.width()
            slider_min = gr.x()
            slider_max = gr.right() - slider_length + 1
        else:
            slider_length = sr.height()
            slider_min = gr.y()
            slider_max = gr.bottom() - slider_length + 1

        return style.sliderValueFromPosition(self.minimum(), self.maximum(),
                                             pos - slider_min, slider_max - slider_min,
                                             opt.upsideDown)

    def light_(self):
        pass

    def dark_(self):
        pass


class MyRawImageWidget(QWidget):

    def __init__(self, parent=None, scaled=False):
        QWidget.__init__(self, parent=None)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self._scaled = scaled
        self.opts = None
        self.image = None
        self._transpose = False
        self._flipH = False
        self._flipV = False
        self._max = 1
        self._min = 0
        self._pmin = 0.
        self._pmax = 1.
        self._blur = 0
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.right_menu)

    def right_menu(self, pos):

        menu = QMenu()

        transpose_option = menu.addAction('Transpose')
        fliph_option = menu.addAction('FlipH')
        flipv_option = menu.addAction('FlipV')
        scaled_option = menu.addAction('Scaled')

        raw_option = menu.addAction('Raw')

        blur_option = menu.addAction('Blur')
        colorscale_option = menu.addAction('Fit colors')
        blur2_option = menu.addAction('Blur dim/2')
        blur4_option = menu.addAction('Blur dim/4')

        fliph_option.triggered.connect(self.flipH)
        flipv_option.triggered.connect(self.flipV)
        transpose_option.triggered.connect(self.transpose)
        scaled_option.triggered.connect(self.scaled)
        raw_option.triggered.connect(self.raw)
        blur_option.triggered.connect(self.blur)
        colorscale_option.triggered.connect(self.colorscale)
        blur2_option.triggered.connect(self.blur2)
        blur4_option.triggered.connect(self.blur4)

        menu.exec_(self.mapToGlobal(pos))

    def transpose(self):
        self._transpose = not self._transpose
        self.image = None
        self.update()

    def flipH(self):
        self._flipH = not self._flipH
        self.image = None
        self.update()

    def flipV(self):
        self._flipV = not self._flipV
        self.image = None
        self.update()

    def scaled(self):
        self._scaled = not self._scaled
        self.image = None
        self.update()

    def setPercent(self, min, max):
        if min >= max:
            self._pmin = 0.
            self._pmax = 1.
        else:
            self._pmax = max / 100.
            self._pmin = min / 100.
        self.image = None
        self.update()

    def colorscale(self):
        self._blur = 0.5
        self.image = None
        self.update()

    def blur(self):
        self._blur = 1
        self.image = None
        self.update()

    def blur2(self):
        self._blur = 2
        self.image = None
        self.update()

    def blur4(self):
        self._blur = 4
        self.image = None
        self.update()

    def raw(self):
        self._blur = 0
        self.image = None
        self.update()

    def setImage(self, img, *args, **kargs):
        if self._blur == 1:
            pass
        elif self._blur == 2:
            im = Image.fromarray(img)
            im = im.resize((im.width // 2, im.height // 2), resample=Image.NEAREST)
            img = np.asarray(im).copy()
        elif self._blur == 4:
            im = Image.fromarray(img)
            im = im.resize((im.width // 4, im.height // 4), resample=Image.NEAREST)
            img = np.asarray(im).copy()

        self._max = img.max()
        self._min = img.min()

        self.opts = (img, args, kargs)

        self.image = None

        self.update()

    def paintEvent(self, ev):

        if self.opts is None:
            return

        if self.image is None:

            try:
                if self._blur > 0:
                    if self._blur == 1:
                        data = ndi.uniform_filter(self.opts[0], size=11)
                    elif self._blur == 2:
                        data = ndi.uniform_filter(self.opts[0], size=7)
                    elif self._blur == 4:
                        data = ndi.uniform_filter(self.opts[0], size=5)

                    self._max = data.max()
                    hmed = 5. * np.median(data)
                    lmed = np.median(data) / 10.
                    if hmed < self._max:
                        self._max = hmed
                    self._min = data.min()
                    if lmed > self._min:
                        self._min = lmed / 10.
                else:
                    data = self.opts[0]

                if self._flipH:
                    data = np.flip(data, axis=1)

                if self._flipV:
                    data = np.flip(data, axis=0)

                self._level_min = int(self._min + (self._max - self._min) * self._pmin)
                self._level_max = int(self._min + (self._max - self._min) * self._pmax)

                argb, alpha = fn.makeARGB(data, levels=[self._level_min, self._level_max])

            except:
                return

            self.image = fn.makeQImage(argb,
                                       alpha=True,
                                       transpose=self._transpose)

        p = QPainter(self)

        if self._scaled:

            rect = self.rect()
            ar = rect.width() / float(rect.height())
            imar = self.image.width() / float(self.image.height())
            if ar > imar:
                rect.setWidth(int(rect.width() * imar / ar))
            else:
                rect.setHeight(int(rect.height() * ar / imar))

            p.drawImage(rect, self.image)
        else:
            rect = self.rect()
            p.drawImage(rect, self.image)

        p.end()

    def light_(self):
        pass

    def dark_(self):
        pass


class ChartImage(QWidget):
    def __init__(self, parent=None, title="", color=Colors.COLOR_LBLUE):
        super(QWidget, self).__init__(parent)

        self.setWindowTitle(title)

        data_widget_opts = {
            "auto": True,
            "show_image": True,
            "show_singlechart": True,
            "history": True,
            "flat": True,
            "show_image_value_axis": True,
            "show_singlechart_value_axis": True,
            "color_single_chart": color
        }

        self.data_widget = datawidget.DataWidget(self,
                                                 title=[""],
                                                 header=False,
                                                 max=100,
                                                 **data_widget_opts)

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(2)

        self.layout.addWidget(self.data_widget, 0, 0)

        self.resize(800, 600)

    def set_data(self, data, datax=None):
        self.data_widget.set_data(data)

    def closeEvent(self, event):
        self.hide()

    def dark_(self):
        self.data_widget.dark_()

    def light_(self):
        self.data_widget.light_()


class ProfileWidget(QWidget):
    supdate_signal = pyqtSignal()
    supdate_H = pyqtSignal()
    supdate_V = pyqtSignal()
    supdate_fit_H = pyqtSignal()
    supdate_fit_V = pyqtSignal()
    srefresh_plots = pyqtSignal()
    supdate_table = pyqtSignal()
    supdate_scale = pyqtSignal()

    def __init__(self,
                 parent,
                 name="default",
                 positionH=Qt.AlignBottom,
                 positionV=Qt.AlignRight,
                 settings=True,
                 show_info=False,
                 show_infotable=True,
                 show_infogauss=True,
                 ratio=False,
                 limit_time=20,
                 profile=True,
                 tv=False,
                 multi=False,
                 mosaic=False,
                 header=True,
                 fit=False,
                 led=False,
                 unitx=None,
                 unity=None,
                 unityh=None,
                 unityv=None,
                 maxcount_mosaic=20,
                 nwidth_mosaic=5,
                 bascule_tab=True,
                 selection=True,
                 baseline=False,
                 usertime=None,
                 avg_sum=False,
                 extfit=False,
                 values_row=None):

        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._parent = parent
        self._name = name

        self._once = 0

        self._usertime = usertime

        self._bascule_tab = bascule_tab

        self._header = header

        self._positionH = positionH
        self._positionV = positionV

        self.unitX = unitx
        self.unitY = unity
        self.unitYh = unityh
        self.unitYv = unityv

        self._autoLevels = True

        self._xscale = 1.
        self._yscale = 1.
        self._xoffset = 0.
        self._yoffset = 0.
        self._xscale_prev = self._xscale
        self._yscale_prev = self._yscale
        self._xoffset_prev = self._xoffset
        self._yoffset_prev = self._yoffset
        self._xoffset_roi = 0
        self._yoffset_roi = 0

        self.width_image = -1
        self.height_image = -1

        self._minsizeV = 285
        self._minsizeH = 200

        self.show_info = show_info
        self.show_infogauss = show_infogauss
        self.show_infotable = show_infotable

        self._mutex = False
        self._time_image = 0

        self._tv = tv
        self._profile = profile
        self._multi = multi
        self._mosaic = mosaic

        self.settings = settings

        self._led_flag = False
        self.led = led

        self._fit = fit
        self.extfit = extfit

        self._data = {}
        self._data_baseline = None
        self._image_ref = None

        self._ymax = 0
        self._absolute_scale = False
        self._maxmax = -999999999
        self._maxmaxavg = -999999999

        self._avg_sum = avg_sum

        self._update_time = 0
        self.layouts = {}

        self._limit_time = limit_time

        self._old_reduction = 1

        self._active = True

        # layout

        self.setAttribute(Qt.WA_StyledBackground, True)

        self.image_widget = QWidget()

        self.layout = QGridLayout(self.image_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        self.layoutmain = QGridLayout(self)
        self.layoutmain.setContentsMargins(0, 0, 0, 0)
        self.layoutmain.setSpacing(0)

        self._tabs = QTabWidget()

        show_tab = False

        if self._tv or self._multi or self._mosaic:
            self.layoutmain.addWidget(self._tabs, 0, 0)
            if self._profile:
                self._tabs.addTab(self.image_widget, "Image")
            show_tab = True

        if not show_tab and self._profile:
            self.layoutmain.addWidget(self.image_widget, 0, 0)

        # TV

        if self._tv:
            self.tv_widget = QWidget()

            self.layouttv = QGridLayout(self.tv_widget)
            self.layouttv.setContentsMargins(0, 0, 0, 0)
            self.layouttv.setSpacing(2)

            self._tv = MyRawImageWidget(self.tv_widget, scaled=True)

            self._headerWidgettv = headerwidget.HeaderWidget(self,
                                                             layout=self.layouttv,
                                                             datawidget=self._tv)

            self.layouttv.addWidget(self._headerWidgettv, 0, 0)
            self.layouttv.setRowStretch(0, 0)
            self.layouttv.setRowStretch(1, 1)
            self.layouttv.addWidget(self._tv, 1, 0)

            slider = MyRangeSlider(Qt.Vertical)
            slider.setMinimumHeight(30)
            slider.setMinimum(0)
            slider.setMaximum(100)
            slider.setLow(0)
            slider.setHigh(100)
            slider.setTickPosition(QSlider.TicksBelow)
            slider.sliderMoved.connect(self.change_slider)

            self.layouttv.addWidget(slider, 1, 1)

            self._tabs.addTab(self.tv_widget, "TV")

        if self._mosaic:
            self.mosaic_widget = QWidget()

            self.layoutmosaic = QGridLayout(self.mosaic_widget)
            self.layoutmosaic.setContentsMargins(0, 0, 0, 0)
            self.layoutmosaic.setSpacing(2)

            self._mosaic_widget = mosaicwidget.MosaicWidget(None, max_count=maxcount_mosaic, nwidth=nwidth_mosaic)

            self._headerWidgetmosaic = headerwidget.HeaderWidget(self,
                                                                 layout=self.layoutmosaic,
                                                                 datawidget=self._mosaic_widget)

            self.layoutmosaic.addWidget(self._headerWidgetmosaic, 0, 0)
            self.layoutmosaic.setRowStretch(0, 0)
            self.layoutmosaic.setRowStretch(1, 1)
            self.layoutmosaic.addWidget(self._mosaic_widget, 1, 0)

            self._tabs.addTab(self.mosaic_widget, "Mosaic")

        if self._multi:
            self.multi_widget = QWidget()

            self.layoutmulti = QGridLayout(self.multi_widget)
            self.layoutmulti.setContentsMargins(0, 0, 0, 0)
            self.layoutmulti.setSpacing(2)

            self._multi_widget = multiimagewidget3D.MultiImageWidget3D(None, bar_position=Qt.LeftEdge,
                                                                       max_count=maxcount_mosaic)

            self._headerWidgetmulti = headerwidget.HeaderWidget(self,
                                                                layout=self.layoutmulti,
                                                                datawidget=self._multi_widget,
                                                                showtime=False)

            self.layoutmulti.addWidget(self._headerWidgetmulti, 0, 0)
            self.layoutmulti.setRowStretch(0, 0)
            self.layoutmulti.setRowStretch(1, 1)
            self.layoutmulti.addWidget(self._multi_widget, 1, 0)

            self._tabs.addTab(self.multi_widget, "Multi")

        # Profile

        if self._profile:

            self._roi2 = None

            from expert_gui_core.gui.widgets.datapanels.boolpanelwidget import BoolPanelWidget
            from expert_gui_core.gui.widgets.datapanels.enumpanelwidget import EnumPanelWidget

            # options

            self.w_options = QWidget()
            self.w_options_layout = QHBoxLayout(self.w_options)
            self.layouts["options"] = self.w_options_layout
            self.layouts["options"] = self.w_options_layout
            self.toggle_panel_options = togglepanelwidget.TogglePanelWidget(self.w_options,
                                                                            align="topleft",
                                                                            iconarrow="caret-up",
                                                                            iconshow="caret-down")

            # baseline

            self._baseline_panel = titledborderwidget.TitledBorderWidget("Baseline")
            self._baseline_panel.setMinimumHeight(70)

            self.baseline_layout = QGridLayout(self._baseline_panel)
            self.layouts["baseline"] = self.baseline_layout
            self.baseline_layout.setContentsMargins(20, 20, 20, 10)

            self._push_baseline = QPushButton("+")
            self.baseline_layout.addWidget(self._push_baseline, 0, 0)
            self._push_baseline.setMaximumWidth(20)
            self._push_baseline.setMaximumHeight(20)
            self._push_baseline.mousePressEvent = self.push_baseline

            self._apply_baseline = BoolPanelWidget(self,
                                                   title=None,
                                                   label_name="",
                                                   type_boolean="toggle",
                                                   edit=True,
                                                   history=False)

            width = 15
            radius = width / 1.6
            self._apply_baseline.get_main_component()[0]._radius = radius
            self._apply_baseline.get_main_component()[0]._width = width
            self._apply_baseline.setMinimumWidth(55)
            self._apply_baseline.get_signal().connect(self.apply_baseline_changed)
            self.baseline_layout.addWidget(self._apply_baseline, 0, 1)

            self._label_baseline = QLabel("none")
            flabel = self._label_baseline.font()
            flabel.setPointSize(6)
            self._label_baseline.setWordWrap(True)
            self._label_baseline.setMaximumWidth(200)
            self._label_baseline.setFont(flabel)
            self.baseline_layout.addWidget(self._label_baseline, 0, 2)

            self.baseline_layout.setColumnStretch(0, 0)
            self.baseline_layout.setColumnStretch(1, 1)
            self.baseline_layout.setColumnStretch(2, 1)

            if baseline:
                self.w_options_layout.addWidget(self._baseline_panel)

            # image (selection)

            self._selection_panel = titledborderwidget.TitledBorderWidget("Image")
            self._selection_panel.setMinimumHeight(70)

            self.selection_layout = QHBoxLayout(self._selection_panel)
            self.layouts["selection"] = self.selection_layout
            self.selection_layout.setContentsMargins(30, 20, 20, 10)

            if selection:
                self.w_options_layout.addWidget(self._selection_panel)

            self.b1 = QRadioButton("ROI")
            self.b1.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b1)

            self._checkboxroi_value = QCheckBox()
            self._checkboxroi_value.stateChanged.connect(self.push_roi)
            self._checkboxroi_value.setChecked(False)
            self.selection_layout.addWidget(self._checkboxroi_value)

            self.b2 = QRadioButton("Distance")
            self.b2.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b2)

            self.b3 = QRadioButton("None")
            self.b3.setChecked(True)
            self.b3.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b3)

            labels_reduction = {
                "1": "Normal",
                "2": "Fast",
                "3": "Very Fast",
            }
            self.enum_reduction_widget = EnumPanelWidget(None,
                                                         title=None,
                                                         label_name="",
                                                         labels=labels_reduction,
                                                         history=False)

            # self.selection_layout.addWidget(self.enum_reduction_widget)

            self.enum_reduction_widget.set_data(1)
            self.enum_reduction_widget.get_signal().connect(self.reduction_changed)

            # fit or not

            labels_fit = {
                "0": "No Fit",
                "1": "Fit"
            }

            self.enum_fit_widget = EnumPanelWidget(None,
                                                   title=None,
                                                   label_name="",
                                                   labels=labels_fit,
                                                   history=False)

            if self._fit:
                self.selection_layout.addWidget(self.enum_fit_widget)

            self.enum_fit_widget.setMinimumWidth(100)
            self.enum_fit_widget.set_data(0)
            self.enum_fit_widget.get_signal().connect(self.fit_changed)

            # ratio

            self._ratio_panel = titledborderwidget.TitledBorderWidget("Ratio")
            self._ratio_panel.setMinimumHeight(70)

            self.ratio_layout = QHBoxLayout(self._ratio_panel)
            self.layouts["ratio"] = self.ratio_layout
            self.ratio_layout.setContentsMargins(25, 20, 10, 10)

            if ratio:
                self.w_options_layout.addWidget(self._ratio_panel)

            self._ratio = BoolPanelWidget(self,
                                          title=None,
                                          label_name="",
                                          type_boolean="toggle",
                                          edit=True,
                                          history=False)

            width = 15
            radius = width / 1.6
            self._ratio.get_main_component()[0]._radius = radius
            self._ratio.get_main_component()[0]._width = width
            self._ratio.setMinimumWidth(55)
            self._ratio.get_signal().connect(self.ratio_changed)
            self.ratio_layout.addWidget(self._ratio)

            # cursor

            self._cursor_panel = titledborderwidget.TitledBorderWidget("Cursor")
            self._cursor_panel.setMinimumHeight(70)

            self.cursor_layout = QHBoxLayout(self._cursor_panel)
            self.layouts["cursor"] = self.cursor_layout
            self.cursor_layout.setContentsMargins(20, 20, 10, 10)

            self.w_options_layout.addWidget(self._cursor_panel)

            labels_cursor = {
                "1": "Mouse Click",
                "2": "Avg"
            }
            self.enum_cursor_widget = EnumPanelWidget(self,
                                                      title=None,
                                                      label_name="",
                                                      labels=labels_cursor,
                                                      history=False)
            self.enum_cursor_widget.setMinimumWidth(150)
            self.cursor_layout.addWidget(self.enum_cursor_widget)
            self.enum_cursor_widget.set_data(1)

            # filter

            self._filter_panel = titledborderwidget.TitledBorderWidget("Filter")
            self._filter_panel.setMinimumHeight(70)

            self.filter_layout = QHBoxLayout(self._filter_panel)
            self.layouts["filter"] = self.filter_layout
            self.filter_layout.setContentsMargins(20, 20, 10, 10)

            # self.w_options_layout.addWidget(self._filter_panel)

            labels_filter = {
                "1": "None",
                "2": "Any Signals",
                "3": "Good Signals"
            }

            self.enum_filter_widget = EnumPanelWidget(self,
                                                      title=None,
                                                      label_name="",
                                                      labels=labels_filter,
                                                      history=False)
            self.filter_layout.addWidget(self.enum_filter_widget)
            self.enum_filter_widget.set_data(1)

            # live

            self._live_panel = titledborderwidget.TitledBorderWidget("Live!")
            self._live_panel.setMinimumHeight(70)

            self.live_layout = QHBoxLayout(self._live_panel)
            self.layouts["live"] = self.live_layout
            self.live_layout.setContentsMargins(20, 20, 10, 10)

            if led:
                self.w_options_layout.addWidget(self._live_panel)

            from expert_gui_core.gui.widgets.common import ledwidget

            self._led_live_widget = ledwidget.LedWidget()
            self._led_live_widget.set_color_inside(QColor("#666"))

            self.live_layout.addWidget(self._led_live_widget)

            # image

            from expert_gui_core.gui.widgets.pyqt.imagewidget import ImageWidget

            self.w_image = QWidget()
            # self.w_image.setStyleSheet("background-color:green;")

            if self._positionV == Qt.AlignTop and self._positionH == Qt.AlignBottom:
                self.w_image.setMinimumWidth(self._minsizeV + 200)
                self.w_image.setMinimumHeight(self._minsizeV + 100)

            self.im_widget = ImageWidget(self, roi=False)
            self.im_widget.add_plot(title="Image", lut=True, ratio=ratio, unitx=unitx, unity=unity)

            self.w_image_layout = QGridLayout(self.w_image)
            self.w_image_layout.setContentsMargins(0, 0, 0, 0)
            self.w_image_layout.setSpacing(0)
            self.w_image_layout.addWidget(self.im_widget, 1, 0)

            self.layouts["image"] = self.w_image_layout

            # header
            self._headerWidget = headerwidget.HeaderWidget(self.w_image,
                                                           layout=self.w_image_layout,
                                                           datawidget=self.im_widget,
                                                           showtime=False)

            if self._header:
                self.w_image_layout.addWidget(self._headerWidget, 0, 0)

            self.w_image_layout.setRowStretch(0, 0)
            self.w_image_layout.setRowStretch(1, 1)

            self.line = pg.LineSegmentROI([[0, 0],
                                           [0, 0]],
                                          pen=pg.mkPen({'color': Colors.COLOR_LIGHTRED, 'width': 2.}))

            self.im_widget.get_plot_item().addItem(self.line)

            # chart H

            from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget

            self.w_chart_H = QWidget()

            self.w_chart_H_layout = QGridLayout(self.w_chart_H)
            self.w_chart_H_layout.setContentsMargins(0, 0, 0, 0)
            self.w_chart_H_layout.setSpacing(0)
            self._chart_H = ChartWidget(self.w_chart_H)
            self._chart_H.add_plot(title="H",
                                   show_grid=True,
                                   unitx=unitx,
                                   unity=unityh)

            self.w_chart_H_layout.addWidget(self._chart_H, 1, 0)
            self._headerWidgetH = headerwidget.HeaderWidget(self.w_chart_H,
                                                            layout=self.w_chart_H_layout,
                                                            datawidget=self._chart_H,
                                                            tick=False,
                                                            pause=False,
                                                            showtime=False)
            self._headerWidgetH.set_comment(fa.icons["eye"])

            if self._header:
                self.w_chart_H_layout.addWidget(self._headerWidgetH, 0, 0)

            self._headerWidgetH.mousePressEvent = self.open_chartimageH

            self.w_chart_H_layout.setRowStretch(0, 0)
            self.w_chart_H_layout.setRowStretch(1, 1)

            self.layouts["chartH"] = self.w_chart_H_layout

            self._chartImageH = ChartImage(title="H", color=Colors.COLOR_H)
            self._chartImageV = ChartImage(title="V", color=Colors.COLOR_V)

            # chart V

            self.w_chart_V = QWidget()

            self.w_chart_V_layout = QGridLayout(self.w_chart_V)
            self.w_chart_V_layout.setContentsMargins(0, 0, 0, 0)
            self.w_chart_V_layout.setSpacing(0)
            self._chart_V = ChartWidget(self.w_chart_V)

            if self._positionV == Qt.AlignTop:
                self._chart_V.add_plot(title="V",
                                       show_grid=True,
                                       unitx=unity,
                                       unity=unityv)
            else:
                self._chart_V.add_plot(title="V",
                                       swap=True,
                                       show_grid=True,
                                       unitx=unityv,
                                       unity=unity)

            self.w_chart_V_layout.addWidget(self._chart_V, 1, 0)
            self._headerWidgetV = headerwidget.HeaderWidget(self.w_chart_V,
                                                            layout=self.w_chart_V_layout,
                                                            datawidget=self._chart_V,
                                                            tick=False,
                                                            pause=False,
                                                            showtime=False)
            self._headerWidgetV.set_comment(fa.icons["eye"])
            self._headerWidgetV.mousePressEvent = self.open_chartimageV

            if self._header:
                self.w_chart_V_layout.addWidget(self._headerWidgetV, 0, 0)

            self.w_chart_V_layout.setRowStretch(0, 0)
            self.w_chart_V_layout.setRowStretch(1, 1)

            self.layouts["chartV"] = self.w_chart_V_layout

            self.im_widget.get_plot_item().sigXRangeChanged.connect(self.updateHScale)
            self.im_widget.get_plot_item().sigYRangeChanged.connect(self.updateVScale)

            # info

            self.w_info = QWidget()
            self.w_info.setMinimumWidth(250)

            self._values = {
                "distance": 0, "npts": 0, "amp": 0, "mu": 0, "sigma": 0, "offset": 0, "fit": False
            }

            if self._fit:
                data = {
                    '': ["distance", "time", "sel x", "sel y"],
                    'Value': [0] * 4
                }
            else:
                data = {
                    '': ["distance", "time", "sel x", "sel y"],
                    'Value': [0] * 4
                }

            if not self.settings:
                data[""][0] = ""

            if values_row is not None:
                size = len(values_row)
                data_gauss = {
                    '': values_row,
                    'H': [0] * size,
                    'V': [0] * size,
                }
            else:
                data_gauss = {
                    '': ["amp", "sigma", "mu", "offset"],
                    'H': [0] * 4,
                    'V': [0] * 4,
                }

            self.df = pd.DataFrame(data)
            blankIndex = [''] * len(self.df)
            self.df.index = blankIndex

            self.df_gauss = pd.DataFrame(data_gauss)
            blankIndex = [''] * len(self.df_gauss)
            self.df_gauss.index = blankIndex

            status_colors = {
                "1": [QColor(255, 0, 0), QColor(255, 0, 0, 20)],
                "2": [QColor(0, 150, 0), QColor(0, 255, 0, 20)],
            }

            self.table = TableWidget(parent=self.w_info, status_colors=status_colors, show_index=False)
            self.table.set_data(self.df)
            self.table.setEditTriggers(QTableWidget.NoEditTriggers)
            self.table.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
            self.table.hide()

            self.table_gauss = TableWidget(parent=self.w_info, status_colors=status_colors, show_index=False)
            self.table_gauss.set_data(self.df_gauss)
            self.table_gauss.setEditTriggers(QTableWidget.NoEditTriggers)
            self.table_gauss.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
            self.table_gauss.hide()

            self.w_info_layout = QGridLayout(self.w_info)
            self.w_info_layout.setContentsMargins(0, 0, 0, 0)
            self.w_info_layout.setSpacing(2)
            self.w_info_layout.addWidget(self.table, 0, 0)
            self.w_info_layout.addWidget(self.table_gauss, 1, 0)

            if self.show_infogauss:
                self.table_gauss.show()
            if self.show_infotable:
                self.table.show()

            self.w_info_layout.setColumnStretch(0, 1)

            self.layouts["info"] = self.w_info_layout

            # positions

            self.pos = QPointF(0, 0)
            self.pos_sel = QPointF(0, 0)

            # ROI

            self._roi = None
            self._roi2 = None
            self._roi_click = False

            self.rearrange()

            self.change_option()

            self.enum_cursor_widget.get_signal().connect(self.change_option)

        self.supdate_signal.connect(self.update_image)
        self.supdate_H.connect(self.update_data_H)
        self.supdate_V.connect(self.update_data_V)
        if self._fit:
            self.supdate_fit_H.connect(self.update_fit_H)
            self.supdate_fit_V.connect(self.update_fit_V)
        self.srefresh_plots.connect(self.refresh_plots)
        self.supdate_table.connect(self.update_table)
        self.supdate_scale.connect(self.update_scale)

        self.resizeEvent = self.onResize

        self.line = None

    def onResize(self, e):
        if "image" in self._data:
            self.supdate_signal.emit()

    def set_usertime(self, usertime):
        self._usertime = usertime

    def set_name(self, name):
        self._name = name

    def open_chartimageH(self, e):
        self._chartImageH.show()

    def open_chartimageV(self, e):
        self._chartImageV.show()

    def get_image_widget(self):
        return self.im_widget

    def change_slider(self, low_value, high_value):
        self._tv.setPercent(low_value, high_value)

    def get_layouts(self):
        return self.layouts

    def get_info(self):
        return self.df

    def push_roi(self, e):
        self.change_option()
        if not self.b1.isChecked():
            if self._roi2 is not None:
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()
                else:
                    self._roi2.hide()

    def set_roi_option(self):
        self.b1.setChecked(True)
        self.reduction_changed()
        self.change_option()

    def set_distance_option(self):
        self.b2.setChecked(True)

    def set_none_option(self):
        self.b3.setChecked(True)
        self.change_option()

    def change_option(self):

        # roi

        if self.b1.isChecked():

            # self._chart_H.get_plot().setXLink(None)
            # if self._positionV == Qt.AlignTop:
            #     self._chart_V.get_plot().setXLink(None)
            # else:
            #     self._chart_V.get_plot().setYLink(None)

            if self._roi is not None:
                self._roi.show()
                self._roi2.hide()
                self.update_roi()

            if self.line is not None:
                self.line.hide()

            self.im_widget.get_plot_item().set_draggable(False)
            self.im_widget.get_plot_item().set_hoverable(False)
            self.im_widget.get_plot_item().set_clickable(False)

        # distance

        # if self._positionV == Qt.AlignTop:
        #                 #     self.im_widget.get_plot_item().sigXRangeChanged.connect(self.updateHScale)
        #                 #     self.im_widget.get_plot_item().sigYRangeChanged.connect(self.updateVScale)
        #                 # else:
        #                 #     self.im_widget.get_plot_item().sigXRangeChanged.connect(self.updateHScale)
        #                 #     self.im_widget.get_plot_item().sigYRangeChanged.connect(self.updateVScale)

        elif self.b2.isChecked():

            # if not self._checkboxroi_value.isChecked():
            #
            #     # if self._positionV == Qt.AlignTop:
            #     #     self._chart_H.get_plot().setXLink(self.im_widget.get_plot_item())
            #     #     self._chart_V.get_plot().setXLink(self.im_widget.get_plot_item())
            #     # else:
            #     #     self._chart_H.get_plot().setXLink(self.im_widget.get_plot_item())
            #     #     self._chart_V.get_plot().setYLink(self.im_widget.get_plot_item())
            # else:
            #     self._chart_H.get_plot().setXLink(None)
            #     if self._positionV == Qt.AlignTop:
            #         self._chart_V.get_plot().setXLink(None)
            #     else:
            #         self._chart_V.get_plot().setYLink(None)

            if self._roi is not None:
                self._roi.hide()
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()

            if self.line is not None:
                self.line.show()

            self.im_widget.get_plot_item().set_draggable(False)
            self.im_widget.get_plot_item().set_hoverable(False)
            self.im_widget.get_plot_item().set_clickable(True)

        # none

        else:

            # if not self._checkboxroi_value.isChecked():
            #     if self._positionV == Qt.AlignTop:
            #         self.im_widget.get_plot_item().sigXRangeChanged.connect(self.updateHScale)
            #         self.im_widget.get_plot_item().sigYRangeChanged.connect(self.updateVScale)
            #     else:
            #         self.im_widget.get_plot_item().sigXRangeChanged.connect(self.updateHScale)
            #         self.im_widget.get_plot_item().sigYRangeChanged.connect(self.updateVScale)
            #         # self._chart_H.get_plot().setXLink(self.im_widget.get_plot_item())
            #         # self._chart_V.get_plot().setYLink(self.im_widget.get_plot_item())
            # else:
            #     self._chart_H.get_plot().setXLink(None)
            #     if self._positionV == Qt.AlignTop:
            #         self._chart_V.get_plot().setXLink(None)
            #     else:
            #         self._chart_V.get_plot().setYLink(None)

            if self._roi is not None:
                self._roi.hide()
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()

            if self.line is not None:
                self.line.hide()

            self.im_widget.get_plot_item()._vline_static.setPos(self.pos.x())
            self.im_widget.get_plot_item()._hline_static.setPos(self.pos.y())

            self.im_widget.get_plot_item().set_draggable(True)
            self.im_widget.get_plot_item().set_hoverable(True)
            self.im_widget.get_plot_item().set_clickable(True)

        self.srefresh_plots.emit()

        self.updateHScale()
        self.updateVScale()

    def updateVScale(self):
        if not self.b1.isChecked():
            if self._positionV == Qt.AlignTop:
                self._chart_V.set_x_range(xmin=self.im_widget.get_plot_item().viewRange()[1][0],
                                          xmax=self.im_widget.get_plot_item().viewRange()[1][1])
            else:
                self._chart_V.set_y_range(ymin=self.im_widget.get_plot_item().viewRange()[1][0],
                                          ymax=self.im_widget.get_plot_item().viewRange()[1][1])

    def updateHScale(self):
        if not self.b1.isChecked():
            self._chart_H.set_x_range(xmin=self.im_widget.get_plot_item().viewRange()[0][0],
                                      xmax=self.im_widget.get_plot_item().viewRange()[0][1])

    def get_roi(self):
        return self._roi

    def get_signal_refresh(self):
        return self.supdate_fit_V

    @pyqtSlot()
    def update_roi(self):

        if self._roi is not None:

            visi_roi2 = self._roi2.isVisible()
            self._roi2.hide()

            x = self._roi.pos()[0]
            y = self._roi.pos()[1]
            w = self._roi.size()[0]
            h = self._roi.size()[1]

            self._roi2.setPos([x, y])
            self._roi2.setSize(size=[w, h])

            self._chart_H.set_x_range(xmin=x, xmax=x + w)

            if self._positionV == Qt.AlignTop:
                self._chart_V.set_x_range(xmin=y, xmax=y + h)
            else:
                self._chart_V.set_y_range(ymin=y, ymax=y + h)

            self.supdate_signal.emit()

            if visi_roi2:
                self._roi2.show()

    def set_roi_range(self, xmin, xmax, ymin, ymax):
        """
        Set ROI range.
        """

        if self._roi is None:
            return

        self._roi.setPos([xmin, ymin])
        self._roi.setSize(size=[xmax - xmin, ymax - ymin])

        self.update_roi()

    def reduction_changed(self):

        xmin = self.get_roi().pos()[0]
        ymin = self.get_roi().pos()[1]

        w = self.get_roi().size()[0]
        h = self.get_roi().size()[1]

        try:
            mapped_pos = QPoint(self.im_widget.get_plot_item()._vline_static.value(),
                                self.im_widget.get_plot_item()._hline_static.value())
        except:
            if self.pos is None:
                return
            mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(self.pos)

        alpha = 1.

        if self.enum_reduction_widget.get_data() == 2:
            if self._old_reduction == 1:
                alpha = 0.5
            elif self._old_reduction == 3:
                alpha = 2.
        elif self.enum_reduction_widget.get_data() == 3:
            if self._old_reduction == 1:
                alpha = 0.25
            elif self._old_reduction == 2:
                alpha = 0.5
        else:
            if self._old_reduction == 2:
                alpha = 2.
            elif self._old_reduction == 3:
                alpha = 4.

        self.pos = QPoint((mapped_pos.x() * alpha), (mapped_pos.y() * alpha))

        self.im_widget.get_plot_item()._vline_static.setPos(self.pos.x())
        self.im_widget.get_plot_item()._hline_static.setPos(self.pos.y())

        self.set_roi_range((xmin * alpha), ((xmin + w) * alpha), (ymin * alpha), ((ymin + h) * alpha))

        self._old_reduction = self.enum_reduction_widget.get_data()

    def set_gauss_values(self, values=None):

        if values is None:
            return

        if len(self.df_gauss["H"]) == 4:
            self.df_gauss["H"][0] = formatting.format_value_to_string(values["ampH"], type_format="sci",
                                                                      format_sci="{:.3e}")  # amp H
            self.df_gauss["H"][1] = formatting.format_value_to_string(values["sigmaH"], type_format="sci",
                                                                      format_sci="{:.3e}")  # sigma H
            self.df_gauss["H"][2] = formatting.format_value_to_string(values["muH"], type_format="sci",
                                                                      format_sci="{:.3e}")  # mu H
            self.df_gauss["H"][3] = formatting.format_value_to_string(values["offsetH"], type_format="sci",
                                                                      format_sci="{:.3e}")  # offset H
            self.df_gauss["V"][0] = formatting.format_value_to_string(values["ampV"], type_format="sci",
                                                                      format_sci="{:.3e}")  # amp V
            self.df_gauss["V"][1] = formatting.format_value_to_string(values["sigmaV"], type_format="sci",
                                                                      format_sci="{:.3e}")  # sigma V
            self.df_gauss["V"][2] = formatting.format_value_to_string(values["muV"], type_format="sci",
                                                                      format_sci="{:.3e}")  # mu V
            self.df_gauss["V"][3] = formatting.format_value_to_string(values["offsetV"], type_format="sci",
                                                                      format_sci="{:.3e}")  # offset V

            if values["sigmaH"] == 0 or values["sigmaV"] == 0:
                self.df_gauss["status_list"][0] = 1
            else:
                self.df_gauss["status_list"][0] = 2
        else:
            try:
                size = len(self.df_gauss["H"])
                for j in range(size):
                    name = self.df_gauss[""][j] + "H"
                    self.df_gauss["H"][j] = formatting.format_value_to_string(values[name], type_format="sci",
                                                                              format_sci="{:.3e}")
                    name = self.df_gauss[""][j] + "V"
                    self.df_gauss["V"][j] = formatting.format_value_to_string(values[name], type_format="sci",
                                                                              format_sci="{:.3e}")

                if values["sigmaH"] == 0 or values["sigmaV"] == 0:
                    self.df_gauss["status_list"][0] = 1
                else:
                    self.df_gauss["status_list"][0] = 2
            except Exception as e:
                print(e)

        self.supdate_table.emit()

    def fit_changed(self):

        if self._fit:
            if self.enum_fit_widget.get_data() == 1:
                self.table_gauss.show()
            else:
                self.table_gauss.hide()

        self.srefresh_plots.emit()

    def fit(self, ty, fo, x, y, thres=0.0):
        """
        Function fit gaussian.
        """

        y_used = np.array(list(y), dtype=float)
        x_used = np.array(list(x), dtype=float)

        if thres == 0.04:
            x_used = self.y_to_y_scale(x_used)
        else:
            x_used = self.x_to_x_scale(x_used)

        try:
            maxy = max(y_used)
            if maxy > 20:
                sy = sum(y_used)
                if sy != 0:
                    mean = sum(x_used * y_used) / sy
                    sigma = np.sqrt(sum(y_used * (x_used - mean) ** 2) / sy)
                    popt, pcov = curve_fit(
                        f=fo,
                        xdata=x_used,
                        ydata=y_used,
                        p0=[maxy, mean, sigma, y_used[0]],
                        maxfev=100,
                        check_finite=False)
                else:
                    raise TypeError("no fit")
            else:
                raise TypeError("no fit")
        except:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        if popt[2] < 0:
            popt[2] = -popt[2]

        if popt[2] > 0:
            self._values["npts"] = len(x)
            self._values["amp"] = popt[0]
            self._values["mu"] = popt[1]
            self._values["sigma"] = popt[2]
            self._values["offset"] = popt[3]
            self._values["fit"] = True
        else:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        return fo_gauss(x_used, *popt)

    def calculate(self, data_H, data_V):
        """
        Do the calculation and update widget.
        """

        data_x_V = range(0, len(data_V))
        data_x_H = range(0, len(data_H))

        fitH = self.fit(1, fo_gauss, data_x_H, data_H, thres=0.4)

        self._values["ampH"] = self._values["amp"]
        self._values["muH"] = self._values["mu"]
        self._values["sigmaH"] = self._values["sigma"]
        self._values["offsetH"] = self._values["offset"]
        self._values["fitH"] = self._values["fit"]

        fitV = self.fit(2, fo_gauss, data_x_V, data_V, thres=0.04)

        self._values["ampV"] = self._values["amp"]
        self._values["muV"] = self._values["mu"]
        self._values["sigmaV"] = self._values["sigma"]
        self._values["offsetV"] = self._values["offset"]
        self._values["fitV"] = self._values["fit"]

        return [fitH, fitV]

    def click_on_image(self, evt):

        if self.b2.isChecked():
            self.pos_sel = QPointF((evt.pos().x()), (evt.pos().y()))
            self.srefresh_plots.emit()
        elif not self.b1.isChecked():
            try:
                self.pos = QPointF(self.im_widget.get_plot_item()._vline_static.value(),
                                   self.im_widget.get_plot_item()._hline_static.value())
            except:
                self.pos = QPointF((evt.pos().x()), (evt.pos().y()))
            self.srefresh_plots.emit()

    def remove_singletons(self, data, time):

        t = formatting.current_milli_time()

        size_avg = 25

        acceptance = 3
        count_max = 2

        modw = size_avg * int(data.shape[0] / size_avg)
        modh = size_avg * int(data.shape[1] / size_avg)

        for i in range(0, int(data.shape[0] / size_avg)):
            for j in range(0, int(data.shape[1] / size_avg)):
                try:
                    datatmp = data[i * size_avg:(i + 1) * size_avg, j * size_avg:(j + 1) * size_avg]
                    avg = np.mean(datatmp)
                    maxs = np.argwhere(datatmp >= acceptance * avg)
                    if len(maxs) >= 1 and len(maxs) <= count_max:
                        datatmp[datatmp >= acceptance * avg] = avg
                except Exception as e:
                    print(e)
                    pass

        for i in range(0, int(data.shape[0] / size_avg)):
            try:
                datatmp = data[i * size_avg:(i + 1) * size_avg, modh:data.shape[1]]
                avg = np.mean(datatmp)
                maxs = np.argwhere(datatmp >= acceptance * avg)
                if len(maxs) >= 1 and len(maxs) <= count_max:
                    datatmp[datatmp >= acceptance * avg] = avg
            except Exception as e:
                print(e)
                pass

        for j in range(0, int(data.shape[1] / size_avg)):
            try:
                datatmp = data[modw:data.shape[0], j * size_avg:(j + 1) * size_avg]
                avg = np.mean(datatmp)
                maxs = np.argwhere(datatmp >= acceptance * avg)
                if len(maxs) >= 1 and len(maxs) <= count_max:
                    datatmp[datatmp >= acceptance * avg] = avg
            except Exception as e:
                print(e)
                pass

        datatmp = data[modw:data.shape[0],
                  modh:data.shape[1]]
        avg = np.mean(datatmp)
        maxs = np.argwhere(datatmp >= 3 * avg)
        if len(maxs) > 0 and len(maxs) >= 1 and len(maxs) <= count_max:
            datatmp[datatmp >= acceptance * avg] = avg

        # print(formatting.current_milli_time() - t)

        return data

    def release_mutex(self):
        try:
            self._mutex = False
        except:
            pass

    def reset_led(self):
        if self._led_flag:
            self._led_live_widget.set_color_inside(Colors.COLOR_TRANSPARENT)
        self._led_flag = not self._led_flag

    def set_image_levels(self, min=0, max=1):
        self._autoLevels = False
        self.im_widget.get_image_item().setLevels(levels=(min, max), update=False)

    def change_scale(self, xscale=1., yscale=1., xoffset=0., yoffset=0.):

        if xscale == 0 or yscale == 0:
            return

        if (self._xscale == xscale) and (self._yscale == yscale) and (self._xoffset == xoffset) and (
                self._yoffset == yoffset):
            return

        self._xscale_prev = self._xscale
        self._yscale_prev = self._yscale
        self._xoffset_prev = self._xoffset
        self._yoffset_prev = self._yoffset

        self._xscale = xscale
        self._yscale = yscale
        self._xoffset = xoffset
        self._yoffset = yoffset

        self.im_widget.get_plot_item().removeItem(self._roi)
        self.im_widget.get_plot_item().removeItem(self._roi2)

        self._roi = None
        self._roi2 = None

        self.supdate_scale.emit()

    def x_to_x_scale(self, x, noint=True):
        if noint:
            return (x * self._xscale - self._xoffset)
        return int(x * self._xscale - self._xoffset)

    def y_to_y_scale(self, y, noint=True):
        if noint:
            return (y * self._yscale - self._yoffset)
        return int(y * self._yscale - self._yoffset)

    def xscale_to_x(self, x):
        return ((x + self._xoffset) / self._xscale)

    def yscale_to_y(self, y):
        return ((y + self._yoffset) / self._yscale)

    def x_to_x_scale_prev(self, x):
        return (x * self._xscale_prev - self._xoffset_prev)

    def y_to_y_scale_prev(self, y):
        return (y * self._yscale_prev - self._yoffset_prev)

    def xscale_to_x_prev(self, x):
        return ((x + self._xoffset_prev) / self._xscale_prev)

    def yscale_to_y_prev(self, y):
        return ((y + self._yoffset_prev) / self._yscale_prev)

    # @pyqtSlot()
    def refresh_plots(self):

        if self.pos is None:
            return

        if self._roi is not None:
            try:
                x = int(self.xscale_to_x(self._roi.pos()[0]))
                y = int(self.yscale_to_y(self._roi.pos()[1]))
                w = int(self._roi.size()[0] / self._xscale)
                h = int(self._roi.size()[1] / self._yscale)
            except:
                return
        else:
            return

        # data

        data = self._data["image"]

        wimage = data.shape[0]
        himage = data.shape[1]

        # if ROI activated

        if not self.b1.isChecked():
            x = 0
            y = 0
            w = wimage
            h = himage

        if x < 0:
            w = w + x
            x = 0

        if y < 0:
            h = h + y
            y = 0

        if h <= 0 or w <= 0:
            return

        if x >= wimage or y >= himage:
            return

        if x + w >= wimage:
            w = wimage - x

        if y + h >= himage:
            h = himage - y

        # map the image coordinates to data coordinates

        try:
            if not self.b2.isChecked():
                self.pos = QPointF(self.im_widget.get_plot_item()._vline_static.value(),
                                   self.im_widget.get_plot_item()._hline_static.value())
                mapped_pos = self.pos
            else:
                if self.pos_sel is None:
                    return
                pos_shifted = QPointF(self.pos_sel.x() + 5, self.pos_sel.y() + 5)
                mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(pos_shifted)
        except:
            if self.pos is None:
                return
            mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(self.pos)

        mapped_pos_real = QPoint(int(self.xscale_to_x(mapped_pos.x())),
                                 int(self.yscale_to_y(mapped_pos.y())))

        if mapped_pos_real.x() < 0 or mapped_pos_real.x() > wimage:
            mapped_pos_real.setX(0)

        if mapped_pos_real.y() < 0 or mapped_pos_real.y() > himage:
            mapped_pos_real.setY(0)

        # extract the x and y indices

        self.x_index = mapped_pos_real.x()
        self.y_index = mapped_pos_real.y()

        if 0 <= self.x_index < data.shape[0] and 0 <= self.y_index < data.shape[1]:

            # update H and V data

            if self.enum_reduction_widget.get_data() != -1:
                if self.enum_cursor_widget.get_data() == 1:
                    self.set_data_H(range(x, w + x), data[x:w + x, self.y_index])
                    self.set_data_V(range(y, h + y), data[self.x_index, y:h + y])
                else:
                    if self._avg_sum:
                        self.set_data_H(range(x, w + x), np.sum(data, axis=1)[x:w + x])
                        self.set_data_V(range(y, h + y), np.sum(data, axis=0)[y:h + y])
                    else:
                        self.set_data_H(range(x, w + x), np.average(data, axis=1)[x:w + x])
                        self.set_data_V(range(y, h + y), np.average(data, axis=0)[y:h + y])

            # create a LineSegmentROI between max value and cursor click

            if self.b2.isChecked():

                self.im_widget.get_plot_item().removeItem(self.line)

                imax = np.argmax(data)

                xm = int(imax / himage)
                ym = int(imax - himage * xm)

                max_pos = (self.x_to_x_scale(xm),
                           self.y_to_y_scale(ym))

                self.line = pg.LineSegmentROI([[max_pos[0], max_pos[1]],
                                               [mapped_pos.x(), mapped_pos.y()]],
                                              pen=pg.mkPen({'color': Colors.COLOR_LIGHTRED, 'width': 2.}))
                self.im_widget.get_plot_item().addItem(self.line)

                # calculate distance between max value and cursor click

                distance = calculate_distance((max_pos[0], max_pos[1]),
                                              (mapped_pos.x(), mapped_pos.y()))
            else:
                distance = -1

            if self.enum_fit_widget.get_data() == 1:
                if self.enum_filter_widget.get_data() != 0 and self.enum_reduction_widget.get_data() > 0:
                    if self.enum_cursor_widget.get_data() == 1:
                        data_fit = self.calculate(data[x:w + x, self.y_index], data[self.x_index, y:h + y])
                    else:
                        if self._avg_sum:
                            data_fit = self.calculate(np.sum(data, axis=1)[x:w + x],
                                                      np.sum(data, axis=0)[y:h + y])
                        else:
                            data_fit = self.calculate(np.average(data, axis=1)[x:w + x],
                                                      np.average(data, axis=0)[y:h + y])

            if self.show_info:

                if distance != -1:
                    self.df["Value"][0] = formatting.format_value_to_string(distance, type_format="sci",
                                                                            format_sci="{:.3e}")  # distance
                else:
                    self.df["Value"][0] = ""
                if self._usertime is not None:
                    self.df["Value"][1] = self._usertime
                else:
                    self.df["Value"][1] = formatting.format_value_to_string(formatting.current_milli_time() / 1000,
                                                                            type_format="date")  # time
                self.df["Value"][2] = str(self.x_index) + " (" + str(mapped_pos.x()) + ")"  # sel x
                self.df["Value"][3] = str(self.y_index) + " (" + str(mapped_pos.y()) + ")"  # sel y

                if self.enum_fit_widget.get_data() == 1:
                    if self.enum_reduction_widget.get_data() > 0:
                        if not self.extfit:
                            self.df_gauss["H"][0] = formatting.format_value_to_string(self._values["ampH"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # amp H
                            self.df_gauss["H"][1] = formatting.format_value_to_string(self._values["sigmaH"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # sigma H
                            self.df_gauss["H"][2] = formatting.format_value_to_string(self._values["muH"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # mu H
                            self.df_gauss["H"][3] = formatting.format_value_to_string(self._values["offsetH"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # offset H
                            self.df_gauss["V"][0] = formatting.format_value_to_string(self._values["ampV"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # amp V
                            self.df_gauss["V"][1] = formatting.format_value_to_string(self._values["sigmaV"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # sigma V
                            self.df_gauss["V"][2] = formatting.format_value_to_string(self._values["muV"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # mu V
                            self.df_gauss["V"][3] = formatting.format_value_to_string(self._values["offsetV"],
                                                                                      type_format="sci",
                                                                                      format_sci="{:.3e}")  # offset V

                            if not self._values["fitH"] or not self._values["fitV"]:
                                self.df_gauss["status_list"][0] = 1
                            else:
                                self.df_gauss["status_list"][0] = 2

                    else:
                        self.df_gauss["H"][0] = ""
                        self.df_gauss["H"][1] = ""
                        self.df_gauss["H"][2] = ""
                        self.df_gauss["H"][3] = ""
                        self.df_gauss["V"][0] = ""
                        self.df_gauss["V"][1] = ""
                        self.df_gauss["V"][2] = ""
                        self.df_gauss["V"][3] = ""
                        self.df_gauss["status_list"][0] = 1
                else:
                    if self._fit:
                        self.df_gauss["H"][0] = ""
                        self.df_gauss["H"][1] = ""
                        self.df_gauss["H"][2] = ""
                        self.df_gauss["H"][3] = ""
                        self.df_gauss["V"][0] = ""
                        self.df_gauss["V"][1] = ""
                        self.df_gauss["V"][2] = ""
                        self.df_gauss["V"][3] = ""
                        self.df_gauss["status_list"][0] = 1

                self.supdate_table.emit()

            if not self.extfit:
                if self.enum_fit_widget.get_data() == 1:
                    if self.enum_reduction_widget.get_data() > 0:
                        self.set_data_fit_H(range(x, w + x), data_fit[0])
                        self.set_data_fit_V(range(y, h + y), data_fit[1])
                    else:
                        self.set_data_fit_H(range(x, 1 + x), [0])
                        self.set_data_fit_V(range(y, 1 + y), [0])
                else:
                    self.set_data_fit_H(range(x, 1 + x), [0])
                    self.set_data_fit_V(range(y, 1 + y), [0])

    def set_data(self, data, time=-1, xroioffset=0, yroioffset=0):
        """
        Set data.
        :param time:
        :param data:
        :return:
        """

        if self._mutex:
            return

        self._mutex = True

        if (time != -1) and (time == self._time_image):
            self.release_mutex()
            return

        self._time_image = time

        modif = False

        try:
            if (self._data["image"].shape[0] != data.shape[0]) or (self._data["image"].shape[1] != data.shape[1]):
                modif = True
        except:
            pass

        if (self._xoffset_roi != xroioffset) or (self._yoffset_roi != yroioffset) or modif:
            self._xoffset_roi = xroioffset
            self._yoffset_roi = yroioffset

            self.im_widget.get_plot_item().removeItem(self._roi)
            self.im_widget.get_plot_item().removeItem(self._roi2)

            self._roi = None
            self._roi2 = None

            modif = True

        text = None

        try:
            index_table = self._tabs.currentIndex()
            text = self._tabs.tabText(index_table)
        except:
            pass

        dataf = data

        if self._bascule_tab:
            if text is not None:
                if "TV" in text:
                    if not self._headerWidgettv.is_pause():
                        self._headerWidgettv.recv()
                        self._tv.setImage(dataf)
                    self.release_mutex()
                    return
                elif "Multi" in text:
                    if not self._headerWidgetmulti.is_pause():
                        self._headerWidgetmulti.recv()
                        self._multi_widget.set_data(dataf)
                    self.release_mutex()
                    return
                elif "Mosaic" in text:
                    if not self._headerWidgetmosaic.is_pause():
                        self._headerWidgetmosaic.recv()
                        self._mosaic_widget.set_data(dataf, time_image=time)
                    self.release_mutex()
                    return
        else:
            if self._tv:
                if not self._headerWidgettv.is_pause():
                    self._headerWidgettv.recv()
                    self._tv.setImage(np.flip(dataf, 0))

            if self._multi:
                if not self._headerWidgetmulti.is_pause():
                    self._headerWidgetmulti.recv()
                    self._multi_widget.set_data(dataf)

            if self._mosaic:
                if not self._headerWidgetmosaic.is_pause():
                    self._headerWidgetmosaic.recv()
                    self._mosaic_widget.set_data(dataf, time_image=time)

        if not self._profile:
            self.release_mutex()
            return

        if (formatting.current_milli_time() - self._update_time) < self._limit_time:
            self.release_mutex()
            return

        if not self._active or self._headerWidget.is_pause():
            self.release_mutex()
            return

        if self.led:
            self._led_live_widget.set_color_inside(QColor("#B6FF00"))

        type_signal = 1

        if self.enum_reduction_widget.get_data() != 1:

            neighbour = 2
            neighbour_distance = 5

            if self.enum_reduction_widget.get_data() == 2:
                im = Image.fromarray(data)
                im = im.resize((im.width // 2, im.height // 2), resample=Image.NEAREST)
                data = np.asarray(im).copy()
                neighbour = 1
            elif self.enum_reduction_widget.get_data() == 3:
                im = Image.fromarray(data)
                im = im.resize((im.width // 4, im.height // 4), resample=Image.NEAREST)
                data = np.asarray(im).copy()
                neighbour = 1
            elif self.enum_reduction_widget.get_data() == 4:
                if "int16" in str(data.dtype):
                    data = (data >> 8).astype(np.int8)
                elif "int32" in str(data.dtype):
                    data = (data >> 16).astype(np.int16)

            max = np.max(data[::neighbour_distance - neighbour, ::neighbour_distance - neighbour])
            max2 = np.max(data[1::neighbour_distance - neighbour, 1::neighbour_distance - neighbour])
            max3 = 0
            max4 = 0

            # no beam saturated

            if max != max2:
                # remove singleton

                maxall = np.max(data)
                minall = np.min(data)

                data[data == maxall] = minall

                max3 = np.max(data[::neighbour_distance - neighbour, ::neighbour_distance - neighbour])
                max4 = np.max(data[1::neighbour_distance - neighbour, 1::neighbour_distance - neighbour])

            # beam + saturation

            if max == max2:
                self._data["image"] = data
                if self._absolute_scale:
                    maxy = np.max(self._data["image"])
                    if maxy > self._ymax:
                        self._ymax = maxy
                        if self._positionV == Qt.AlignTop:
                            self._chart_H.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                            self._chart_V.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                        else:
                            self._chart_H.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                            self._chart_V.set_x_range(xmin=self.x_to_x_scale(0), xmax=self.x_to_x_scale(self._ymax))
                if self.led:
                    self._led_live_widget.set_color_inside(QColor("#FF2900"))
                type_signal = 2

            # beam

            elif max3 == max4:
                self._data["image"] = data
                if self._absolute_scale:
                    maxy = np.max(self._data["image"])
                    if maxy > self._ymax:
                        self._ymax = maxy
                        if self._positionV == Qt.AlignTop:
                            self._chart_H.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                            self._chart_V.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                        else:
                            self._chart_H.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._ymax))
                            self._chart_V.set_x_range(xmin=self.x_to_x_scale(0), xmax=self.x_to_x_scale(self._ymax))
                if self.led:
                    self._led_live_widget.set_color_inside(QColor("#B6FF00"))
                type_signal = 3

            # none

            else:
                if self.led:
                    self._led_live_widget.set_color_inside(QColor("#666"))
                type_signal = 1

        if self.led:
            self.reset_led()

        if (formatting.current_milli_time() - self._update_time) < 1:
            self.release_mutex()
            return

        self._update_time = formatting.current_milli_time()

        if self.enum_filter_widget.get_data() == 1 or (self.enum_filter_widget.get_data() == type_signal):
            self._image_ref = data
            if (self._apply_baseline.get_data() == 1) and (self._data_baseline is not None):
                try:
                    self._data["image"] = data - self._data_baseline
                except Exception as e:
                    print(e)
                    self._data["image"] = data
            else:
                self._data["image"] = data
            if modif:
                self.supdate_scale.emit()
            self.supdate_signal.emit()
        else:
            self.release_mutex()

        if self._once < 2:
            self.set_image_ratio(not self._ratio.get_data())
            self.set_image_ratio(self._ratio.get_data())
            self._once = self._once + 1

    @pyqtSlot()
    def update_scale(self):

        self.im_widget.change_scale(xscale=self._xscale,
                                    yscale=self._yscale,
                                    xoffset=self._xoffset,
                                    yoffset=self._yoffset,
                                    xmax=self._data["image"].shape[0],
                                    ymax=self._data["image"].shape[1])

        self.im_widget.set_x_range(xmin=self.x_to_x_scale(0), xmax=self.x_to_x_scale(self._data["image"].shape[0]))
        self.im_widget.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._data["image"].shape[1]))

        self.supdate_signal.emit()

    @pyqtSlot()
    def update_table(self):
        self.table.refresh_table()
        if "status_list" in self.df_gauss:
            if self.df_gauss["status_list"][0] != 0:
                if self.table_gauss.isHidden():
                    self.table_gauss.show()
        self.table_gauss.refresh_table()

    @pyqtSlot()
    def update_image(self):

        if self._usertime is not None:
            self._headerWidget.recv(self._usertime)
        else:
            self._headerWidget.recv()

        self.refresh_plots()

        width = len(self._data["image"])

        if width > 0:

            h_rect = self.geometry().height() / 2
            w_rect = self.geometry().width() - 250

            # print("#", self.geometry().height(), h_rect, w_rect)

            if w_rect < 10:
                w_rect = 10

            if h_rect < 10:
                h_rect = 10

            ref_length = h_rect
            if w_rect < h_rect:
                ref_length = w_rect

            # print(ref_length)
            height = len(self._data["image"][0])
            ratio_value = height / width

            if height > width:
                height_image = round(1. * (ref_length))
                width_image = round(height_image / ratio_value)
            else:
                width_image = round(1. * (ref_length))
                height_image = round(width_image * ratio_value)

            if width_image < 100:
                width_image = 100

            if height_image < 100:
                height_image = 100

            if width_image > height_image:
                width_image = width_image - 0
                height_image = height_image + 0
            else:
                width_image = width_image - 0
                height_image = height_image - 0

            # print(width_image, height_image)

            if (
                    self.width_image != width_image or self.height_image != height_image) and self._positionV == Qt.AlignTop and self._positionH == Qt.AlignBottom:
                self.width_image = width_image
                self.height_image = height_image
                self.w_image.setMaximumWidth(width_image)
                self.w_image.setMaximumHeight(height_image)
                self.w_image.setMinimumWidth(width_image)
                self.w_image.setMinimumHeight(height_image)

            self.im_widget.set_data(self._data["image"], autoLevels=self._autoLevels)

            if self._roi is None:
                self._roi = pg.ROI(
                    [(self.x_to_x_scale((0.25 * self._data["image"].shape[0]))),
                     (self.y_to_y_scale((0.25 * self._data["image"].shape[1])))],
                    [(self._xscale * 0.50 * self._data["image"].shape[0]),
                     (self._yscale * 0.50 * self._data["image"].shape[1])],
                    maxBounds=QRectF(self.x_to_x_scale(0, noint=False),
                                     self.y_to_y_scale(0, noint=False),
                                     (self._xscale * self._data["image"].shape[0]),
                                     (self._yscale * self._data["image"].shape[1])),
                    rotatable=False,
                    removable=False)

                self._roi.addScaleHandle([0.5, 1], [0.5, 0.5])
                self._roi.addScaleHandle([0, 0.5], [0.5, 0.5])
                self._roi.addScaleHandle([1, 0.5], [0.5, 0.5])
                self._roi.addScaleHandle([0.5, 0], [0.5, 0.5])
                self._roi.setZValue(10)
                self._roi.sigRegionChangeFinished.connect(self.update_roi)

                self._roi2 = pg.ROI(
                    [self.x_to_x_scale((0.25 * self._data["image"].shape[0])),
                     self.y_to_y_scale((0.25 * self._data["image"].shape[1]))],
                    [(self._xscale * 0.50 * self._data["image"].shape[0]),
                     (self._yscale * 0.50 * self._data["image"].shape[1])],
                    maxBounds=QRectF(self.x_to_x_scale(0),
                                     self.y_to_y_scale(0),
                                     (self._xscale * self._data["image"].shape[0]),
                                     (self._yscale * self._data["image"].shape[1])),
                    movable=False,
                    rotatable=False,
                    resizable=False,
                    removable=False,
                    pen=pg.mkPen(color=Colors.COLOR_LIGHT0GREEN, width=2, dash=[2, 2]))

                self._roi2.setZValue(9)
                self._roi2.hide()

                self.change_option()

                self.im_widget.get_plot_item().addItem(self._roi)
                self.im_widget.get_plot_item().addItem(self._roi2)

                self.im_widget.get_plot_item().get_mousepressevent().connect(self.click_on_image)

        self.release_mutex()

    @pyqtSlot()
    def update_data_H(self):
        if self._chartImageH.isVisible():
            self._chartImageH.set_data(self._data["H"], datax=self._data["xH"])
        else:
            if self._usertime is not None:
                self._headerWidgetH.recv(self._usertime)
            else:
                self._headerWidgetH.recv()
            self._chart_H.set_data(self._data["H"],
                                   datax=self._data["xH"],
                                   skipFiniteCheck=True,
                                   autoDownsample=True,
                                   antialias=True,
                                   pen=pg.mkPen({'color': Colors.COLOR_H, 'width': 1.}))

    def set_data_H(self, datax, data):
        if data is not None:
            self._data["xH"] = self.x_to_x_scale(np.array(datax), noint=True)
            self._data["H"] = data
            self.supdate_H.emit()

    @pyqtSlot()
    def update_fit_H(self):
        self._chart_H.set_data(self._data["FitH"],
                               datax=self._data["xFitH"],
                               name="fitH",
                               skipFiniteCheck=True,
                               autoDownsample=True,
                               antialias=True,
                               pen=pg.mkPen({'color': Colors.COLOR_LIGHT0GREEN, 'width': 1.}))

    def set_data_fit_H(self, datax, data):
        if data is not None:
            self._data["xFitH"] = self.x_to_x_scale(np.array(datax))
            self._data["FitH"] = data
            self.supdate_fit_H.emit()
        else:
            self._data["xFitH"] = [0, 0]
            self._data["FitH"] = [0, 0]
            self.supdate_fit_H.emit()

    @pyqtSlot()
    def update_data_V(self):
        if self._chartImageV.isVisible():
            self._chartImageV.set_data(self._data["V"], datax=self._data["xV"])
        else:
            if self._usertime is not None:
                self._headerWidgetV.recv(self._usertime)
            else:
                self._headerWidgetV.recv()
            self._chart_V.set_data(self._data["V"],
                                   datax=self._data["xV"],
                                   skipFiniteCheck=True,
                                   autoDownsample=True,
                                   antialias=True,
                                   pen=pg.mkPen({'color': Colors.COLOR_V, 'width': 1.}))

    def set_data_V(self, datax, data):
        if data is not None:
            if self._positionV == Qt.AlignTop:
                self._data["xV"] = self.y_to_y_scale(np.array(datax), noint=True)
            else:
                self._data["xV"] = self.y_to_y_scale(np.array(datax), noint=True)
            self._data["V"] = data
            self.supdate_V.emit()

    @pyqtSlot()
    def update_fit_V(self):
        self._chart_V.set_data(self._data["FitV"],
                               datax=self._data["xFitV"],
                               name="fitV",
                               skipFiniteCheck=True,
                               autoDownsample=True,
                               antialias=True,
                               pen=pg.mkPen({'color': Colors.COLOR_LIGHT0GREEN, 'width': 1.}))

    def set_data_fit_V(self, datax, data):
        if data is not None:
            if self._positionV == Qt.AlignTop:
                self._data["xFitV"] = self.y_to_y_scale(np.array(datax))
            else:
                self._data["xFitV"] = self.y_to_y_scale(np.array(datax))
            self._data["FitV"] = data
            self.supdate_fit_V.emit()
        else:
            self._data["xFitV"] = [0, 0]
            self._data["FitV"] = [0, 0]
            self.supdate_fit_V.emit()

    def set_avg(self, avg=False):
        if avg:
            self.enum_cursor_widget.set_data(2)
        else:
            self.enum_cursor_widget.set_data(1)
        self.srefresh_plots.emit()

    def light_(self):

        Colors.COLOR_LIGHT = True

        if self._mosaic:
            self._mosaic_widget.light_()

        if self._multi:
            self._multi_widget.light_()

        if self._profile:
            self._push_baseline.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLUE + ";")

            self._chart_V.light_()
            self._chart_H.light_()

            self._chartImageH.light_()
            self._chartImageV.light_()

            self.im_widget.light_()
            self.table.light_()
            self.table_gauss.light_()

            self._apply_baseline.light_()
            self._baseline_panel.light_()

            self._selection_panel.light_()
            self.toggle_panel_options.light_()

            self._ratio.light_()
            self._ratio_panel.light_()

            self._cursor_panel.light_()
            self.enum_cursor_widget.light_()

            self._filter_panel.light_()
            self.enum_filter_widget.light_()

            self.enum_fit_widget.light_()

            self._live_panel.light_()
            self._led_live_widget.light_()

    def dark_(self):

        Colors.COLOR_LIGHT = False

        if self._mosaic:
            self._mosaic_widget.dark_()

        if self._multi:
            self._multi_widget.dark_()

        if self._profile:
            self._push_baseline.setStyleSheet(
                "background-color:" + Colors.STR_COLOR_L3BLACK + ";color:" + Colors.STR_COLOR_BLUE + ";")

            self._chart_V.dark_()
            self._chart_H.dark_()

            self._chartImageH.dark_()
            self._chartImageV.dark_()

            self.im_widget.dark_()

            self.table.dark_()
            self.table_gauss.dark_()

            self._apply_baseline.dark_()
            self._baseline_panel.dark_()

            self._selection_panel.dark_()
            self.toggle_panel_options.dark_()

            self._ratio.dark_()
            self._ratio_panel.dark_()

            self._cursor_panel.dark_()
            self.enum_cursor_widget.dark_()

            self._filter_panel.dark_()
            self.enum_filter_widget.dark_()

            self.enum_fit_widget.dark_()

            self._live_panel.dark_()
            self._led_live_widget.dark_()

    def clear_layout(self):
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)

    def get_values(self):
        return self._values

    def get_w_options(self):
        return self.w_options

    def get_w_options_layout(self):
        return self.w_options_layout

    def rearrange(self):

        self.clear_layout()

        if self._positionV == Qt.AlignTop and self._positionH == Qt.AlignBottom:

            if self.settings:
                self.layout.addWidget(self.toggle_panel_options, 0, 0, Qt.AlignCenter)

            self.pbblue = QWidget()
            self.layoutH = QGridLayout(self.pbblue)
            self.layoutH.setContentsMargins(0, 0, 0, 0)
            self.layoutH.setSpacing(0)
            self.pbblue.setStyleSheet("background-color:" + Colors.STR_COLOR_H + ";color:white;")
            self.pbblue.setMinimumWidth(25)
            self.labelH = QLabel(" H")
            self.layoutH.addWidget(self.labelH, 0, 0)
            self.pbred = QWidget()
            self.layoutV = QGridLayout(self.pbred)
            self.layoutV.setContentsMargins(0, 0, 0, 0)
            self.layoutV.setSpacing(0)
            self.pbred.setStyleSheet("background-color:" + Colors.STR_COLOR_V + ";color:white;")
            self.pbred.setMinimumWidth(25)
            self.labelV = QLabel(" V")
            self.layoutV.addWidget(self.labelV, 0, 0)

            top_widget = QWidget()
            layout_top = QGridLayout(top_widget)
            layout_top.setContentsMargins(0, 0, 0, 0)
            layout_top.setSpacing(0)
            layout_top.addWidget(self.w_image, 0, 0, Qt.AlignLeft)
            if self.show_info:
                layout_top.addWidget(self.w_info, 0, 1)
            layout_top.setColumnStretch(0, 0)
            layout_top.setColumnStretch(1, 1)

            self.w_chart_H.setMinimumHeight(100)
            self.w_chart_V.setMinimumHeight(100)

            bottom_widget = QWidget()
            layout_bottom = QGridLayout(bottom_widget)
            layout_bottom.setContentsMargins(0, 0, 0, 0)
            layout_bottom.setSpacing(0)
            layout_bottom.addWidget(self.pbblue, 0, 0)
            layout_bottom.addWidget(self.w_chart_H, 0, 1)
            layout_bottom.addWidget(self.pbred, 1, 0)
            layout_bottom.addWidget(self.w_chart_V, 1, 1)

            layout_bottom.setRowStretch(0, 1)
            layout_bottom.setRowStretch(1, 1)

            self.layout.addWidget(top_widget, 1, 0)
            self.layout.addWidget(bottom_widget, 2, 0)

            self.layout.setColumnStretch(0, 1)
            self.layout.setColumnStretch(1, 0)

            self.layout.setRowStretch(1, 1)
            self.layout.setRowStretch(2, 1)

        elif self._positionV == Qt.AlignLeft:
            if self._positionH == Qt.AlignTop:
                if self.settings:
                    self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 1, 1, 1, 1)
                self.layout.addWidget(self.w_chart_V, 2, 1, 1, 1)
                self.layout.addWidget(self.w_chart_H, 1, 2, 1, 1)
                self.layout.addWidget(self.w_image, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 1)
                # self.w_chart_H.setMinimumHeight(self._minsizeH)
                # self.w_chart_H.setMaximumHeight(self._minsizeH)
                # self.w_chart_V.setMinimumWidth(self._minsizeV)
                # self.w_chart_V.setMaximumWidth(self._minsizeV)

                self.w_chart_H.setMinimumHeight(100)
                self.w_chart_V.setMinimumWidth(100)

            elif self._positionH == Qt.AlignBottom:
                if self.settings:
                    self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                self.layout.addWidget(self.w_chart_V, 1, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 2, 1, 1, 1)
                self.layout.addWidget(self.w_image, 1, 2, 1, 1)
                self.layout.addWidget(self.w_chart_H, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 1)
                # self.w_chart_H.setMinimumHeight(self._minsizeH)
                # self.w_chart_H.setMaximumHeight(self._minsizeH)
                # self.w_chart_V.setMinimumWidth(self._minsizeV)
                # self.w_chart_V.setMaximumWidth(self._minsizeV)

                self.w_chart_H.setMinimumHeight(100)
                self.w_chart_V.setMinimumWidth(100)
        elif self._positionV == Qt.AlignRight:
            if self._positionH == Qt.AlignTop:
                if self.settings:
                    self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                self.layout.addWidget(self.w_chart_H, 1, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 1, 2, 1, 1)
                self.layout.addWidget(self.w_image, 2, 1, 1, 1)
                self.layout.addWidget(self.w_chart_V, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 1)
                # self.w_chart_H.setMinimumHeight(self._minsizeH)
                # self.w_chart_H.setMaximumHeight(self._minsizeH)
                # self.w_chart_V.setMinimumWidth(self._minsizeV)
                # self.w_chart_V.setMaximumWidth(self._minsizeV)

                self.w_chart_H.setMinimumHeight(100)
                self.w_chart_V.setMinimumWidth(100)
            elif self._positionH == Qt.AlignBottom:
                if self.settings:
                    self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                self.layout.addWidget(self.w_image, 1, 1, 1, 1)
                self.layout.addWidget(self.w_chart_H, 2, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 2, 2, 1, 1)
                self.layout.addWidget(self.w_chart_V, 1, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 1)
                # self.w_chart_H.setMinimumHeight(self._minsizeH)
                # self.w_chart_H.setMaximumHeight(self._minsizeH)
                # self.w_chart_V.setMinimumWidth(self._minsizeV)
                # self.w_chart_V.setMaximumWidth(self._minsizeV)

                self.w_chart_H.setMinimumHeight(100)
                self.w_chart_V.setMinimumWidth(100)

    def ratio_changed(self):
        self.set_image_ratio(self._ratio.get_data())

    def apply_baseline_changed(self):
        self.set_data(self._image_ref)

    def set_image_ratio(self, ratio):

        if ratio:
            self.im_widget.get_plot_item().setAspectLocked(lock=True, ratio=1)
        else:
            self.im_widget.get_plot_item().setAspectLocked(lock=False)

        self.check_ratio()

    def check_ratio(self):

        if self.im_widget.get_plot_item().vb.state['aspectLocked']:
            if self.im_widget.width() < self.im_widget.height():
                self.im_widget.set_x_range(xmin=self.x_to_x_scale(0),
                                           xmax=self.x_to_x_scale(self._data["image"].shape[0]))
            else:
                self.im_widget.set_y_range(ymin=self.y_to_y_scale(0),
                                           ymax=self.y_to_y_scale(self._data["image"].shape[1]))
        else:
            self.im_widget.set_x_range(xmin=self.x_to_x_scale(0), xmax=self.x_to_x_scale(self._data["image"].shape[0]))
            self.im_widget.set_y_range(ymin=self.y_to_y_scale(0), ymax=self.y_to_y_scale(self._data["image"].shape[1]))

    def set_active(self, active=True):
        self._active = active

    def set_baseline(self, data):
        self._data_baseline = data
        self._label_baseline.setText(self._name)
        self.supdate_signal.emit()

    def push_baseline(self, e):
        self.set_baseline(self._image_ref)

    def get_chart_h(self):
        return self._chart_H

    def get_chart_v(self):
        return self._chart_V

    def set_data_fit(self, values):

        self.df_gauss["H"][0] = formatting.format_value_to_string(values["ampH"], type_format="sci",
                                                                  format_sci="{:.3e}")  # amp H
        self.df_gauss["H"][1] = formatting.format_value_to_string(values["sigmaH"], type_format="sci",
                                                                  format_sci="{:.3e}")  # sigma H
        self.df_gauss["H"][2] = formatting.format_value_to_string(values["muH"], type_format="sci",
                                                                  format_sci="{:.3e}")  # mu H
        self.df_gauss["H"][3] = formatting.format_value_to_string(values["offsetH"], type_format="sci",
                                                                  format_sci="{:.3e}")  # offset H
        self.df_gauss["V"][0] = formatting.format_value_to_string(values["ampV"], type_format="sci",
                                                                  format_sci="{:.3e}")  # amp V
        self.df_gauss["V"][1] = formatting.format_value_to_string(values["sigmaV"], type_format="sci",
                                                                  format_sci="{:.3e}")  # sigma V
        self.df_gauss["V"][2] = formatting.format_value_to_string(values["muV"], type_format="sci",
                                                                  format_sci="{:.3e}")  # mu V
        self.df_gauss["V"][3] = formatting.format_value_to_string(values["offsetV"], type_format="sci",
                                                                  format_sci="{:.3e}")  # offset V

        self.supdate_table.emit()


# EXAMPLE1
class Listener(simucomm.SimuCommListener):

    def __init__(self, parent):
        self._parent = parent

        self.simuDataObject = simucomm.SimuDataObject()

        self.change_data()

        self.simucomm = simucomm.SimuComm("",
                                          "",
                                          self.simuDataObject,
                                          listener=self,
                                          timer_period=1000)

    def start(self, cycle=""):
        self.simucomm.subscribe()

    def stop(self):
        self.simucomm.unsubscribe()

    def get(self):
        data = self.simucomm.get()

        self.handle_event(self.simucomm._device + "/" + self.simucomm._property, data)

    def handle_event(self, name, value):
        """
        Handle event (notification).
        """

        self._parent.handle_event(name, value)

    def change_data(self):
        """
        Data change function to observe the change of data
        """
        pass


class _Example1(QMainWindow, fesacommDA.FesaCommListener):
    update_signal = pyqtSignal(object)

    # mutex = threading.Lock()

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.gauss = []
        from random import random
        for i in range(0, 10):
            # size1 = 500
            # size2 = 500
            # sigma = 1. + random.random()
            # muu = 0
            # x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            # dst = np.sqrt(((x-1) - 1) ** 2 + (y-1) ** 2)
            # if i % 10 == 0:
            #     self.gauss.append(150 * np.exp(-((dst - muu) ** 2 / (0.2 * sigma ** 2))) + np.random.normal(50, 5,
            #                                                                                                 [size2,
            #                                                                                                  size1]) - 40)
            # else:
            #     self.gauss.append(150 * np.exp(-((dst - muu) ** 2 / (0.2 * sigma ** 2))) + np.random.normal(50, 5,
            #                                                                                                 [size2,
            #                                                                                                  size1]) - 40)

            size1 = 500
            size2 = 500
            sigma = 1.
            muu = 0.
            x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            dst = np.sqrt((x - i * 0.2) ** 2 + (y - i * .2) ** 2)

            gauss = np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))

            self.gauss.append(gauss)
            # for ii in range(0,size2):
            #     self.gauss[i][ii][0] = 10000
            #     self.gauss[i][ii][1] = 10000
            #     self.gauss[i][ii][2] = 10000
            #     self.gauss[i][ii][-1] = 10000
            #     self.gauss[i][ii][-2] = 10000
            #     self.gauss[i][ii][-3] = 10000
            #
            # for jj in range(0,size1):
            #     self.gauss[i][0][jj] = 10000
            #     self.gauss[i][1][jj] = 10000
            #     self.gauss[i][2][jj] = 10000
            #     self.gauss[i][-1][jj] = 10000
            #     self.gauss[i][-2][jj] = 10000
            #     self.gauss[i][-3][jj] = 10000

        self._ind = 0

        self.profile = ProfileWidget(self,
                                     limit_time=0,
                                     # unitx="s",
                                     # unity="mm",
                                     profile=True,
                                     positionH=Qt.AlignBottom,
                                     positionV=Qt.AlignTop,
                                     settings=True,
                                     show_info=True,
                                     ratio=False,
                                     tv=False,
                                     multi=False,
                                     mosaic=False,
                                     fit=True,
                                     led=False,
                                     header=True,
                                     unitx=None,
                                     unity=None,
                                     unityh=None,
                                     unityv=None,
                                     maxcount_mosaic=20,
                                     nwidth_mosaic=5,
                                     bascule_tab=True,
                                     selection=True,
                                     baseline=False,
                                     usertime=None,
                                     avg_sum=True
                                     )

        # self.profile.set_image_levels(0, 2000)

        data_widget_opts = {
            "auto": True,
            "show_table": False,
            "show_image": False,
            "show_chart": True,
            "show_singlechart": False,
            "show_scalar": False,
            "show_bitenum": False,
            "show_chartgl": False,
            "show_chart_grid": False,
            "show_chart_waterfall": False,
            "show_chartgl_3d": False,
            "show_chartgl_points": False,
            "show_chartgl_surface": False,
            "show_chartgl_lines": False,
            "chart_palette": "stroke",
            "history": True,
            "show_image_value_axis": True,
            "show_singlechart_value_axis": False,
            "show_singlechart_value_axis": False,
            "show_chart_value_axis": False,
            "size_max_table": 10000,
            "statistics": True,
            "fitting": False,
            "color_amplitude": False,
            "low_res_chart": False
        }

        self.data_widget = datawidget.DataWidget(self,
                                                 title=["TEST"],
                                                 name=["TEST"],
                                                 max=200,
                                                 **data_widget_opts)
        self._update_time = 0

        self._listener = Listener(self)
        self._listener.start()

        self.update_signal.connect(self.update)

        w = QWidget()
        layout = QGridLayout(w)
        layout.addWidget(self.profile, 0, 0)

        self.layout.addWidget(w)
        # self.layout.addWidget(self.data_widget)

        # self._headerWidget = headerwidget.HeaderWidget(self,
        #                                                layout=self.layout,
        #                                                datawidget=None,
        #                                                tick=True,
        #                                                pause=True
        #                                                )

        # self.layout.addWidget(self._headerWidget)

        # self.imv = pg.ImageItem()
        glw = pg.GraphicsLayoutWidget()
        # self.plot = glw.addPlot(row=0, col=0)
        self.plot = BasicPlotItem()
        self.imv = BasicImageItem(self.plot, name="AA")
        self.plot.addItem(self.imv)
        glw.addItem(self.plot, 0, 0)
        # self.layout.addWidget(glw)
        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

    def handle_event(self, name, value):

        # dt = value['_time'] - self._update_time
        t = formatting.current_milli_time()
        dt = t - self._update_time
        # print(dt,int(1000/dt))
        self.data_widget.set_data(dt, name="dt", name_parent="TEST")
        # print(dt)
        if dt > 0 and self._update_time != -1:
            self._ind = self._ind + 1
            if self._ind > 9:
                self._ind = 0
            self.profile.set_data(self.gauss[self._ind])
            if self._update_time == 0:
                pass
                # self.profile.change_scale(0.0025, 0.0025, 10, 5)
                self.profile.change_scale(xscale=4. / 500,
                                          xoffset=2,
                                          yscale=1.8 / 500,
                                          yoffset=0.39
                                          )
                # self.profile.get_chart_v().set_y_range(ymin=20, ymax=30)

            # self.update_signal.emit(self.gauss[self._ind])
            # self._update_time = value['_time']
            self._update_time = t

    @pyqtSlot(object)
    def update(self, value):
        self.imv.setImage(value)

    def write_fit_values_on_logger(self):
        print(self.profile._values)

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


# EXAMPLE2

class _Example2(QMainWindow, fesacommDA.FesaCommListener):
    update_signal = pyqtSignal(object, float)

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.profile = ProfileWidget(self,
                                     show_info=True,
                                     limit_time=20)
        self._update_time = 0
        # self._fesacomm = fesacomm.FesaComm("BTVDEV.DigiCam.CAM2",
        #                                    "LastImage",
        #                                    listener=self)

        self._fesacomm = fesacommDA.FesaComm("TST.865.LHCBSRT",
                                           "Image",
                                           listener=self)

        self.gauss = []
        from random import random
        for i in range(0, 10):
            size1 = 1500
            size2 = 2000
            sigma = 1. + random()
            muu = 0
            # x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            # dst = np.sqrt(x ** 2 + y ** 2)
            # self.gauss.append(np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2))))
            # self.gauss.append(np.random.normal(50, 5, [size1, size2]))
            self.gauss.append(pg.gaussianFilter(np.random.normal(size=(size1, size2)), (5, 5)))
        self._ind = 0

        self.update_signal.connect(self.update)

        # self.layout.addWidget(self.profile)

        self._headerWidget = headerwidget.HeaderWidget(self,
                                                       layout=self.layout,
                                                       datawidget=None,
                                                       tick=True,
                                                       pause=True)

        self.layout.addWidget(self._headerWidget)

        self.imv = pg.ImageItem()
        glw = pg.GraphicsLayoutWidget()
        self.plot = glw.addPlot(row=0, col=0)
        # self.plot = _PlotItem()
        # self.imv = _ImageItem(self.plot, name="AA")
        self.plot.addItem(self.imv)
        glw.addItem(self.plot, 0, 0)
        self.layout.addWidget(glw)

        self._fesacomm.subscribe("")
        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

    def handle_event(self, name, value):
        if self._usertime is not None:
            self._headerWidget.recv(self._usertime)
        else:
            self._headerWidget.recv()
        if value['_time'] - self._update_time > 20:
            # print(str(value['_time'] - self._update_time))

            # self.profile.set_data(value['image2D'])

            self._ind = self._ind + 1
            if self._ind > 9:
                self._ind = 0
            # self.update_signal.emit(self.gauss[self._ind])
            if value['image2D'].dtype == "int16":
                values = (value['image2D'] >> 8).astype(np.int8)
                time_image = float(value['_time'])
                self.update_signal.emit(values, time_image)

            self._update_time = value['_time']

    @pyqtSlot(object, float)
    def update(self, value):
        self.imv.setImage(value)

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


# EXAMPLE 3

class _Example3(QMainWindow, fesacommDA.FesaCommListener):

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.gauss = []
        from random import random
        for i in range(0, 10):
            size1 = 2200
            size2 = 1600
            sigma = 1. + random()
            muu = 0
            x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            dst = np.sqrt((x - 1) ** 2 + y ** 2)
            if i % 10 == 0:
                self.gauss.append(150 * np.exp(-((dst - muu) ** 2 / (0.2 * sigma ** 2))) + np.random.normal(50, 5,
                                                                                                            [size2,
                                                                                                             size1]) - 40)
            else:
                self.gauss.append(150 * np.exp(-((dst - muu) ** 2 / (0.2 * sigma ** 2))) + np.random.normal(50, 5,
                                                                                                            [size2,
                                                                                                             size1]) - 40)

        self._ind = 0

        self.profile = ProfileWidget(self,
                                     name="This is a test",
                                     show_info=True,
                                     # fit=True,
                                     limit_time=40,
                                     profile=True,
                                     multi=True,
                                     mosaic=True,
                                     tv=True,
                                     maxcount_mosaic=48,
                                     nwidth_mosaic=8,
                                     positionH=Qt.AlignBottom,
                                     positionV=Qt.AlignTop,
                                     usertime="MYTIME"
                                     )

        data_widget_opts = {
            "auto": True,
            "show_table": False,
            "show_image": False,
            "show_chart": True,
            "show_singlechart": False,
            "show_scalar": False,
            "show_bitenum": False,
            "show_chartgl": False,
            "show_chart_grid": False,
            "show_chart_waterfall": False,
            "show_chartgl_3d": False,
            "show_chartgl_points": False,
            "show_chartgl_surface": False,
            "show_chartgl_lines": False,
            "chart_palette": "stroke",
            "history": True,
            "show_image_value_axis": True,
            "show_singlechart_value_axis": False,
            "show_singlechart_value_axis": False,
            "show_chart_value_axis": False,
            "size_max_table": 10000,
            "statistics": True,
            "fitting": False,
            "color_amplitude": False,
            "low_res_chart": False
        }

        self.data_widget = datawidget.DataWidget(None,
                                                 title=["TEST"],
                                                 name=["TEST"],
                                                 max=200,
                                                 **data_widget_opts)
        self._update_time = 0
        # self._fesacomm = fesacommDA.FesaComm("BTVDC_865_BTVDIGITAL_1",
        # self._fesacomm = fesacommDA.FesaComm("BTVDEV.DigiCam.CAM2",
        # self._fesacomm = fesacommDA.FesaComm("DI.BTV6068.DigiCam",
        # self._fesacomm = fesacommDA.FesaComm("BGC.DigiCam",
        #                                    "LastImage",

        print("subscribe")
        self._fesacomm = fesacommDA.FesaComm(
            # "BISWRefSB", "Acquisition",
            "TST.865.LHCBSRT",
            "Image",
            # "FTA.BTV.9064.DigiCam", "LastImage",

            # noPyConversion=True,
            min_time_between_event=10,
            listener=self)

        self.time = 0
        self.layout.addWidget(self.profile)
        # self.layout.addWidget(self.data_widget)

        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

        # from threading import Timer
        # t = Timer(1., lambda: self._fesacomm.subscribe(""))
        # t.start()
        self._fesacomm.subscribe()

    def handle_event(self, name, value):
        t = formatting.current_milli_time()
        dt = t - self._update_time

        print("recv")
        val = {}
        val["ampH"] = 1. + dt
        val["muH"] = 2. + dt
        val["sigmaH"] = 3. + dt
        val["offsetH"] = 4. + dt
        val["emitH"] = 5. + dt
        val["ampV"] = 5. + dt
        val["muV"] = 6. + dt
        val["sigmaV"] = 7. + dt
        val["offsetV"] = 4. + dt
        val["emitV"] = 8. + dt

        if len(value["projPositionSet1"]) == 0 and len(value["projPositionSet2"]) == 0:
            return

        self.profile.set_gauss_values(val)

        if dt > 20 and self._update_time != -1:

            self.data_widget.set_data(dt, name="dt", name_parent="TEST")
            self._ind = self._ind + 1
            if self._ind > 9:
                self._ind = 0
            w = len(value["projPositionSet1"])
            h = len(value["projPositionSet2"])

            data = value['imageSet'].reshape(h, w).T

            if value["projPositionSet1"][0] > value["projPositionSet1"][1]:
                data = np.flip(data, axis=0)

            if value["projPositionSet2"][0] > value["projPositionSet2"][1]:
                data = np.flip(data, axis=1)

            time_image = float(value['_time'])

            self.profile.set_data(data, time=time_image / 1000)

            if self._update_time == 0:
                xoffset = -min(value["projPositionSet1"][0], value["projPositionSet1"][-1])
                yoffset = -min(value["projPositionSet2"][0], value["projPositionSet2"][-1])

                xscale = abs(value["projPositionSet1"][0] - value["projPositionSet1"][1])
                yscale = abs(value["projPositionSet2"][0] - value["projPositionSet2"][1])

                print("")
                print(value["projPositionSet1"][0], value["projPositionSet1"][-1], value["projPositionSet2"][0],
                      value["projPositionSet2"][-1])
                print(xoffset, yoffset, xscale, yscale)

                self.profile.change_scale(xscale, yscale, xoffset, yoffset)

        self._update_time = t

    def handle_event2(self, name, value):

        # print(name,value)
        # dt = value['_time'] - self._update_time

        t = formatting.current_milli_time()

        dt = t - self._update_time
        # print(dt,int(1000/dt))

        self.data_widget.set_data(dt, name="dt", name_parent="TEST")

        if dt > 0:
            # print(dt)

            # t=formatting.current_milli_time()

            # if (float(value['imageTimeStamp']) <= self.time):
            #     print("reject")
            #     return

            # print((value['imageTimeStamp']-self.time)/1000000)

            # self.profile.set_data2(value['image2D'],time=float(value['imageTimeStamp']))

            # print(value.getValue().getArray2D("image2D").getShortArray2D())
            # formatting.type_value(value.getValue().getArray2D("image2D"),"short")
            # self.profile.set_data(value.getValue().getArray2D("image2D").getIntArray2D(), time=0)#float(value['imageTimeStamp']))

            # self.profile.set_data(np.float_(value['image2D']), float(value['imageTimeStamp']))
            #
            # if self._update_time == 0:
            #     self.profile.change_scale(120./value['image2D'].shape[1], 120./value['image2D'].shape[0], -60, -60)

            # if self.profile._mutex:
            #     print("Skipping image...", (float(value['imageTimeStamp']-self.time)/1000000))
            #     self.time = float(value['imageTimeStamp'])
            #     return;

            # displayThread = threading.Thread(target=self.profile.set_data,
            #                                  args=(value['image2D'], float(value['imageTimeStamp'])))
            #
            # self.time = float(value['imageTimeStamp'])

            # print("#: " + str(formatting.current_milli_time() - t))

            # self._update_time = value['_time']

            self._update_time = t

            # displayThread.start()
            # displayThread.join()

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


class _Example(fesacomm.FesaCommListener):
    _update_time = 0

    def subscribe(self):
        fesa_comm = fesacomm.FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)
        fesa_comm.subscribe("")
        sleep(20000)

    def handle_event(self, name, value):
        # val = int(value['_time'] - self._update_time)
        # if val != 82:
        #     print("# ", int(value['_time'] - self._update_time))
        # self._update_time = value['_time']
        print("# ", (value['_time'] - self._update_time))

        self._update_time = value['_time']


if __name__ == "__main__":
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    # font_text_user = app.font()
    # font_text_user.setPointSize(15)
    # app.setFont(font_text_user)

    qss = """
                QMenuBar::item {
                    spacing: 2px;
                    padding: 2px 10px;
                    background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
                }
                QMenuBar::item:selected {
                    background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }
                QMenuBar::item:pressed {
                    background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }
                QScrollArea {
                    background-color: transparent;
                }
                QRadioButton::checked {
                    color: """ + Colors.STR_COLOR_L2BLUE + """;
                }
                QRadioButton::indicator {
                    border-radius: 6px;
                }
                QRadioButton::indicator::unchecked{
                    border-radius: 6px;
                    border:2px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;
                }

                QRadioButton::indicator::checked{
                    border: 2px solid;
                    border-color: """ + Colors.STR_COLOR_L2BLUE + """;
                    border-radius: 6px;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;
                }
                QCheckBox {
                    color: """ + Colors.STR_COLOR_WHITE + """;
                }
                QCheckBox::indicator {
                    max-width:14;
                    max-height:14;
                    border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;
                }
                QCheckBox::indicator:checked {
                    max-width:14;
                    max-height:14;
                    border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;
                }
                QLineEdit {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    text-align:left;
                    padding-left:2px;
                    padding-top:2px;
                    padding-bottom:2px;
                    border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }
                QComboBox {
                    border:none;
                    background-color:""" + Colors.STR_COLOR_L1BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }
                QListView {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }
            """
    app.setStyleSheet(qss)

    pg.setConfigOptions(antialias=True)

    ex = _Example3()
    ex.show()
    ex.dark_()
    # ex.light_()

    # ex = _Example()
    # ex.subscribe()

    sys.exit(app.exec_())
