import sys
from time import time, sleep
import numpy as np
import pyqtgraph as pg
import os
from typing import List
from traceback import print_exception

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QThread

from PyQt5.QtGui import QPixmap, QImage

from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QGridLayout, QMainWindow, QVBoxLayout, QScrollArea, QLabel

from expert_gui_core.tools import formatting
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm.data import DataObject


@staticmethod
def get_size_array(arr):
    try:
        size = arr.shape[0] * arr.shape[1]
        return [size, arr.shape[0], arr.shape[1], 1]
    except:
        try:
            size = len(arr) * len(arr[0])
            return [size, len(arr[0]), len(arr), 2]
        except:
            return [-1, 0, 0]


class _DialogImage(QWidget):

    def __init__(self, parent=None, title=""):
        super(QWidget, self).__init__(parent)

        self.setWindowTitle(title)

        from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
        self.profile_widget = profilewidget.ProfileWidget(self,
                                                          name=title,
                                                          show_info=True,
                                                          settings=True,
                                                          fit=True,
                                                          ratio=False,
                                                          baseline=True,
                                                          selection=False,
                                                          positionH=Qt.AlignBottom,
                                                          positionV=Qt.AlignTop,
                                                          usertime=title
                                                          )
        self.profile_widget.enum_fit_widget.set_data(1)

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(2)

        self.layout.addWidget(self.profile_widget, 0, 0)

        self.resize(900, 900)

    def set_data(self, data, time_image=-1):
        if time_image != -1:
            title = formatting.format_value_to_string(time_image, type_format="date")
            self.setWindowTitle(title)
            self.profile_widget.set_name(title)
            self.profile_widget.set_usertime(title)
        self.profile_widget.set_data(data)

    def closeEvent(self, event):
        self.hide()

    def dark_(self):
        self.profile_widget.dark_()

    def light_(self):
        self.profile_widget.light_()


class _VignetteUpdate:
    def __init__(self, vignette, grab=False, clear_previous=True, clear_selected=False):
        self._grab = grab
        self._clear_previous = clear_previous
        self._vignette = vignette
        self._clear_selected = clear_selected

    def clear_selected(self):
        return self._clear_selected

    def vignette(self):
        return self._vignette

    def grab(self):
        return self._grab

    def clear_previous(self):
        return self._clear_previous


class _Vignette(QWidget):
    signal_clicked = pyqtSignal(_VignetteUpdate)
    signal_mouse_enter = pyqtSignal()
    signal_mouse_leave = pyqtSignal()
    signal_selected_changed = pyqtSignal()

    def __init__(self, parent, object_id: str, pixmap: QPixmap, max_width=250, time_image=-1):
        super(QWidget, self).__init__(parent)

        self._parent = parent
        self._width = 0
        self._object_id = object_id
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        self._pixmap_label = QLabel()
        self.layout().addWidget(self._pixmap_label)
        self._pixmap_scaled = pixmap.scaledToWidth(max_width)

        self._pixmap = pixmap
        self._pixmap_label.setPixmap(self._pixmap_scaled)
        self._selection_active = False
        self._pixmap_label_id = None

        if time_image != -1:
            self._pixmap_label_id = QLabel()
            font_text_user = self._pixmap_label_id.font()
            font_text_user.setPointSize(int(max_width / 16))
            self._pixmap_label_id.setFont(font_text_user)
            label_image = formatting.format_value_to_string(time_image, type_format="date")[:-4]
            self._pixmap_label_id.setText(label_image)
            self.layout().addWidget(self._pixmap_label_id)

        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()

    def get_pixmap(self):
        return self._pixmap

    def change_width(self, width):
        if self._width == width:
            return
        self._pixmap_scaled = self._pixmap.scaledToWidth(width)
        self._pixmap_label.setPixmap(self._pixmap_scaled)
        self._width = width
        if self._pixmap_label_id is not None:
            font_text_user = self._pixmap_label_id.font()
            font_text_user.setPointSize(int(self._width / 16))
            self._pixmap_label_id.setFont(font_text_user)

    def mouseReleaseEvent(self, e):
        grab = bool((QApplication.keyboardModifiers() & Qt.ShiftModifier))
        clear_previous = not grab and e.button() != Qt.MiddleButton

        self.signal_clicked.emit(
            _VignetteUpdate(self, clear_selected=self._selection_active, clear_previous=clear_previous, grab=grab)
        )

    def keyPressEvent(self, event):
        if event.key() == Qt.ShiftModifier:
            pass

    def mousePressEvent(self, e):
        self._selection_active = True

    def mouseMoveEvent(self, e):
        if self._selection_active:
            pass

    def set_active_styles(self):
        self.setStyleSheet('border: 2px solid red;')

    def set_inactive_styles(self):
        self.setStyleSheet('border: none;')

    def object_id(self):
        return self._object_id

    def light_(self):
        if self._pixmap_label_id is not None:
            self._pixmap_label_id.setStyleSheet("color:" + Colors.STR_COLOR_BLACK)

    def dark_(self):
        if self._pixmap_label_id is not None:
            self._pixmap_label_id.setStyleSheet("color:" + Colors.STR_COLOR_WHITE)


class _Vignettes(QWidget):
    selection_changed = pyqtSignal(object)
    update_signal = pyqtSignal()

    def __init__(self, parent, max_width=250, nwidth=10):
        super(QWidget, self).__init__()

        self._parent = parent

        self._nwidth = nwidth
        self._active_vignettes = []
        self._vignettes = {}
        self._vignettes_ordered = []
        self._max_vignette_width = max_width

        self.setFocusPolicy(Qt.StrongFocus)
        self.setLayout(QGridLayout())
        self.layout().setSpacing(4)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.update_signal.connect(self.update_vignettes)

    def change_nwidth(self, nwidth):
        self._nwidth = nwidth

    def clear_data(self):
        self._vignettes = {}
        self._vignettes_ordered = []
        self._active_vignettes = []

    def resize_widgets(self):
        self.update_signal.emit()

    @pyqtSlot()
    def update_vignettes(self):
        for vignette in self._vignettes.values():
            vignette.change_width(int((self._parent.width() - 25 - 2 * self._nwidth) / self._nwidth))

    def add(self, object_id: str, rgba_images, time_image=-1):
        if not isinstance(rgba_images, List):
            rgba_images = [rgba_images]
        for image in rgba_images:
            pixmap = QPixmap(QImage(image, len(rgba_images[0][0]), len(rgba_images[0]), QImage.Format_RGBA8888))
            vignette = _Vignette(self, object_id, pixmap, max_width=self._max_vignette_width, time_image=time_image)
            vignette.signal_clicked.connect(self.update_selection)
            self._vignettes[object_id] = vignette
            self._vignettes_ordered.append(object_id)
        self.redo_layout()

    def redo_layout(self):
        ind = 0
        for object_id in self._vignettes_ordered:
            vignette = self._vignettes[object_id]
            self.layout().removeWidget(vignette)
            i = int(ind / self._parent._nwidth)
            j = int(ind % self._parent._nwidth)
            self.layout().addWidget(vignette, i, j)
            self.layout().setRowStretch(i, 0)
            self.layout().setColumnStretch(j, 0)
            ind = ind + 1
        self.layout().setRowStretch(i + 1, 1)
        self.layout().setColumnStretch(self._parent._nwidth + 1, 1)
        self.resize_widgets()

    def dark_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLACK + ";")
        for object_id in self._vignettes_ordered:
            self._vignettes[object_id].dark_()

    def light_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")
        for object_id in self._vignettes_ordered:
            self._vignettes[object_id].light_()

    def update_selection(self, update: _VignetteUpdate):
        if update.grab() and len(self._active_vignettes) == 1:
            try:
                index_1 = self._vignettes_ordered.index(self._active_vignettes[0].object_id())
                index_2 = index_1
            except:
                self.set_none_active()
                return
            start = index_1 if index_1 < index_2 else index_2
            end = index_2 if index_2 > index_1 else index_1
            new_active_vignettes = []
            for object_id in self._vignettes_ordered[start:end + 1]:
                new_active_vignettes.append(self._vignettes[object_id])
            self.set_active_vignettes(new_active_vignettes)
        elif update.clear_previous():
            self.set_active_vignettes([update.vignette()])
        elif update.clear_selected():
            if update.vignette() in self._active_vignettes:
                self._active_vignettes.remove(update.vignette())
            else:
                self._active_vignettes.append(update.vignette())
            self.set_active_vignettes(self._active_vignettes)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Up or key == Qt.Key_Left or key == Qt.Key_Backtab:
            self.set_previous_vignette_active()
        elif key == Qt.Key_Down or key == Qt.Key_Right or key == Qt.Key_Tab:
            self.set_next_vignette_active()
        elif key == Qt.Key_Escape:
            self.set_none_active()

    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            self.set_previous_vignette_active()
        elif event.angleDelta().y() < 0:
            self.set_next_vignette_active()

    def set_previous_vignette_active(self):
        if len(self._active_vignettes) == 1:
            try:
                index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) - 1
                if index < 0:
                    index = len(self._vignettes_ordered) - 1
                self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])
            except:
                pass

    def set_next_vignette_active(self):
        if len(self._active_vignettes) == 1:
            try:
                index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) + 1
                if index > len(self._vignettes_ordered) - 1:
                    index = 0
                self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])
            except:
                pass

    def set_none_active(self):
        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()
        self._active_vignettes = []
        self.selection_changed.emit(self._vignettes_ordered)

    def get_object_ids(self, vignettes):
        object_ids = []
        for object_id, vignette in self._vignettes.items():
            if vignette in vignettes:
                object_ids.append(object_id)
        return object_ids

    def set_active_vignettes(self, vignettes):
        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()
        for vignette in vignettes:
            self._vignettes[vignette.object_id()].set_active_styles()
        self._active_vignettes = vignettes
        self.selection_changed.emit(self.get_object_ids(vignettes))


class MosaicWidget(QWidget):
    update_signal = pyqtSignal(str, bool, tuple, dict)

    def __init__(self, parent, max_count=10, nwidth=10):
        super(QWidget, self).__init__(parent)

        self._count = 0
        self._nwidth = 0

        self._update_time = 0
        self._min_refresh_time = 0
        self._mutex = False
        self._filter = None

        self._cmap = None
        self._lut = None

        self._rgba_images = {}
        self._image_items = {}
        self._image_nparrays = []

        self._np_images_keys = []
        self._np_images_keys_x = []
        self._np_images = {}

        self._max_count = max_count - 1
        self._nwidth = nwidth
        max_width = int((self.width()) / self._nwidth)

        self._vignettes = _Vignettes(self, max_width=max_width, nwidth=self._nwidth)

        self._dialog = _DialogImage(title="Image")

        self.init_layout()

        self._data = DataObject(history=True, max=max_count)
        self._data_real = DataObject(history=True, max=max_count)

        self._cmap = pg.colormap.getFromMatplotlib('jet')
        self._lut = self._cmap.getLookupTable(alpha=True)

        scrollArea = QScrollArea()
        scrollArea.setWidgetResizable(True)
        scrollArea.setWidget(self._vignettes)
        self.layout().addWidget(scrollArea)

        self.update_signal.connect(self.update)

        self._vignettes.selection_changed.connect(self.handle_selection_changed)

        self.dark_()

    def remove_last(self):
        self._vignettes.clear_data()

    def change_nwidth(self, nwidth):
        self._nwidth = nwidth
        self._vignettes._max_vignette_width = int((self.width()) / self._nwidth)
        self._vignettes.change_nwidth(nwidth)
        self._vignettes.resize_widgets()

    def resizeEvent(self, event):
        self._vignettes.resize_widgets()

    def set_data(self, data, time_image=-1, update=True, name="default", *args, **kwargs):
        self._rgba_images = {}
        self._image_items = {}
        self._image_nparrays = []

        si = get_size_array(data)

        self._data.add(data, datax=time_image)

        if update and self._mutex == False:
            self.update_signal.emit(name, update, args, kwargs)

    def np_to_rgba(self, np_image):
        levels = [np_image.min(), np_image.max()]
        lut = self._lut
        return pg.makeRGBA(np_image, levels=levels, lut=lut)[0]

    def init_layout(self):
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    @pyqtSlot(str, bool, tuple, dict)
    def update(self, name='default', update=True, args=None, kwargs=None):
        """ Updates the component, with the current contents of the data object.
            The data object contains all the images to be rendered.

            We don't know if we need the last 3, the last one, or if all the images are already drawn.
        """
        self._mutex = True
        try:
            key_image = None
            raw_image = self._data.get_last(name)
            raw_image_x = self._data.get_lastx(name)
            try:
                object_id = self._count
                self._count = self._count + 1
                if self._count > 10000000000:
                    self._count = 0
                if len(self._np_images) > self._max_count:
                    key_image = self._np_images_keys[0]
                    try:
                        self._vignettes._vignettes_ordered.pop(0)
                        self._np_images_keys.pop(0)
                        self._np_images_keys_x.pop(0)
                    except:
                        pass
                if object_id not in self._np_images:
                    np_image = np.array(raw_image)
                    total_size = np_image.shape[0] * np_image.shape[1]
                    if total_size > 100000:
                        mod = int(total_size / 50000)
                        np_image_vignette = np_image[::mod, ::mod]
                    else:
                        np_image_vignette = np.array(raw_image)
                    if key_image is not None:
                        try:
                            vignette = self._vignettes._vignettes.pop(key_image)
                            self._vignettes.layout().removeWidget(vignette)
                        except:
                            pass
                    self._np_images_keys.append(object_id)
                    self._np_images_keys_x.append(raw_image_x)
                    self._np_images[object_id] = np_image
                    self._vignettes.add(object_id, self.np_to_rgba(np_image_vignette), time_image=raw_image_x)
            except Exception as e:
                pass
        except Exception as ex:
            print_exception(type(ex), ex, ex.__traceback__)
        self._mutex = False

    def handle_selection_changed(self, object_ids):
        if len(object_ids) == 1:
            try:
                index_time_image = self._np_images_keys.index(object_ids[0])
                return self.show_imagewidget(self._np_images[object_ids[0]],
                                             time_image=self._np_images_keys_x[index_time_image])
            except:
                return None

    def show_imagewidget(self, np_image, time_image=-1):
        np_image = np.rot90(np_image, -1)
        self._dialog.show()
        self._dialog.set_data(np_image,
                              time_image=time_image)

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + "; }"
                                                                                                           "QSplitter::handle { background-color: " + Colors.STR_COLOR_DWHITE + "; }"
        )
        self._vignettes.light_()
        self._dialog.light_()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_LIGHT1GRAY + ";color:" + Colors.STR_COLOR_WHITE + "; }"
                                                                                                                "QSplitter::handle { background-color: " + Colors.STR_COLOR_L3BLACK + "; }")
        self._vignettes.dark_()
        self._dialog.dark_()

    def data_object(self):
        return self._data

    def images(self, name="default"):
        return self._data.get(name)

    def get_image_arrays(self, images):
        np_arrays = []
        for image in images:
            if isinstance(image, str) and os.path.isfile(image):
                np_arrays.append(self.file_to_image_array(image))
            elif isinstance(image, np.ndarray):
                if image.shape[2] == 3:
                    image = np.insert(image, 3, 255, axis=2)
                np_arrays.append(image)
            else:
                raise Exception("Invalid image format supplied to MosaicWidget.")
        return np_arrays


class _UpdateThread(QThread):
    new_image = pyqtSignal(np.ndarray, float)

    @staticmethod
    def gaussian(sigma=1., xoffset=0):
        x_grid = 900
        y_grid = 500
        muu = 0
        x, y = np.meshgrid(np.linspace(-5, 5, x_grid), np.linspace(-5, 5, y_grid))
        dst = np.sqrt((x - xoffset) ** 2 + y ** 2)
        return np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))

    def run(self):
        count = 0
        while count < 200:
            sleep(0.1)
            count += 1
            self.new_image.emit(_UpdateThread.gaussian(xoffset=(-5 + 0.1 * count)), float(time()))


class _Example(QMainWindow):

    def __init__(self):
        super().__init__()

        w = QWidget()
        w.setLayout(QVBoxLayout())

        self._mosaic_widget = MosaicWidget(None, max_count=50, nwidth=3)
        # self._mosaic_widget.dark_()
        self._mosaic_widget.light_()

        # arr = [4, 5, 6, 7, 8, 1, 2, 3, 0, 0]
        # narr = self._mosaic_widget.back_rotation(arr, 3, 8)
        # print(narr)

        updater = _UpdateThread()
        updater.new_image.connect(self.add_image)
        updater.start()

        w.layout().addWidget(self._mosaic_widget)

        self.setCentralWidget(w)
        self.show()

    @pyqtSlot(np.ndarray, float)
    def add_image(self, raw_image, time_image=None):
        self._mosaic_widget.set_data(raw_image, time_image=time_image)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.resize(400, 400)
    ex.show()
    sys.exit(app.exec_())
