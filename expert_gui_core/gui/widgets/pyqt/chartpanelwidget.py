import sys
from random import randint, uniform
from datetime import datetime
from collections import deque

import pyqtgraph as pg
from PyQt5.QtCore import Qt, QTimer, pyqtSlot
from PyQt5.QtWidgets import QApplication, QCheckBox, QHBoxLayout, QMainWindow, QScrollArea, QVBoxLayout, QWidget

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt import chartwidget, checkablecombowidget


class ChartPanel(QWidget):
    """Contains set of Chart Widgets that display Acquisition data.

    Arguments:
        parent (BIApplicationFrame): MainWidget Instance.
        charts (Dict): Data map with ChartWidgets and Plot related attributes.
        time_in_charts (Boolean): Use timestamps as datax. Defaults to False.
        show_grid (Boolean): Use grid in chart. Defaults to True.

    Attributes:
        toolbar (QWidget): Widget with chart related utils.
        charts  (Dict): Dictionary containing Chart Widget objects and metadata.

    Methods:
        set_data - loops over chart widgets calling set_data method.
    """

    def __init__(self, parent, charts, time_in_charts=False, show_grid=True):
        super().__init__(parent)

        self._parent_widget = parent
        self.charts = charts
        self._time_in_charts = time_in_charts
        self._show_grid = show_grid

        self.toolbar = self._create_toolbar()

        sa = QScrollArea()
        sa.setWidgetResizable(True)
        sa.setStyleSheet("border: 0px;")
        w = QWidget()
        self._layout_panel = QVBoxLayout(w)
        sa.setWidget(w)

        self._create_charts()

        self._layout = QVBoxLayout(self)
        self._layout.setContentsMargins(10, 10, 10, 10)
        self._layout.setSpacing(0)

        self._layout.addWidget(self.toolbar)
        self._layout.addWidget(sa)

        self.setAttribute(Qt.WA_StyledBackground, True)

    def _create_toolbar(self):
        """Add checkbox called 'Use Timestamp' and CheckableComboBox with default visibility state."""
        toolbar_layout = QHBoxLayout(self)
        toolbar_items = QWidget(self)
        toolbar_items.setLayout(toolbar_layout)

        self.dynamic_scaling_checkbox = QCheckBox("Dynamic Scaling", self)
        self.dynamic_scaling_checkbox.setChecked(True)
        self.dynamic_scaling_checkbox.stateChanged.connect(self._on_dyn_scaling_changed)

        self.chart_selector = checkablecombowidget.CheckableComboBox(title="Select Charts", parent=self)
        for chart_name, chart_data in self.charts.items():
            self.chart_selector.addItem(chart_name)
            item = self.chart_selector.model().item(self.chart_selector.count() - 1)

            # Set initial checkbox state based on default_visibility
            if chart_data["default_visibility"]:
                item.setCheckState(Qt.Checked)
            else:
                item.setCheckState(Qt.Unchecked)

        self.chart_selector.itemCheckedSignal.connect(self.update_chart_visibility)

        toolbar_layout.addWidget(self.dynamic_scaling_checkbox)
        toolbar_layout.addWidget(self.chart_selector)

        return toolbar_items

    @pyqtSlot(int)
    def _on_dyn_scaling_changed(self, state):
        chart_name = list(self.charts.keys())[0]
        widget = self.charts[chart_name]["widget"]
        if state == Qt.Checked:
            widget.set_dynamic_scaling(dynamic_scaling=True)
        else:
            widget.set_dynamic_scaling(dynamic_scaling=False)

    @pyqtSlot(object)
    def mouse_moved(self, evt):
        """Handle mouse movement over the chart.

        Set the x position of the cross-hairs in all charts to the provided x-coordinate.
        """
        pos = evt[0] if isinstance(evt, tuple) and len(evt) > 0 else None

        if pos is not None:
            for chart_name, chart in self.charts.items():
                plot_item = chart["widget"].get_plot()
                vb = plot_item.vb  # View box of the plot item

                mouse_point = vb.mapSceneToView(pos)  # Map scene position to data coordinates
                x = mouse_point.x()

                for item in plot_item.items:
                    if isinstance(item, pg.InfiniteLine) and item.angle == 90:  # Find the vertical crosshair
                        item.setPos(x)  # Set the new x position

    @pyqtSlot(str, bool)
    def update_chart_visibility(self, chart_name, checked):
        """Update the visibility of chart widgets based on the CheckableComboBox state."""
        if chart_name in self.charts:
            widget = self.charts[chart_name]["widget"]
            if checked:
                self._layout_panel.addWidget(widget)
                widget.setVisible(True)
            else:
                self._layout_panel.removeWidget(widget)
                widget.setVisible(False)

    def _create_charts(self):
        """Create and add ChartWidget objects to charts map and panel layout based on default visibility."""
        self.proxies = []  # Store proxies to avoid overwriting

        scale = True  # dynamic scale first

        for chart_name, chart_data in self.charts.items():
            chart_widget = chartwidget.ChartWidget(self)
            chart_widget.add_plot(title=chart_data["title"], time_in_chart=self._time_in_charts, show_grid=self._show_grid, dynamic_scaling=scale)
            chart_data["widget"] = chart_widget
            scale = False
            chart_widget.setMinimumHeight(220)

            # Add chart to the layout if default_visibility is True
            if chart_data["default_visibility"]:
                self._layout_panel.addWidget(chart_widget)
                chart_widget.setVisible(True)
            else:
                chart_widget.setVisible(False)

            plot_item = chart_widget.get_plot()
            vb = plot_item.getViewBox()

            # Initialize signal proxy for the plot to track mouse movement
            proxy = pg.SignalProxy(plot_item.scene().sigMouseMoved, rateLimit=60, slot=self.mouse_moved)
            self.proxies.append(proxy)
            # Connect each chart's sigXRangeChanged signal to the update method for synchronization
            vb.sigXRangeChanged.connect(self._update_x_range)

    @pyqtSlot(object)
    def _update_x_range(self, vb):
        """Update the x-range for all charts when one changes."""
        x_range = vb.viewRange()[0]

        for chart_data in self.charts.values():
            chart_vb = chart_data["widget"].get_plot().getViewBox()

            if chart_vb != vb:
                chart_vb.setXRange(x_range[0], x_range[1], padding=0)

    def set_data(self, chart_data_map=None):
        """
        Updates data on all chart elements.

        The method uses default pen if no specific color is provided.

        Args:
            chart_data_map (Dict[str, Dict[str, Any]]):
                A dictionary containing chart names as keys and dictionaries of BPM data as values.
                The BPM data dictionary contains:
                    - 'data': The actual chart data for the BPM.
                    - 'color': A tuple representing the RGB color assigned to this BPM.

        Example:
            chart_data_map = {
                "Sigma": {
                    "BPM1": {"data": [1, 2, 3], "datax": [4,5,6], "color": (255, 0, 0)},
                    "BPM2": {"data": [4, 5, 6], "datax": [4,5,6], "color": (0, 255, 0)},
                },
                "Horizontal": {
                    "BPM1": {"data": [7, 8, 9], "datax": [4,5,6], "color": (255, 0, 0)},
                    "BPM2": {"data": [4, 5, 6], "datax": [4,5,6], "color": (0, 255, 0)},
                },
            }

        This method updates each chart with the provided data and applies the corresponding color for each BPM line.
        """
        if chart_data_map is None:
            chart_data_map = {}
        for chart_name, chart_data in chart_data_map.items():
            for data_name, data_value in chart_data.items():
                data = data_value.get("data")
                datax = data_value.get("datax", None)
                pen = data_value.get("pen", self.charts[chart_name]["data"].get(data_name, {}).get("pen"))
                self.charts[chart_name]["widget"].set_data(
                    data=data,
                    datax=datax,
                    update=True,
                    name=data_name,
                    pen=pen,
                )

    def dark_(self):
        """Set dark theme."""
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        for chart in self.charts.values():
            chart["widget"].dark_()
        self.chart_selector.dark_()

    def light_(self):
        """Set light theme."""
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        for chart in self.charts.values():
            chart["widget"].light_()
        self.chart_selector.light_()


class ExampleWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Chart Panel Example")

        # Define charts with configuration and data buffers
        self.charts = {
            "Chart 1": {
                "title": "First Chart",
                "default_visibility": True,
                "data": {
                    "BPM1": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                    "BPM2": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                },
            },
            "Chart 2": {
                "title": "Second Chart",
                "default_visibility": True,
                "data": {
                    "BPM1": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                    "BPM2": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                },
            },
            "Chart 3": {
                "title": "Third Chart",
                "default_visibility": False,
                "data": {
                    "BPM1": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                    "BPM2": {
                        "data": deque([], maxlen=100),
                        "datax": deque([], maxlen=100),
                        "pen": pg.mkPen({"color": (randint(0, 255), randint(0, 255), randint(0, 255)), "width": 1.0}),
                    },
                },
            },
        }

        self.chart_panel = ChartPanel(self, self.charts, "Device 1")
        self.setCentralWidget(self.chart_panel)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_chart_data)
        self.timer.start(1000)  # Update every 1000 ms (1 second)

    def update_chart_data(self):
        """Generates new data and updates the charts while keeping the buffers for data and timestamps."""
        chart_data_map = {}
        current_time = int(datetime.now().timestamp())  # Get the current timestamp

        for chart_name, chart_config in self.charts.items():
            chart_data_map[chart_name] = {}

            for bpm_name, bpm_data in chart_config["data"].items():
                # Append new data to buffers
                bpm_data["data"].append(uniform(0, 10))
                bpm_data["datax"].append(current_time)

                # Populate chart_data_map with the updated data and pen
                chart_data_map[chart_name][bpm_name] = {
                    "data": list(bpm_data["data"]),
                    "datax": list(bpm_data["datax"]),
                }

        # Update the chart panel with the new data
        self.chart_panel.set_data(chart_data_map)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = ExampleWindow()
    main_window.setGeometry(100, 100, 800, 600)
    main_window.show()
    sys.exit(app.exec_())
