import sys
import numpy as np
import pyqtgraph as pg
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QTimer

from PyQt5.QtGui import QPalette

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QMainWindow

from expert_gui_core.comm import fesacomm
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.basicplotitem import BasicPlotItem


class ChartWidget(QWidget):
    """
    Low-level chart widget displaying multi-arrays

    :param parent: Parent object.
    :type param: object
    :param name:  List of charts inside this chart widget (default="default")
    :type name: list, optional
    """

    def __init__(self, parent, name=["default"]):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        # names
        self._names = name

        # parent
        self._parent = parent

        # layout

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # chart

        self._chart_layout_widget = {}
        max_col_show = 3
        if len(name) == 4:
            max_col_show = 2
        if len(name) == 2:
            max_col_show = 1
        elif len(name) >= 16:
            max_col_show = 4
        for i in range(len(name)):
            self._chart_layout_widget[name[i]] = _ChartLayoutWidget(self)
            self.layout.addWidget(self._chart_layout_widget[name[i]], int(i / max_col_show), int(i % max_col_show))

    def handle_height(self, nrow_shown = 1):
        for i in range(len(self._names)):
            self._chart_layout_widget[self._names[i]].setMinimumHeight(nrow_shown * 100)

    def clear_data(self):
        """
        Clear data.
        """
        for i in range(len(self._names)):
            self._chart_layout_widget[self._names[i]].clear_data()

    def show_as_line(self, name_parent="default", name_pi="default", name="default", all=True):
        """
        Show plot as line.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        :param all: Applied to all (default=True).
        :type all: bool, optional
        """
        self._chart_layout_widget[name_parent].show_as_line(name=name, name_pi=name_pi, all=all)

    def show_as_area(self, name_parent="default", name_pi="default", name="default", all=True):
        """
        Show plot as area.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        :param all: Applied to all (default=True).
        :type all: bool, optional
        """
        self._chart_layout_widget[name_parent].show_as_area(name=name, name_pi=name_pi, all=all)

    def show_as_point(self, name_parent="default", name_pi="default", name="default", all=True):
        """
        Show plot as point.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        :param all: Applied to all (default=True).
        :type all: bool, optional
        """
        self._chart_layout_widget[name_parent].show_as_point(name=name, name_pi=name_pi, all=all)

    def show_as_waterfall(self, name_parent="default", name_pi="default", name="default"):
        """
        Show plot as waterfall.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        """
        if self._parent is not None and "DataWidget" in str(type(self._parent)):
            self._parent._show_chart_waterfall = not self._parent._show_chart_waterfall
            QTimer.singleShot(1, self._parent.update_signal_not_forced.emit)

    def show_grid(self, name_parent="default", name_pi="default"):
        """
        Show grid.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        """
        self._chart_layout_widget[name_parent].show_grid(name_pi=name_pi)

    def dark_(self):
        """
        Set dark theme.
        """
        for name in self._chart_layout_widget.keys():
            self._chart_layout_widget[name].dark_()

    def light_(self):
        for name in self._chart_layout_widget.keys():
            self._chart_layout_widget[name].light_()

    def add_plot(self,
                 title="",
                 name_parent="default",
                 name="default",
                 dyn_cursor=False,
                 show_value_axis=True,
                 show_grid=False,
                 show_legend=False,
                 multi_axis=False,
                 statistics=False,
                 fitting=False,
                 padding=None,
                 time_in_chart=False,
                 unitx=None,
                 unity=None,
                 add_region=True,
                 swap=False,
                 low_res=False,
                 dynamic_scaling=False,
                 glue_cursor=True):

        """
        Add new plot.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        :param dyn_cursor: Not used.
        :type dyn_cursor: bool, optional
        :param show_value_axis: Show value x y axis (default=True).
        :type show_value_axis: bool, optional
        :param show_grid: Show plot grid (default=False).
        :type show_grid: bool, optional
        :param multi_axis: Enable or not mulit axis (default=False).
        :type multi_axis: bool, optional
        :param statistics: Statistics option (default=False).
        :type statistics: bool, optional
        :param fitting: Fitting option (default=False).
        :type fitting: bool, optional
        :param time_in_chart: X axis as timestamps (default=False).
        :type time_in_chart: bool, optional
        :param unity: Y unit.
        :type unity: str, optional
        """
        self._chart_layout_widget[name_parent].add_plot(title=title,
                                                        name=name,
                                                        dyn_cursor=dyn_cursor,
                                                        show_value_axis=show_value_axis,
                                                        show_grid=show_grid,
                                                        show_legend=show_legend,
                                                        multi_axis=multi_axis,
                                                        statistics=statistics,
                                                        fitting=fitting,
                                                        time_in_chart=time_in_chart,
                                                        unitx=unitx,
                                                        unity=unity,
                                                        add_region=add_region,
                                                        swap=swap,
                                                        low_res=low_res,
                                                        dynamic_scaling=dynamic_scaling,
                                                        glue_cursor=glue_cursor)

    def set_dynamic_scaling(self, name_parent="default", name="default", dynamic_scaling=True):
        self._chart_layout_widget[name_parent].set_dynamic_scaling(name, dynamic_scaling=dynamic_scaling)

    def set_x_range(self, name_parent="default", name="default", xmin=0, xmax=1):
        """
        Set x min max range.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        :param xmin: X min (default=0).
        :type xmin: float, optional
        :param xmax: X max (default=1).
        :type xmax: float, optional
        """
        self._chart_layout_widget[name_parent].set_x_range(name, xmin, xmax)

    def set_y_range(self, name_parent="default", name="default", ymin=0, ymax=1):
        """
        Set y min max range.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        :param ymin: Y min (default=0).
        :type ymin: float, optional
        :param ymax: Y max (default=1).
        :type ymax: float, optional
        """
        self._chart_layout_widget[name_parent].set_y_range(name, ymin, ymax)

    def set_labels(self, name_parent="default", name="default", left="", bottom="", right="", top=""):
        """
        Set labels around plot.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        :param left: Label left axis position.
        :type left: str, optional
        :param bottom: Label bottom axis position.
        :type bottom: str, optional
        :param right: Label right axis position.
        :type right: str, optional
        :param top: Label top axis position.
        :type top: str, optional
        """
        self._chart_layout_widget[name_parent].set_labels(name, left, bottom, right, top)

    def set_data(self, data, datax=None, update=True, name="default", name_parent="default", name_pi="default",
                 type_plotitem="p", *args, **kwargs):
        """
        Set data in a plot item.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        :param type_plotitem: Type of the PlotDataItem (p or b, default="p").
        :type type_plotitem: str, optional
        """
        self._chart_layout_widget[name_parent].set_data(data, datax=datax, update=update, name=name, name_pi=name_pi,
                                                        type_plotitem=type_plotitem, *args, **kwargs)

    def fit_to_data(self, name_parent="default", name="default"):
        """
        Fit to data.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        """
        self._chart_layout_widget[name_parent].fit_to_data(name_pi=name)

    def get_region(self, name_parent="default", name_pi="default", name="default"):
        """
        Get region horizontal or vertical.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name Region.
        :type name: str, optional

        """
        if name_parent in self._chart_layout_widget.keys():
            return self._chart_layout_widget[name_parent].get_region(name_pi=name_pi, name=name)

    def add_region(self, name_parent="default", name_pi="default", name="default", min=0, max=1,
                   orientation='horizontal', pen=Colors.COLOR_BLUEGRAYT1, brush=Colors.COLOR_BLUEGRAYT2, movable=True,
                   label=None, font_size=8, arrow=False):
        """
        Add region horizontal or vertical.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the Region.
        :type name: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param min: Minimum range (default=0)
        :type min: float, optional
        :param max: Maximum range (default=1)
        :type max: float, optional
        :param pen: Color region lines (default=BLUEGRAYT1).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=BLUEGRAYT2).
        :type brush: QColor, optional
        :param movable: Region movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        :param arrow: Show or not arrows inside region (defaul=False).
        :type arrow: bool, optional
        """
        if name_parent in self._chart_layout_widget.keys():
            self._chart_layout_widget[name_parent].add_region(name_pi=name_pi, name=name, min=min, max=max,
                                                              orientation=orientation, pen=pen, brush=brush,
                                                              movable=movable, label=label, font_size=font_size,
                                                              arrow=arrow)

    def add_marker_line(self, name_parent="default", name_pi="default", name="default", orientation='vertical', pos=0,
                        pen=Colors.COLOR_LPINKT, movable=True, label=None, font_size=8):
        """
        Add marker line.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the marker.
        :type name: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param pos: Position of the marker (default=0)
        :type pos: float, optional
        :param pen: Color marker line (default=LPINKT).
        :type pen: QColor, optional
        :param movable: Marker movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        """
        if name_parent in self._chart_layout_widget.keys():
            self._chart_layout_widget[name_parent].add_marker_line(name_pi=name_pi, name=name, orientation=orientation,
                                                                   pos=pos, pen=pen, movable=movable, label=label,
                                                                   font_size=font_size)

    def add_marker_points(self, name_parent="default", name_pi="default", name="default", posx=[], posy=[],
                          pen=Colors.COLOR_LPINK, brush=Colors.COLOR_LPINK, size=8, symbol='o'):
        """
        Add marker points.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the marker.
        :type name: str, optional
        :param posx: List of x positions
        :type posx: list
        :param posy: List of y positions
        :type posy: list
        :param pen: Color marker line (default=LPINK).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=LPINK).
        :type brush: QColor, optional
        :param size: Size symbol (default=6)
        :type size: int, optional
        :param symbol: Type of symbol (default="o")
        :type symbol: str, optional
        """
        if name_parent in self._chart_layout_widget.keys():
            self._chart_layout_widget[name_parent].add_marker_points(name_pi=name_pi, name=name, posx=posx, posy=posy,
                                                                     pen=pen, brush=brush, size=size, symbol=symbol)

    def remove_marker_points(self, name_parent="default", name_pi="default", name="default"):
        """
        Remove all marker points

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name of the plot.
        :type name: str, optional
        :param name_pi: Name of the plot item.
        :type name_pi: str, optional

        """

        if name_parent in self._chart_layout_widget.keys():
            self._chart_layout_widget[name_parent].remove_marker_points(name_pi=name_pi, name=name)

    def remove_marker_point(self, name_parent="default", name_pi="default", name="default", index=0):
        """
        Remove a marker points

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name of the plot.
        :type name: str, optional
        :param name_pi: Name of the plot item.
        :type name_pi: str, optional
        :param index: Index marker.
        :type index: int, optional

        """

        if name_parent in self._chart_layout_widget.keys():
            self._chart_layout_widget[name_parent].remove_marker_point(name_pi=name_pi, name=name, index=index)

    def get_plot_items(self, name_parent="default"):
        return self._chart_layout_widget[name_parent]._plot_items

    def get_plot(self, name="default", name_parent="default"):
        """
        Get plot item using title key.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name: Name PlotItem.
        :type name: str, optional
        """
        return self._chart_layout_widget[name_parent].get_plot(name)

    def get_plot_data_item(self, name="default", name_pi="default", name_parent="default"):
        """
        Get plot item using title key.

        :param name_parent: Chart name.
        :type name_parent: str, optional
        :param name_pi: Name PlotItem.
        :type name_pi: str, optional
        :param name: Name of the PlotDataItem.
        :type name: str, optional
        """
        return self._chart_layout_widget[name_parent].get_plot_data_item(name=name, name_pi=name_pi)

    def arrange_charts(self, name_parent="default"):
        return self._chart_layout_widget[name_parent].arrange_charts()


class _ChartLayoutWidget(pg.GraphicsLayoutWidget):
    """
    Low-level chart widget layout displaying multi-arrays.
    """

    update_signal = pyqtSignal(str, str, str, tuple, dict)
    point_signal = pyqtSignal(str, str, bool)

    # mutex = threading.Lock()

    def __init__(self, parent):
        super().__init__()
        self._parent = parent
        self.ci.layout.setContentsMargins(5, 5, 5, 5)
        self._plot_items = {}
        self._plot_items_pen = {}
        self._grid_items = None
        self._grid_items_pen = None
        self._plot_data_items = {}
        self._viewbox = {}
        self.color_plot_data_items = {}
        self.color_brush_plot_data_items = {}
        self.type_plot_data_item = {}
        self._data = {}
        self._datax = {}
        self._data_ref = {}
        self._datax_ref = {}
        self._curve = None
        self._low_res = {}
        self._multi_axis = {}
        self._grid_items = {}
        self._grid_items_pen = {}

        self.update_signal.connect(self.update_plot)
        self.point_signal.connect(self.show_as_point_update)
        # self.installEventFilter(self)

    # def eventFilter(self, object, event):
    # if event.type() == 2:
    #     if event.button() == Qt.LeftButton:
    #         self.mouse_ReleaseEvent(event)
    # return False

    # def mouse_ReleaseEvent(self, e):
    #     for plot_item in self._plot_items.values():
    #         plot_item.mouseReleaseEvent(e)

    def wheelEvent(self, event):
        """
        Mouse wheel event.
        """
        super().wheelEvent(event)

    def show_as_waterfall(self, name_parent="default", name_pi="default", name="default"):
        self._parent.show_as_waterfall(name_parent, name_pi, name)

    def clear_data(self):
        """
        Clear data.
        """
        try:
            for name_pi in self._plot_items.keys():
                plot_item = self._plot_items[name_pi]
                plot_item._list_in_plot_item = None
                for name in self._plot_data_items[name_pi].keys():
                    plot_data_item = self._plot_data_items[name_pi][name]
                    plot_item.removeItem(plot_data_item)
                plot_item.updateViews()
            self._plot_data_items = {}
            self.color_plot_data_items = {}
            self.color_brush_plot_data_items = {}
            self.type_plot_data_item = {}
        except:
            pass

    def add_plot(self,
                 title="",
                 name='default',
                 dyn_cursor=False,
                 show_value_axis=True,
                 show_grid=False,
                 show_legend=False,
                 multi_axis=False,
                 statistics=False,
                 fitting=False,
                 time_in_chart=False,
                 unitx=None,
                 unity=None,
                 add_region=True,
                 swap=False,
                 low_res=False,
                 dynamic_scaling=False,
                 glue_cursor=True):
        """
        Add new plot.

        :param title: Title top centered.
        :type title: str, optional
        :param name: PlotItem name.
        :type name: str, optional
        :param dyn_cursor: Not used.
        :type dyn_cursor: bool, optional
        :param show_value_axis: Show value x y axis (default=True).
        :type show_value_axis: bool, optional
        :param show_grid: Show plot grid (default=False).
        :type show_grid: bool, optional
        :param show_legend: Show plot legend (default=False).
        :type show_legend: bool, optional
        :param multi_axis: Enable or not mulit axis (default=False).
        :type multi_axis: bool, optional
        :param statistics: Statistics option (default=False).
        :type statistics: bool, optional
        :param fitting: Fitting option (default=False).
        :type fitting: bool, optional
        :param time_in_chart: X axis as timestamps (default=False).
        :type time_in_chart: bool, optional
        """
        final_show_value_axis = show_value_axis
        if name in self._plot_items:
            plot_item = self._plot_items[name]
        else:
            plot_item = BasicPlotItem(
                parent=self,
                name=name,
                title=title,
                multi_axis=multi_axis,
                dyn_cursor=dyn_cursor,
                show_legend=show_legend,
                show_value_axis=final_show_value_axis,
                statistics=statistics,
                fitting=fitting,
                time_in_chart=time_in_chart,
                unitx=unitx,
                unity=unity,
                add_region=add_region,
                swap=swap,
                low_res=low_res,
                dynamic_scaling=dynamic_scaling,
                glue_cursor=glue_cursor
            )

            self.clear()

            self._plot_items[name] = plot_item
            self._low_res[name] = low_res

            max_col_show = 3
            if len(self._plot_items) == 4:
                max_col_show = 2
            if len(self._plot_items) == 2:
                max_col_show = 1
            elif len(self._plot_items) >= 16:
                max_col_show = 4

            count = 0
            for pl in self._plot_items.values():
                col = int(count % max_col_show)
                row = int(count / max_col_show)
                count = count + 1
                self.addItem(pl, col=col, row=row)

            self.setMinimumHeight(150 * row)

            if name not in self._grid_items:
                self._grid_items[name] = pg.GridItem()
                self._grid_items_pen[name] = self._grid_items[name].opts["pen"]
                self._grid_items[name].setTextPen(None)

            if show_grid:
                plot_item.addItem(self._grid_items[name])

            plot_item.set_grid_pos(col, row)

        return plot_item

    def view_grids(self, show=True):
        for grid in self._grid_items.values():
            if show:
                grid.show()
            else:
                grid.hide()

    def arrange_charts(self):

        self.clear()

        len_pi = 0
        for pl in self._plot_items.values():
            if pl.isVisible():
                len_pi = len_pi + 1

        max_col_show = 3
        if len_pi == 4:
            max_col_show = 2
        if len_pi == 2:
            max_col_show = 1
        elif len_pi >= 16:
            max_col_show = 4

        count = 0
        for pl in self._plot_items.values():
            if pl.isVisible():
                col = int(count % max_col_show)
                row = int(count / max_col_show)
                count = count + 1
                self.addItem(pl, col=col, row=row)

    def get_plot(self, name="default"):
        """
        Get plot item using title key.

        :param name: PlotItem name.
        :type name: str, optional
        """
        return self._plot_items[name]

    def get_plot_data_item(self, name="default", name_pi="default"):
        """
        Get plot data item using title key.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        """
        return self._plot_data_items[name_pi][name]

    def change_resolution(self, name_pi="default"):
        self._low_res[name_pi] = not self._low_res[name_pi]
        for name in self._plot_data_items[name_pi].keys():
            self.set_data(self._data_ref[name], datax=self._datax_ref[name], update=True, name=name, name_pi=name_pi)

    def compress_data(self, xIn, yIn, min_len=100000, num_bins=None):

        if len(yIn) < min_len:
            return [xIn, yIn]

        if num_bins is None:
            num_bins = len(yIn) // 10

        if xIn is None:
            xIn = range(len(yIn))

        x = np.array(xIn)
        y = np.array(yIn)

        pts_per_bin = x.size // num_bins

        x_view = x[:pts_per_bin * num_bins].reshape(num_bins, pts_per_bin)
        y_view = y[:pts_per_bin * num_bins].reshape(num_bins, pts_per_bin)
        i_min = np.argmin(y_view, axis=1)
        i_max = np.argmax(y_view, axis=1)

        r_index = np.repeat(np.arange(num_bins), 2)
        c_index = np.sort(np.stack((i_min, i_max), axis=1)).ravel()

        return [x_view[r_index, c_index], y_view[r_index, c_index]]

    def set_data(self, data, datax=None, update=True, name="default", name_pi="default", type_plotitem="p", *args, **kwargs):
        """
        Set data in a plot item.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param type_plotitem: Type of the PlotDataItem (p or b, default="p").
        :type type_plotitem: str, optional
        """

        try:
            if data.shape[1] == 0:
                return
        except Exception as e:
            pass
        self._data[name] = data
        self._datax[name] = datax

        if update:
            self.update_signal.emit(name, name_pi, type_plotitem, args, kwargs)

    def show_as_line(self, name="default", name_pi="default", all=True):
        """
        Show plot as line.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param all: Apply to all (default=True).
        :type all: bool, optional
        """
        if all:
            for name in self._plot_data_items[name_pi].keys():
                plot_data_item = self._plot_data_items[name_pi][name]
                if self.type_plot_data_item[name_pi][name] != "b":
                    pen = self._plot_items_pen[name_pi][name]
                    color_pen = pen.color()
                    color_pen.setAlpha(255)
                    pen.setColor(color_pen)
                    plot_data_item.opts["pen"] = pen
                    plot_data_item.setBrush(color_pen)
                    plot_data_item.setFillBrush(None)
                    plot_data_item.opts["symbol"] = None
                    plot_data_item.updateItems(styleUpdate=True)
        else:
            if name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    pen = self._plot_items_pen[name_pi][name]
                    color_pen = pen.color()
                    color_pen.setAlpha(255)
                    self._plot_data_items[name_pi][name].opts["pen"] = pen
                    self._plot_data_items[name_pi][name].setBrush(color_pen)
                    self._plot_data_items[name_pi][name].setFillBrush(None)
                    self._plot_data_items[name_pi][name].opts["symbol"] = None
                    self._plot_data_items[name_pi][name].updateItems(styleUpdate=True)

    def show_as_area(self, name="default", name_pi="default", all=True):
        """
        Show plot as area.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param all: Apply to all (default=True).
        :type all: bool, optional
        """
        if all:
            for name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    plot_data_item = self._plot_data_items[name_pi][name]
                    pen = self._plot_items_pen[name_pi][name]
                    plot_data_item.setFillLevel(0)
                    color_pen = pen.color()
                    color_pen.setAlpha(60)
                    plot_data_item.setFillBrush(color_pen)
                    plot_data_item.opts["symbol"] = None
                    plot_data_item.updateItems(styleUpdate=True)
        else:
            if name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    pen = self._plot_items_pen[name_pi][name]
                    self._plot_data_items[name_pi][name].setFillLevel(0)
                    color_pen = pen.color()
                    color_pen.setAlpha(60)
                    self._plot_data_items[name_pi][name].setFillBrush(color_pen)
                    self._plot_data_items[name_pi][name].opts["symbol"] = None
                    self._plot_data_items[name_pi][name].updateItems(styleUpdate=True)

    def show_as_point(self, name="default", name_pi="default", all=True):
        """
        Show data as point.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param all: Apply to all (default=True).
        :type all: bool, optional
        """
        self.point_signal.emit(name, name_pi, all)

    @pyqtSlot(str, str, bool)
    def show_as_point_update(self, name="default", name_pi="default", all=True):
        """
        Show plot as point.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param all: Apply to all (default=True).
        :type all: bool, optional
        """
        count = 0
        if all:
            for name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    plot_data_item = self._plot_data_items[name_pi][name]
                    pen = self._plot_items_pen[name_pi][name]
                    if count < 50:
                        plot_data_item.setFillBrush(None)
                        plot_data_item.setBrush(None)
                        plot_data_item.opts["symbol"] = "o"
                        plot_data_item.opts["symbolSize"] = 6
                        plot_data_item.opts["symbolBrush"] = pen.color()
                        plot_data_item.opts["symbolPen"] = pen.color()
                        plot_data_item.opts["pen"] = None
                        plot_data_item.updateItems(styleUpdate=True)
                    else:
                        color_pen = pen.color()
                        color_pen.setAlpha(255)
                        pen.setColor(color_pen)
                        plot_data_item.opts["pen"] = pen
                        plot_data_item.setBrush(color_pen)
                        plot_data_item.setFillBrush(None)
                        plot_data_item.opts["symbol"] = None
                        plot_data_item.updateItems(styleUpdate=True)
                    count = count + 1
        else:
            if name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    pen = self._plot_items_pen[name_pi][name]
                    self._plot_data_items[name_pi][name].setFillBrush(None)
                    self._plot_data_items[name_pi][name].setBrush(None)
                    self._plot_data_items[name_pi][name].opts["symbol"] = "o"
                    self._plot_data_items[name_pi][name].opts["symbolSize"] = 6
                    self._plot_data_items[name_pi][name].opts["symbolBrush"] = pen.color()
                    self._plot_data_items[name_pi][name].opts["symbolPen"] = pen.color()
                    self._plot_data_items[name_pi][name].opts["pen"] = None
                    self._plot_data_items[name_pi][name].updateItems(styleUpdate=True)

    def show_as_linepoint(self, name="default", name_pi="default", all=True):
        """
        Show plot as point.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param all: Apply to all (default=True).
        :type all: bool, optional
        """
        count = 0
        if all:
            for name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    plot_data_item = self._plot_data_items[name_pi][name]
                    pen = self._plot_items_pen[name_pi][name]
                    color_pen = pen.color()
                    color_pen.setAlpha(255)
                    pen.setColor(color_pen)
                    if count < 50:
                        plot_data_item.setFillBrush(None)
                        plot_data_item.setBrush(None)
                        plot_data_item.opts["symbol"] = "o"
                        plot_data_item.opts["symbolSize"] = 6
                        plot_data_item.opts["symbolBrush"] = None
                        plot_data_item.opts["symbolPen"] = pen
                        plot_data_item.opts["pen"] = pen
                        plot_data_item.updateItems(styleUpdate=True)
                    else:
                        color_pen = pen.color()
                        color_pen.setAlpha(255)
                        pen.setColor(color_pen)
                        plot_data_item.opts["pen"] = pen
                        plot_data_item.setBrush(color_pen)
                        plot_data_item.setFillBrush(None)
                        plot_data_item.opts["symbol"] = None
                        plot_data_item.updateItems(styleUpdate=True)
                    count = count + 1
        else:
            if name in self._plot_data_items[name_pi].keys():
                if self.type_plot_data_item[name_pi][name] != "b":
                    pen = self._plot_items_pen[name_pi][name]
                    color_pen = pen.color()
                    color_pen.setAlpha(255)
                    pen.setColor(color_pen)
                    self._plot_data_items[name_pi][name].setFillBrush(None)
                    self._plot_data_items[name_pi][name].setBrush(None)
                    self._plot_data_items[name_pi][name].opts["symbol"] = "o"
                    self._plot_data_items[name_pi][name].opts["symbolSize"] = 6
                    self._plot_data_items[name_pi][name].opts["symbolBrush"] = None
                    self._plot_data_items[name_pi][name].opts["symbolPen"] = pen
                    self._plot_data_items[name_pi][name].opts["pen"] = pen
                    self._plot_data_items[name_pi][name].updateItems(styleUpdate=True)

    def show_grid(self, name_pi="default"):
        """
        Show grid.

        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        """

        if self._hide_grid:
            if self._grid_items[name_pi] in self._plot_items[name_pi].items:
                self._show_grid[name_pi] = False
                self._plot_items[name_pi].removeItem(self._grid_items[name_pi])
        else:
            if self._grid_items[name_pi] in self._plot_items[name_pi].items:
                self._show_grid[name_pi] = False
                self._plot_items[name_pi].removeItem(self._grid_items[name_pi])
            else:
                self._show_grid[name_pi] = True
                self._plot_items[name_pi].addItem(self._grid_items[name_pi])

    def fit_to_data(self, name_pi='default', xmin=None, xmax=None, ymin=None, ymax=None):
        """
        Fit scale to data.

        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param xmin: X min.
        :type xmin: float: optional
        :param ymin: Y min.
        :type ymin: float: optional
        :param xmax: X max.
        :type xmax: float: optional
        :param ymax: Y max.
        :type ymax: float: optional
        """
        if name_pi not in self._plot_items.keys():
            return
        self._plot_items[name_pi].fit_to_data(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

    @pyqtSlot(str, str, str, tuple, dict)
    def update_plot(self, name='default', name_pi='default', type_plotitem="p", args=None, kwargs=None):
        """
        Update the chart component.

        :param name: PlotDataItem name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param type_plotitem: Type of the PlotDataItem (p or b, default="p").
        :type type_plotitem: str, optional
        """

        if name_pi not in self._plot_items:
            return

        try:
            if len(self._data[name]) == 0:
                return
        except:
            pass

        # x data processing
        if name_pi not in self._plot_data_items.keys() or self._plot_data_items[name_pi] is None or name not in \
                self._plot_data_items[name_pi].keys():

            xdata = None

            try:
                data = self._data[name].copy()
                datax = self._datax[name]

                if type_plotitem == "b":
                    xdata = datax
                    if datax is None or len(data) != len(datax):
                        xdata = None
                else:
                    try:
                        if self._datax[name] is not None:
                            try:
                                datax = list(self._datax[name])
                            except Exception as ee:
                                try:
                                    datax = []
                                    datax.append(self._datax[name])
                                except:
                                    datax = None
                    except Exception as e:
                        datax = None
                    xdata = datax
                    if datax is not None and len(data) != len(datax):
                        xdata = None

            except Exception as e:
                try:
                    if xdata is None or len(xdata) == 0:
                        xdata = range(len(data))
                except:
                    xdata = None

            try:
                if xdata is None or len(xdata) == 0:
                    xdata = range(len(data))
            except:
                pass

            # type plot p:normal h:histo b:bar

            # fast = False
            # if self._datax[name] is None:
            #     fast = True
            fast=True

            if type_plotitem == "p":

                # inject

                if self.get_plot(name_pi).get_swap() and xdata is not None:
                    plot_data_item = self._plot_items[name_pi].plotdata(xdata, x=data, name=name, ignoreBounds=True, fast=fast,
                                                                        *args, **kwargs)
                else:
                    plot_data_item = self._plot_items[name_pi].plotdata(data, x=xdata, name=name, ignoreBounds=True, fast=fast,
                                                                        *args, **kwargs)
            elif type_plotitem == "h":

                if len(xdata) > 1:
                    width = (xdata[len(xdata) - 1] - xdata[0]) / (len(xdata) - 1)
                else:
                    width = 0.5

                if width > 0.5:
                    width = 0.5

                # inject

                plot_data_item = pg.BarGraphItem(x=xdata, name=name, height=data, width=width, *args, **kwargs)

                self._plot_items[name_pi].addItem(plot_data_item, ignoreBounds=True)
                if self._plot_items[name_pi].get_region(name="default") is not None:
                    self._plot_items[name_pi].get_region(name="default").hide()
                self._plot_items[name_pi].fit_to_data()
            else:

                try:
                    pen = kwargs["pen"]
                except:
                    pen = pg.mkPen({'color': Colors.strokeColor(0), 'width': 1.})

                # inject

                plot_data_item = self._plot_items[name_pi].plotdata(data, x=xdata, name=name, ignoreBounds=True, fast=fast,
                                                                    type_plotitem=type_plotitem, brush=pen.color(),
                                                                    *args, **kwargs)
                self._plot_items_pen[name_pi][name] = pen

            if name_pi not in self._plot_data_items or len(self._plot_data_items[name_pi]) == 0:
                self._plot_data_items[name_pi] = {}
                self._plot_items_pen[name_pi] = {}
                self.type_plot_data_item[name_pi] = {}

            self._plot_data_items[name_pi][name] = plot_data_item

            if type_plotitem != "b":
                self._plot_items_pen[name_pi][name] = plot_data_item.opts["pen"]

            if self.get_plot(name_pi)._statistics is not None:
                self.get_plot(name_pi).set_statistics(data, name=name, x=xdata)

            if self.get_plot(name_pi)._fitting is not None:
                xmin = self.get_plot(name_pi).getViewBox().viewRange()[0][0]
                xmax = self.get_plot(name_pi).getViewBox().viewRange()[0][1]
                self.get_plot(name_pi).set_fitting(data, name=name, x=xdata, xmin=xmin, xmax=xmax)

            self.type_plot_data_item[name_pi][name] = type_plotitem

        else:

            # set y

            if self._data[name] is None:
                return

            data = self._data[name].copy()

            if self._datax[name] is not None:
                try:
                    datax = self._datax[name]
                except:
                    try:
                        datax = []
                        datax.append(self._datax[name])
                    except:
                        datax = None
            else:
                datax = None

            # set x

            xdata = datax
            try:
                if len(data) != len(datax):
                    xdata = None
            except:
                xdata = None

            try:
                if xdata is None or len(xdata) == 0:
                    xdata = range(len(data))
            except:
                pass

            try:

                # set pen

                pen = None

                try:

                    pen = kwargs["pen"]

                    if pen.color() != self.get_plot_data_item(name=name, name_pi=name_pi).opts['pen'].color():
                        self.get_plot_data_item(name=name, name_pi=name_pi).opts['pen'] = pen
                except:
                    pass

                # type plot p:normal h:histo b:bar

                if name not in self.type_plot_data_item[name_pi]:
                    return

                if self.type_plot_data_item[name_pi][name] == "p":

                    # inject

                    if self.get_plot(name_pi).get_swap():
                        if ".PlotDataItem" in str(type(self.get_plot_data_item(name=name, name_pi=name_pi))):
                            self.get_plot_data_item(name=name, name_pi=name_pi).setData(xdata, x=data)
                        else:
                            self.get_plot_data_item(name=name, name_pi=name_pi).set_data(xdata, x=data)
                    else:
                        if ".PlotDataItem" in str(type(self.get_plot_data_item(name=name, name_pi=name_pi))):
                            self.get_plot_data_item(name=name, name_pi=name_pi).setData(data, x=xdata)
                        else:
                            self.get_plot_data_item(name=name, name_pi=name_pi).set_data(data, x=xdata)

                    self._plot_items[name_pi].check_visibility(name)
                    if self.get_plot(name_pi)._dynamic_scaling:
                        self._plot_items[name_pi].fit_to_data()
                elif self.type_plot_data_item[name_pi][name] == "h":

                    # pen

                    pen = self.get_plot_data_item(name=name, name_pi=name_pi).opts['pen']

                    if len(xdata) > 1:
                        width = (xdata[len(xdata) - 1] - xdata[0]) / (len(xdata) - 1)
                    else:
                        width = 0.5

                    if width > 0.5:
                        width = 0.5

                    # inject

                    self.get_plot_data_item(name=name, name_pi=name_pi).setOpts(x=xdata, height=data, width=width,
                                                                                pen=pen, brush=pen.color().lighter(120))

                    self._plot_items[name_pi].check_visibility(name)
                    self._plot_items[name_pi].fit_to_data()

                else:

                    pen = self._plot_items_pen[name_pi][name]

                    # inject

                    self.get_plot_data_item(name=name, name_pi=name_pi).setOpts(x=xdata, height=data, pen=pen,
                                                                                brush=pen.color())
                    self._plot_items[name_pi].check_visibility(name)
                    if self.get_plot(name_pi)._dynamic_scaling:
                        self._plot_items[name_pi].fit_to_data()

                # set stat
                if self.get_plot(name_pi)._statistics is not None:
                    self.get_plot(name_pi).set_statistics(data, name=name, x=xdata)

                # set fit
                if self.get_plot(name_pi)._fitting is not None:
                    xmin = self.get_plot(name_pi).getViewBox().viewRange()[0][0]
                    xmax = self.get_plot(name_pi).getViewBox().viewRange()[0][1]

                    self.get_plot(name_pi).set_fitting(data, name=name, x=xdata, xmin=xmin, xmax=xmax)

            except Exception as ex:
                print(ex)
                # traceback.print_exception(type(ex), ex, ex.__traceback__)

    def set_dynamic_scaling(self, name_pi="default", dynamic_scaling=True):
        if name_pi in self._plot_items:
            self.get_plot(name_pi)._dynamic_scaling = dynamic_scaling
            self.get_plot(name_pi).update()

    def set_x_range(self, name="default", xmin=0, xmax=1):
        """
        Set x min max range.

        :param name: PlotItem name.
        :type name: str, optional
        :param xmin: X min (default=0).
        :type xmin: float, optional
        :param xmax: X max (default=1).
        :type xmax: float, optional
        """
        if xmin >= xmax:
            xmax = xmin + 1
        self._plot_items[name].setRange(xRange=[xmin, xmax])

    def set_y_range(self, name="default", ymin=0, ymax=1):
        """
        Set y min max range.

        :param name: PlotItem name.
        :type name: str, optional
        :param ymin: Y min (default=0).
        :type ymin: float, optional
        :param ymax: Y max (default=1).
        :type ymax: float, optional
        """
        if ymin >= ymax:
            ymax = ymin + 1
        self._plot_items[name].setRange(yRange=[ymin, ymax])

    def set_labels(self, name="default", left="", bottom="", right="", top=""):
        """
        Set labels around plot.

        :param name: PlotItem name.
        :type name: str, optional
        :param left: Label left axis position.
        :type left: str, optional
        :param bottom: Label bottom axis position.
        :type bottom: str, optional
        :param right: Label right axis position.
        :type right: str, optional
        :param top: Label top axis position.
        :type top: str, optional
        """
        self._plot_items[name].setLabels(left=left, bottom=bottom, top=top, right=right)

    def get_region(self, name_pi="default", name="default"):
        """
        Get a region in the plotitem.

        :param name: Region name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        """
        if name_pi in self._plot_items.keys():
            return self._plot_items[name_pi].get_region(name=name)

    def add_region(self, name_pi="default", name="default", min=0, max=1, orientation='horizontal',
                   pen=Colors.COLOR_BLUEGRAYT1, brush=Colors.COLOR_BLUEGRAYT2, movable=True, label=None, font_size=8,
                   arrow=False):
        """
        Add a region in the plotitem.

        :param name: Region name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param min: Minimum range (default=0)
        :type min: float, optional
        :param max: Maximum range (default=1)
        :type max: float, optional
        :param pen: Color region lines (default=BLUEGRAYT1).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=BLUEGRAYT2).
        :type brush: QColor, optional
        :param movable: Region movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        :param arrow: Show or not arrows inside region (defaul=False).
        :type arrow: bool, optional
        """
        if name_pi in self._plot_items.keys():
            self._plot_items[name_pi].add_region(name=name, orientation=orientation, min=min, max=max, pen=pen,
                                                 brush=brush, movable=movable, label=label, font_size=font_size,
                                                 arrow=arrow)

    def add_marker_line(self, name_pi="default", name="default", orientation='vertical', pos=0, pen=Colors.COLOR_LPINKT,
                        movable=True, label=None, font_size=8):
        """
        Add a marker line in the plotitem.

        :param name: Marker name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param pos: Position of the marker (default=0)
        :type pos: float, optional
        :param pen: Color marker line (default=LPINKT).
        :type pen: QColor, optional
        :param movable: Marker movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        """
        if name_pi in self._plot_items.keys():
            self._plot_items[name_pi].add_marker_line(name=name, orientation=orientation, pos=pos, pen=pen,
                                                      movable=movable, label=label, font_size=font_size)

    def add_marker_points(self, name_pi="default", name="default", posx=[], posy=[], pen=Colors.COLOR_LPINK,
                          brush=Colors.COLOR_LPINK, size=8, symbol='o'):
        """
        Add a marker point in the plot data item.
        The array of marker is a scatter plot item.

        :param name: Marker point name.
        :type name: str, optional
        :param name_pi: PlotItem name.
        :type name_pi: str, optional
        :param posx: List of x positions
        :type posx: list
        :param posy: List of y positions
        :type posy: list
        :param pen: Color marker line (default=LPINK).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=LPINK).
        :type brush: QColor, optional
        :param size: Size symbol (default=6)
        :type size: int, optional
        :param symbol: Type of symbol (default="o")
        :type symbol: str, optional
        """
        if name_pi in self._plot_items.keys():
            self._plot_items[name_pi].add_marker_points(name=name, posx=posx, posy=posy, pen=pen, brush=brush,
                                                        size=size, symbol=symbol)

    def remove_marker_points(self, name_pi="default", name="default"):
        """
        Remove all marker points

        :param name: Name of the plot.
        :type name: str, optional
        :param name_pi: Name of the plot item.
        :type name_pi: str, optional

        """

        if name_pi in self._plot_items.keys():
            self._plot_items[name_pi].remove_marker_points(name=name)

    def remove_marker_point(self, name_pi="default", name="default", index=0):
        """
        Remove marker point

        :param name: Name of the plot.
        :type name: str, optional
        :param name_pi: Name of the plot item.
        :type name_pi: str, optional
        :param index: Index cursor.
        :type index: int, optional

        """

        if name_pi in self._plot_items.keys():
            self._plot_items[name_pi].remove_marker_point(name=name, index=index)

    def dark_(self):
        """
        Set dark theme.
        """
        self.setBackground(Colors.COLOR_LBLACK)
        for namepi in self._plot_items.keys():
            self._plot_items[namepi].dark_()

    def light_(self):
        """
        Set light theme,
        """
        self.setBackground(Colors.COLOR_DWHITE)
        for namepi in self._plot_items.keys():
            self._plot_items[namepi].light_()


class _Example(QMainWindow, fesacomm.FesaCommListener):
    """

    Example class to test

    """

    def __init__(self):

        super().__init__()

        Colors.COLOR_LIGHT = False
        Colors.init_font()

        self.resize(1600, 600)
        self.move(100, 300)

        self.init_ui()
        self._update_time = 0

        self._fesacomm = fesacomm.FesaComm("BISWRef2", "Acquisition", listener=self)
        self._fesacomm.subscribe("")


    def init_ui2(self):

        self.chart_widget = ChartWidget(self, name=["PARENT1", "PARENT2", "PARENT3"])

        self.chart_widget.add_plot(title="MyTitleA",
                                   name_parent="PARENT1",
                                   name="Plot1",
                                   show_grid=True,
                                   multi_axis=True,
                                   dyn_cursor=True)

        self.chart_widget.add_plot(title="MyTitleB",
                                   name_parent="PARENT1",
                                   name="Plot2")

        self.chart_widget.add_plot(title="MyTitleC",
                                   name_parent="PARENT2",
                                   name="Plot3")

        self.chart_widget.add_plot(title="MyTitleD",
                                   name_parent="PARENT3",
                                   name="Plot4",
                                   show_grid=True)

        self.setCentralWidget(self.chart_widget)
        self.resize(1600, 600)
        self.move(100, 300)

        if not self.li:
            self.chart_widget.dark_()
        else:
            self.chart_widget.light_()

        mu, sigma = 10, 10000
        s = np.random.normal(mu, sigma, 100)

        mean = 0;
        std = 1;
        variance = np.square(std)
        x = np.arange(-5, 5, .05)
        s2 = np.exp(-np.square(x - mean) / 2 * variance) / (np.sqrt(2 * np.pi * variance))
        s2v = 2 * np.exp(-np.square(x - mean) / 2 * variance) / (np.sqrt(2 * np.pi * variance))

        s2b = 5 * np.random.normal(mu, sigma, 2000)
        s2c = 5 * np.random.normal(mu, sigma, 2000)
        s3 = 10 * np.random.normal(mu, sigma, 30)

        self.chart_widget.set_data(s,
                                   name="A1",
                                   name_parent="PARENT1",
                                   name_pi="Plot1",
                                   pen=pg.mkPen({'color': Colors.strokeColor(0), 'width': 1.}),
                                   skipFiniteCheck=True,
                                   antialias=False,
                                   autoDownSample=True)

        self.chart_widget.set_data(s2,
                                   name="A2",
                                   name_parent="PARENT1",
                                   name_pi="Plot1",
                                   pen=pg.mkPen({'color': Colors.getColor(0, max=1, palette="purple"), 'width': 1.}),
                                   skipFiniteCheck=True,
                                   antialias=False,
                                   autoDownSample=True)

        # self.chart_widget.set_data(s3,
        #                             name="A3",
        #                             type_plotitem="b",
        #                             name_parent="PARENT1",
        #                             name_pi="Plot2",
        #                             pen=pg.mkPen({'color': Colors.strokeColor(2), 'width': 1.}),
        #                             skipFiniteCheck=True,
        #                             antialias=False,
        #                             autoDownSample=True)

        # self.chart_widget.set_data(s, update=True, name="B1", name_parent="PARENT1", name_pi="NAMEPIb", pen=pg.mkPen({'color': Colors.strokeColor(0), 'width': 1. }), skipFiniteCheck=True, antialias=False, autoDownSample=True)
        # self.chart_widget.set_data(s3, update=True, name="B2", name_parent="PARENT1", name_pi="NAMEPIb", pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 1. }), skipFiniteCheck=True, antialias=False, autoDownSample=True)

        # self.chart_widget.set_data(s2, datax=x, update=True, name="C2", name_parent="PARENT2", name_pi="NAMEPI2", pen=pg.mkPen({'color': Colors.strokeColor(3), 'width': 1.}), skipFiniteCheck=True, antialias=False, autoDownSample=True)
        # self.chart_widget.set_data(s2v, datax=x, update=True, name="C2v", name_parent="PARENT2", name_pi="NAMEPI2", pen=pg.mkPen({'color': Colors.strokeColor(4), 'width': 1.}), skipFiniteCheck=True, antialias=False, autoDownSample=True)

    def init_ui3(self):

        mu, sigma = 10, 10000
        s = np.random.normal(mu, sigma, 1000)
        xs = range(0, 2000, 2)

        self.chart_widget = ChartWidget(self)

        self.setCentralWidget(self.chart_widget)

        self.chart_widget.add_plot(title="my title",
                                   show_grid=True
                                   )

        self.chart_widget.set_data(s,
                                   name="Plot1",
                                   datax=xs,
                                   pen=pg.mkPen({'color': Colors.strokeColor(0), 'width': 1.}),
                                   skipFiniteCheck=True, antialias=False, autoDownSample=True)

    def init_ui(self):

        # Data

        mu, sigma = 10, 10000
        s = 10 * np.random.normal(mu, sigma, 100)
        # sbig = 0.00001 * np.random.normal(mu, sigma, 40000000)
        bigsize = 40_000_000
        medsize = 40_000_000
        mu, sigma = medsize/2., 10000
        xbig = np.arange(-5_000_000, 15_000_000, 20_000_000./float(bigsize))
        sbig = 0.1*np.sin(np.linspace(0, 8.*np.pi, bigsize)*1.)
        x = np.arange(0, medsize, 1.,dtype=float)
        std = 0.000001
        variance = np.square(std)
        sbig2 = np.exp(-np.square(x - medsize/2.) / 2. * variance) / (np.sqrt(2 * np.pi * variance))
        sbig2 = sbig2 / (10.*np.max(sbig2))
        print(variance, np.exp(-np.square(1999 - 2000.) / 2. * variance), (np.sqrt(2 * np.pi * variance*0.0001)), np.exp(-np.square(1999 - 2000.) / 2. * variance) / (np.sqrt(2 * np.pi * variance*0.0001)))
        # for i, v in enumerate(x):
        #     if i>1995 and i<2005:
        #         print(i,v)
        # for i,v in enumerate(sbig2):
        #     if i>1995 and i<2005:
        #         print(i,v)
        sbig3 = 0.00000001 * np.random.normal(mu, sigma, medsize)

        mean = 0;
        std = 1;
        variance = np.square(std)
        x = np.arange(-5, 5, .05)
        s2 = np.exp(-np.square(x - mean) / 2 * variance) / (np.sqrt(2 * np.pi * variance))
        s2v = 2 * np.exp(-np.square(x - mean) / 2 * variance) / (np.sqrt(2 * np.pi * variance))

        s2b = 5 * np.random.normal(mu, sigma, 2000)
        s2c = 5 * np.random.normal(mu, sigma, 2000)
        s3 = 10 * np.random.normal(mu, sigma, 30)

        # Creation of a chart widget with 3 components

        self.chart_widget = ChartWidget(self, name=["Parent1", "Parent2", "Parent3"])

        w = QWidget()
        layout = QGridLayout(w)
        layout.addWidget(self.chart_widget,0,0)

        self.chart_widget_rt = ChartWidget(self)
        layout.addWidget(self.chart_widget_rt, 0, 1)
        self.chart_widget_rt.add_plot(title="TEST", show_grid=True)

        layout.setColumnStretch(0,1)
        layout.setColumnStretch(1, 1)

        self.setCentralWidget(w)
        self.resize(1600, 600)
        self.move(100, 300)

        # Add 4 plot items
        # 2 plot items in component Parent1 : PlotItem1 and PlotItem2
        # 1 plot items in component Parent2 : PlotItem3
        # 1 plot items in component Parent3 : PlotItem4
        self.chart_widget.add_plot(title="MyTitleA", name_parent="Parent1", name="PlotItem1", multi_axis=True)
        self.chart_widget.add_plot(title="MyTitleB", name_parent="Parent1", name="PlotItem2", show_grid=True)
        self.chart_widget.add_plot(title="MyTitleB", name_parent="Parent2", name="PlotItem3", show_grid=True)
        self.chart_widget.add_plot(title="MyTitleC", name_parent="Parent3", name="PlotItem4",  show_grid=True)

        # Set data in PlotItem1 and plot A1
        # Set data in PlotItem1 and plot A2
        # Set data in PlotItem1 and plot A3
        self.chart_widget.set_data(s,
                                   name="A1",
                                   name_parent="Parent1",
                                   name_pi="PlotItem1",
                                   pen=pg.mkPen({'color': Colors.strokeColor(0), 'width': 1.}),
                                   skipFiniteCheck=True, antialias=False, autoDownSample=True)

        self.chart_widget.set_data(s2,
                                   name="A2",
                                   name_parent="Parent1",
                                   name_pi="PlotItem1",
                                   pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 2.}),
                                   skipFiniteCheck=True, antialias=True, autoDownSample=True)

        self.chart_widget.set_data(s2v,
                                   name="A3",
                                   type_plotitem="b",
                                   name_parent="Parent1",
                                   name_pi="PlotItem1",
                                   pen=pg.mkPen({'color': Colors.strokeColor(2), 'width': 2.}),
                                   skipFiniteCheck=True, antialias=True, autoDownSample=True)

        # Set data in PlotItem2 and plot B1
        # Set data in PlotItem2 and plot B2
        self.chart_widget.set_data(s, name="B1", name_parent="Parent1", name_pi="PlotItem2",
                                   pen=pg.mkPen({'color': Colors.strokeColor(0), 'width': 2.}), skipFiniteCheck=True,
                                   antialias=True, autoDownSample=True)
        self.chart_widget.set_data(s3, name="B2", name_parent="Parent1", name_pi="PlotItem2",
                                   pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 2.}), skipFiniteCheck=True,
                                   antialias=True, autoDownSample=True)

        # Set data in PlotItem3 and plot C1
        # Set data in PlotItem3 and plot C2
        self.chart_widget.set_data(s2, datax=x, name="C1", name_parent="Parent2", name_pi="PlotItem3",
                                   pen=pg.mkPen({'color': Colors.strokeColor(3), 'width': 2.}), skipFiniteCheck=True,
                                   antialias=True, autoDownSample=True)
        self.chart_widget.set_data(s2v, datax=x, name="C2", name_parent="Parent2", name_pi="PlotItem3",
                                   pen=pg.mkPen({'color': Colors.strokeColor(4), 'width': 2.}), skipFiniteCheck=True,
                                   antialias=True, autoDownSample=True)

        # Set data in PlotItem4 and plot D1
        self.chart_widget.set_data(sbig, datax=xbig, name="D1", name_parent="Parent3", name_pi="PlotItem4",
                                   pen=pg.mkPen({'color': Colors.strokeColor(5), 'width': 2}), antialias=True,skipFiniteCheck=False)
        self.chart_widget.set_data(sbig2, name="D2", name_parent="Parent3", name_pi="PlotItem4",
                                   pen=pg.mkPen({'color': Colors.strokeColor(4), 'width': 2}), antialias=True,skipFiniteCheck=False)
        self.chart_widget.set_data(sbig3, name="D3", name_parent="Parent3", name_pi="PlotItem4",
                                   pen=pg.mkPen({'color': Colors.strokeColor(7), 'width': 2}), antialias=True,skipFiniteCheck=False)

        # Add region on PlotItem3
        # self.chart_widget.add_region(name_parent="Parent2", name_pi="PlotItem3", name="1", min=30, max=130,
        #                              orientation='vertical', label="<span>&#x394;V</span>", font_size=8, arrow=True)

        # Add a marker line in PlotItem3
        # for i in range(0, 1):
        #     self.chart_widget.add_marker_line(name_parent="Parent2", name_pi="PlotItem3", name="1", pos=150 + 0.1 * i,
        #                                       label="TEST", font_size=10)

        # Add markers in PlotItem3
        # x = []
        # y = []
        # for i in range(0, 10):
        #     x.append(20 * i)
        #     y.append(0.1 * i)
        # self.chart_widget.add_marker_points(name_parent="Parent2", name_pi="PlotItem3", name="1", posx=x, posy=y,
        #                                     pen=QColor(255, 255, 0, 255), brush=QColor(0, 0, 0, 255), size=6,
        #                                     symbol='+')
        #
        # # Dark theme
        if not Colors.COLOR_LIGHT:
            self.chart_widget.dark_()
        else:
            self.chart_widget.light_()

    def handle_event(self, name, value):
        self.chart_widget_rt.set_data(value["anArray"], name="D1",
                                   pen=pg.mkPen({'color': Colors.strokeColor(8), 'width': 2}), antialias=True,
                                   skipFiniteCheck=False)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
