import sys
import numpy as np
import pyqtgraph as pg
import pandas as pd
from functools import partial

from PyQt5.QtCore import pyqtSignal, Qt, QPointF, QDateTime

from PyQt5.QtGui import QColor, QFont

from PyQt5.QtWidgets import QSizePolicy, QWidget, QGridLayout, QHBoxLayout, QRadioButton, QCheckBox, \
    QFrame, QScrollArea, QLabel, QComboBox, QListView
from pyqtgraph import BarGraphItem

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.fastplotdataitem import FastPlotDataItem


class BasicPlotItem(pg.PlotItem):
    """
    Low-level plotitem to draw data

    :param parent: Parent object
    :type parent: object
    :param name: Name to identify the widget (default="default").
    :type name: strl, optional
    :param title: Title shown top center.
    :type title: str, optional
    :param titlex: Title x axis.
    :type titlex: str, optional
    :param titley: Title y axis.
    :type unitx: str, optional
    :param unitx: Unit x axis.
    :type unity: str, optional
    :param unity: Unit y axis.
    :type titley: str, optional
    :param dyn_cursor: Not used.
    :type dyn_cursor: bool, optional
    :param multi_axis: Enable or not mulit axis (default=False).
    :type multi_axis: bool, optional
    :param dyn_cursor: Not used.
    :type dyn_cursor: bool, optional
    :param show_legend: Show plot legend (default=False).
    :type show_legend: bool, optional
    :param show_value_axis: Show value x y axis (default=True).
    :type show_value_axis: bool, optional
    :param statistics: Statistics option (default=False).
    :type statistics: bool, optional
    :param fitting: Fitting option (default=False).
    :type fitting: bool, optional
    :param time_in_chart: X axis as timestamps (default=False).
    :type time_in_chart: bool, optional
    """

    mousepress_signal = pyqtSignal(object)

    def __init__(self,
                 parent=None,
                 name="default",
                 image=False,
                 title=None,
                 titlex=None,
                 titley=None,
                 unitx=None,
                 unity=None,
                 multi_axis=False,
                 dyn_cursor=False,
                 show_legend=False,
                 show_value_axis=True,
                 statistics=False,
                 fitting=False,
                 time_in_chart=False,
                 add_region=True,
                 swap=False,
                 low_res=False,
                 parentimage=None,
                 dynamic_scaling=False,
                 glue_cursor=True):
        pg.PlotItem.__init__(self)

        self._parent = parent
        self._parentimage = parentimage
        self._name = name

        self.getAxis("bottom").setStyle(tickLength=5)
        self.getAxis("left").setStyle(tickLength=5)

        # if image used by a parent component

        self._image = image
        self._roi = False

        # row/col position

        self._row = 0
        self._col = 0

        self._statistics = None

        if statistics:
            from expert_gui_core.gui.widgets.pyqt import statwidget
            self._statistics = statwidget.StatWidget()
            self._statistics.hide()
        else:
            self._statistics = None

        if fitting:
            from expert_gui_core.gui.widgets.pyqt import fitwidget
            self._fitting = fitwidget.FitWidget()
            self._fitting.hide()
        else:
            self._fitting = None

        self._dynamic_scaling = False

        # time axis

        self._time_in_chart = time_in_chart
        self._time_in_chart_final = time_in_chart

        # scatter

        self._scatters = {}

        # region

        self._region = None
        self._show_region = False
        self._regions = {}
        self._regions_text = {}
        self._region_plot = None
        self._name_region_selected = None
        self._region_arrow = {}
        self._antialias = False

        # list

        self._show_list = False
        self._list_in_plot_item = None

        # cursors

        self._show_cursors = False
        self._list_cursors_in__item = None
        self.glue_cursor = glue_cursor

        # scaling

        self._show_scaling = False
        self._scaling = None

        # markers

        self._markers = {}

        # view boxes

        self._viewbox = {}

        # title

        self._title = title
        if title is not None:
            if len(title) > 26:
                half = int(len(title)/2)
                self.setTitle(
                    "<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + title[0:half]+"<br>"+title[half:] + "</span>")
            else:
                self.setTitle(
                    "<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + title + "</span>")

        # cursor

        self._dyn_cursor = dyn_cursor
        self._hline_dyn = None
        self._vline_dyn = None
        self.hoverable = True
        self.draggable = True
        self.clickable = True

        # menu

        if parent is not None:
            menu_ = self.vb.getMenu(None)

            if low_res:
                self.resAction = pg.QtGui.QAction('High Res.', menu_)
            else:
                self.resAction = pg.QtGui.QAction('Low Res.', menu_)
            # menu_.insertAction(menu_.actions()[1], self.resAction)
            self.resAction.triggered.connect(self.change_resolution)

            self._actions = {}

            aaAction = pg.QtGui.QAction('Antialias', menu_)
            aaAction.setCheckable(True)
            aaAction.setChecked(False)
            self._actions["antialias"] = aaAction
            menu_.insertAction(menu_.actions()[1], aaAction)
            aaAction.triggered.connect(self.change_antialias)

            gotoAction = pg.QtGui.QAction('Show lines', menu_)
            gotoAction.setCheckable(True)
            gotoAction.setChecked(True)
            self._actions["lines"] = gotoAction
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(partial(self.show_as_line, self._name, "", True))

            gotoAction = pg.QtGui.QAction('Show lines and points', menu_)
            gotoAction.setCheckable(True)
            self._actions["lines and points"] = gotoAction
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(partial(self.show_as_linepoint, self._name, "", True))

            gotoAction = pg.QtGui.QAction('Show points', menu_)
            gotoAction.setCheckable(True)
            self._actions["points"] = gotoAction
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(partial(self.show_as_point, self._name,  "", True))

            gotoAction = pg.QtGui.QAction('Show area', menu_)
            gotoAction.setCheckable(True)
            self._actions["area"] = gotoAction
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(partial(self.show_as_area, self._name, "", True))

            if parent is not None and parent._parent is not None and parent._parent._parent is not None and "DataWidget" in str(
                    type(parent._parent._parent)):
                gotoAction = pg.QtGui.QAction('Show waterfall', menu_)
                gotoAction.setCheckable(True)
                menu_.insertAction(menu_.actions()[1], gotoAction)
                gotoAction.triggered.connect(partial(parent.show_as_waterfall, "", self._name))

            gotoAction = pg.QtGui.QAction('Fit to data', menu_)
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(partial(self.fit_to_data, None, None, None, None))

            gotoAction = pg.QtGui.QAction('Dynamic scaling', menu_)
            gotoAction.setCheckable(True)
            menu_.insertAction(menu_.actions()[1], gotoAction)
            gotoAction.triggered.connect(self.dynamic_scaling)

            if dynamic_scaling:
                self.dynamic_scaling()
                gotoAction.setChecked(dynamic_scaling)

            if statistics:
                gotoAction = pg.QtGui.QAction('Statistics', menu_)
                menu_.insertAction(menu_.actions()[1], gotoAction)
                gotoAction.triggered.connect(self.show_stat)

            if fitting:
                gotoAction = pg.QtGui.QAction('Fitting', menu_)
                menu_.insertAction(menu_.actions()[1], gotoAction)
                gotoAction.triggered.connect(self.show_fit)

            gotoAction = pg.QtGui.QAction('Show grid', menu_)
            gotoAction.setCheckable(True)
            gotoAction.setChecked(True)
            menu_.insertAction(menu_.actions()[5], gotoAction)
            gotoAction.triggered.connect(partial(parent.show_grid, self._name))

            gotoAction = pg.QtGui.QAction('Show axis', menu_)
            gotoAction.setCheckable(True)
            gotoAction.setChecked(True)
            menu_.insertAction(menu_.actions()[5], gotoAction)
            gotoAction.triggered.connect(self.show_axis)

            gotoAction = pg.QtGui.QAction('Show legend', menu_)
            gotoAction.setCheckable(True)
            menu_.insertAction(menu_.actions()[5], gotoAction)
            gotoAction.triggered.connect(self.show_legend)

            gotoAction = pg.QtGui.QAction('Show list', menu_)
            menu_.insertAction(menu_.actions()[5], gotoAction)
            gotoAction.triggered.connect(self.show_list)

            gotoAction = pg.QtGui.QAction('Show cursors', menu_)
            menu_.insertAction(menu_.actions()[5], gotoAction)
            gotoAction.triggered.connect(self.show_cursors)

            if add_region:
                gotoAction = pg.QtGui.QAction('Show region', menu_)
                gotoAction.setCheckable(True)
                menu_.insertAction(menu_.actions()[5], gotoAction)
                gotoAction.triggered.connect(self.show_region)

        # mouse both axis

        self.setMouseEnabled(x=True, y=True)
        self._drag_pos = None

        # default padding

        self.setDefaultPadding(0.)

        # swap x and y

        self._swap = swap

        # optimize

        if not self._swap:
            self.setDownsampling(ds=True, auto=True)

        self.setClipToView(True)

        # axis values

        self.set_show_value_axis(show_value_axis)

        # axis unit
        if not multi_axis:
            if unitx is not None or titlex is not None:
                self.setLabel('bottom', text=titlex, units=unitx)
            if unity is not None or titley is not None:
                self.setLabel('left', text=titley, units=unity)

                # default legend

        self._show_legend = show_legend
        if self._show_legend:
            self._plotLegend = self.addLegend()
            self._plotLegend.setLabelTextSize('6pt')
            self._plotLegend.setColumnCount(6)

        # cross hair dyn

        self.pen_cursor_line_dyn = pg.mkPen(color=QColor(200, 200, 200, 160), width=1.5, dash=[2, 4])
        self.pen_cursor_line_dyn_dark = pg.mkPen(color=QColor(200, 200, 200, 160), width=1.5, dash=[2, 4])
        self.target_item = pg.TargetItem(
            pos=(0, -0),
            pen="#FF8888",
            movable=False
        )
        self.target_item.setZValue(20000)
        self.target_item.hide()
        self.addItem(self.target_item)

        self.target_image_item = pg.TargetItem(
            pos=(0, -0),
            pen="#FF8888",
            movable=False
        )
        self.target_image_item.hide()

        self._hline_static = None
        self._vline_static = None

        self._vline_static = pg.InfiniteLine(angle=90, movable=False, pen=self.pen_cursor_line_dyn)
        self._hline_static = pg.InfiniteLine(angle=0, movable=False, pen=self.pen_cursor_line_dyn)
        self._vline_static.setZValue(11000)
        self._hline_static.setZValue(11000)
        self.addItem(self._vline_static, ignoreBounds=True)
        self.addItem(self._hline_static, ignoreBounds=True)

        if self._image:
            self.target_image_item.setZValue(20000)
            self.addItem(self.target_image_item)

        # multi axis

        self.multi_axis = multi_axis

        if self.multi_axis:
            self.vb.sigResized.connect(self.updateViews)

        # default theme

        self._light = True

        self._vblast = None

        # add region default

        if add_region:
            self.add_region(name="default")

        self._drag = False
        self.setAcceptHoverEvents(True)

        # resize

        self.resizeEvent = self.on_resize

    def on_resize(self, event):
        try:
            if self._parent is not None:
                if self.height() < 200:
                    self._parent.view_grids(False)
                else:
                    self._parent.view_grids()
        except:
            pass

    def change_antialias(self):
        self._antialias = not self._antialias
        for name_pi in self._parent._plot_data_items:
            for name in self._parent._plot_data_items[name_pi].keys():
                plot_data_item = self._parent._plot_data_items[name_pi][name]
                if self._parent.type_plot_data_item[name_pi][name] != "b":
                    plot_data_item.opts["antialias"] = self._antialias
                    plot_data_item.updateItems(styleUpdate=True)

    def show_as_point(self, name_pi="default", name="default", all=True):
        self._actions["lines"].setChecked(False)
        self._actions["lines and points"].setChecked(False)
        self._actions["area"].setChecked(False)
        self._parent.show_as_point(name=name, name_pi=name_pi, all=all)

    def show_as_line(self, name_pi="default", name="default", all=True):
        self._actions["points"].setChecked(False)
        self._actions["lines and points"].setChecked(False)
        self._actions["area"].setChecked(False)
        self._parent.show_as_line(name=name, name_pi=name_pi, all=all)

    def show_as_linepoint(self, name_pi="default", name="default", all=True):
        self._actions["lines"].setChecked(False)
        self._actions["points"].setChecked(False)
        self._actions["area"].setChecked(False)
        self._parent.show_as_linepoint(name=name, name_pi=name_pi, all=all)

    def show_as_area(self, name_pi="default", name="default", all=True):
        self._actions["lines"].setChecked(False)
        self._actions["points"].setChecked(False)
        self._actions["lines and points"].setChecked(False)
        self._parent.show_as_area(name=name, name_pi=name_pi, all=all)

    def change_resolution(self):
        if self._parent._low_res[self._name]:
            self.resAction.setText("Low Res.")
        else:
            self.resAction.setText("High Res.")
        self._parent.change_resolution(name_pi=self._name)

    def get_swap(self):
        return self._swap

    def set_roi(self, roi):
        """
        Indicate that the ROI rectangle has been activated in the image parent.
        :param roi: ROI shown or not.
        :type roi: bool
        """

        if self._image:
            self._roi = roi

    def get_roi(self):
        """
        Is ROI activated or not.
        """

        return self._roi

    def show_stat(self):
        """
        Show statiistics dialog.
        """

        if self._statistics is not None:
            self._statistics.show()

    def show_fit(self):
        """
        Show statiistics dialog.
        """

        if self._fitting is not None:
            self._fitting.show()

    def updateViews(self):
        """
        Update all view boxes.
        """
        doit = False
        for name in self._viewbox.keys():
            if doit:
                self._viewbox[name].setGeometry(self.vb.sceneBoundingRect())
            doit = True

    def plotdata(self, data, x=None, name="default", type_plotitem="p", fast=False, *args, **kwargs):
        """
        Plot data.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param type_plotitem: Type of plot ("p" normal, "b" bar, default="p").
        :type type_plotitem: str, optional
        """

        if self.multi_axis:
            fast = False

        if "antialias" not in kwargs:
            kwargs["antialias"] = self._antialias
        else:
            self._antialias = kwargs["antialias"]
            self._actions["antialias"].setChecked(self._antialias)

        if self._time_in_chart:
            self.setAxisItems(axisItems={'bottom': pg.DateAxisItem()})
            self._time_in_chart = False

        if self.multi_axis:
            if len(self._viewbox.keys()) == 0:
                self.setLabels(left=name)
                self.layout.setHorizontalSpacing(10)
                labelStyle = {'color': '#dddddd'}
                self.getAxis("left").setLabel(name, units='', **labelStyle)
                self._viewbox[name] = self.vb
                if type_plotitem == "b":
                    plot_data_item = pg.BarGraphItem(x=x, name=name, height=data, width=0.5, *args, **kwargs)
                else:
                    if not fast:
                        plot_data_item = self.plot(data, x=x, name=name, *args, **kwargs)
                    else:
                        plot_data_item = FastPlotDataItem(x=x, y=data, name=name, *args, **kwargs)
                        self.addItem(plot_data_item)
                return plot_data_item
            else:

                if type_plotitem == "b":
                    plot_data_item = pg.BarGraphItem(x=x, name=name, height=data, width=0.5, *args, **kwargs)
                else:
                    if not fast:
                        plot_data_item = pg.PlotDataItem(data, x=x, name=name, *args, **kwargs)
                    else:
                        plot_data_item = FastPlotDataItem(x=x, y=data, name=name, *args, **kwargs)
                        self.addItem(plot_data_item)
                vbox = pg.ViewBox()
                self._viewbox[name] = vbox
                self.scene().addItem(vbox)
                vbox.setXLink(self.vb)
                axis = pg.AxisItem("right")
                self.layout.addItem(axis, 2, len(self._viewbox.keys()) + 1)
                axis.linkToView(vbox)
                labelStyle = {'color': '#dddddd'}
                axis.setLabel(name, units='', **labelStyle)
                vbox.addItem(plot_data_item)
                if self._region_plot is not None:
                    if self._show_region:
                        self._region_plot.update_data(plot_data_item.name, plot_data_item.getData(),
                                                      plot_data_item.opts['pen'])
                return plot_data_item
        else:

            if not fast:
                plot_data_item = self.plot(data, x=x, name=name, *args, **kwargs)
            else:
                plot_data_item = FastPlotDataItem(x=x, y=data, name=name, *args, **kwargs)
                self.addItem(plot_data_item)
            if self._region_plot is not None:
                if self._show_region:
                    self._region_plot.update_data(plot_data_item.name, plot_data_item.getData(),
                                                  plot_data_item.opts['pen'])
            return plot_data_item

    def set_statistics(self, data, name="default", x=None):
        """
        Input data into statistics window.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param name: Name of the data object
        :type name: str, optional
        :param x: Not used.
        :type x: list: optional
        """

        if self._statistics is not None:
            self._statistics.set_data(data, name=name, x=x)

    def set_fitting(self, data, name="default", x=None, xmin=None, xmax=None):
        """
        Input data into fitting window.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param name: Name of the data object.
        :type name: str, optional
        :param x: Data x input.
        :type x: list: optional
        :param xmin: X min to be used.
        :type xmin: float: optional
        :param xmax: X max to be used.
        :type xmax: float: optional
        """

        if self._fitting is not None:
            self._fitting.set_data(data, name=name, x=x, xmin=xmin, xmax=xmax)

    def fit_to_step(self, step=1, nleft=10, nright=1):
        """
        Move x range automatically using step range.

        :param step: Step to be used for adjusting x min,max (default=1).
        :type step: float: optional
        :param nleft: Extra left side of x min.
        :type nleft: int: optional
        :param nright: Extra right side of x max..
        :type nright: int: optional
        """

        if step == 0:
            return

        xminc = sys.float_info.max
        xmaxc = -sys.float_info.max

        for item in self.listDataItems():
            if item.isVisible():
                try:
                    if "Fast" in str(type(item)):
                        dataset = (item.plotData.x, item.plotData.y)
                    else:
                        dataset = (item._dataset.x, item._dataset.y)

                    bar = False
                except:
                    dataset = item.getData()
                    bar = True
                try:
                    if xminc > dataset[0][0]:
                        xminc = dataset[0][0]
                    if xmaxc < dataset[0][-1]:
                        xmaxc = dataset[0][-1]
                except:
                    pass
                if bar:
                    if len(dataset[0]) > 0:
                        delta = (xmaxc - xminc) / (2 * (len(dataset[0]) - 1))
                        xminc = xminc - delta
                        xmaxc = xmaxc + delta

        if xmaxc <= xminc:
            xmaxc = xminc + 1

        vr_xmin = self.viewRange()[0][0]
        vr_xmax = self.viewRange()[0][1]

        if xmaxc > (vr_xmax - step):
            if step != 1:
                xminc = xmaxc - nleft * step
            xmaxc = xmaxc + nright * step
            self.setXRange(xminc, xmaxc)

    def dynamic_scaling(self):
        self._dynamic_scaling = not self._dynamic_scaling
        self.update()

    def fit_to_data(self, xmin=None, xmax=None, ymin=None, ymax=None):
        """
        Fit scale to data.

        :param xmin: X min.
        :type xmin: float: optional
        :param ymin: Y min.
        :type ymin: float: optional
        :param xmax: X max.
        :type xmax: float: optional
        :param ymax: Y max.
        :type ymax: float: optional
        """

        xminc = sys.float_info.max
        xmaxc = -sys.float_info.max
        yminc = sys.float_info.max
        ymaxc = -sys.float_info.max

        for item in self.listDataItems():

            if item.isVisible():

                try:
                    if "Fast" in str(type(item)):
                        dataset = (item.plotData.x, item.plotData.y)
                    elif  isinstance(item, BarGraphItem):
                        bar = True
                        dataset = item.getData()
                    else:
                        dataset = (item._dataset.x, item._dataset.y)
                    bar = False
                except Exception as error:
                    print(error)

                tminx = dataset[0][0]
                tmaxx = dataset[0][-1]

                npdatay = np.array(dataset[1])
                tminy = np.min(npdatay)
                tmaxy = np.max(npdatay)
                try:
                    if xminc > tminx:
                        xminc = tminx
                    if xmaxc < tmaxx:
                        xmaxc = tmaxx
                    if yminc > tminy:
                        yminc = tminy
                    if ymaxc < tmaxy:
                        ymaxc = tmaxy
                except:
                    pass

                if bar:
                    if len(dataset[0]) > 1:
                        delta = (xmaxc - xminc) / (2 * (len(dataset[0]) - 1))
                        xminc = xminc - delta
                        xmaxc = xmaxc + delta
                    else:
                        return

        if xmin is not None:
            xminc = xmin
        if ymin is not None:
            yminc = ymin
        if xmax is not None:
            xmaxc = xmax
        if ymax is not None:
            ymaxc = ymax
        if xmaxc <= xminc:
            xmaxc = xminc + 1
        if ymaxc <= yminc:
            ymaxc = yminc + 1

        try:
            self.setXRange(xminc, xmaxc, update=False)
            self.setYRange(yminc, ymaxc)
        except:
            pass

    def get_region(self, name="default"):
        """
        Get region by its name

        :param name: Name of the region.
        :type name: str, optional
        """

        if name in self._regions:
            return self._regions[name]
        return None

    def show_region(self):
        """
        Show region plot component.
        """

        if self._name_region_selected is not None:
            self._show_region = not self._show_region
            if self._region_plot is not None:
                if self._show_region:
                    for item in self.listDataItems():
                        if isinstance(item, pg.PlotDataItem):
                            self._region_plot.update_data(item.name, item.getData(), item.opts['pen'])
                    self._region_plot.show()
                else:
                    self._region_plot.hide()

    def show_legend(self):
        """
        Show legend on top of the plot.
        """

        if self._parent is None:
            return

        self._show_legend = not self._show_legend

        if self._show_legend:
            self._plotLegend = self.addLegend()
            for name in self._parent._plot_data_items[self._name].keys():
                self._plotLegend.addItem(self._parent._plot_data_items[self._name][name], name)
            self._plotLegend.setLabelTextSize('6pt')
            self._plotLegend.setColumnCount(6)
            self._plotLegend.updateSize()
        else:
            self._plotLegend.clear()

        self.update()

    def get_list(self):
        """
        Get list plot items
        """
        if self._list_in_plot_item is None:
            self._list_in_plot_item = _ListInPlotItem(self)

        return self._list_in_plot_item

    def show_list(self):
        """
        Show list of plot items.
        """

        self._show_list = not self._show_list

        if self._list_in_plot_item is None:
            self._list_in_plot_item = _ListInPlotItem(self)
        if self._show_list:
            self._list_in_plot_item.show()
        else:
            self._list_in_plot_item.hide()

    def show_cursors(self):
        """
        Show list of cursors.
        """

        self._show_cursors = not self._show_cursors

        if self._list_cursors_in__item is None:
            self._list_cursors_in__item = _CursorsItem(self)
        self._list_cursors_in__item.update()
        if self._show_cursors:
            self._list_cursors_in__item.show()
        else:
            self._list_cursors_in__item.hide()

    def show_scaling(self):
        """
        Show scaling parameters.
        """

        self._show_scaling = not self._show_scaling

        if self._scaling is None:
            self._scaling = _Scale(self)
        self._scaling.update()
        if self._show_scaling:
            self._scaling.show()
        else:
            self._scaling.hide()

    def set_show_value_axis(self, show_axis):
        """
        Show/hide x/y axis.

        ;param show_axis: Show or not plot axis.
        :type show_axis: bool
        """

        self._show_value_axis = show_axis

        self.getAxis("left").setStyle(showValues=self._show_value_axis)
        self.getAxis("top").setStyle(showValues=self._show_value_axis)
        self.getAxis("right").setStyle(showValues=self._show_value_axis)
        self.getAxis("bottom").setStyle(showValues=self._show_value_axis)

    def show_axis(self):
        """
        Handling of the show/hide x/y axis.
        """

        self._show_value_axis = not self._show_value_axis
        self.set_show_value_axis(self._show_value_axis)

    def dark_(self):
        """
        Set dark theme.
        """

        self._light = False

        if not self.multi_axis:
            self.getViewBox().setBackgroundColor(Colors.COLOR_LBLACK)

        if self._vline_dyn is not None:
            self._vline_dyn.setPen(self.pen_cursor_line_dyn_dark)
            self._hline_dyn.setPen(self.pen_cursor_line_dyn_dark)
        if self._vline_static is not None:
            self._vline_static.setPen(self.pen_cursor_line_dyn_dark)
            self._hline_static.setPen(self.pen_cursor_line_dyn_dark)

        for item in self.items:
            if isinstance(item, pg.TextItem):
                item.setColor(Colors.COLOR_DWHITE)

        if self._region_plot is not None:
            self._region_plot.dark_()

        for region_arrow in self._region_arrow.values():
            region_arrow[0].setPen(Colors.COLOR_DWHITE)
            region_arrow[0].setSymbolPen(Colors.COLOR_DWHITE)
            region_arrow[0].setSymbolBrush(Colors.COLOR_DWHITE)
            region_arrow[1].setStyle(brush=Colors.COLOR_DWHITE)
            region_arrow[2].setStyle(brush=Colors.COLOR_DWHITE)

        if self._statistics is not None:
            self._statistics.dark_()
        if self._fitting is not None:
            self._fitting.dark_()

    def light_(self):
        """
        Set light theme.
        """

        self._light = True

        if not self.multi_axis:
            self.getViewBox().setBackgroundColor(Colors.COLOR_WHITE)

        if self._vline_dyn is not None:
            self._vline_dyn.setPen(self.pen_cursor_line_dyn)
            self._hline_dyn.setPen(self.pen_cursor_line_dyn)
        if self._vline_static is not None:
            self._vline_static.setPen(self.pen_cursor_line_dyn_dark)
            self._hline_static.setPen(self.pen_cursor_line_dyn_dark)

        for item in self.items:
            if isinstance(item, pg.TextItem):
                item.setColor(Colors.COLOR_LBLACK)

        if self._region_plot is not None:
            self._region_plot.light_()

        for region_arrow in self._region_arrow.values():
            region_arrow[0].setPen(Colors.COLOR_LBLACK)
            region_arrow[0].setSymbolPen(Colors.COLOR_LBLACK)
            region_arrow[0].setSymbolBrush(Colors.COLOR_LBLACK)
            region_arrow[1].setStyle(brush=Colors.COLOR_LBLACK)
            region_arrow[2].setStyle(brush=Colors.COLOR_LBLACK)

        if self._statistics is not None:
            self._statistics.light_()
        if self._fitting is not None:
            self._fitting.light_()

    def set_grid_pos(self, col, row):
        """
        Set grid positions row/col.

        :param col: Col position.
        :type col: int
        :param row: Row position.
        :type row: int
        """

        self._row = row
        self._col = col

    def mousePressEvent(self, e):
        """
        Mouse press event.
        """

        if self.clickable:
            self._drag_pos = e.pos()
            if self._dyn_cursor:
                gapx = 5
                gapy = 5
                if self._col != 0:
                    gapx = self._parent.width() / (self._col + 1)
                if self._row != 0:
                    gapy = self._parent.height() / (self._row + 1)
                pos = QPointF((e.pos().x() + gapx), (e.pos().y() + gapy))
                mousePoint = self.getViewBox().mapSceneToView(pos)
                if self._vline_dyn is None:
                    self._vline_dyn = pg.InfiniteLine(angle=90, movable=False, pen=self.pen_cursor_line_dyn)
                    self._hline_dyn = pg.InfiniteLine(angle=0, movable=False, pen=self.pen_cursor_line_dyn)
                    self._vline_dyn.setZValue(10000)
                    self._hline_dyn.setZValue(10000)
                    self.addItem(self._vline_dyn, ignoreBounds=True)
                    self.addItem(self._hline_dyn, ignoreBounds=True)
                self._vline_dyn.setPos(mousePoint.x())
                self._hline_dyn.setPos(mousePoint.y())
                if self._image and not self._roi:
                    x = mousePoint.x()
                    y = mousePoint.y()
                    self.target_image_item.setPos(x, y)
                    self.target_image_item.show()
                    self._vline_static.setPos(x)
                    self._hline_static.setPos(y)
                    self.mousepress_signal.emit(e)
                self.show()

        super().mousePressEvent(e)

    # def mouseReleaseEvent(self, e, pressed):
    #     """
    #     Mouse release event.
    #     """
    #
    #     if pressed:
    #         return
    #
    #     print("released",self._pressed)
    #
    #     self._pressed = 0
    #     # if self.clickable:
    #     #
    #     #     gapx = 5
    #     #     gapy = 5
    #     #
    #     #     if self._col != 0:
    #     #         gapx = self._parent.width() / (self._col + 1)
    #     #     if self._row != 0:
    #     #         gapy = self._parent.height() / (self._row + 1)
    #     #
    #     #     if self._drag_pos is not None:
    #     #         pos = QPoint(int(self._drag_pos.x() + gapx), int(self._drag_pos.y() + gapy))
    #     #         mousePoint = self.getViewBox().mapSceneToView(pos)
    #     #     else:
    #     #         return
    #     #
    #     #     if self._image:
    #     #         x = int(mousePoint.x())
    #     #         y = int(mousePoint.y())
    #     #         self.target_image_item.setPos(x, y)
    #     #         self.target_image_item.show()
    #     #         self.mousepress_signal.emit(e)
    #     #         if self._vline_static is None:
    #     #             self._vline_static = pg.InfiniteLine(angle=90, movable=False, pen=self.pen_cursor_line_dyn)
    #     #             self._hline_static = pg.InfiniteLine(angle=0, movable=False, pen=self.pen_cursor_line_dyn)
    #     #             self._vline_static.setZValue(11000)
    #     #             self._hline_static.setZValue(11000)
    #     #             self.addItem(self._vline_static, ignoreBounds=True)
    #     #             self.addItem(self._hline_static, ignoreBounds=True)
    #     #         self._vline_static.setPos(x)
    #     #         self._hline_static.setPos(y)
    #     #
    #     #     self.show()

    def mouseDoubleClickEvent(self, e):
        """
        Mouse click event.
        """

        super().mouseDoubleClickEvent(e)

        gapx = 5
        gapy = 5

        if self._col != 0:
            gapx = self._parent.width() / (self._col + 1)
        if self._row != 0:
            gapy = self._parent.height() / (self._row + 1)

        # pos = QPoint(int(e.pos().x() + gapx), int(e.pos().y() + gapy))
        pos = QPointF((e.pos().x() + gapx), (e.pos().y() + gapy))
        mousePoint = self.getViewBox().mapSceneToView(pos)

        if self._image:
            posx = mousePoint.x()
            posy = mousePoint.y()
        else:
            posx = mousePoint.x()
            posy = mousePoint.y()

        xmin = self.getViewBox().viewRange()[0][0]
        xmax = self.getViewBox().viewRange()[0][1]
        ymin = self.getViewBox().viewRange()[1][0]
        ymax = self.getViewBox().viewRange()[1][1]
        resx = (xmax - xmin) / 100.
        resy = (ymax - ymin) / 100.
        found = False
        name_found = "cursors"
        index_found = None

        for name in self._scatters.keys():
            ind = 0
            for spot in self._scatters[name].data:
                xspot = spot[0]
                yspot = spot[1]
                if abs(posx - xspot) < resx and abs(posy - yspot) < resy:
                    found = True
                    name_found = name
                    index_found = ind
                    break
                ind = ind + 1

        if not found:
            self.add_marker_points(name=name_found, posx=[posx], posy=[posy])
            self.show()
        else:
            self.remove_marker_point(name=name_found, index=index_found)
            self.show()

    def abs(self, val):
        """
        Absolute value.

        :param val: Value to process.
        :type val: float or int

        :return: return absolute value.
        :rtype: float or int
        """

        if val < 0:
            return -val
        return val

    def set_hoverable(self, hoverable):
        self.hoverable = hoverable

    def set_draggable(self, draggable):
        self.draggable = draggable

    def set_clickable(self, clickable):
        self.clickable = clickable

    # def mouseDragEvent(self, ev):
    #     print("drag")
    # #
    #     if not self.draggable:
    #         return
    #
    #     if ev.button() == Qt.RightButton:
    #         ev.ignore()
    #     else:
    #         pass
    #         if not ev.isFinish():
    #             self._drag = True
    #         else:
    #             self._drag = False
    #         pg.ViewBox.mouseDragEvent(self, ev)

    def hoverEvent(self, e):
        """
        Mouse hover event.
        """

        if not self.hoverable:
            return

        hover = False
        if not e.isExit():
            if not self.draggable:
                if e.acceptDrags(Qt.LeftButton):
                    hover = True
            else:
                hover = True

        if not hover:
            return

        if self._drag:
            return

        gapx = 5
        gapy = 5
        xf = 0
        yf = 0

        if self._col != 0:
            gapx = self._parent.width() / (self._col + 1)
        if self._row != 0:
            gapy = self._parent.height() / (self._row + 1)

        try:
            pos = QPointF((e.pos().x() + gapx), (e.pos().y() + gapy))
        except:

            if self._title is not None:
                if len(self._title) > 26:
                    half = int(len(self._title) / 2)
                    self.setTitle("<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + self._title[0:half] + "<br>" + self._title[half:] + "</span>")
                else:
                    self.setTitle("<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + self._title + "</span>")                      
            else:
                self.setTitle(None)
            return

        inscreen = True
        if (pos.x() < 5) or (pos.y() < 5) or (pos.x() > self.sceneBoundingRect().width() - 5) or (
                pos.y() > self.sceneBoundingRect().height() - 5):
            inscreen = False

        if self.sceneBoundingRect().contains(pos) and inscreen:

            mousePoint = self.getViewBox().mapSceneToView(pos)
            if self._vline_dyn is None:
                self._vline_dyn = pg.InfiniteLine(angle=90, movable=False, pen=self.pen_cursor_line_dyn)
                self._hline_dyn = pg.InfiniteLine(angle=0, movable=False, pen=self.pen_cursor_line_dyn)
                self._vline_dyn.setZValue(10000)
                self._hline_dyn.setZValue(10000)
                self.addItem(self._vline_dyn, ignoreBounds=True)
                self.addItem(self._hline_dyn, ignoreBounds=True)

            index = mousePoint.x()
            index_y = mousePoint.y()
            y = sys.float_info.max
            x = sys.float_info.max
            plot_index = 0

            if self.glue_cursor:
                if self._image == False and not self._roi:
                    if self._list_in_plot_item is not None:
                        index_plot = 0
                        for rb in self._list_in_plot_item._rb_cursor:
                            if rb.isChecked():
                                plot_index = index_plot
                            index_plot = index_plot + 1
                        if len(self._list_in_plot_item._rb_cursor_item) > 0:
                            item = self._list_in_plot_item._rb_cursor_item[plot_index]
                            item.setZValue(19999)
                            try:
                                if not self._swap:
                                    # Normal x search
                                    index2 = np.where(item.xData >= index)[0][0]
                                    if len(item.xData) > index2 + 1:
                                        index3 = index - (item.xData[index2 + 1] - item.xData[index2]) / 2
                                        index2 = np.where(item.xData >= index3)[0][0]
                                    index = item.xData[index2]
                                    if y == sys.float_info.max and index2 < len(item.yData):
                                        y = item.yData[index2]
                                        yf = y
                                else:
                                    # Swap y search
                                    index2 = np.where(item.yData >= index_y)[0][0]
                                    if len(item.yData) > index2 + 1:
                                        index3 = index - (item.yData[index2 + 1] - item.yData[index2]) / 2
                                        index2 = np.where(item.yData >= index3)[0][0]
                                    index_y = item.yData[index2]
                                    if x == sys.float_info.max and index2 < len(item.xData):
                                        x = item.xData[index2]
                                        xf = x
                            except:
                                pass
                    else:
                        for item in self.listDataItems():
                            if item.name() is not None:
                                try:
                                    if not self._swap:
                                        # Normal x search
                                        index2 = np.where(item.xData >= index)[0][0]
                                        if len(item.xData) > index2 + 1:
                                            index3 = index - (item.xData[index2 + 1] - item.xData[index2]) / 2
                                            index2 = np.where(item.xData >= index3)[0][0]
                                        index = item.xData[index2]
                                        if y == sys.float_info.max and index2 < len(item.yData):
                                            if index2 < len(item.yData):
                                                y = item.yData[index2]
                                                yf = y
                                        break
                                    else:
                                        # Swap y search
                                        index2 = np.where(item.yData >= index_y)[0][0]
                                        if len(item.yData) > index2 + 1:
                                            index3 = index_y - (item.yData[index2 + 1] - item.yData[index2]) / 2
                                            index2 = np.where(item.yData >= index3)[0][0]
                                        index_y = item.yData[index2]
                                        if x == sys.float_info.max and index2 < len(item.xData):
                                            if index2 < len(item.yData):
                                                x = item.xData[index2]
                                                xf = x
                                        break
                                except Exception as e:
                                    continue
                else:
                    x = mousePoint.x()
                    index = x
                    y = mousePoint.y()
                    index_y = y
                    xf = x
                    yf = y
            else:
                x = mousePoint.x()
                index = x
                y = mousePoint.y()
                index_y = y
                xf = x
                yf = y

            if not self._swap and y != sys.float_info.max:
                self._vline_dyn.setPos(index)
                self._hline_dyn.setPos(y)
                if not self._roi:
                    self.target_item.setPos(index, y)
                    self.target_item.show()
                xf = index
                if self._time_in_chart_final:
                    time_index = QDateTime()
                    time_index.setMSecsSinceEpoch(int(index * 1000))
                    str_x = time_index.toString()
                else:
                    str_x = "%0.4f" % index
                str_y = "%0.4f" % y
            elif self._swap and x != sys.float_info.max:
                self._hline_dyn.setPos(index_y)
                self._vline_dyn.setPos(x)
                if not self._roi:
                    self.target_item.setPos(x, index_y)
                    self.target_item.show()
                yf = index_y
                if self._time_in_chart_final:
                    time_index = QDateTime()
                    time_index.setMSecsSinceEpoch(int(x * 1000))
                    str_x = time_index.toString()
                else:
                    str_x = "%0.4f" % x
                str_y = "%0.4f" % index_y
            else:
                str_x = "%0.4f" % mousePoint.x()
                str_y = "%0.4f" % mousePoint.y()
                xf = mousePoint.x()
                yf = mousePoint.y()

            try:
                xscale = self._parentimage.xscale
                yscale = self._parentimage.yscale
                xoffset = self._parentimage.xoffset
                yoffset = self._parentimage.yoffset
                if xscale != 0. and yscale != 0.:
                    xz = (xf + xoffset) / xscale
                    yz = (yf + yoffset) / yscale
                    z = self._parentimage._data[int(xz)][int(yz)]
                    str_z = "%0.4f" % z
                else:
                    str_z = ""
            except:
                str_z = ""

            if self.sceneBoundingRect().width() > 300:
                if str_z != "":
                    self.setTitle(
                        "<span style='font-size:8pt;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";'>x: </span><span style='color:" + Colors.STR_COLOR_BLUE + ";font-size: 8pt'>" + str_x + \
                        ", </span><span style='font-size:8pt;'>y: </span><span style='font-size:8pt;color:" + Colors.STR_COLOR_BLUE + ";'>" + str_y + "</span>" + \
                        ", <span style='font-size:8pt;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";'>z: </span><span style='color:" + Colors.STR_COLOR_BLUE + ";font-size: 8pt'>" + str_z + "</span>")
                else:
                    self.setTitle(
                        "<span style='font-size: 8pt;color:" + Colors.STR_COLOR_LIGHT3GRAY + ";'>x: </span><span style='color:" + Colors.STR_COLOR_BLUE + ";font-size: 8pt'>" + str_x + \
                        ", </span><span style='font-size: 8pt;'>y: </span><span style='font-size: 8pt;color:" + Colors.STR_COLOR_BLUE + ";'>" + str_y + "</span>")

            for name in self._regions:
                self.update_region(name)

        else:

            self.target_item.hide()

            if self._title is not None:
                if len(self._title) > 26:
                    half = int(len(self._title) / 2)
                    self.setTitle("<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + self._title[0:half] + "<br>" + self._title[half:] + "</span>")
                else:
                    self.setTitle("<div style='color:" + Colors.STR_COLOR_BLUE + ";font-weight:bold;font-size:6pt;'>" + self._title + "</span>")
            else:
                self.setTitle(None)

    def get_region(self, name="default"):
        """
        Return selected region.

        :param name: Name of the plot.
        :type name: str, optional
        """

        if name in self._regions:
            return self._regions[name]
        return None

    def del_region(self, name="default"):
        """
        Return selected region.

        :param name: Name of the plot.
        :type name: str, optional
        """

        if name in self._regions:
            self.removeItem(self._regions[name])
            del self._regions[name]

    def add_region(self,
                   name="default",
                   orientation='vertical',
                   min=0,
                   max=0,
                   pen=Colors.COLOR_BLUEGRAYT1,
                   brush=Colors.COLOR_BLUEGRAYT2,
                   movable=True,
                   label=None,
                   font_size=8,
                   arrow=False):
        """
        Add a region.

        :param name: Name of the plot.
        :type name: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param min: Minimum range (default=0)
        :type min: float, optional
        :param max: Maximum range (default=1)
        :type max: float, optional
        :param pen: Color region lines (default=BLUEGRAYT1).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=BLUEGRAYT2).
        :type brush: QColor, optional
        :param movable: Region movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        :param arrow: Show or not arrows inside region (defaul=False).
        :type arrow: bool, optional
        """

        self._name_region_selected = name

        if self._region_plot is None:
            self._region_plot = _RegionPlotItem(self, self._name)
            if self._light:
                self._region_plot.light_()
            else:
                self._region_plot.dark_()

        region = pg.LinearRegionItem(brush=brush, pen=pen, orientation=orientation, movable=movable)
        region.setZValue(10)
        self.addItem(region, ignoreBounds=True)
        self._regions[name] = region

        if label is not None:
            text = pg.TextItem(html=str(label))
            font = self.font()
            font.setPointSize(font_size)
            font.setItalic(True)
            text.setFont(font)
            if self._light:
                text.setColor(Colors.COLOR_LBLACK)
            else:
                text.setColor(Colors.COLOR_DWHITE)
            self.addItem(text)
            self._regions_text[name] = text

        region.sigRegionChanged.connect(lambda: self.update_region(name))
        region.setRegion([min, max])

        if arrow:
            ymax = self.getViewBox().viewRange()[1][1]
            ymin = self.getViewBox().viewRange()[1][0]
            posarrow = ymin + 0.935 * (ymax - ymin)
            col_arrow = Colors.COLOR_DWHITE
            if self._light:
                col_arrow = Colors.COLOR_LBLACK
            line = self.plot([min, max], [posarrow, posarrow], pen=col_arrow, symbol=None, symbolPen=col_arrow,
                             symbolBrush=col_arrow)
            line.setZValue(-1)
            arrowl = pg.ArrowItem(angle=0, tipAngle=30, baseAngle=0, headLen=10, tailLen=0, tailWidth=0, pen=None,
                                  brush=col_arrow)
            arrowl.setPos(min, posarrow)
            self.addItem(arrowl)
            arrowr = pg.ArrowItem(angle=180, tipAngle=30, baseAngle=0, headLen=10, tailLen=0, tailWidth=0, pen=None,
                                  brush=col_arrow)
            arrowr.setPos(max, posarrow)
            self.addItem(arrowr)
            self._region_arrow[name] = [line, arrowl, arrowr]

    def add_marker_line(self,
                        name="default",
                        orientation='vertical',
                        pos=0,
                        pen=Colors.COLOR_LPINKT,
                        movable=True,
                        angle=-1,
                        label=None, font_size=8):
        """
        Add a marker line (infinite) H or V.

        :param name: Name of the plot.
        :type name: str, optional
        :param orientation: Vertical or horizontal linear region (default="vertical").
        :type orientation: str, optional
        :param pos: Position of the marker (default=0)
        :type pos: float, optional
        :param pen: Color marker line (default=LPINKT).
        :type pen: QColor, optional
        :param movable: Marker movable or not (default=True).
        :type movable: bool, optional
        :param label: Label shown in the center of the region.
        :type label: str, optional
        :param font-size: Font size of the label (default=8).
        :type font_size: int, optional
        :param angle: Not used.
        :type angle: float, optional
        """

        angle = -1

        if angle == -1:
            angle = 0
            if orientation == "vertical":
                angle = 90

        if name not in self._markers:
            infinite_line = pg.InfiniteLine(pos=pos, angle=angle, movable=movable, pen=pen, name=name, label=label,
                                            labelOpts={'position': 0.9, 'rotateAxis': (1, 0), 'anchor': (1, 1)})
            try:
                if infinite_line.label is not None:
                    fo = QFont()
                    fo.setPointSize(font_size)
                    infinite_line.label.setFont(fo)
            except:
                pass
            self.addItem(infinite_line, ignoreBounds=True)
            self._markers[name] = infinite_line
        else:
            self._markers[name].setValue(pos)

    def remove_marker_points(self, name="default"):
        """
        Remove all marker points

        :param name: Name of the plot.
        :type name: str, optional
        """

        if name in self._scatters.keys():
            x = []
            y = []
            self._scatters[name].clear()
            self.add_marker_points(name=name, posx=x, posy=y)
            self.show()

    def remove_marker_point(self, name="default", index=0):
        """
        Remove a marker point after dbl click.

        :param name: Name of the plot.
        :type name: str, optional
        :param index: Remove marker[index] (default=0)
        :type index: int, optional
        """

        if name in self._scatters.keys():
            data = np.delete(self._scatters[name].data, [index])
            x = []
            y = []
            for spot in data:
                x.append(spot[0])
                y.append(spot[1])
            self._scatters[name].clear()
            self.add_marker_points(name=name, posx=x, posy=y)
            self.show()

    def add_marker_points(self,
                          name="default",
                          posx=[],
                          posy=[],
                          pen=Colors.COLOR_DORANGE,
                          brush=None,
                          size=10,
                          symbol='crosshair'):
        """
        Add list markers.

        :param name: Name of the plot.
        :type name: str, optional
        :param posx: List of x positions
        :type posx: list
        :param posy: List of y positions
        :type posy: list
        :param pen: Color marker line (default=LPINK).
        :type pen: QColor, optional
        :param brush: Color region brush inside (default=LPINK).
        :type brush: QColor, optional
        :param size: Size symbol (default=6)
        :type size: int, optional
        :param symbol: Type of symbol (default="o")
        :type symbol: str, optional
        """

        if posx is None or len(posx) == 0 or len(posy) == 0:
            return

        if name not in self._scatters.keys():
            scatter = pg.ScatterPlotItem(size=size, pen=pen, symbol=symbol, brush=brush)
            self._scatters[name] = scatter
            self.addItem(scatter)

        self._scatters[name].addPoints(x=posx, y=posy)

        self.show()

    def update_region(self, name):
        """
        Update region info + region plot item linked to this component.

        :param name: Name of the region.
        :type name: str
        """

        self._name_region_selected = name

        region = self._regions[name]
        region.setZValue(10)

        minX, maxX = region.getRegion()
        xmin = self.getViewBox().viewRange()[0][0]
        xmax = self.getViewBox().viewRange()[0][1]
        ymin = self.getViewBox().viewRange()[1][0]
        ymax = self.getViewBox().viewRange()[1][1]

        if name in self._regions_text.keys():
            self._regions_text[name].setPos((maxX + minX) / 2, ymin + 0.98 * (ymax - ymin))

        if name in self._region_arrow:
            posarrow = ymin + 0.92 * (ymax - ymin)
            line = self._region_arrow[name][0]
            line.setData(x=[minX, maxX], y=[posarrow, posarrow])
            arrowl = self._region_arrow[name][1]
            arrowl.setPos(minX, posarrow)
            arrowr = self._region_arrow[name][2]
            arrowr.setPos(maxX, posarrow)

        self._region_plot.update_range(minX, maxX)

    def is_item_visible(self, name):
        """
        Check if the checkbox for the plot visibility.

        :param name: Name of the plot.
        :type name: str
        """

        if self._list_in_plot_item is not None:
            return self._list_in_plot_item.is_checkbox_selected(name)
        return True

    def check_visibility(self, name):
        """
        Make sure the plot is visible if checkbox selected.

        :param name: Name of the plot.
        :type name: str
        """

        if self._list_in_plot_item is None:
            return

        if not self._list_in_plot_item._check_boxes_by_name[name].isChecked():
            self._list_in_plot_item._check_boxes_item[name].curve.hide()
        else:
            self._list_in_plot_item._check_boxes_item[name].curve.show()

    def get_cursor(self):
        """
        Get cursor target pos.
        """
        return self.target_image_item

    def get_mousepressevent(self):
        """
        Get mouse press event signal.
        """
        return self.mousepress_signal


class _RegionPlotItem(QWidget):
    """
    Frame to show region.

    :param parent: Parent object
    :type parent: object
    :param name: Name to identify the region (default="").
    :type name: strl, optional
    """

    def __init__(self, parent=None, name=""):
        super().__init__()

        self._parent = parent
        self.setWindowTitle("Region " + name)
        self._layout = QGridLayout()
        self.setLayout(self._layout)
        self._win = pg.GraphicsLayoutWidget(show=False)
        self._plot_item = self._win.addPlot(row=0, col=0)
        self._layout.addWidget(self._win, 0, 0)
        self.resize(500, 250)

    def show(self):
        """
        Show frame.
        """

        super().show()

    def closeEvent(self, event):
        """
        Close/hide frame.
        """

        self._parent._show_region = False
        self.hide()
        event.ignore()

    def clear_items(self):
        """
        Clear all items.
        """

        self._plot_item.clear()

    def update_range(self, minX, maxX):
        """
        Update range in plot.

        :param minX: Min x.
        :type minX: float
        :param maxX: Max x.
        :type maxX: float
        """

        self._plot_item.setXRange(minX, maxX, padding=0)

    def update_data(self, name, data, pen=None):
        """
        Update data in plot.

        :param name: Name of the plot.
        :type name: str
        :param data: (x,y) values.
        :type data: list
        :param pen: Color pen of the plot curve.
        :type pen: QPen, optional
        """

        self._plot_item.clear()
        self._plot_item.plot(x=data[0], y=data[1], name=name, pen=pen)

    def light_(self):
        """
        Set light theme.
        """

        self._win.setBackground(Colors.COLOR_WHITE)

    def dark_(self):
        """
        Set dark theme.
        """

        self._win.setBackground(Colors.COLOR_LBLACK)


class _CursorsItem(QWidget):
    """
    Frame to display list of cursors in plot item

    :param parent: Parent object
    :type parent: object
    """

    def __init__(self, parent=None):
        super().__init__()

        self._parent = parent
        self.setWindowTitle("Cursors " + parent._name)

        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)

        self._scatters = parent._scatters

        self._enum_names = QComboBox()
        self._names = []
        self._list_names = QListView(self._enum_names)
        self._enum_names.setView(self._list_names)
        self._enum_names.setEditable(False)
        self._enum_names.currentTextChanged.connect(self.update)

        self.gridLayout.addWidget(self._enum_names, 0, 0)

        self._data_table = {}
        data = {}
        index = []
        self._df = pd.DataFrame(data, index=index)
        frame_table = QFrame()

        from expert_gui_core.gui.widgets.pyqt import tablewidget
        self._table = tablewidget.TableWidget(parent=frame_table)
        self._table.set_data(self._df)

        self.gridLayout.addWidget(self._table, 1, 0)

        self.gridLayout.addWidget(QLabel(""), 2, 0)

        self.gridLayout.setRowStretch(0, 1)

        self.update()

        self.resize(800, 300)

    def update(self):

        # update list

        for name in self._scatters.keys():
            if name not in self._names:
                self._enum_names.addItem(name)
                self._names.append(name)

        i = 0
        name = self._enum_names.currentText()
        namecols = ["X", "Y", "dX", "dY"]

        if name in self._scatters.keys():
            scatter = self._scatters[name]
        else:
            return

        self._data_table = {}
        self._df = pd.DataFrame({}, index=[], columns=[])

        val_sort = []

        if len(scatter.getData()) > 0:
            sorted_idxs = np.argsort(scatter.getData()[0])
            val_sort.append(scatter.getData()[0][sorted_idxs])
            val_sort.append(scatter.getData()[1][sorted_idxs])

        for i in range(0, 4):

            if i < 2:
                values = val_sort[i]
            else:
                values = []
                ind = 0
                if len(val_sort[i - 2]) > 0:
                    valprev = val_sort[i - 2][0]
                    for val in val_sort[i - 2]:
                        values.append(val - valprev)
                        valprev = val

            namecol = namecols[i]

            if self._df.values.shape[0] != len(values) or (namecol not in self._df.columns):

                self._data_table[namecol] = values

                index = []

                for j in range(len(values)):
                    index.append(str(j + 1))

                    # clear df

                self._df.squeeze()

                # create df

                self._df = pd.DataFrame(self._data_table, index=index)

                # inject

                self._table.set_data(self._df)

            else:

                # inject

                self._df[namecol] = values

        def light_(self):
            """
            Set light theme.
            """

            pass

        def dark_(self):
            """
            Set dark theme.
            """

            pass

    def show(self):
        """
        Show frame.
        """

        super().show()

    def closeEvent(self, event):
        """
        Close/hide frame.
        """

        self._parent._show_cursors = False
        self.hide()
        event.ignore()


class _Scale(QWidget):
    """
    Frame to display scaling  plot item

    :param parent: Parent object
    :type parent: object
    """

    def __init__(self, parent=None):
        super().__init__()

        self._parent = parent
        self.setWindowTitle("Scaling " + parent._name)

        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)

        self._scatters = parent._scatters

        self._enum_names = QComboBox()
        self._names = []
        self._list_names = QListView(self._enum_names)
        self._enum_names.setView(self._list_names)
        self._enum_names.setEditable(False)
        self._enum_names.currentTextChanged.connect(self.update)

        # self.gridLayout.addWidget(self._enum_names,0,0)

        # self._data_table = {}
        # data = {}
        # index = []
        # self._df = pd.DataFrame(data, index=index)
        # frame_table = QFrame()
        # self._table = tablewidget.TableWidget(parent=frame_table)
        # self._table.set_data(self._df)

        # self.gridLayout.addWidget(self._table,1,0)

        # auto scale
        # zmin
        # zmax

        self.gridLayout.addWidget(QLabel(""), 2, 0)

        self.gridLayout.setRowStretch(0, 1)

        self.update()

        self.resize(800, 300)

    def update(self):
        # update list

        pass

        # for name in self._scatters.keys():
        #     if name not in self._names:
        #         self._enum_names.addItem(name)
        #         self._names.append(name)

        # i=0
        # name = self._enum_names.currentText()
        # namecols = ["X","Y","dX","dY"]

        # if name in self._scatters.keys():
        #     scatter = self._scatters[name]
        # else:
        #     return

        # self._data_table = {}
        # self._df = pd.DataFrame({}, index=[], columns=[])

        # val_sort = []

        # if len(scatter.getData()) > 0:
        #     sorted_idxs = np.argsort(scatter.getData()[0])
        #     val_sort.append(scatter.getData()[0][sorted_idxs])
        #     val_sort.append(scatter.getData()[1][sorted_idxs])

        # for i in range(0,4):

        #     if i < 2:
        #         values = val_sort[i]
        #     else:
        #         values = []
        #         ind = 0
        #         if len(val_sort[i-2]) > 0:
        #             valprev = val_sort[i-2][0]
        #             for val in val_sort[i-2]:
        #                 values.append(val-valprev)
        #                 valprev = val

        #     namecol = namecols[i]

        #     if self._df.values.shape[0] != len(values) or (namecol not in self._df.columns):

        #         self._data_table[namecol] = values

        #         index = []

        #         for j in range(len(values)):

        #             index.append(str(j+1))

        #         # clear df

        #         self._df.squeeze()

        #         # create df

        #         self._df = pd.DataFrame(self._data_table, index=index)

        #         # inject

        #         self._table.set_data(self._df)

        #     else:

        #         # inject

        #         self._df[namecol] = values

        def light_(self):
            """
            Set light theme.
            """

            pass

        def dark_(self):
            """
            Set dark theme.
            """

            pass

    def show(self):
        """
        Show frame.
        """

        super().show()

    def closeEvent(self, event):
        """
        Close/hide frame.
        """

        self._parent._show_cursors = False
        self.hide()
        event.ignore()


class _ListInPlotItem(QWidget):
    """
    Frame to display list of items in plot item

    :param parent: Parent object
    :type parent: object
    """

    def __init__(self, parent=None):
        super().__init__()

        self._parent = parent
        self.setWindowTitle("List items " + parent._name)

        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)

        self._check_boxes = []
        self._rb_cursor = []
        self._rb_cursor_item = []
        self._check_boxes_by_name = {}
        self._check_boxes_ind_by_name = {}
        self._check_boxes_name_by_ind = {}
        self._check_boxes_item = {}

        self.createContent()

        self.resize(500, 800)

    def createContent(self):
        """
        Create content.
        """

        ind = 0
        ind_plot_item = 0
        self._check_boxes = []

        for i in range(self.gridLayout.count()):
            self.gridLayout.itemAt(0).widget().close()
            self.gridLayout.takeAt(0)

        label_plot = QLabel("all")
        label_plot.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        label_plot.setAlignment(Qt.AlignRight)
        self.gridLayout.addWidget(label_plot, ind, 0)

        checkbox_plot = QCheckBox("")
        checkbox_plot.setChecked(True)
        checkbox_plot.setMaximumWidth(18)
        checkbox_plot.toggled.connect(self.pressall)
        self._check_boxes.append(checkbox_plot)
        self.gridLayout.addWidget(checkbox_plot, ind, 1)

        self.gridLayout.setRowStretch(ind, 0)

        ind = ind + 1
        tot = 0
        for item in self._parent.listDataItems():
            if item.name() is not None:
                tot = tot + 1

        for item in self._parent.listDataItems():
            if item.name() is not None:
                try:
                    color = item.opts['pen'].color()
                except:
                    color = item.opts['symbolPen']
                extra_info = ""
                try:
                    extra_info = " " + str(item.getData()[0].shape) + " "
                except:
                    pass
                if ind == tot:
                    label_plot = QLabel("[" + str(tot) + "] " + item.name() + extra_info)
                else:
                    label_plot = QLabel(item.name() + extra_info)
                label_plot.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
                label_plot.setAlignment(Qt.AlignRight)
                self.gridLayout.addWidget(label_plot, ind, 0)
                checkbox_plot = QCheckBox("")
                checkbox_plot.setChecked(True)
                checkbox_plot.setMaximumWidth(18)
                checkbox_plot.setStyleSheet("background-color:" + color.name())
                checkbox_plot.toggled.connect(partial(self.press, ind_plot_item))
                self._check_boxes.append(checkbox_plot)
                self._check_boxes_by_name[item.name()] = checkbox_plot
                self._check_boxes_ind_by_name[item.name()] = ind_plot_item
                self._check_boxes_name_by_ind[ind_plot_item] = item.name()
                self._check_boxes_item[item.name()] = item
                self.gridLayout.addWidget(checkbox_plot, ind, 1)
                rb_cursor = QRadioButton()
                self._rb_cursor.append(rb_cursor)
                self._rb_cursor_item.append(item)
                if ind == 1:
                    rb_cursor.setChecked(True)
                    item.setZValue(19999)
                self.gridLayout.addWidget(rb_cursor, ind, 2)
                self.gridLayout.setRowStretch(ind, 0)
                ind = ind + 1
            ind_plot_item = ind_plot_item + 1

        self.gridLayout.addWidget(QLabel(""), ind, 0)
        self.gridLayout.setRowStretch(ind, 1)
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 0)

    def press_rb(self, ind):
        try:
            self._rb_cursor[ind].setChecked(True)
        except:
            pass

    def press_cb(self, ind, val=True):
        try:
            name = self._check_boxes_name_by_ind[ind]
            self._check_boxes_by_name[name].setChecked(val)
            self.press(ind, None)
        except:
            pass

    def is_checkbox_selected(self, name=""):
        """
        Check whether checkbox is selected.

        :param name: Name of the plot.
        "type name: str
        """

        if self._check_boxes_by_name is not None and name in self._check_boxes_by_name:
            if not self._check_boxes_by_name[name].isChecked():
                return False
        return True

    def pressall(self, e):
        """
        Press one checkbox.
        """

        for cb in self._check_boxes:
            cb.setChecked(self._check_boxes[0].isChecked())

    def press(self, ind, e):
        """
        Press one checkbox.
        """
        name = self._check_boxes_name_by_ind[ind]

        if not self._check_boxes_by_name[name].isChecked():
            self._check_boxes_item[name].curve.setVisible(False)
            for f in self._parent._parent._plot_data_items:
                for d in self._parent._parent._plot_data_items[f]:
                    if d == name:
                        self._parent._parent._plot_data_items[f][d].hide()
        else:
            self._check_boxes_item[name].curve.setVisible(True)
            for f in self._parent._parent._plot_data_items:
                for d in self._parent._parent._plot_data_items[f]:
                    if d == name:
                        self._parent._parent._plot_data_items[f][d].show()

    def show(self):
        """
        Show frame.
        """

        super().show()

    def closeEvent(self, event):
        """
        Close/hide frame.
        """

        self._parent._show_list = False
        self.hide()
        event.ignore()

    def light_(self):
        """
        Set light theme.
        """

        pass

    def dark_(self):

        """
        Set dark theme.
        """

        pass