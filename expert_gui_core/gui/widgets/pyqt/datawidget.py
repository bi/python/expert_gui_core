import sys
from time import time
import numpy as np
import pyqtgraph as pg
import pandas as pd
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QTimer

from PyQt5.QtGui import QPalette

from PyQt5.QtWidgets import QWidget, QApplication, QGridLayout, QMainWindow, QTabWidget, QFrame, QHeaderView

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import fesacomm
from expert_gui_core.comm.data import DataObject
from expert_gui_core.gui.widgets.common import headerwidget
from expert_gui_core.gui.widgets.pyqt import chartwidget, pandas_table
from expert_gui_core.gui.widgets.pyqt import chartglwidget
from expert_gui_core.gui.widgets.pyqt import imagewidget
from expert_gui_core.gui.widgets.pyqt import tablewidget
from expert_gui_core.gui.widgets.pyqt import bitenumwidget
from expert_gui_core.gui.widgets.pyqt import scalarwidget
from expert_gui_core.gui.widgets.pyqt import pandas_table
from expert_gui_core.tools import formatting


class DataWidget(QWidget):
    """

    Class data widget displaying any datamap types

    List of settings :

        :auto: auto scaling
        :show_table: show table component
        :show_image: show image component (2d or history)
        :show_chart: show chart component (2d, 1d or history)
        :show_singlechart: show chart component (one line 1d or last in history)
        :show_scalar: show value component
        :show_bitenum: show bitenum value component
        :show_chartgl: show 3d component
        :show_chartgl_3d: 3d or 2d in GL comp,
        :show_chartgl_points: 3d as points
        :show_chartgl_surface: 3d as surface
        :show_chartgl_lines: 3d as lines
        :show_multi_images: 3d multi images
        :chart_palette: chart palette colors (cf. Colors object)
        :history: keep or not history (scalar or 1d),
        :show_image_value_axis: show axis on image
        :show_chart_value_axis: show axis on chart
        :show_singlechart_value_axis: show axis on single chart
        :show chart grid: show/hide grid on chart
        :size_max_table: max elements in the table
        :time_in_table: show timestamps in table
        :show_chart_waterfall: show chart multi line as waterfall (pseudo 3d)
        :size_max_row_table: maximum number of rows to be shown in table
        :optimization: if more than 100 lines and modulo then n lines reduced automatically
        :hide_timing_header: show/hide header tming ticks
        :unity: show unit y
        :unitx: show unit x
        :ratio: lock image ratio
        :color_single_chart: color of the single last chart (obj)
        :color_amplitude: 3d line shown with color vs. amplitude
        :labels_bitenum:labels
        :size_bit_bitenum:size_bit
    """

    def __init__(self, parent, title=None, name=["default"], header=True, max=None, **opts):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        # layout

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(2)

        # header

        self.header = header

        # create _datawidget

        self._data_widget = _DataWidget(parent, title, name, max=max, **opts)

        # palette

        palette = self.get_opt('chart_palette', type='str')
        if palette is None:
            palette = "stroke"

        # header

        tick = not self.get_opt('hide_timing_header', type='bool')
        self._headerWidget = headerwidget.HeaderWidget(self, tick=tick, palette=palette)
        if header:
            self.layout.addWidget(self._headerWidget, 0, 0)

        # add _datawidget

        self.layout.addWidget(self._data_widget, 1, 0)

    def clear_data(self):
        """
        Clear data.
        """

        self._data_widget.clear_data()

    def update_all_components(self):
        """
        Refresh plot components.
        """

        self._data_widget.update_signal.emit()

    def get_opt(self, name, type='str'):
        """
        Get option values.
        """

        return self._data_widget.get_opt(name, type)

    def set_data(self, value, valuex=None, time_value=0, cycle=None, name="default", name_parent="default", color=None,
                 width=None, swidth=None):
        """
        Set data in the low data widget component.
        """

        if self.header:
            if self._headerWidget.is_pause():
                return

                # inject

        self._data_widget.set_data(value, valuex, time_value=time_value, cycle=cycle, name=name,
                                   name_parent=name_parent, color=color, width=width, swidth=swidth)

        if self.header:
            if self._headerWidget.get_name() is None:
                self._headerWidget.set_name(name)
            self._headerWidget.recv(cycle=cycle, name=name)

    def get_chart(self):
        """
        Get chart.
        """

        return self._data_widget.get_chart()

    def get_singlechart(self):
        """
        Get single chart.
        """

        return self._data_widget.get_singlechart()

    def get_table(self):
        """
        Get table.
        """

        return self._data_widget.get_table()

    def get_image(self):
        """
        Get image.
        """

        return self._data_widget.get_image()

    def get_table_col_length(self):
        """
        Return data table column number.
        """

        return self._data_widget.get_table_col_length()

    def update_chart_gl(self, name="default"):
        """
        Update chart gl.
        """

        self._data_widget.update_chart_gl(name)

    def update_multi_images(self, name="default"):
        """
        Update multi images.
        """

        self._data_widget.update_multi_images(name)

    def update_chart(self, name="default"):
        """
        Update chart.
        """

        self._data_widget.update_chart(name)

    def update_singlechart(self, name="default", width=None):
        """
        Update chart.
        """
        self._data_widget.update_singlechart(name, width=width)

    def update_singletable(self, name="default", force=False):
        """
        Update table data.
        """

        self._data_widget.update_singletable(name, force=force)

    def update_table(self, name="default", force=False):
        """
        Update table data.
        """

        self._data_widget.update_table(name, force=force)

    def update_image(self, name="default"):
        """
        Update image.
        """

        self._data_widget.update_image(name)

    def update_scalar(self, name="default"):
        """
        Update scalar.
        """

        self._data_widget.update_scalar(name)

    def change_max(self, new_size):
        """
        Change the MAX_ARRAY_NUMBER in the DataObject.
        :param new_size: New array number
        :type new_size: int
        """

        self._data_widget.change_max(new_size)

    def set_font_size(self, val):
        """
        Change font size.
        """

        self._data_widget.set_font_size(val)

    def dark_(self):
        """
        Set dark theme.
        """

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

        self._data_widget.dark_()

        if self.header:
            self._headerWidget.dark_()

    def light_(self):
        """
        Set light theme.
        """

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        self._data_widget.light_()

        if self.header:
            self._headerWidget.light_()


class _DataWidget(QWidget):
    """

    Class data widget displaying any datamap types

    """

    update_signal = pyqtSignal()
    update_signal_not_forced = pyqtSignal()

    def __init__(self, parent, title=None, name=["default"], max=None, **opts):
        super(QWidget, self).__init__(parent)

        self.curve = None

        # Options

        self._opts = opts

        # Data

        self._history = self.get_opt('history', type='bool')
        self._data = DataObject(self._history, max=max)
        self._names_parent = name
        self._names = {}
        for name_parent in self._names_parent:
            self._names[name_parent] = []

        # Layout

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # Data components

        if title is not None:
            self._title = title
        else:
            self._title = []
            for i in range(len(name)):
                self._title.append(None)

        # mutex

        self._mutex = False

        # tab indexes

        self._tabs_index = {}

        # parameters

        self.show_chart = self.get_opt('show_chart', type='bool')
        self.show_singlechart = self.get_opt('show_singlechart', type='bool')
        self.show_image = self.get_opt('show_image', type='bool')
        self.show_table = self.get_opt('show_table', type='bool')
        self.show_singletable = self.get_opt('show_singletable', type='bool')
        self.show_scalar = self.get_opt('show_scalar', type='bool')
        self.show_bitenum = self.get_opt('show_bitenum', type='bool')
        self.show_chartgl = self.get_opt('show_chartgl', type='bool')
        self.show_chartgl_3d = self.get_opt('show_chartgl_3d', type='bool')
        self.show_chartgl_surface = self.get_opt('show_chartgl_surface', type='bool')
        self.show_chartgl_points = self.get_opt('show_chartgl_points', type='bool')
        self.show_multi_images = self.get_opt('show_multi_images', type='bool')
        self.time_in_table = self.get_opt('time_in_table', type='bool')
        self._show_chart_waterfall = self.get_opt('show_chart_waterfall', type='bool')
        self._optimization = self.get_opt('optimization', type='str')
        self._time_in_chart = self.get_opt('time_in_chart', type='bool')
        self._ratio = self.get_opt('ratio', type='bool')
        self._color_single_chart = self.get_opt('color_single_chart', type='obj')
        self._color_amplitude = self.get_opt('color_amplitude', type='bool')
        self.low_res_chart = self.get_opt('low_res_chart', type='bool')
        self._flat = self.get_opt('flat', type='bool')
        self._glue_cursor = not self.get_opt('not_glue_cursor', type='bool')

        self._unity = self.get_opt('unity', type='str')
        if self._unity == "":
            self._unity = None

        self._unitx = self.get_opt('unitx', type='str')
        if self._unitx == "":
            self._unitx = None

        statistics = self.get_opt('statistics', type='bool')

        fitting = self.get_opt('fitting', type='bool')

        self._type_plot_3d = "lines"
        if self.show_chartgl_surface:
            self._type_plot_3d = "surface"
        if self.show_chartgl_points:
            self._type_plot_3d = "points"

        self.show_image_value_axis = self.get_opt('show_image_value_axis', type='bool')
        self.show_chart_value_axis = self.get_opt('show_chart_value_axis', type='bool')
        self.show_chart_grid = self.get_opt("show_chart_grid", type='bool')
        self.show_singlechart_value_axis = self.get_opt('show_singlechart_value_axis', type='bool')

        self._multi_axis = self.get_opt("multi_axis", type='bool')

        self.chart_palette = self.get_opt('chart_palette', type='str')
        if self.chart_palette is None:
            self.chart_palette = ""

        if self.chart_palette == "":
            self.chart_palette = "stroke"

        self._palette_line = self.get_opt('palette_line', type='str')
        if self._palette_line is None:
            self._palette_line = ""

        if self._palette_line == "":
            self._palette_line = "stroke"

        self._size_max_table = self.get_opt('size_max_table', type='int')
        self._size_max_row_table = self.get_opt('size_max_row_table', type='int')
        if self._size_max_row_table == 0:
            self._size_max_row_table = 200000
        if self._size_max_table == 0:
            self._size_max_table = 1000000000

        # components

        self._chart = None
        self._singlechart = None
        self._image = None
        self._table = None
        self._singletable = None
        self._scalar = None
        self._bitenum = None
        self._chart_gl = None
        self._multi_images = None
        self._label = None

        # local data

        self._components = []
        self._data_table = {}
        self._data_table_col_names = []
        self._data_singletable = {}

        # tabs

        self._tabs = None

        # assing index of each tab

        self.n_elements = 0
        if self.show_bitenum:
            self._tabs_index["bitenum"] = self.n_elements
            self.n_elements += 1
        if self.show_scalar:
            self._tabs_index["scalar"] = self.n_elements
            self.n_elements += 1
        if self.show_singlechart:
            self._tabs_index["last chart"] = self.n_elements
            self.n_elements += 1
        if self.show_image:
            self._tabs_index["image"] = self.n_elements
            self.n_elements += 1
        if self.show_chart:
            self._tabs_index["chart"] = self.n_elements
            self.n_elements += 1
        if self.show_table:
            self._tabs_index["table"] = self.n_elements
            self.n_elements += 1
        if self.show_singletable:
            self._tabs_index["last table"] = self.n_elements
            self.n_elements += 1
        if self.show_chartgl:
            self._tabs_index["chartgl"] = self.n_elements
            self.n_elements += 1
        if self.show_multi_images:
            self._tabs_index["3d images"] = self.n_elements
            self.n_elements += 1

        if self.n_elements == 1:

            # Only one component to show
            if self.show_chart:
                multi_axis = False
                self._chart = chartwidget.ChartWidget(self, name=name)
                self._components.append(self._chart)
                self.layout.addWidget(self._chart, 2, 0)
                for i in range(len(name)):
                    self._chart.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                         show_value_axis=self.show_chart_value_axis, show_grid=self.show_chart_grid,
                                         statistics=statistics, fitting=fitting, time_in_chart=self._time_in_chart,
                                         unitx=self._unitx,
                                         unity=self._unity, add_region=False, low_res=self.low_res_chart,
                                         multi_axis=self._multi_axis, glue_cursor=self._glue_cursor)
            elif self.show_singlechart:
                multi_axis = False
                self._singlechart = chartwidget.ChartWidget(self, name=name)
                self._components.append(self._singlechart)
                self.layout.addWidget(self._singlechart, 2, 0)
                for i in range(len(name)):
                    self._singlechart.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                               show_value_axis=self.show_singlechart_value_axis,
                                               show_grid=self.show_chart_grid, statistics=statistics, fitting=fitting,
                                               time_in_chart=self._time_in_chart, unitx=self._unitx, unity=self._unity,
                                               add_region=False,
                                               low_res=self.low_res_chart, glue_cursor=self._glue_cursor)
            elif self.show_image:
                self._image = imagewidget.ImageWidget(self, name=name)
                self._components.append(self._image)
                for i in range(len(name)):
                    self._image.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                         show_value_axis=self.show_image_value_axis, ratio=self._ratio)
                self.layout.addWidget(self._image, 2, 0)
            elif self.show_table:
                self._df = pd.DataFrame({}, index=[])
                self._table = pandas_table.PandasTableWidget(dataframe=self._df)
                self._table.setAlternatingRowColors(True)
                self._table.resizeColumnsToContents()
                header = self._table.horizontalHeader()
                header.setSectionResizeMode(QHeaderView.Stretch)
                self.layout.addWidget(self._table, 2, 0)
            elif self.show_singletable:
                self._singledf = pd.DataFrame({}, index=[])
                self._singletable = pandas_table.PandasTableWidget(dataframe=self._singledf)
                self._singletable.setAlternatingRowColors(True)
                self._singletable.resizeColumnsToContents()
                header = self._singletable.horizontalHeader()
                header.setSectionResizeMode(QHeaderView.Stretch)
                self.layout.addWidget(self._singletable, 2, 0)
            elif self.show_scalar:
                self._label = scalarwidget.ScalarWidget(self, color=self._color_single_chart)
                self.layout.addWidget(self._label, 2, 0)
            elif self.show_bitenum:
                labels = self.get_opt('labels_bitenum', type='obj')
                size_bit = self.get_opt('size_bit_bitenum', type='int')
                if size_bit == 0:
                    size_bit = 32
                self._bitenum = bitenumwidget.BitEnumWidget(size_bit=size_bit, title=name[0], labels=labels)
                self.layout.addWidget(self._bitenum, 2, 0)
            elif self.show_chartgl:
                self._chart_gl = chartglwidget.ChartGLWidget(
                    self,
                    type_plot=self._type_plot_3d,
                    spatial=self.show_chartgl_3d,
                    palette_line=self._palette_line,
                    color_amplitude=self._color_amplitude)
                self._components.append(self._chartgl)
                self.layout.addWidget(self._chart_gl, 2, 0)
        else:

            # Multi components in tab

            if self._flat:

                nelt = 0

                if self.show_singlechart:
                    self._singlechart = chartwidget.ChartWidget(self, name=name)
                    self._components.append(self._singlechart)
                    self.layout.addWidget(self._singlechart, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1
                    for i in range(len(name)):
                        self._singlechart.add_plot(title=self._title[i], name_parent=name[i], name=name[i],
                                                   dyn_cursor=True,
                                                   show_value_axis=self.show_singlechart_value_axis,
                                                   show_grid=self.show_chart_grid, statistics=statistics,
                                                   fitting=fitting,
                                                   time_in_chart=self._time_in_chart,
                                                   unitx=self._unitx,
                                                   unity=self._unity,
                                                   add_region=False, glue_cursor=self._glue_cursor)

                if self.show_image:
                    self._image = imagewidget.ImageWidget(self, name=name)
                    self._components.append(self._image)
                    for i in range(len(name)):
                        self._image.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                             show_value_axis=self.show_image_value_axis, ratio=self._ratio)
                    self.layout.addWidget(self._image, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                if self.show_chart:
                    self._chart = chartwidget.ChartWidget(self, name=name)
                    self._components.append(self._chart)
                    self.layout.addWidget(self._chart, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1
                    for i in range(len(name)):
                        self._chart.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                             show_value_axis=self.show_chart_value_axis, show_grid=self.show_chart_grid,
                                             statistics=statistics, fitting=fitting, time_in_chart=self._time_in_chart,
                                             unity=self._unity, unitx=self._unitx, add_region=False, glue_cursor=self._glue_cursor)

                if self.show_table:
                    self._df = pd.DataFrame({}, index=[])
                    self._table = pandas_table.PandasTableWidget(dataframe=self._df)
                    self._table.resizeColumnsToContents()
                    header = self._table.horizontalHeader()
                    header.setSectionResizeMode(QHeaderView.Stretch)
                    self.layout.addWidget(self._table, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                if self.show_singletable:
                    self._singledf = pd.DataFrame({}, index=[])
                    self._singletable = pandas_table.PandasTableWidget(dataframe=self._singledf)
                    self._singletable.resizeColumnsToContents()
                    header = self._singletable.horizontalHeader()
                    header.setSectionResizeMode(QHeaderView.Stretch)
                    self.layout.addWidget(self._singletable, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                if self.show_scalar:
                    self._label = scalarwidget.ScalarWidget(self, color=self._color_single_chart)
                    self.layout.addWidget(self._label, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                if self.show_bitenum:
                    labels = self.get_opt('labels_bitenum', type='obj')
                    size_bit = self.get_opt('size_bit_bitenum', type='int')
                    if size_bit == 0:
                        size_bit = 32
                    self._bitenum = bitenumwidget.BitEnumWidget(size_bit=size_bit, title="", labels=labels)
                    self.layout.addWidget(self._bitenum, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                if self.show_chartgl:
                    self._chart_gl = chartglwidget.ChartGLWidget(
                        self,
                        type_plot=self._type_plot_3d,
                        spatial=self.show_chartgl_3d,
                        palette_line=self._palette_line,
                        color_amplitude=self._color_amplitude)
                    self._components.append(self._chart_gl)
                    self.layout.addWidget(self._chart_gl, 2 + int(nelt % 2), int(nelt / 2))
                    nelt = nelt + 1

                self.layout.setColumnStretch(0, 1)

                if nelt > 2:
                    self.layout.setColumnStretch(1, 1)

            else:

                self._tabs = QTabWidget()
                self.layout.addWidget(self._tabs, 2, 0)
                self._tabs.tabBarClicked.connect(self.tab_click)

                if self.show_bitenum:
                    labels = self.get_opt('labels_bitenum', type='obj')
                    size_bit = self.get_opt('size_bit_bitenum', type='int')
                    if size_bit == 0:
                        size_bit = 32
                    self._bitenum = bitenumwidget.BitEnumWidget(size_bit=size_bit, title="", labels=labels)
                    self._tabs.addTab(self._bitenum, "BitEnum")

                if self.show_scalar:
                    self._label = scalarwidget.ScalarWidget(self, color=self._color_single_chart)
                    self._components.append(self._label)
                    self._tabs.addTab(self._label, "Label")

                if self.show_singlechart:
                    self._singlechart = chartwidget.ChartWidget(self, name=name)
                    self._components.append(self._singlechart)
                    self._tabs.addTab(self._singlechart, "Last Chart")
                    for i in range(len(name)):
                        self._singlechart.add_plot(title=self._title[i], name_parent=name[i], name=name[i],
                                                   dyn_cursor=True,
                                                   show_value_axis=self.show_singlechart_value_axis,
                                                   show_grid=self.show_chart_grid, statistics=statistics,
                                                   fitting=fitting,
                                                   time_in_chart=self._time_in_chart, unity=self._unity,
                                                   unitx=self._unitx, add_region=False, glue_cursor=self._glue_cursor)

                if self.show_image:
                    self._image = imagewidget.ImageWidget(self, name=name)
                    self._components.append(self._image)
                    for i in range(len(name)):
                        self._image.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                             show_value_axis=self.show_image_value_axis, ratio=self._ratio)
                    self._tabs.addTab(self._image, "Image")

                if self.show_chart:
                    self._chart = chartwidget.ChartWidget(self, name=name)
                    self._components.append(self._chart)
                    self._tabs.addTab(self._chart, "Chart")
                    for i in range(len(name)):
                        self._chart.add_plot(title=self._title[i], name_parent=name[i], name=name[i], dyn_cursor=True,
                                             show_value_axis=self.show_chart_value_axis, show_grid=self.show_chart_grid,
                                             statistics=statistics, fitting=fitting, time_in_chart=self._time_in_chart,
                                             unitx=self._unitx,
                                             unity=self._unity, add_region=False, glue_cursor=self._glue_cursor)

                if self.show_table:
                    self._df = pd.DataFrame({}, index=[])
                    self._table = pandas_table.PandasTableWidget(dataframe=self._df)
                    self._tabs.addTab(self._table, "Table")

                    self._table.resizeColumnsToContents()
                    header = self._table.horizontalHeader()
                    header.setSectionResizeMode(QHeaderView.Stretch)

                if self.show_singletable:
                    self._singledf = pd.DataFrame({}, index=[])
                    self._singletable = pandas_table.PandasTableWidget(dataframe=self._singledf)
                    self._tabs.addTab(self._singletable, "Last Table")

                    self._singletable.resizeColumnsToContents()
                    header = self._singletable.horizontalHeader()
                    header.setSectionResizeMode(QHeaderView.Stretch)

                if self.show_chartgl:
                    self._chart_gl = chartglwidget.ChartGLWidget(
                        self,
                        type_plot=self._type_plot_3d,
                        spatial=self.show_chartgl_3d,
                        palette_line=self._palette_line,
                        color_amplitude=self._color_amplitude)
                    self._components.append(self._chart_gl)
                    self._tabs.addTab(self._chart_gl, "Chart GL")

        # signals

        self.update_signal.connect(self.update_all_components)
        self.update_signal_not_forced.connect(self.update_components_not_forced)

    def clear_data(self):
        """
        Clear data.
        """

        self._data.clear_data()

        if self.show_chart:
            self._chart.clear_data()
        if self.show_table:
            self._data_table = {}
            self._df = pd.DataFrame({}, index=[], columns=[])
        if self.show_singletable:
            self._data_singletable = {}
            self._singledf = pd.DataFrame({}, index=[], columns=[])

    def tab_click(self, tab_index):
        """
        Click tabs.
        """

        QTimer.singleShot(1, self.update_signal_not_forced.emit)

    def get_opt(self, name, type='str'):
        """
        Get values from options.
        """

        try:
            option = self._opts[name]
        except:
            if type == 'str':
                return ""
            elif type == 'int':
                return 0
            elif type == 'float':
                return 0.
            elif type == 'bool':
                return False
            elif type == 'obj':
                return None

        return option

    def add_data(self, value, valuex=None, time_value=0, name="default"):
        """
        Add data in data buffer object.
        """
        # inject
        self._data.add(value, valuex, time_value=time_value, name=name)

        if self._data._optimize == True:
            self._optimization = "modulo"

    @pyqtSlot()
    def update_all_components(self):
        """
        Update all components.
        """

        for name_parent in self._names_parent:
            for name in self._names[name_parent]:
                try:
                    self.set_data(value=None, name=name, name_parent=name_parent, force=True, forcedata=True,
                                  set_data=False)
                except:
                    pass

    @pyqtSlot()
    def update_components_not_forced(self):
        """
        Update all components.
        """

        for name_parent in self._names_parent:
            for name in self._names[name_parent]:
                try:
                    self.set_data(value=None,
                                  name=name,
                                  name_parent=name_parent,
                                  force=False,
                                  forcedata=True,
                                  set_data=False)
                except:
                    pass

    def set_data(self, value, valuex=None, cycle=None, time_value=0, name="default", name_parent="default", force=False,
                 forcedata=False, set_data=False, color=None, width=None, swidth=None):
        """
        Set data in all data components.
        """

        # mutex on ?

        if self._mutex:
            return

        # value empty ?

        if not force:
            try:
                if (value is not None) and len(value) == 0:
                    self._mutex = False
                    return
            except Exception as e:
                try:
                    if (value is not None) and value.size == 0:
                        self._mutex = False
                        return
                except Exception as ee:
                    pass

        # mutex on

        self._mutex = True

        # not a passif refresh but data to be used

        if (value is not None) and not forcedata:
            if name not in self._names[name_parent]:
                self._names[name_parent].append(name)

            self.add_data(value, valuex=valuex, time_value=time_value, name=name)

        # widget visible ?
        if (not force) and self.isVisible() == False:
            self._mutex = False
            return

        # only set data ?

        if set_data:
            self._mutex = False
            return

        # update all active components

        if self.show_chart:
            self.update_chart(name=name, name_parent=name_parent, force=force, color=color, width=width)
        if self.show_singlechart:
            self.update_singlechart(name=name, name_parent=name_parent, force=force, color=color, width=swidth)
        if self.show_image:
            self.update_image(name=name, name_parent=name_parent, force=force)
        if self.show_table:
            self.update_table(name, force=force)
        if self.show_singletable:
            self.update_singletable(name, force=force)
        if self.show_scalar:
            self.update_scalar(name, force=force)
        if self.show_bitenum:
            self.update_bitenum(name, force=force)
        if self.show_chartgl:
            self.update_chart_gl(name=name, name_parent=name_parent, force=force)
        if self.show_multi_images:
            self.update_multi_images(name=name, name_parent=name_parent, force=force)

            # mutex off

        self._mutex = False

    def get_chart(self):
        """
        Return Chart object.
        """

        if not self.show_chart:
            return None

        return self._chart

    def get_image(self):
        """
        Return Image object.
        """

        if not self.show_image:
            return None

        return self._image

    def get_singlechart(self):
        """
        Return single chart object.
        """

        if not self.show_singlechart:
            return None

        return self._singlechart

    def get_table(self):
        """
        Return single chart object.
        """

        if self._singletable is not None:
            return self._singletable

        return self._table

    def get_table_col_length(self):
        """
        Return data table column number.
        """

        if self._singletable is not None:
            if self._singledf is not None:
                if len(self._singledf.columns) < 2:
                    return 0
                else:
                    return len(self._singledf.columns)
            else:
                return 0

        if self._df is not None:
            if 'error_list' in self._df.columns:
                return 0
            else:
                return len(self._df.columns)
        else:
            return 0

        return 0

    def update_chart_gl(self, name="default", name_parent="default", force=False):
        """
        Update chart gl.
        """

        if not self.show_chartgl:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["chartgl"] or force):
                valid = True

        if valid:
            self._chart_gl.set_data(self._data.get(name),
                                    name=name,
                                    name_parent=name_parent,
                                    update=True,
                                    max=self._data.MAX_ARRAY_NUMBER)

    def update_multi_images(self, name="default", name_parent="default", force=False):
        """
        Update multi images.
        """

        if not self.show_multi_images:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["3d images"] or force):
                valid = True

        if valid:
            # inject

            self._multi_images.set_data(self._data.get(name))

    def update_chart(self, name="default", name_parent="default", force=False, color=None, width=None):
        """
        Update chart component.
        """

        if not self.show_chart:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["chart"] or force):
                valid = True

        max_n_plot = 0
        max_n_plot_tot = 0

        if valid:

            # number of plot lines

            for nameplot in self._data.keys():
                max_n_plot_tot = max_n_plot_tot + len(self._data.get(nameplot))

            if max_n_plot_tot == 0:
                return
            if max_n_plot_tot > self._data.MAX_ARRAY_NUMBER:
                max_n_plot_tot = self._data.MAX_ARRAY_NUMBER

                # number of plot lines until name reached

            for nameplot in self._data.keys():
                if nameplot == name:
                    break
                max_n_plot = max_n_plot + len(self._data.get(nameplot))

            # number of different names

            max = len(self._data.get(name))
            try:
                numline = len(self._data.get(name)[0])
            except:
                numline = 0

                # one line (array)

            width_line = 1.
            if width is not None:
                width_line = width

            if numline == 0:

                # color
                if color is None:
                    color_plot = Colors.getColor((max_n_plot) / max_n_plot_tot, max=max_n_plot_tot,
                                                 palette=self.chart_palette)
                else:
                    color_plot = color

                # data

                data = np.array(self._data.get(name), dtype=np.float64)

                # datax

                datax = None

                if self._data.getx(name) is not None and len(self._data.getx(name)) != 0:
                    datax = self._data.getx(name)

                # inject
                self._chart.set_data(
                    data,
                    datax=datax,
                    update=True,
                    name=name,
                    name_pi=name_parent,
                    name_parent=name_parent,
                    skipFiniteCheck=True,
                    antialias=True,
                    pen=pg.mkPen({'color': color_plot, 'width': width_line}),
                    autoDownsample=True)

            # multi lines (2d array)

            else:

                # modulo calculated if optimization on

                delta = 0
                modulo = 1

                if self._optimization is not None and self._optimization == "modulo":
                    if max > self._data.MAX_ARRAY_NUMBER:
                        modulo = int(max / self._data.MAX_ARRAY_NUMBER)
                else:
                    if max > self._data.MAX_ARRAY_NUMBER:
                        max = self._data.MAX_ARRAY_NUMBER

                # show as waterfall

                if self._show_chart_waterfall:
                    try:
                        data = np.array(self._data.get(name), dtype=np.float64).transpose()
                        ymax = np.amax(data)
                        ymin = np.amin(data)
                        delta = modulo * 4 * (ymax - ymin) / max
                    except Exception as e:
                        print(e)

                if self.curve is None:
                    self.curve = max * [None]

                i = 0

                # main loop set data in chart

                for imod in range(0, max, modulo):

                    # extract a shape

                    try:
                        if type(self._data.get(name)[imod]) is tuple or type(self._data.get(name)[imod]) is list:
                            shape = [len(self._data.get(name)[imod]), ]
                        else:
                            shape = self._data.get(name)[imod].shape
                    except Exception as e:
                        print(e)

                    # 1 array

                    if len(shape) == 1:

                        # color

                        color_plot = Colors.getColor((max_n_plot + i) / max_n_plot_tot, max=max_n_plot_tot,
                                                     palette=self.chart_palette)

                        # data

                        data = np.array(self._data.get(name)[imod], dtype=np.float64)

                        # datax

                        datax = None
                        if self._data.getx(name) is not None and len(self._data.getx(name)) != 0:
                            datax = self._data.getx(name)[imod]

                        # name plots (index or time)

                        name_plot = str(imod + 1)

                        # inject

                        self._chart.set_data(
                            data + i * delta,
                            datax=datax,
                            update=True,
                            name=name_plot,
                            name_pi=name_parent,
                            name_parent=name_parent,
                            skipFiniteCheck=True,
                            antialias=True,
                            pen=pg.mkPen({'color': color_plot, 'width': width_line}),
                            autoDownsample=True)

                        # n arrays

                    else:

                        # dimensions

                        nlines = shape[0]
                        npoints = shape[1]

                        # modulo calculated if optimization on

                        modulo2d = 1
                        if self._optimization is not None and self._optimization == "modulo":
                            if nlines > self._data.MAX_ARRAY_NUMBER:
                                modulo2d = int(nlines / self._data.MAX_ARRAY_NUMBER)

                        j = 0

                        # main loop set data in table

                        for jmod in range(0, nlines, modulo2d):

                            # color

                            color_plot = Colors.getColor((j) / nlines, max=nlines, palette=self.chart_palette)

                            # data

                            data = np.array(self._data.get(name)[imod][jmod], dtype=np.float64)

                            # datax

                            datax = None
                            if self._data.getx(name) is not None and len(self._data.getx(name)) != 0:
                                datax = self._data.getx(name)[imod][jmod]
                            if datax is not None:
                                datax = np.array(datax, dtype=np.float64)

                                # inject
                            self._chart.set_data(
                                data + i * delta,
                                datax=datax,
                                update=True,
                                name=str(j + 1),
                                name_pi=name_parent,
                                name_parent=name_parent,
                                skipFiniteCheck=True,
                                antialias=True,
                                pen=pg.mkPen({'color': color_plot, 'width': width_line}),
                                autoDownsample=True)

                            j = j + 1
                    # incr
                    i = i + 1

    def current_milli_time(self):
        """
        Return time in millisecond.
        """

        return round(time() * 1000)

    def update_singlechart(self, name="default", name_parent="default", force=False, color=None, width=None):
        """
        Update single chart component.
        """

        if not self.show_singlechart:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["last chart"] or force):
                valid = True

        if valid:

            # data

            data = self._data.get(name)
            datax = self._data.getx(name)

            if datax is not None and len(datax) == 0:
                datax = None

            valuesx = None

            # history

            if self._history:

                # set data

                values = data[len(data) - 1]

                # set datax

                if datax is not None:
                    valuesx = datax[len(datax) - 1]

            # normal

            else:

                # set data

                values = data

                # set datax

                if datax is not None:
                    valuesx = datax

            # color

            if self._color_single_chart is None:
                if color is None:
                    color_plot = Colors.getColor(0, max=1, palette=self.chart_palette)
                else:
                    color_plot = color
            else:
                color_plot = self._color_single_chart

            width_line = 1.
            if width is not None:
                width_line = width

            # inject

            self._singlechart.set_data(
                values,
                datax=valuesx,
                update=True,
                name=name,
                name_pi=name_parent,
                name_parent=name_parent,
                skipFiniteCheck=True,
                autoDownsample=True,
                antialias=True,
                pen=pg.mkPen({'color': color_plot, 'width': width_line}))

    def update_table(self, name="default", force=False):
        """
        Update table data.
        """

        if not self.show_table:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["table"] or force):
                valid = True

        if valid:
            # data

            data = self._data.get(name)

            try:
                if data.shape[1] == 0:
                    return
            except:
                pass

            # data is a list ?

            if isinstance(data, list):
                try:
                    if "float" in str(type(data[0][0])):
                        data = np.array(data, dtype=np.float32)
                    elif "int" in str(type(data[0][0])):
                        data = np.array(data, dtype=np.int32)
                    else:
                        data = np.array(data, dtype=object)
                except:
                    data = np.array(data, dtype=object)

            # data shape wrong ?

            if len(data.shape) == 1:
                data = [data]

            # extract a shape

            try:
                shape = data.shape
            except:
                shape = data[0].shape

            # finalize what are dim1 and dim2

            if len(shape) == 1:
                dim1 = 1
                dim2 = shape[0]
            else:
                dim1 = shape[0]
                dim2 = shape[1]

            # No more than self._data.MAX_ARRAY_NUMBER columns => otherwise too slow

            if dim1 > self._data.MAX_ARRAY_NUMBER:
                dim1 = self._data.MAX_ARRAY_NUMBER

            # 1 array

            if len(shape) == 1:

                # dimensions

                ncol = dim1
                nrow = dim2

                sizetot = ncol * nrow

                # check sizes

                if nrow != 0 and sizetot > self._size_max_table:
                    ncol = int(self._size_max_table / float(nrow))
                if ncol > self._size_max_row_table:
                    ncol = self._size_max_row_table
                if ncol == 0:
                    ncol = 1

                max = ncol
                modulo = 1

                # modulo calculated if optimization on

                if self._optimization is not None and self._optimization == "modulo":
                    max = len(self._data.get(name))
                    if max > self._data.MAX_ARRAY_NUMBER:
                        modulo = int(max / self._data.MAX_ARRAY_NUMBER)

                i = 0

                # main loop set data in table

                for imod in range(0, max, modulo):

                    # name col and values

                    namecol = str(i + 1)
                    values = data[imod]

                    try:

                        print(namecol)

                        if self._df.values.shape[0] != len(values) or (namecol not in self._df.columns):

                            # update local data

                            self._data_table[namecol] = values
                            # print("a",namecol,self._df)
                            # self._df[namecol] = values
                            self._table.model.dataframe = pd.DataFrame(self._data_table)

                        else:

                            # inject

                            if self._history:
                                pass
                            else:
                                self._df[namecol] = values
                            self._table.refresh()
                        # incr
                        i = i + 1

                    except Exception as e:
                        print(e)


            # n arrays

            else:

                # dimensions

                ncol = dim1
                nrow = dim2

                sizetot = ncol * nrow

                # check sizes

                if nrow != 0 and sizetot > self._size_max_table:
                    ncol = int(self._size_max_table / float(nrow))

                if ncol > self._size_max_row_table:
                    ncol = self._size_max_row_table

                if ncol == 0:
                    ncol = 1

                max = ncol
                modulo = 1

                # modulo calculated if optimization on

                if self._optimization is not None and self._optimization == "modulo":
                    max = len(self._data.get(name))
                    if max > self._data.MAX_ARRAY_NUMBER:
                        modulo = int(max / self._data.MAX_ARRAY_NUMBER)

                # main loop set data in table

                changed = False

                i = 0
                for imod in range(0, max, modulo):

                    namecol = str(i + 1)

                    values = data[imod]

                    if self._df.values.shape[0] != len(values) or (namecol not in self._df.columns):
                        changed = True

                    i = i + 1

                i = 0
                if changed:

                    # index = []
                    #
                    # for j in range(nrow):
                    #     index.append(str(j + 1))

                    for imod in range(0, max, modulo):
                        namecol = str(i + 1)
                        values = data[imod]
                        self._data_table[namecol] = values
                        i = i + 1

                    self._table.model.dataframe= pd.DataFrame(self._data_table)

    def update_singletable(self, name="default", force=False):
        """
        Update single table data.
        """

        if not self.show_singletable:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["last table"] or force):
                valid = True

        if valid:

            # data

            data = self._data.get(name)

            # extract a shape

            if isinstance(self._data.get(name), list):

                try:
                    if "float" in str(type(self._data.get(name)[0][0])):
                        data = np.array(self._data.get(name), dtype=np.float32)
                    elif "int" in str(type(self._data.get(name)[0][0])):
                        data = np.array(self._data.get(name), dtype=np.int32)
                    else:
                        data = np.array(self._data.get(name), dtype=object)
                except Exception as e:
                    data = np.array(self._data.get(name), dtype=object)
                shape = data[len(data) - 1].shape
            else:
                shape = data.shape

            # dimensions

            if len(shape) == 1:
                dim1 = 1
                dim2 = shape[0]
            else:
                dim1 = shape[0]
                dim2 = shape[1]

            # 1 array

            if len(shape) == 1:

                # check nrow ncol

                ncol = 1
                nrow = dim2
                sizetot = ncol * nrow

                if nrow != 0 and sizetot > self._size_max_table:
                    ncol = int(self._size_max_table / float(nrow))

                if ncol > self._size_max_row_table:
                    ncol = self._size_max_row_table

                # main loop set data in table

                for i in range(ncol):

                    # name col

                    namecol = str(i + 1)

                    # data

                    if self._history:
                        values = data[len(data) - 1]
                    else:
                        values = data

                    # if self._singledf.values.shape[0] != len(values) or (namecol not in self._singledf.columns):
                    #
                    #     # update local data
                    #
                    #     self._data_singletable[namecol] = values
                    #
                    #     self._singledf = self._data_singletable[namecol]
                    #
                    #     # name rows
                    #
                    #     # index = []
                    #     # for j in range(nrow):
                    #     #     index.append(str(j + 1))
                    #     #
                    #     #     # clear df
                    #     #
                    #     # self._singledf.squeeze()
                    #     #
                    #     # # create df
                    #     #
                    #     # self._singledf = pd.DataFrame(self._data_singletable, index=index)
                    #
                    #     # inject
                    #
                    #     # self._singletable.set_data(self._singledf)
                    #
                    # else:
                    #
                    #     # inject

                    self._singledf[namecol] = values

                self._singletable.refresh()

            else:

                # check ncol and nrow

                ncol = dim1
                nrow = dim2

                sizetot = ncol * nrow

                if nrow != 0 and sizetot > self._size_max_table:
                    ncol = int(self._size_max_table / float(nrow))

                if ncol > self._size_max_row_table:
                    ncol = self._size_max_row_table

                # main loop set data in table

                for i in range(ncol):

                    # namecol

                    namecol = str(i + 1)

                    # data

                    values = data[0][i]

                    if self._singledf.values.shape[0] != len(values) or (namecol not in self._singledf.columns):

                        # update local data

                        self._data_singletable[namecol] = values

                        # name rows

                        # index = []
                        # for j in range(nrow):
                        #     index.append(str(j + 1))
                        #
                        # # clear df
                        #
                        # self._singledf.squeeze()
                        #
                        # # create df
                        #
                        # self._singledf = pd.DataFrame(self._data_singletable, index=index)
                        #
                        # # inject

                        # self._singletable.set_data(self._singledf)

                        self._singletable.model.dataframe = pd.DataFrame(self._data_singletable)

                    else:

                        # inject

                        self._singledf[namecol] = values

            # refresh table

                        self._singletable.refresh_table()

    def update_image(self, name="default", name_parent="default", force=False):
        """
        Update image component.
        """

        if not self.show_image:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["image"] or force):
                valid = True

        if valid:

            try:
                data = np.array(self._data.get(name), dtype=np.float64).transpose()
            except Exception as e:
                print(e)
                return

            self._image.set_data(
                data,
                name=name_parent,
                name_parent=name_parent,
                update=True)

    def update_scalar(self, name="default", force=False):
        """
        Update scalar component.
        """

        if not self.show_scalar:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["scalar"] or force):
                valid = True

        if valid:
            val = self._data.get_last(name)
            self._label.set_data(val, name=name, update=True)

    def update_bitenum(self, name="default", force=False):
        """
        Update bitenum component.
        """

        if not self.show_bitenum:
            return

        valid = False

        if self._flat:
            valid = True
        else:
            if self.n_elements == 1 or (self._tabs.currentIndex() == self._tabs_index["bitenum"] or force):
                valid = True

        if valid:
            try:
                self._bitenum.set_data((int)(self._data.get_last(name)))
            except Exception as e:
                print(e)

    def change_max(self, new_size):
        """
        Change the MAX_ARRAY_NUMBER in the DataObject.
        :param new_size: New array number
        :type new_size: int
        """

        self._data.change_max_array_number(new_size)
        self.clear_data()

    def set_font_size(self, val):
        """
        Change font size.
        """

        pass

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        if self._chart is not None:
            self._chart.dark_()
        if self._label is not None:
            self._label.dark_()
        if self._singlechart is not None:
            self._singlechart.dark_()
        if self._image is not None:
            self._image.dark_()
        if self._table is not None:
            self._table.dark_()
        if self._singletable is not None:
            self._singletable.dark_()
        if self._chart_gl is not None:
            self._chart_gl.dark_()
        if self._multi_images is not None:
            self._multi_images.dark_()
        if self._bitenum is not None:
            self._bitenum.sel_bgColors(bglight=Colors.COLOR_WHITE, bgdark=Colors.COLOR_LBLACK)
            self._bitenum.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE+";")
        if self._chart is not None:
            self._chart.light_()
        if self._label is not None:
            self._label.light_()
        if self._singlechart is not None:
            self._singlechart.light_()
        if self._image is not None:
            self._image.light_()
        if self._table is not None:
            self._table.light_()
        if self._singletable is not None:
            self._singletable.light_()
        if self._chart_gl is not None:
            self._chart_gl.light_()
        if self._multi_images is not None:
            self._multi_images.light_()
        if self._bitenum is not None:
            self._bitenum.sel_bgColors(bglight=Colors.COLOR_WHITE,bgdark=Colors.COLOR_LBLACK)
            self._bitenum.light_()


class _Example(QMainWindow, fesacomm.FesaCommListener):
    """

    Example class to test

    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        # self._fesacomm = fesacommDA.FesaComm("BISWRef2", "Acquisition", listener=self)
        # self._fesacomm = fesacomm.FesaComm("PR.BPM", "AcquisitionMean", listener=self)

        # self._fesacomm = fesacomm.FesaComm("SPS.BWS.41677.V", "SPSSetting", listener=self)

        # self._fesacomm = fesacomm.FesaComm("SPS.BCSPILLSPS.DEV", "RawDataFast", listener=self)

        self._fesacomm = fesacomm.FesaComm("BISWRef2", "Acquisition", listener=self)
        self._fesacomm.subscribe("")

    def init_ui(self):
        """Init user interface"""

        data_widget_opts = {
            "auto": True,
            "show_table": True,
            "show_image": False,
            "show_chart": True,
            "show_singlechart": True,
            "show_singletable": True,
            "show_scalar": False,
            "show_bitenum": False,
            "show_chartgl": False,
            "show_chart_grid": False,
            "show_chart_waterfall": False,
            "show_chartgl_3d": False,
            "show_chartgl_points": False,
            "show_chartgl_surface": False,
            "show_chartgl_lines": False,
            "chart_palette": "stroke",
            "history": True,
            "show_image_value_axis": True,
            "show_singlechart_value_axis": True,
            "show_chart_value_axis": True,
            # "size_max_table": 10000,
            "statistics": False,
            "fitting": False,
            # "flat": True,
            "color_amplitude": False,
            "low_res_chart": False,
            "not_glue_cursor":True
        }

        # self.data_widget = DataWidget(self,
        #                               title=["TEST","TEST2","TEST3","TEST4"],
        #                               name=["TEST","TEST2","TEST3","TEST4"],
        #                               max=10,
        #                               **data_widget_opts)

        self.data_widget = DataWidget(self,
                                      title=[""],
                                      name=["TEST"],
                                      max=100,
                                      **data_widget_opts)

        data_widget_opts_sum = {
            "menubar": False,
            "toolbar": False,
            "auto": True,
            "chart_palette": "#FFB400",
            "show_singlechart": False,
            "show_table": False,
            "show_image": False,
            "show_chart": True,
            "show_scalar": False,
            "show_bitenum": False,
            "show_chartgl": False,
            "show_chartgl_3d": False,
            "show_chartgl_surface": False,
            "toolbar_chart": False,
            "time_in_chart": True,
            "history": True,
            "show_chart_value_axis": True,
            "show_image_value_axis": False,
            "show_singlechart_value_axis": False,
            "show_chart_grid": True,
            "size_max_table": 10000,
            "show_chart_waterfall": False,
            "palette_line": "lecroy1",
            "statistics": True,
            "fitting": True
        }
        self.data_widget_single = DataWidget(self,
                                      title=[""],
                                      name=["TEST"],
                                      max=100,
                                      **data_widget_opts_sum)

        self._time_now = 0

        w = QWidget()
        layout = QGridLayout(w)

        layout.addWidget(self.data_widget, 0, 0)
        layout.addWidget(self.data_widget_single, 0, 1)

        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(1, 1)

        self.setCentralWidget(w)

        self.resize(800, 600)

        # self.data_widget.light_()
        self.data_widget.dark_()
        Colors.COLOR_LIGHT = False

    def handle_event(self, name, value):
        """Handle event dispatcher"""

        # if value['_time'] - self._update_time > 40:
        # print("@")
        # print(value['_time'])

        # self.data_widget.set_data(
        #     value['anArray'],
        #     time_value=value['_time'] / 1000.,
        #     name="anArray",
        #     name_parent="TEST")

        # val = [value['pmtSelection']]

        if formatting.current_milli_time() - self._time_now < 20:
            return

        self._time_now = formatting.current_milli_time()

        self.data_widget.set_data(
            value['anArray'],
            time_value=value['_time'] / 1000.,
            name="anArray",
            name_parent="TEST")

        self.data_widget_single.set_data(
            sum(value['anArray']),
            valuex=value["_time"]/1000.,
            # time_value=value['_time'] / 1000.,
            name="anArray",
            name_parent="TEST")

        # self.data_widget.set_data(
        #     value['fastSpillRawData'],
        #     time_value = value['_time']/1000.,
        #     name="anArray",
        #     name_parent="TEST")

        # self.data_widget.set_data(
        #         value['anArray'],
        #         name="anArray2",
        #         name_parent="TEST2")
        self._update_time = value['_time']


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_L3BLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    app.setPalette(darkpalette)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
