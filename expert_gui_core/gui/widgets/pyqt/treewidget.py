"""
This module provides an interactive TreeWidget class that displays hierarchical data from nested dictionaries.
The widget supports multi-column structures, case-sensitive filtering, and a customizable interface.

TreeWidget is the parent widget that instantiates the model, the filter QLineEdit and the custom QTreeWidget that
we call TreeWidgetView. TreeWidgetView is the View/Controller class that contains the actual implementation of the
tree hierarchy logic.

Usage:
    1. Instantiate the TreeWidget class with a nested dictionary as data.
    2. Optionally, connect a QLineEdit or similar widget to the filter function to enable real-time filtering.
"""

import sys
from typing import List, Dict

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QEvent, QObject

from PyQt5.QtGui import QBrush, QColor

from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QHBoxLayout, QHeaderView, QVBoxLayout, QLabel, \
    QLineEdit, QSpacerItem, QTreeWidgetItem, QTreeWidget

from expert_gui_core.gui import biapplicationframe
from expert_gui_core.gui.common.colors import Colors


class TreeWidgetModel(QObject):
    """QObject that holds the raw data.

    Arguments:
        data (dict): Nested dictionary that initializes the model.

    Attributes:
        data (dict): Nested dictionary that holds the tree data
    """
    data_changed = pyqtSignal()  # Signal to notify when data changes

    def __init__(self, data: Dict):
        super().__init__()
        self._data = data

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_data: Dict):
        self._data = new_data
        self.data_changed.emit()  # Notify the view that the data has changed

    def calculate_max_columns(self) -> int:
        """Calculate the maximum number of columns required to display the tree data."""
        max_columns = 1

        def check_columns(dict_data):
            nonlocal max_columns
            for key, value in dict_data.items():
                if isinstance(key, tuple):
                    max_columns = max(max_columns, len(key))
                if isinstance(value, dict):
                    check_columns(value)
                elif isinstance(value, list):
                    for list_item in value:
                        if isinstance(list_item, tuple):
                            max_columns = max(max_columns, len(list_item))

        check_columns(self._data)
        return max_columns


class TreeWidgetView(QTreeWidget):
    """A view class for displaying the hierarchical data in a tree structure.

    Arguments:
        model (TreeWidgetModel): Data model.
        parent (QObject, optional): Parent widget, defaults to None.
        header_labels (List[str], optional): Labels to be added in header, defaults to None.
        header_hidden (bool, optional): Show  or not header, defaults to True.
        root_is_decorated (bool, optional): Show Tree root, defaults to True.
        alternating_row_colors (bool, optional): Alternate subsequent rows' colors, defaults to False.

    Attributes:
         model (TreeWidgetModel): Data model.
    """

    def __init__(self, model: TreeWidgetModel, parent=None, header_labels: List[str] = None,
                 header_hidden: bool = True, root_is_decorated: bool = True, alternating_row_colors: bool = False):
        super().__init__(parent)
        self.model = model
        self._signal = None
        self._root = None

        # Store the styling options
        self._header_labels = header_labels
        self._header_hidden = header_hidden
        self._root_is_decorated = root_is_decorated
        self._alternating_row_colors = alternating_row_colors

        self.init_ui()
        self._connect_signals()
        self.populate_tree()

        # Connect the model's data_changed signal to the update method
        self.model.data_changed.connect(self.refresh_tree)

    def init_ui(self):
        self.setHeaderHidden(self._header_hidden)
        self.setRootIsDecorated(self._root_is_decorated)
        self.setAlternatingRowColors(self._alternating_row_colors)

        if self._header_labels:
            self.setColumnCount(len(self._header_labels))
            self.setHeaderLabels(self._header_labels)
            self.header().setSectionResizeMode(0, QHeaderView.ResizeToContents)
            for i in range(len(self._header_labels)):
                if i > 0:
                    self.header().setSectionResizeMode(i, QHeaderView.Stretch)
        else:
            max_columns = self.model.calculate_max_columns()
            self.setColumnCount(max_columns)

        # Create the root item
        self._root = QTreeWidgetItem(self, ["Root"])
        self.insertTopLevelItem(0, self._root)
        self.expandItem(self._root)

    def _connect_signals(self):
        self.itemSelectionChanged.connect(self._on_item_selected)

    def populate_tree(self):
        """Adds items to self._root recursively."""
        data = self.model.data

        def add_items(parent_item, dict_data):
            for key, value in dict_data.items():
                # Convert the key to a string if it's not a tuple
                if isinstance(key, tuple):
                    key_list = [str(item) for item in key]
                else:
                    key_list = [str(key)]

                # Create the node item
                node_item = QTreeWidgetItem(parent_item, key_list)
                parent_item.addChild(node_item)

                # Recursively add children
                if isinstance(value, dict):
                    add_items(node_item, value)
                elif isinstance(value, list):
                    for list_item in value:
                        # Convert list items to strings if they are not strings
                        if isinstance(list_item, tuple):
                            list_item_list = [str(item) for item in list_item]
                        else:
                            list_item_list = [str(list_item)]

                        list_item_node = QTreeWidgetItem(node_item, list_item_list)
                        node_item.addChild(list_item_node)

        add_items(self._root, data)
        self.expandAll()

    @pyqtSlot()
    def refresh_tree(self):
        """Clear and repopulate the tree when the model data changes."""
        self.clear()  # Clear existing items
        self._root = QTreeWidgetItem(self, ["Root"])
        self.insertTopLevelItem(0, self._root)
        self.populate_tree()  # Repopulate with updated data

    def add_signal(self, signal: pyqtSignal):
        """Add a PyQt signal to be triggered when a leaf node is selected.

        Arguments:
            signal (pyqtSignal): PyQt signal to be triggered when a leaf node is selected.
        """
        self._signal = signal

    @pyqtSlot()
    def _on_item_selected(self):
        selected_items = self.selectedItems()
        if selected_items:
            node = selected_items[0]
            if node.childCount() == 0:  # Leaf node
                item_text = node.text(0)
                if self._signal is not None:
                    self._signal.emit(item_text)

    def filter_items(self, filter_text: str):
        """Filter tree items based on the provided text, expanding and highlighting matching nodes.

        If the filter text is empty, all highlights are removed.

        Arguments:
            filter_text: Text to filter the tree items.
        """

        def ensure_children_visible(node):
            """Recursively ensure all children of the given node are visible."""
            for i in range(node.childCount()):
                child = node.child(i)
                child.setHidden(False)
                ensure_children_visible(child)  # Recurse to show all descendants

        def filter_node(node):
            has_match = False
            child_has_match = False

            # Recursively filter child nodes
            for i in range(node.childCount()):
                child = node.child(i)
                child_match = filter_node(child)
                child_has_match = child_has_match or child_match

            # Check if this node matches the filter in any column
            for column in range(self.columnCount()):
                if filter_text in node.text(column):
                    has_match = True
                    break  # Stop checking once a match is found

            # If this node matches, recursively make sure all its children are visible
            if has_match:
                ensure_children_visible(node)

            # Show or hide the node based on match or if any of its children have a match
            node.setHidden(not (has_match or child_has_match))

            # Expand the node if there's a match or any of its children match
            if has_match or child_has_match:
                self.expandItem(node)

            return has_match or child_has_match

        filter_node(self._root)

    def light_(self):
        self.setStyleSheet("background-color: "+Colors.STR_COLOR_WHITE+";")
        self._set_item_foreground_color(Colors.COLOR_BLACK)

    def dark_(self):
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_L1BLACK + ";")
        self._set_item_foreground_color(Colors.COLOR_WHITE)

    def _set_item_foreground_color(self, color: QColor):
        """Set the foreground color of all items in the tree."""
        def set_color_recursive(item):
            for column in range(self.columnCount()):
                item.setForeground(column, QBrush(color))

            for i in range(item.childCount()):
                set_color_recursive(item.child(i))

        set_color_recursive(self._root)


class TreeWidget(QWidget):
    """A QTreeWidget-based widget that populates a tree structure from a nested dictionary.

    This widget includes additional UI components like a title label and a filter bar,
    making it ideal for applications that require hierarchical data representation with search functionality.

    Arguments:
        data (Dict): Nested dictionary that contains data to be displayed.
        parent (QObject, optional): Parent widget, defaults to None.
        header_labels (List[str], optional): Labels to be added in header, defaults to None.
        header_hidden (bool, optional): Show  or not header, defaults to True.
        root_is_decorated (bool, optional): Show Tree root, defaults to True.
        alternating_row_colors (bool, optional): Alternate subsequent rows' colors, defaults to False.
        title (str, optional): Title to be displayed at the top of the widget, defaults to None.
        include_filter (bool, optional): Add filter to widget, defaults to True.

    Attributes:
        tree_widget (TreeWidgetView): The main QTreeWidget displaying the hierarchical data.
        panel_filter (QWidget): A widget containing the filter bar for real-time item filtering.
        text_filter (QLineEdit): The line edit widget used to input the filter text.
        filter_changed (pyqtSignal): Signal emitted when the filter text changes.
        label_title (QLabel): Optional title label for the tree widget.

    Methods:
        light_():
            Sets a light theme for the widget.

        dark_():
            Sets a dark theme for the widget.

        eventFilter(widget, event):
            Handles keyboard events for the filter widget, particularly the Enter/Return key to trigger filtering.

    Example:
        Instantiate the TreeWidget with a nested dictionary to display a hierarchical tree.

        >>> tree_data = {
        ...    "Category 1": {
        ...        "Subcategory 1A": {"Item 1A-1": [], "Item 1A-2": []},
        ...        "Subcategory 1B": {"Item 1B-1": [], "Item 1B-2": []}
        ...    },
        ...    "Category 2": {
        ...        "Subcategory 2A": {"Item 2A-1": [], "Item 2A-2": []}
        ...    }
        ...}
        ... tree_widget = TreeWidget(data=tree_data, header_labels=["Item", "Detail"], include_filter=True)
        ... tree_widget.show()
        ... sys.exit(app.exec_())
    """
    filter_changed = pyqtSignal()

    def __init__(self, data: Dict, parent=None, header_labels: List[str] = None,
                 header_hidden: bool = True, root_is_decorated: bool = True, alternating_row_colors: bool = False,
                 title: str = None, include_filter: bool = True):
        super().__init__(parent)
        self.label_title = title
        self._include_filter = include_filter
        self.model = TreeWidgetModel(data)
        self.tree_widget = TreeWidgetView(model=self.model, parent=self,
                                          header_labels=header_labels,
                                          header_hidden=header_hidden,
                                          root_is_decorated=root_is_decorated,
                                          alternating_row_colors=alternating_row_colors)
        self._init_ui()

    def _init_ui(self):
        if self.label_title:
            self.label_title = QLabel(self.label_title)
            self.label_title.setStyleSheet(f"color: {Colors.STR_COLOR_BLUE};font-weight:bold;")
            self.label_title.setAlignment(Qt.AlignCenter)

        # Create a central layout to contain both the filter and the tree widget
        central_widget = QWidget()
        central_layout = QVBoxLayout(central_widget)
        if self.label_title:
            central_layout.addWidget(self.label_title)

        if self._include_filter:
            # Initialize filter widget and tree widget
            self.panel_filter = self._create_filter_widget()
            central_layout.addWidget(self.panel_filter)

        central_layout.addWidget(self.tree_widget)

        # Set the central widget of the application frame
        self.setLayout(central_layout)
        self.setGeometry(0, 0, 800, 1000)

    def _create_filter_widget(self) -> QWidget:
        """Create the filter widget layout."""
        panel_filter = QWidget()
        layout_filter = QHBoxLayout(panel_filter)
        layout_filter.setContentsMargins(0, 0, 0, 0)

        # Add a spacer to push the label and line edit to the right
        spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        layout_filter.addItem(spacer)

        label = QLabel("Filter Devices: ")
        layout_filter.addWidget(label, alignment=Qt.AlignRight)

        self.text_filter = QLineEdit("")
        self.text_filter.installEventFilter(self)
        self.filter_changed.connect(self._filter_fields)
        layout_filter.addWidget(self.text_filter, alignment=Qt.AlignRight)

        return panel_filter

    def add_signal(self, signal: pyqtSignal):
        self.tree_widget.add_signal(signal)

    def eventFilter(self, widget: QObject, event: QEvent) -> bool:
        """Handle keyboard events for the filter."""
        if event.type() == QEvent.KeyPress and widget is self.text_filter and \
                event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
            if Colors.COLOR_LIGHT:
                self.light_()
            else:
                self.dark_()
        return False

    @pyqtSlot()
    def _filter_fields(self):
        """Filter visible field panels using their names."""
        filter_text = self.text_filter.text()
        self.tree_widget.filter_items(filter_text)

    @pyqtSlot(str)
    def filter_fields(self, filter_text: str):
        """Slot to be used by External Widgets that want to connect a search field to the TreeWidget.

        Arguments:
            filter_text (str): New text added to the QlineEdit.
        """
        self.tree_widget.filter_items(filter_text)

    def light_(self):
        """Set light theme."""
        self.tree_widget.light_()
        if self._include_filter:
            self.text_filter.setStyleSheet(
                "background-color: white; color: black; border: 1px solid #d3d3d3;")
        if self.label_title:
            self.label_title.setStyleSheet(f"color: {Colors.STR_COLOR_BLUE};font-weight:bold;")

    def dark_(self):
        """Set dark theme."""
        self.tree_widget.dark_()
        if self._include_filter:
            self.text_filter.setStyleSheet(
                "background-color: #1c1c1c; color: white; border: 1px solid #808080;")
        if self.label_title:
            self.label_title.setStyleSheet(f"color: {Colors.STR_COLOR_BLUE};font-weight:bold;")

    def update_model_data(self, new_data: Dict):
        """Update the model's data.

        Arguments:
            new_data (Dict): Nested dictionary containing updated data.
        """
        self.model.data = new_data


class BaseExample(biapplicationframe.BIApplicationFrame):
    """
    Base class for demonstrating the different types of TreeWidgets.
    """

    def __init__(self, q_app=None, example_title="Example", use_rbac=True):
        super().__init__(app=q_app, use_log_console=True, use_rbac=use_rbac, use_timing_bar=False)
        self.setWindowTitle(example_title)

        self.tree_widget = None
        self._init_ui()

    def _init_ui(self):
        self.log_console.console.expanded = False
        self.tree_widget = self._add_tree_widget_example()
        self._tabs.addTab(self.tree_widget, "TreeWidget")
        self._tabs.setTabsClosable(False)
        self.setGeometry(0, 0, 800, 1000)

    def _add_tree_widget_example(self):
        raise NotImplementedError("Subclasses should implement this method.")

    def dark_(self):
        """Set dark theme."""
        super().dark_()
        self.tree_widget.dark_()

    def light_(self):
        """Set light theme."""
        super().light_()
        self.tree_widget.light_()


class Example1(BaseExample):
    """
    Example 1: Test TreeWidget with a nested dictionary.
    """

    def __init__(self, application=None, example_title="Tree Widget Example"):
        super().__init__(example_title=example_title, q_app=application)

    def _add_tree_widget_example(self):
        # Define a nested dictionary with 5 levels
        nested_data = {
            "Level 1 - A": {
                "Level 2 - A": {
                    "Level 3 - A": {
                        "Level 4 - A": {
                            "Level 5 - A1": [],
                            "Level 5 - A2": [],
                        },
                        "Level 4 - B": {
                            "Level 5 - B1": [],
                            "Level 5 - B2": [],
                        }
                    },
                    "Level 3 - B": {
                        "Level 4 - C": {
                            "Level 5 - C1": [],
                            "Level 5 - C2": [],
                        }
                    }
                }
            },
            "Level 1 - B": {
                "Level 2 - B": {
                    "Level 3 - C": {
                        "Level 4 - D": {
                            "Level 5 - D1": [],
                            "Level 5 - D2": [],
                        }
                    }
                }
            }
        }

        # Instantiate the TreeWidget with the nested data
        tree_widget = TreeWidget(data=nested_data, header_hidden=True, include_filter=False)
        return tree_widget


class Example2(BaseExample):
    """
    Example 2: Demonstrates TreeWidget customization with optional parameters and nested tuples.
    """

    def __init__(self, application=None, example_title="Custom Tree Widget Example"):
        super().__init__(example_title=example_title, q_app=application)

    def _add_tree_widget_example(self):
        # Define a nested dictionary with multiple levels and tuples within lists
        nested_data = {
            "Category 1": {
                "Subcategory 1A": {
                    ("Item 1A-1", "Detail for 1A-1", "Extra Info A1"): [],
                    ("Item 1A-2", "Detail for 1A-2", "Extra Info A2"): [
                        ("Nested Item 1A-2-1", "Nested Detail 1A-2-1", "Nested Extra Info 1A-2-1"),
                        ("Nested Item 1A-2-2", "Nested Detail 1A-2-2", "Nested Extra Info 1A-2-2"),
                    ],
                },
                "Subcategory 1B": {
                    ("Item 1B-1", "Detail for 1B-1", "Extra Info B1"): [],
                    ("Item 1B-2", "Detail for 1B-2", "Extra Info B2"): [],
                }
            },
            "Category 2": {
                "Subcategory 2A": {
                    ("Item 2A-1", "Detail for 2A-1", "Extra Info C1"): [],
                    ("Item 2A-2", "Detail for 2A-2", "Extra Info C2"): [
                        ("Nested Item 2A-2-1", "Nested Detail 2A-2-1", "Nested Extra Info 2A-2-1"),
                    ],
                }
            },
            "Category 3": {
                "Subcategory 3A": {
                    ("Item 3A-1", "Detail for 3A-1", "Extra Info D1"): [],
                    ("Item 3A-2", "Detail for 3A-2", "Extra Info D2"): [
                        ("Nested Item 3A-2-1", "Nested Detail 3A-2-1", "Nested Extra Info 3A-2-1"),
                        ("Nested Item 3A-2-2", "", "Nested Extra Info 3A-2-2"),
                    ],
                }
            }
        }

        # Instantiate the TreeWidget with custom parameters
        tree_widget = TreeWidget(
            data=nested_data,
            header_hidden=False,  # Show the header
            header_labels=["Item", "Detail", "Extra Info"],  # Define the column headers
            title="TreeWidget Example with multiple columns"
        )

        return tree_widget


if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    # Create instances of each example window
    example = Example2(application=app)
    example.light_()
    # Show all example windows
    example.show()
    sys.exit(app.exec_())
