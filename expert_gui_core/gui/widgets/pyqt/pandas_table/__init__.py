"""
Pandas Table Module

This module provides a model-view framework for displaying and editing
pandas DataFrames using a Qt interface.

Classes:
    - PandasTableModel: A Qt model that interfaces with a pandas DataFrame.
    - PandasTableView: A Qt view for displaying the data from PandasTableModel.
    - PandasTableWidget: A QTableView extension that integrates the pandas DataFrame
      with custom delegates for enhanced UI customization.
    - DefaultDelegate: A delegate that combines input validation and default text
      alignment for cells in a table.

Usage:
    You can use the classes in this module to create a table view that displays
    data from a pandas DataFrame and allows editing of the values, including customized
    behaviors for individual columns.

    ```python

    import sys \n
    import pandas as pd \n
    import numpy as np \n
    from PyQt5 import QtWidgets \n
    from expert_gui_core.gui.widgets import pandas_table \n

    if __name__ == "__main__":
        app = QtWidgets.QApplication(sys.argv)

        df = pd.DataFrame(np.random.randint(0, 100, size=(3, 4)), columns=list("ABCD"))
        model = pandas_table.PandasTableModel(df)

        view = pandas_table.PandasTableView()
        view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        view.setModel(model)
        view.show()

        sys.exit(app.exec())
    ```

    The PandasTableWidget provides a customized QTableView widget
    that already integrates the model, view and delegate classes.

Example:
    This example demonstrates how to create a table view using a pandas DataFrame
    with customized behavior for checkbox and boolean status columns:

    ```python

    import sys \n
    import pandas as pd \n
    import numpy as np \n
    from PyQt5 import QtWidgets \n
    from expert_gui_core.gui.widgets import pandas_table \n

    if __name__ == "__main__":
        app = QtWidgets.QApplication(sys.argv)

        # Sample DataFrame with random values and additional columns for selection and status
        df = pd.DataFrame(np.random.randint(0, 100, size=(3, 4)), columns=list("ABCD"))
        df['MySelection'] = False
        df['Status'] = np.random.choice([True, False], size=len(df))

        checkable_columns = ['MySelection']
        status_columns = ['Status']

        # Initialize the PandasTableWidget with custom columns
        table_widget = pandas_table.PandasTableWidget(dataframe=df,
                                                      checkable_columns=checkable_columns,
                                                      status_columns=status_columns)
        table_widget.resize(800, 500)
        table_widget.show()

        sys.exit(app.exec())
    ```

DefaultDelegate:
    A delegate that combines input validation and center-left alignment for cell content.
    It ensures that data validation is applied during editing and that content is aligned
    to the left and centered vertically, making it easy to apply default behavior to all
    columns.

Example:
    Using DefaultDelegate in PandasTableWidget:

    ```python

    from expert_gui_core.gui.widgets import pandas_table

    # Create a PandasTableWidget with default delegate
    table_widget = pandas_table.PandasTableWidget(dataframe=df)
    table_widget.setItemDelegate(pandas_table.DefaultDelegate(self))
    ```

    The DefaultDelegate can be overridden to create custom alignment or perform conditional coloring
    of the table rows for example.

   ```python

    class LeftAlignmentDelegate(pandas_table.DefaultDelegate):
        @override
        def initStyleOption(self, option, index) -> None:
            super().initStyleOption(option, index)
            option.displayAlignment = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter
    ```

"""

from ._model import PandasTableModel
from ._view import PandasTableView
from .pandastable import PandasTableWidget, DefaultDelegate


__all__ = ["PandasTableModel", "PandasTableView", "PandasTableWidget", "DefaultDelegate"]
