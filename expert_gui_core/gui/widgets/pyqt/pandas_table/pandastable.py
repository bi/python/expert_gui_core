import sys
import typing

if sys.version_info >= (3, 12):
    pass
else:
    pass

import pandas as pd
from PyQt5 import QtWidgets

from expert_gui_core.gui.widgets.pyqt import pandas_table

from ._view import (
    BooleanPaintingDelegate,
    CheckboxCenteringDelegate,
    InputValidationDelegate,
    AlignCenterDelegate,
    PandasTableView
)

class DefaultDelegate(InputValidationDelegate, AlignCenterDelegate):
    def __init__(self, parent: typing.Optional[QtWidgets.QWidget] = None):
        """
        Default delegate that combines input validation and text center alignment.

        Args:
            parent (typing.Optional[QtWidgets.QWidget]): Parent widget, if any.
        """
        super().__init__(parent)


class PandasTableWidget(PandasTableView):
    def __init__(
            self,
            dataframe: pd.DataFrame,
            checkable_columns: typing.Optional[typing.List[str]] = None,
            status_columns: typing.Optional[typing.List[str]] = None,
            parent: typing.Optional[QtWidgets.QWidget] = None,
    ):
        """
        A QTableView that uses a PandasTableModel to display a DataFrame.

        Note:
            This class is a wrapper around the PandasTableView, that initializes some delegates depending on
            the parameters passed to the init method.

        Args:
            dataframe (pd.DataFrame): The pandas DataFrame to display.
            checkable_columns (typing.Optional[typing.List[str]]): Columns with checkboxes.
            status_columns (typing.Optional[typing.List[str]]): Columns with status (e.g., True/False).
            parent (typing.Optional[QtWidgets.QWidget]): Parent widget, if any.
        """
        super().__init__(parent)
        self.checkable_columns = checkable_columns or []
        self.status_columns = status_columns or []

        self.model = pandas_table.PandasTableModel(
            dataframe,
            checkable_columns=self.checkable_columns,
            status_columns=self.status_columns,
        )
        self.setModel(self.model)

        self.setItemDelegate(DefaultDelegate(self))

        for col in self.checkable_columns:
            checkbox_delegate = CheckboxCenteringDelegate(self)
            idx = self.model.dataframe.columns.get_loc(col)
            self.setItemDelegateForColumn(idx, checkbox_delegate)

        for col in self.status_columns:
            boolean_delegate = BooleanPaintingDelegate(self)
            idx = self.model.dataframe.columns.get_loc(col)
            self.setItemDelegateForColumn(idx, boolean_delegate)

    def refresh(self):
        """
        Refreshes the table view, reflecting any changes in the DataFrame.

        Thread-safe implementation using a custom signal.
        """
        self.model.layout_refresh_requested.emit()

    def dark_(self):
        """Switches to a dark theme."""
        pass

    def light_(self):
        """Switches to a light theme."""
        pass
