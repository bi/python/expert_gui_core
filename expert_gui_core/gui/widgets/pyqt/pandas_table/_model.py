import sys
import typing

import pandas as pd

if sys.version_info >= (3, 12):
    from typing import override
else:
    from typing_extensions import override

from PyQt5 import QtCore


class SetDataError(Exception):
    def __init__(self, exception, value, row, column):
        """
        Groups possible exceptions that can happen during PandasTableModel.setData.

        Args:
            exception (Exception): The original exception that caused the error.
            value (typing.Any): The value that was attempted to be set.
            row (int): The row index where the error occurred.
            column (int): The column index where the error occurred.
        """
        message = (
            f"{exception.__class__.__name__}: {exception}. Failed to set value '{value}' at row {row}, column {column}"
        )
        super().__init__(message)


class PandasTableModel(QtCore.QAbstractTableModel):
    layout_refresh_requested = QtCore.pyqtSignal()
    checkbox_state_changed = QtCore.pyqtSignal(QtCore.QModelIndex, bool)

    def __init__(
            self,
            dataframe: pd.DataFrame,
            status_columns: typing.Optional[typing.List[str]] = None,
            checkable_columns: typing.Optional[typing.List[str]] = None,
            parent: typing.Optional[QtCore.QObject] = None,
    ) -> None:
        """
        A model to interface a Qt view with pandas DataFrame.
        Initialize the table model with a DataFrame.

        Note:
            This implementation wraps the layoutChanged signal in a slot for thread safety.
            Developers can Instantiate a PandasTableView in the main app thread and emit layout_refresh_requested
            or set a new dataframe to the model from a different thread.

        Args:
            dataframe (pd.DataFrame): The initial DataFrame to display in the view.
            status_columns (List[str], optional): List of column names indicating status columns.
            checkable_columns (List[str], optional): List of column names to be displayed as checkboxes.
            parent (QtCore.QObject, optional): The parent object of the model, if any.
        """
        super().__init__(parent)
        self._dataframe = dataframe
        self._status_columns = status_columns or []
        self._checkable_columns = checkable_columns or []

        self.layout_refresh_requested.connect(self.layoutChanged.emit)

    @property
    def dataframe(self) -> pd.DataFrame:
        return self._dataframe

    @dataframe.setter
    def dataframe(self, df: pd.DataFrame) -> None:
        self._dataframe = df
        self.layout_refresh_requested.emit()

    @property
    def status_columns(self) -> typing.List[str]:
        """
        Get-only property. Indicates which columns from the pd.Dataframe model are status bound.
        """
        return self._status_columns

    @property
    def checkable_columns(self) -> typing.List[str]:
        """
        Get-only property. Indicates which columns are drawn as checkboxes.
        """
        return self._checkable_columns

    @override
    def rowCount(self, parent=QtCore.QModelIndex()) -> int:
        """
        Get the number of rows in the DataFrame.

        Args:
            parent (QtCore.QModelIndex, optional): The parent index. Defaults to a new QModelIndex.

        Returns:
            int: The number of rows in the DataFrame.
        """
        return len(self._dataframe) if parent == QtCore.QModelIndex() else 0

    @override
    def columnCount(self, parent: QtCore.QModelIndex = QtCore.QModelIndex()) -> int:
        """
        Get the number of columns in the DataFrame.

        Args:
            parent (QtCore.QModelIndex, optional): The parent index. Defaults to a new QModelIndex.

        Returns:
            int: The number of columns in the DataFrame.
        """
        return len(self._dataframe.columns)

    @override
    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.ItemDataRole.DisplayRole) -> typing.Any:
        """
        Retrieve the data at the specified index for the given role.

        Args:
            index (QtCore.QModelIndex): The index of the cell.
            role (QtCore.Qt.ItemDataRole, optional): The data role, typically DisplayRole. Defaults to QtCore.Qt.DisplayRole.

        Returns:
            typing.Any: The data in the specified cell or None if invalid.
        """
        if not index.isValid():
            return None

        column_name = self._dataframe.columns[index.column()]

        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            if column_name in self._checkable_columns:
                return
            return str(self._dataframe.iloc[index.row(), index.column()])

        elif role == QtCore.Qt.ItemDataRole.EditRole:
            return self._dataframe.iloc[index.row(), index.column()]

        elif role == QtCore.Qt.ItemDataRole.CheckStateRole:
            if column_name in self._checkable_columns:
                return (
                    QtCore.Qt.CheckState.Checked
                    if self._dataframe.iloc[index.row(), index.column()]
                    else QtCore.Qt.CheckState.Unchecked
                )

        elif role == QtCore.Qt.ItemDataRole.UserRole:
            return self._dataframe.iloc[index.row(), index.column()]

        return None

    @override
    def setData(
            self, index: QtCore.QModelIndex, value: typing.Any, role: int = QtCore.Qt.ItemDataRole.EditRole
    ) -> bool:
        """
        Set the data for the given index in the model.

        Args:
            index (QtCore.QModelIndex): The index of the item to set data for.
            value (typing.Any): The value to set at the specified index.
            role (int, optional): The role for which the data is set. Defaults to EditRole.

        Returns:
            bool: True if the data was set successfully, False otherwise.

        Raises:
            SetDataError: If setting the data fails due to type conversion or other errors.
        """
        if index.isValid() and role == QtCore.Qt.ItemDataRole.EditRole:
            column_type = self._dataframe.dtypes.iloc[index.column()]

            try:
                cast_value = column_type.type(value)
                self._dataframe.iat[index.row(), index.column()] = cast_value
                self.dataChanged.emit(index, index)
                return True
            except (ValueError, TypeError, OverflowError) as e:
                raise SetDataError(e, value, index.row(), index.column())

        if index.isValid() and role == QtCore.Qt.ItemDataRole.CheckStateRole:
            column_name = self._dataframe.columns[index.column()]
            if column_name in self._checkable_columns:
                self._dataframe.iat[index.row(), index.column()] = value == QtCore.Qt.CheckState.Checked
                self.checkbox_state_changed.emit(index, value == QtCore.Qt.CheckState.Checked)
                self.dataChanged.emit(index, index, (role,))
                return True

        return False

    @override
    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        """
        Get the item flags for the specified index.

        Args:
            index (QtCore.QModelIndex): The index for which to get the item flags.

        Returns:
            QtCore.Qt.ItemFlags: The flags indicating the capabilities of the item at the index.
        """
        default_flags = super().flags(index)
        if index.isValid():
            column_name = self._dataframe.columns[index.column()]
            if column_name in self._checkable_columns:
                return default_flags | QtCore.Qt.ItemFlag.ItemIsEditable | QtCore.Qt.ItemFlag.ItemIsUserCheckable
            return default_flags | QtCore.Qt.ItemFlag.ItemIsEditable
        return default_flags

    @override
    def headerData(
            self, section: int, orientation: QtCore.Qt.Orientation, role: int = QtCore.Qt.ItemDataRole.DisplayRole
    ) -> typing.Optional[str]:
        """
        Get the header data for the specified section and orientation.

        Args:
            section (int): The section index, representing either a row or column.
            orientation (QtCore.Qt.Orientation): The orientation, horizontal for columns, vertical for rows.
            role (QtCore.Qt.ItemDataRole, optional): The data role, typically DisplayRole. Defaults to QtCore.Qt.DisplayRole.

        Returns:
            Optional[str]: The header data as a string or None if the role is not DisplayRole.
        """
        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            if orientation == QtCore.Qt.Orientation.Horizontal:
                return str(self._dataframe.columns[section])
            if orientation == QtCore.Qt.Orientation.Vertical:
                return str(self._dataframe.index[section])
        return None
