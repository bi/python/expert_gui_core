import logging
import sys
import typing

import qtawesome as qta

from expert_gui_core.gui import utils

if sys.version_info >= (3, 12):
    from typing import override
else:
    from typing_extensions import override

from PyQt5 import QtCore, QtWidgets, QtGui

from . import _model


class InputValidationDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent: typing.Optional[QtWidgets.QWidget] = None):
        """
        QItemDQStyledItemDelegate implementation with input validation.

        Args:
            parent (typing.Optional[QtWidgets.QWidget], optional): The parent widget, if any.
        """
        super().__init__(parent)
        self._logger = logging.getLogger(parent.__class__.__name__)
        logging.basicConfig(level=logging.WARNING)

    @override
    def setModelData(
            self,
            editor: typing.Optional[QtWidgets.QWidget],
            model: typing.Optional[QtCore.QAbstractItemModel],
            index: QtCore.QModelIndex,
    ) -> None:
        """
        Override setModelData to validate input before setting it in the model.

        Args:
            editor (typing.Optional[QtWidgets.QWidget]): The editor widget containing the new data.
            model (typing.Optional[QtCore.QAbstractItemModel]): The model to update.
            index (QtCore.QModelIndex): The index of the item being edited.
        """
        if editor is None or model is None:
            return

        value = editor.text() if isinstance(editor, QtWidgets.QLineEdit) else editor.value()
        try:
            model.setData(index, value, QtCore.Qt.ItemDataRole.EditRole)
        except _model.SetDataError as e:
            self._logger.warning(e)


class AlignCenterDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent: typing.Optional[QtWidgets.QWidget] = None):
        """
        QItemDQStyledItemDelegate implementation that centers text within cells in a QTableView.

        Args:
            parent (typing.Optional[QtWidgets.QWidget], optional): The parent widget, if any.
        """
        super().__init__(parent)

    @override
    def initStyleOption(
            self, option: typing.Optional[QtWidgets.QStyleOptionViewItem], index: QtCore.QModelIndex
    ) -> None:
        super().initStyleOption(option, index)
        if option is None:
            return
        option.displayAlignment = QtCore.Qt.AlignmentFlag.AlignCenter


class BooleanPaintingDelegate(AlignCenterDelegate):
    def __init__(self, parent: typing.Optional[QtWidgets.QWidget] = None):
        """
        QStyledItemDelegate implementation for PandasTableView(QtWidgets.QTableView) datatype highlighting.

        Args:
            parent (typing.Optional[QtWidgets.QWidget], optional): The parent widget, if any.
        """
        super().__init__(parent)

    @override
    def paint(
            self,
            painter: typing.Optional[QtGui.QPainter],
            option: QtWidgets.QStyleOptionViewItem,
            index: QtCore.QModelIndex,
    ) -> None:
        """
        Paints table cells with a semi-transparent green or red background
        if the cell's value is a boolean, while retaining the cell's text visibility.

        Args:
            painter (typing.Optional[QtGui.QPainter]): The painter to draw the cell content.
                It may be None, in which case no painting should occur.
            option (QtWidgets.QStyleOptionViewItem): Style options for the item.
            index (QtCore.QModelIndex): Index of the item to be painted.
        """
        super().paint(painter, option, index)

        if painter is None:
            return
        value = index.data(QtCore.Qt.ItemDataRole.DisplayRole)
        if value in {"True", "False"}:
            color = QtGui.QColor(0, 255, 0, 100) if value == "True" else QtGui.QColor(255, 0, 0, 100)
            painter.fillRect(option.rect, color)


class CheckboxCenteringDelegate(AlignCenterDelegate):
    def __init__(self, parent: typing.Optional[QtWidgets.QWidget] = None):
        """
        QItemDelegate implementation for aligning checkboxes added to TableViews.

        Args:
            parent (typing.Optional[QtWidgets.QWidget], optional): The parent widget, if any.
        """
        super().__init__(parent)

    @override
    def paint(
            self,
            painter: typing.Optional[QtGui.QPainter],
            option: QtWidgets.QStyleOptionViewItem,
            index: QtCore.QModelIndex,
    ) -> None:
        """
        Redraws checkboxes centered.

        Args:
            painter (typing.Optional[QtGui.QPainter]): The painter to draw the cell content.
                It may be None, in which case no painting should occur.
            option (QtWidgets.QStyleOptionViewItem): Style options for the item.
            index (QtCore.QModelIndex): Index of the item to be painted.
        """
        if painter is None:
            return
        if index.data(QtCore.Qt.ItemDataRole.CheckStateRole) is not None:
            painter.save()
            check_box_style_option = QtWidgets.QStyleOptionButton()
            check_box_style_option.state = (
                QtWidgets.QStyle.StateFlag.State_On
                if index.data(QtCore.Qt.ItemDataRole.CheckStateRole) == QtCore.Qt.CheckState.Checked
                else QtWidgets.QStyle.StateFlag.State_Off
            )
            check_box_style_option.rect = QtCore.QRect(
                option.rect.center() - QtCore.QPoint(10, 10), QtCore.QSize(20, 20)
            )
            app_style = QtWidgets.QApplication.style()
            if app_style:
                app_style.drawControl(
                    QtWidgets.QStyle.ControlElement.CE_CheckBox,
                    check_box_style_option,
                    painter,
                )
            painter.restore()
        else:
            super().paint(painter, option, index)

    @override
    def editorEvent(
            self,
            event: typing.Optional[QtCore.QEvent],
            model: typing.Optional[QtCore.QAbstractItemModel],
            option: QtWidgets.QStyleOptionViewItem,
            index: QtCore.QModelIndex,
    ) -> bool:
        """
        Handles mouse events for checkable items in the table view.

        Args:
            event (typing.Optional[QtCore.QEvent]): The event to handle.
            model (typing.Optional[QtCore.QAbstractItemModel]): The model backing the view.
            option (QtWidgets.QStyleOptionViewItem): Style options for the item.
            index (QtCore.QModelIndex): Index of the item being interacted with.

        Returns:
            bool: True if the event was handled, False otherwise.
        """
        if event is None or model is None:
            return False
        if event.type() == QtCore.QEvent.Type.MouseButtonRelease:
            if index.flags() & QtCore.Qt.ItemFlag.ItemIsUserCheckable:
                current_state = model.data(index, QtCore.Qt.ItemDataRole.CheckStateRole)
                new_state = (
                    QtCore.Qt.CheckState.Unchecked
                    if current_state == QtCore.Qt.CheckState.Checked
                    else QtCore.Qt.CheckState.Checked
                )
                model.setData(index, new_state, QtCore.Qt.ItemDataRole.CheckStateRole)
                return True
        return super().editorEvent(event, model, option, index)


class PandasTableView(QtWidgets.QTableView):
    def __init__(
            self,
            model: typing.Optional[_model.PandasTableModel] = None,
            parent: typing.Optional[QtWidgets.QWidget] = None,
    ):
        """
        A view for displaying and editing a pandas DataFrame through a table interface.

        Note:
            Not passing the model instance in the constructor will create a PandasTableView object without any delegates.

        Args:
            model (typing.Optional[PandasTableModel], optional): The model backing the view, if any.
            parent (typing.Optional[QtWidgets.QWidget], optional): The parent widget, if any.
        """
        super().__init__(parent)

        self.setItemDelegate(InputValidationDelegate(self))  # Default Delegate used if editing is enabled
        self.setAlternatingRowColors(True)

        if model:
            self.setModel(model)
            for col in model.checkable_columns:
                checkbox_delegate = CheckboxCenteringDelegate(self)
                idx = model.dataframe.columns.get_loc(col)
                if isinstance(idx, int):
                    self.setItemDelegateForColumn(idx, checkbox_delegate)

            for col in model.status_columns:
                boolean_delegate = BooleanPaintingDelegate(self)
                idx = model.dataframe.columns.get_loc(col)
                if isinstance(idx, int):
                    self.setItemDelegateForColumn(idx, boolean_delegate)

    @override
    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        """
        Handles key press events to enable copying to the clipboard.

        Args:
            event (QtGui.QKeyEvent): The key event.
        """
        if event.matches(QtGui.QKeySequence.Copy):
            self._copy_selection_to_clipboard()
        else:
            super().keyPressEvent(event)

    @override
    def mouseDoubleClickEvent(self, e: typing.Optional[QtGui.QMouseEvent]) -> None:
        """
        Custom Implementation that blocks editing TableItems Flagged as ItemIsUserCheckable.

        Args:
            e (typing.Optional[QtGui.QMouseEvent], optional): Mouse Event.
        """
        if e is None:
            return

        index = self.indexAt(e.pos())
        delegate = self.itemDelegateForColumn(index.column())

        if isinstance(delegate, BooleanPaintingDelegate) or isinstance(delegate, CheckboxCenteringDelegate):
            return
        super().mouseDoubleClickEvent(e)

    @override
    def contextMenuEvent(self, event: QtGui.QContextMenuEvent) -> None:
        """
        Overrides the context menu event to display a custom context menu.

        Args:
            event (QtGui.QContextMenuEvent): The context menu event.
        """
        menu = QtWidgets.QMenu(self)

        # Copy to Clipboard action
        copy_icon = qta.icon("fa5.clipboard")
        copy_action = menu.addAction(copy_icon, "Copy to Clipboard")
        copy_action.triggered.connect(self._copy_selection_to_clipboard)

        # Export Selection action
        export_icon = qta.icon("fa5.file")
        export_action = menu.addAction(export_icon, "Export Selection")
        export_action.triggered.connect(self._export_selection_to_file)

        menu.exec(event.globalPos())

    def _extract_selection_text(
            self,
            separator: str = '\t',
            include_headers: bool = True
    ) -> str:
        """
        Extracts the text from the selected table cells, optionally including headers.

        Args:
            separator (str): The separator used to join columns. Defaults to '\t'.
            include_headers (bool): Whether to include column headers. Defaults to True.

        Returns:
            str: The formatted text representing the selection.
        """
        selection = self.selectionModel().selectedIndexes()
        if not selection:
            return ''

        selected_columns = sorted(set(index.column() for index in selection))
        headers = [self.model.headerData(col, QtCore.Qt.Orientation.Horizontal, QtCore.Qt.ItemDataRole.DisplayRole)
                   for col in selected_columns] if include_headers else []

        rows = [separator.join(headers)] if headers else []

        current_row = selection[0].row()
        row_data = [""] * len(selected_columns)

        for index in selection:
            if index.row() != current_row:
                rows.append(separator.join(row_data))
                row_data = [""] * len(selected_columns)
                current_row = index.row()

            col_index = selected_columns.index(index.column())
            value = self.model.data(index, QtCore.Qt.ItemDataRole.UserRole)
            row_data[col_index] = str(value) if value is not None else ""

        if any(row_data):
            rows.append(separator.join(row_data))

        return '\n'.join(rows)

    def _copy_selection_to_clipboard(self) -> None:
        """
        Copies the selected table data to the clipboard, including column headers
        for the selected columns.
        """
        selection_text = self._extract_selection_text()
        QtWidgets.QApplication.clipboard().setText(selection_text)

    class _FileDialog(QtWidgets.QFileDialog):
        """
        Custom QFileDialog that adds a QComboBox for choosing separator input
        instead of the file type filter.
        """

        def __init__(self, parent=None):
            super().__init__(parent)

            self._separator_label = QtWidgets.QLabel("Separator:", self)
            self._separator_combo = QtWidgets.QComboBox(self)
            self._separator_map = {"'\\t' (Tab)": "\t", "',' (Comma)": ",", "' ' (Space)": " "}
            self._separator_combo.addItems(self._separator_map.keys())

            self._replace_file_type_elements()

        def _replace_file_type_elements(self):
            """
            Replaces file type filtering widgets in the layout.
            """
            file_type_label = self.findChild(QtWidgets.QWidget, "fileTypeLabel")
            file_type_combo = self.findChild(QtWidgets.QWidget, "fileTypeCombo")

            # noinspection PyArgumentList
            self.layout().addWidget(self._separator_label, 3, 0)
            self.layout().removeWidget(file_type_label)
            # noinspection PyArgumentList
            self.layout().addWidget(self._separator_combo, 3, 1)
            self.layout().removeWidget(file_type_combo)

        def get_separator(self) -> str:
            """Returns the selected separator."""
            return self._separator_map[self._separator_combo.currentText()]

    def _export_selection_to_file(self) -> None:
        """
        Exports the selected table data to a file chosen by the user.
        """
        dialog = self._FileDialog(self)

        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            file_path = dialog.selectedFiles()[0]
            file_type = "txt" if file_path.endswith(".txt") else "csv"
            selection_text = self._extract_selection_text(separator=dialog.get_separator())
            try:
                utils.save_to_file(selection_text, file_path, file_type)
                QtWidgets.QMessageBox.information(self, "Export Successful", "Selection exported successfully.")
            except Exception as e:
                QtWidgets.QMessageBox.critical(self, "Export Failed", f"Failed to export selection: {e}")
