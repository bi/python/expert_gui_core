import sys
import urllib.request

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QTimer

from PyQt5.QtGui import QPixmap

from PyQt5.QtWidgets import QApplication, QSizePolicy, QWidget, QGridLayout, QMainWindow, QLabel


class MultiVistarWidget(QWidget):

    def __init__(self, vistars={"LHC1"}):
        super().__init__()

        self.layout = QGridLayout(self)
        ind = 0
        for vistar in vistars:
            self.layout.addWidget(VistarWidget(vistar=vistar), int(ind / 4), ind % 4 )
            ind = ind + 1

class VistarWidget(QWidget):

    _VISTAR = {
        "LINAC3": "http://cs-ccr-www3.cern.ch/vistar_capture/lin.png",
        "LINAC4": "http://cs-ccr-www3.cern.ch/vistar_capture/ln4.png",
        "PSB": "http://cs-ccr-www3.cern.ch/vistar_capture/psb.png",
        "PSBBLM": "http://cs-ccr-www3.cern.ch/vistar_capture/psbblm.png",
        "GPS": "http://cs-ccr-www3.cern.ch/vistar_capture/gps.png",
        "HRS": "http://cs-ccr-www3.cern.ch/vistar_capture/hrs.png",
        "LEIR": "http://cs-ccr-www3.cern.ch/vistar_capture/leir.png",
        "CPS": "http://cs-ccr-www3.cern.ch/vistar_capture/cps.png",
        "CPSBLM": "http://cs-ccr-www3.cern.ch/vistar_capture/cpsblm.png",
        "PEA": "http://cs-ccr-www3.cern.ch/vistar_capture/pea.png",
        "ADE": "http://cs-ccr-www3.cern.ch/vistar_capture/ade.png",
        "ELENA": "http://cs-ccr-www3.cern.ch/vistar_capture/elena.png",
        "SPS1": "http://cs-ccr-www3.cern.ch/vistar_capture/sps.png",
        "SPSBSRT": "http://cs-ccr-www3.cern.ch/vistar_capture/spsbsrt.png",
        "LHC1": "http://cs-ccr-www3.cern.ch/vistar_capture/lhc.png",
        "LHC3": "http://cs-ccr-www3.cern.ch/vistar_capture/lhc3.png",
        "LHCBSRT":'http://cs-ccr-www3.cern.ch/vistar_capture/lhcbsrt.png',
        "LHCABORTGAP": "http://cs-ccr-www3.cern.ch/vistar_capture/lhcabortgap.png",
        "LHCLUMINOSITY": "http://cs-ccr-www3.cern.ch/vistar_capture/lhclumi.png"
    }

    img_changed = pyqtSignal()

    def __init__(self, vistar="LHC1"):
        super().__init__()

        self._name = vistar
        self.vistar = QLabel()
        self.vistar.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.vistar.setScaledContents(True)

        if self._name not in VistarWidget._VISTAR:
            return

        self.pixmap = QPixmap()
        img = urllib.request.urlopen(VistarWidget._VISTAR[self._name]).read()
        self.pixmap.loadFromData(img)

        self.vistar.setPixmap(self.pixmap)

        layout_vistar = QGridLayout(self)
        layout_vistar.setContentsMargins(0, 0, 0, 0)
        layout_vistar.setSpacing(0)

        layout_vistar.addWidget(self.vistar,0,0)

        layout_vistar.setRowStretch(0,1)
        layout_vistar.setColumnStretch(0,1)

        self.img_changed.connect(self.refresh_image)

        self.timer = QTimer()
        self.timer.timeout.connect(self.update)

        self.timer.start(1000)

    @staticmethod
    def get_list_vistars():
        return VistarWidget._VISTAR.keys()

    @pyqtSlot()
    def refresh_image(self):
        self.vistar.setPixmap(self.pixmap)

    def update(self):
        img = urllib.request.urlopen(VistarWidget._VISTAR[self._name]).read()
        self.pixmap.loadFromData(img)
        self.img_changed.emit()

class _Example(QMainWindow):
    """

    Example class to test

    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """Init user interface"""
        central_widget = QWidget()

        self.layout = QGridLayout(central_widget)

        # ind=0
        # for vistar in VistarWidget.get_list_vistars():
        #     self.layout.addWidget(VistarWidget(vistar=vistar), ind%4, int(ind/4))
        #     ind=ind+1

        multi_vistars = MultiVistarWidget(vistars=VistarWidget.get_list_vistars())
        # multi_vistars = MultiVistarWidget(vistars={"LHC1","LHCBSRT"})
        self.layout.addWidget(multi_vistars,0,0)

        self.setCentralWidget(central_widget)
        self.resize(400, 400)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
