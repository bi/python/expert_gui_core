import pyqtgraph as pg

from expert_gui_core.gui.common.colors import Colors


class BasicImageItem(pg.ImageItem):
    """
    Low-level imageitem to draw data
    
    :param parent: Parent object
    :type parent: object
    :param title: Title shown top center.
    :type title: str, optional
    :param name: Name to identify the widget (default="default").
    :type name: strl, optional
    :param dyn_cursor: Not used.
    :type dyn_cursor: bool, optional    
    :param palette: Image palette (default:"CET-D1A")       
    :type string: str, optional
    """

    def __init__(self, parent, name="default", title="", dyn_cursor=False, palette="CET-D1A", roi=True, lut=False):
        pg.ImageItem.__init__(self, title=title)

        self._parent = parent
        self._name = name
        self._image_roi = None
        self._row = 0
        self._col = 0
        self._dyn_cursor = dyn_cursor
        self._title = title

        # palette

        if palette != "default":
            self.setColorMap(colorMap=palette)
        elif palette == "dyngray":
            cmap = Colors.CMAP_IM_DYNGRAY
            self.setColorMap(cmap)
        elif palette == "dbgray":
            cmap = Colors.CMAP_IM_DBGRAY
            self.setColorMap(cmap)
        elif palette == "expgray":
            cmap = Colors.CMAP_IM_EXPGRAY
            self.setColorMap(cmap)
        elif palette == "hawai":
            cmap = Colors.CMAP_IM_HAWAI
            self.setColorMap(cmap)
        elif palette == "purple":
            cmap = Colors.CMAP_IM_PURPLE
            self.setColorMap(cmap)
        else:
            cmap = Colors.CMAP_IM_DEFAULT
            self.setColorMap(cmap)

        # cross hair dyn

        self.pen_cursor_line_dyn = pg.mkPen(color=Colors.COLOR_BLUE, width=1, dash=[2, 4])
        self.pen_cursor_line = pg.mkPen(color=Colors.COLOR_BLUE, width=1)
        self._hline_dyn = None
        self._vline_dyn = None

        # custom ROI for selecting an image region

        self._update_roi_range = True
        if roi:
            self._roi = pg.ROI([5, 5], [30, 30])
            self._roi.addScaleHandle([0.5, 1], [0.5, 0.5])
            self._roi.addScaleHandle([0, 0.5], [0.5, 0.5])
            self._roi.addScaleHandle([1, 0.5], [0.5, 0.5])
            self._roi.addScaleHandle([0.5, 0], [0.5, 0.5])
            self._roi.setZValue(-1)
            self._roi.sigRegionChanged.connect(self.update_roi)
        else:
            self._roi = None

        if lut:
            self._hist = pg.HistogramLUTItem()
            self._hist.setImageItem(self)
            self._hist.hide()
        else:
            self._hist = None

    def set_roi_range(self, xmin, xmax, ymin, ymax):
        """
        Set ROI range.
        """
        if not self._update_roi_range:
            return
        if self._roi is None:
            return
        self._roi.setPos([xmin, ymin])
        self._roi.setSize(size=[xmax - xmin, ymax - ymin])
        self._update_roi_range = False

    def get_plot_item(self):
        """
        Get the parent plot item.
        """
        return self._parent

    def dark_(self):
        """
        Set dark theme.
        """
        self.getViewBox().setBackgroundColor(Colors.COLOR_LBLACK)

        if self._vline_dyn is not None:
            self._vline_dyn.setPen(self.pen_cursor_line_dyn_dark)
            self._hline_dyn.setPen(self.pen_cursor_line_dyn_dark)

    def light_(self):
        """
        Set light theme.
        """
        self.getViewBox().setBackgroundColor(Colors.COLOR_WHITE)

        if self._vline_dyn is not None:
            self._vline_dyn.setPen(self.pen_cursor_line_dyn)
            self._hline_dyn.setPen(self.pen_cursor_line_dyn)

    def set_grid_pos(self, col, row):
        """
        Set the grid col/row parameters.
        
        :param row: Row position.
        :type row: int
        :param col: Col position.
        :type col: int
        """
        self._row = row
        self._col = col

    def show_roi(self, show):
        """
        Show/hide ROI rectangle.
        
        :param show: Show Region of Interest.
        :type show: bool
        """
        if self._roi is not None:
            if show:
                self._parent.addItem(self._roi)
                self._parent.set_roi(True)
                self._roi.setZValue(10)
                self.update_roi()
            else:
                self._roi.setZValue(-1)
                self._parent.set_roi(False)
                self._parent.removeItem(self._roi)

    def show_lut(self, show):
        """
        Show/hide LUT right window rectangle.
        
        :param show: Show or not.
        :type show: bool
        """
        if self._hist is not None:
            if show:
                self._hist.show()
            else:
                self._hist.hide()

    def get_roi(self):
        """
        Return ROI region.
        
        :return: Region of interest.
        :rtype: pg.ROI
        """
        return self._roi

    def set_image_roi(self, roi):
        """
        Set the image ROI image item.

        :param roi: Region of interest.
        :type roi: pg.ROI
        """
        self._image_roi = roi

    def update_roi(self):
        """
        Update ROI image item widget linked to this image.
        """
        if self._image_roi is not None and self._roi.zValue() > 0:
            x = int(self._roi.pos()[0])
            y = int(self._roi.pos()[1])
            w = int(self._roi.size()[0])
            h = int(self._roi.size()[1])
            wimage = self.image.shape[0]
            himage = self.image.shape[1]
            if x < 0:
                w = w + x
                x = 0
            if y < 0:
                h = h + y
                y = 0
            if h <= 0 or w <= 0:
                return
            if x >= wimage or y >= himage:
                return
            if x + w >= wimage:
                w = wimage - x
            if y + h >= himage:
                h = himage - y
            self._image_roi.setImage(self.image[x:w + x, y:h + y])
