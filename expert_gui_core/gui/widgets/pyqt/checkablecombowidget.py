import sys

from PyQt5.QtCore import pyqtSignal, Qt

from PyQt5.QtGui import QStandardItemModel

from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QComboBox, QListView

from expert_gui_core.gui.common.colors import Colors


class CheckableComboBox(QComboBox):
    """QtWidgets QComboBox subclass that supports multiple selection.

    Arguments:
        title (str): Text that will be displayed in the widget.
        items (list[str], optional): A list of strings to initialize the combo box with checkable items.

    Reference:
        https://stackoverflow.com/questions/22775095/pyqt-how-to-set-combobox-items-be-checkable
    """
    itemCheckedSignal = pyqtSignal(str, bool)

    def __init__(self, title="", items=None, parent=None):
        super().__init__(parent)
        self._title = title
        self.setView(QListView(self))
        self.view().pressed.connect(self.handleItemPressed)
        self.setModel(QStandardItemModel(self))
        self._changed = False
        if items is not None:
            self.addItems(items)
            for i in range(len(items)):
                self.setItemChecked(i, False)

        self.repaint()

    def handleItemPressed(self, index):
        item = self.model().itemFromIndex(index)
        if item.checkState() == Qt.Checked:
            item.setCheckState(Qt.Unchecked)
        else:
            item.setCheckState(Qt.Checked)
        self.itemChecked(item)
        self._changed = True

    def hidePopup(self):
        if not self._changed:
            super(CheckableComboBox, self).hidePopup()
        self._changed = False

    def itemChecked(self, item):
        self.itemCheckedSignal.emit(item.text(), item.checkState() == Qt.Checked)

    def setItemChecked(self, index, checked=True):
        item = self.model().item(index, self.modelColumn())
        if checked:
            item.setCheckState(Qt.Checked)
        else:
            item.setCheckState(Qt.Unchecked)

    def light_(self):
        self.setStyleSheet(
            "padding:3px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT5GRAY + ";background-color:" + Colors.STR_COLOR_WHITE + ";selection-color:" + Colors.STR_COLOR_BLACK + ";color:" + Colors.STR_COLOR_BLACK + ";selection-background-color:" + Colors.STR_COLOR_DWHITE + ";")

    def dark_(self):
        self.setStyleSheet(
            "padding:3px;border-radius:4px;border:1px solid " + Colors.STR_COLOR_LIGHT0GRAY + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";selection-color:" + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_WHITE + ";selection-background-color:" + Colors.STR_COLOR_LBLUE + ";")


class ExampleWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.combo = CheckableComboBox(self, items=['a', 'b', 'c'])

        # Optionally add more items if needed
        curr_items_nr = self.combo.model().rowCount()
        for index in range(curr_items_nr, curr_items_nr + 6):
            self.combo.addItem('Item %d' % index)
            self.combo.setItemChecked(index, False)

        self.combo.itemCheckedSignal.connect(self.on_item_checked)
        layout = QVBoxLayout(self)
        layout.addWidget(self.combo)

    def on_item_checked(self, text, checked):
        print(f"Item: {text}, Checked: {checked}")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = ExampleWindow()
    window.setGeometry(100, 100, 400, 100)
    window.show()
    sys.exit(app.exec_())
