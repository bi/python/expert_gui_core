import sys
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot

from PyQt5.QtGui import QPainter, QFontMetrics, QPalette

from PyQt5.QtWidgets import QApplication, QWidget, QMenu, QGridLayout, QMainWindow, QLabel

from expert_gui_core.tools import formatting
from expert_gui_core.gui.common.colors import Colors


class ScalarWidget(QWidget):
    """
    
    Class low-level chart widget displaying multi-arrays
    
    """

    update_signal = pyqtSignal()

    def __init__(self, parent, title="", color=None, background=None, unit="", type_format="str"):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        # Layout        
        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # color
        self.color = color
        self.background = background

        # Data & text
        self.type_format = "str"
        self.text = ""
        self.data = 0
        self.label = _CustomLabel(self.text)

        self.labelleft = QLabel(" ")
        self.labelright = QLabel(" ")
        self.layout.addWidget(self.labelleft, 0, 0, 3, 1)
        self.layout.addWidget(self.labelright, 0, 2, 3 ,1)

        # Unit and title
        self.labelunit = QLabel(unit)
        self.labeltitle = QLabel(title)
        self.layout.addWidget(self.labeltitle, 0, 1)
        self.layout.addWidget(self.label, 1, 1)
        self.layout.addWidget(self.labelunit, 2, 1)
        self.font = self.label.font()
        self.label.setAlignment(Qt.AlignCenter)
        self.labelunit.setAlignment(Qt.AlignCenter)
        self.labeltitle.setAlignment(Qt.AlignCenter)

        self.layout.setRowStretch(0, 1)
        self.layout.setRowStretch(1, 3)
        self.layout.setRowStretch(2, 1)

        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 0)

        self.labeltitle.setStyleSheet("color:rgb(84,190,230);font-weight:bold;")
        self.labelunit.setStyleSheet("color:rgb(160,160,160);font-weight:italic;")

        if self.background is not None:
            self.setStyleSheet("background-color:" + self.background + ";")
            self.labelright.setStyleSheet("background-color:" + self.background + ";")
            self.labelleft.setStyleSheet("background-color:" + self.background + ";")

        if color is not None:
            self.label.setStyleSheet("color:" + color + ";")

        self.update_signal.connect(self.update_data)

    def contextMenuEvent(self, event):
        """Menu event"""
        contextMenu = QMenu(self)
        strAct = contextMenu.addAction("None")
        sciAct = contextMenu.addAction("Scientific")
        hexAct = contextMenu.addAction("HexaDecimal")
        binAct = contextMenu.addAction("Binary")
        dateAct = contextMenu.addAction("Time")
        action = contextMenu.exec_(self.mapToGlobal(event.pos()))
        if action == strAct:
            self.type_format = "str"
        elif action == sciAct:
            self.type_format = "sci"
        elif action == hexAct:
            self.type_format = "hex"
        elif action == binAct:
            self.type_format = "bin"
        elif action == dateAct:
            self.type_format = "date"
        self.label.setText(formatting.format_value_to_string(self.data, type_format=self.type_format))

    def dark_(self):
        """Set dark theme"""
        if self.color is None:
            self.label.setStyleSheet("color:" + Colors.STR_COLOR_DARK_FW + ";")
        else:
            self.label.setStyleSheet("color:" + self.color + ";")
        if self.background is None:
            self.setStyleSheet("background-color:" + Colors.STR_COLOR_FWBLACK + ";")
            self.labelright.setStyleSheet("background-color:" + Colors.STR_COLOR_FWBLACK + ";")
            self.labelleft.setStyleSheet("background-color:" + Colors.STR_COLOR_FWBLACK + ";")

    def light_(self):
        """Set light theme"""
        if self.color is None:
            self.label.setStyleSheet("color:" + Colors.STR_COLOR_LIGHT_FW + ";")
        else:
            self.label.setStyleSheet("color:" + self.color + ";")
        if self.background is None:
            self.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")
            self.labelright.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")
            self.labelleft.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")


    def set_data(self, data, update=True, name="default", name_parent="default", *args, **kwargs):
        """Set data in a plot item"""
        self.data = data
        if update:
            self.update_signal.emit()

    @pyqtSlot()
    def update_data(self):
        """Update data (text)"""
        self.label.setText(formatting.format_value_to_string(self.data, type_format=self.type_format))


class _CustomLabel(QLabel):
    """
    
    Custom QLabel adjusting font size automatically
    
    """

    def adjustFont(self):
        """Adjust font auto"""
        if self.parent():
            rect = self.parent().rect() & self.geometry()
            width = rect.width()
            height = rect.height()
        else:
            width = self.width()
            height = self.height()
        font = self.font()
        text = self.text()
        while True:
            textWidth = QFontMetrics(font).size(
                Qt.TextSingleLine, text).width()
            textHeight = QFontMetrics(font).size(
                Qt.TextSingleLine, text).height()
            if textHeight >= height or textWidth >= width or font.pointSize() >= 600:
                font.setPointSize(font.pointSize() - 1)
                break
            font.setPointSize(font.pointSize() + 1)
        self._font = font

    def setText(self, text):
        """Set text function"""
        super().setText(text)
        self.adjustFont()

    def resizeEvent(self, event):
        """resize widget"""
        self.adjustFont()

    def paintEvent(self, event):
        """Paint event"""
        if self.text():
            qp = QPainter(self)
            qp.setFont(self._font)
            qp.drawText(self.rect(), self.alignment(), self.text())
        else:
            super().paintEvent(event)


class _Example(QMainWindow):
    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """Init user interface"""
        w = QWidget()
        self.scalar = ScalarWidget(w,title="Test",unit="mV")
        self.scalar.set_data(20000000)
        self.setWindowTitle('Scalar')
        self.scalar.dark_()
        mainLayout = QGridLayout()
        mainLayout.addWidget(self.scalar, 0, 0)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(300, 300)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
