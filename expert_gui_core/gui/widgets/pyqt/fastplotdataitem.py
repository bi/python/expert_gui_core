import sys
from threading import Thread
import numpy as np
import pyqtgraph as pg

from PyQt5.QtCore import pyqtSignal, pyqtSlot


class FastPlotDataItem(pg.PlotDataItem):
    """
    (pyqtgraph) PlotDataItem using a compression algorithm based on the size of the array,
    the pixel width to draw and the ViewBox range detection mechanism
    """

    refresh_signal = pyqtSignal(object)

    def __init__(self, *args, **kwds):

        pg.PlotDataItem.__init__(self, *args, **kwds)

        self._plotData = None

        self._rescale = False
        self._update = False
        self._once = True
        self._time_rescale = 0

        self._isviewRangeChanged = False

        self.refresh_signal.connect(self.refresh_plot)

        self.setPlotData(PlotDataClass(x=kwds["x"],
                                       y=kwds["y"],
                                       name=kwds["name"]))

    @property
    def plotData(self):
        return self._plotData

    def set_data(self, y, x=None):

        """
        Main PlotDataItem function visible from outside to set and show data in the plot
        :param y:
        :param x:
        :return:
        """

        self._plotData._x = x
        self._plotData._y = y

        self.setPlotData(self._plotData)

    def setPlotData(self, plotData):

        self._plotData = plotData

        if self.getViewBox() is None:

            # save xmin and xmax from vb range

            self.x1 = plotData._x[0]
            self.x2 = plotData._x[-1]

            self._rescale = False
            self._update = True

        if self.x1 == self.x2:

            self._rescale = False
            self._update = True

            self.x1 = plotData._x[0]
            self.x2 = plotData._x[-1]

        self.reload()

    def _viewRangeChanged(self):
        """
        Engine function called by Thread mechanism only if the vb changed and not already called
        :return:
        """

        if self._plotData is None:
            self._isviewRangeChanged = False
            return

        if self._rescale:

            vb = self.getViewBox()
            if vb is None:
                self._isviewRangeChanged = False
                return

            try:
                if self._update or vb._updatingRange:
                    x1, x2 = vb.viewRange()[0]
                    self.x1 = x1
                    self.x2 = x2
                self._update = True
            except:
                pass

        if self.x1 == self.x2:
            return
        elif self._once and (self.x1==0 and self.x2 == 1):
            self._once = False
            self._isviewRangeChanged = False
            return

        self.reload()

    def viewRangeChanged(self):
        """
        Trigger the refreshing thread of the plot because of a new vb range detected
        The refreshing is protected by a boolean flag
        :return:
        """

        vb = self.getViewBox()

        if self._isviewRangeChanged:
            return

        self._isviewRangeChanged = True

        worker = FastPlotDataItem.VRCWorker(self)
        thread = FastPlotDataItem.VRCWorkerThread(worker)
        thread.start()
        thread.join()

    def reload(self):
        """
        Refresh the plot meaning :
            - new compression
            - refresh graph
        :return:
        """

        if self._plotData is None:
            self.setData([])
            self._isviewRangeChanged = False
            return

        data = self._plotData.sb_downsample(self.x1, self.x2)

        if data is not None:
            self._rescale = False
            self.refresh_signal.emit(data)

    @pyqtSlot(object)
    def refresh_plot(self, data):
        """
        Set data in the pg plot and free the boolean mutex
        :param data:
        :return:
        """

        self.setData(*data)

        self._rescale = True
        self._isviewRangeChanged = False


    class VRCWorker:
        """
        ViewRangeChanged worker manipulate by a dedicated Thread
        """

        def __init__(self, fastPlotDataItem):
            self._fastPlotDataItem = fastPlotDataItem

        @property
        def fastPlotDataItem(self):
            return self._fastPlotDataItem

        def run_task(self):
            self.fastPlotDataItem._viewRangeChanged()


    class VRCWorkerThread(Thread):
        """
        ViewRangeChanged Thread to execute vrc engine function
        """
        def __init__(self, worker_obj):
            super().__init__()
            self.worker_obj = worker_obj

        def run(self):
            self.worker_obj.run_task()


class PlotDataClass:
    """
    Array (x,y)  used byt the plot to be comnpressed
    """
    def __init__(self, x, y, name = ""):

        if x is None:
            x = len(y)
        elif len(x) != len(y):
            raise ValueError

        self._name = name
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def sb_downsample(self, min, max, width=None):
        """
        Custom compression based on visible pixel
        By default the resolution is defined for 2000 pixels
        :param min:
        :param max:
        :param width:
        :return:
        """

        if width is None:
            width = 2000

        # no x array defined

        if self._x is None:
            self._x = np.arange(0, len(self._y)-1)

        if len(self._y) < (4 * width):
            return self._x, self._y

        # retrieve index in array using min and max

        imin, imax = self.sb_searchsorted(self._x, [min, max]).tolist()

        imax=imax-1

        length_x = len(self._x)

        if imin > 0:
            imin = imin - 1
        else:
            imin = 0

        if imax < length_x - 1:
            imax = imax + 1
        else:
            imax = length_x - 1

        # visible array width
        # if array size < 8000 (default) then no compression

        if (imax - imin) < 4 * width:
            return self._x[imin:imax+1], self._y[imin:imax+1]

        # pixel intervals width

        step = int((imax - imin) / width)

        start = imin

        axres = 5 * width * [0]
        ayres = 5 * width * [0]
        count = 0
        prev = None

        # loop for all small intervals (pixels)
        # get min and max
        # if min == max then only one value is defined
        # else then 4 values are set prev,min,max,last

        for i in range(imin, imax, step):

            ytmp = self._y[start: start + step]

            imintmp = np.argmin(ytmp)
            imaxtmp = np.argmax(ytmp)

            mintmp = ytmp[imintmp]
            maxtmp = ytmp[imaxtmp]

            imintmp = start + imintmp
            imaxtmp = start + imaxtmp

            if mintmp == maxtmp:

                if start != prev:
                    axres[count] = self._x[start]
                    ayres[count] = self._y[start]
                    count = count + 1
                    prev = start
            else:

                if imintmp < imaxtmp:

                    if imintmp != start and start != prev:
                        axres[count] = self._x[start]
                        ayres[count] = self._y[start]
                        prev = start
                        count = count + 1

                    if imintmp != prev:
                        axres[count] = self._x[imintmp]
                        ayres[count] = mintmp
                        count = count + 1
                        prev = imintmp

                    if imaxtmp != prev:
                        axres[count] = self._x[imaxtmp]
                        ayres[count] = maxtmp
                        count = count + 1
                        prev = imaxtmp
                else:

                    if imaxtmp != start and start != prev:
                        axres[count] = self._x[start]
                        ayres[count] = self._y[start]
                        prev = start
                        count = count + 1

                    if imaxtmp != prev:
                        axres[count] = self._x[imaxtmp]
                        ayres[count] = maxtmp
                        count = count + 1
                        prev = imaxtmp

                    if imintmp != prev:
                        axres[count] = self._x[imintmp]
                        ayres[count] = mintmp
                        count = count + 1
                        prev = imintmp

                if start + step < length_x - 1:

                    if (start + step) != prev:
                        axres[count] = self._x[start + step]
                        ayres[count] = self._y[start + step]
                        prev = start + step
                        count = count + 1
                else:

                    axres[count] = self._x[-1]
                    ayres[count] = self._y[-1]
                    count = count + 1

            start = start + step

        return np.array(axres[0:count-1]), np.array(ayres[0:count-1])

    def sb_searchsorted(self, sorted_array, values):
        """
        Fast indexes search within an array
        :param sorted_array:
        :param values:
        :return:
        """

        indices = np.empty(len(values), dtype=np.int32)

        for i, value in enumerate(values):

            lo, hi = 0, len(sorted_array)

            while lo < hi:
                mid = (lo + hi) // 2
                if sorted_array[mid] < value:
                    lo = mid + 1
                else:
                    hi = mid
            indices[i] = lo

        return indices

