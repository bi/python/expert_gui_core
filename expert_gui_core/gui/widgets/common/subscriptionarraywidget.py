import numpy as np
from PyQt5.QtCore import pyqtSignal, QEvent, Qt, pyqtSlot
from PyQt5.QtWidgets import QWidget, QApplication, QGridLayout, QMainWindow, QGroupBox, QScrollArea, QLabel, \
                            QLineEdit
from PyQt5.QtGui import QPalette

import sys
import pyqtgraph as pg
from typing import Dict

from expert_gui_core.comm import ccda, fesacomm
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget
from expert_gui_core.tools import formatting


class PropertySubscriptionArrayWidget(QWidget,fesacomm.FesaCommListener):

    filter_changed = pyqtSignal()

    def __init__(self,
                 parent=None,
                 device_names=None, property_name=None, cycle_name: str = "",
                 fields= None, palette="stroke", filter=True):

        super().__init__(parent)
        self._devices = device_names
        self._property = property_name
        self._cycle = cycle_name
        self.fesa_comms = None

        self._time_now = {}

        self.chart_widget = None
        self.chart_palette = palette
        self._colors_plot = {}

        class_info = ccda.get_class_from_device(device_names[0])
        get_pffc_opts = {
            'class_name': class_info["class"],
            'property_name': property_name,
            'class_version': class_info["version"]
        }
        fields_tmp = []
        propertyfields = ccda.get_property_fields_from_class(**get_pffc_opts)

        for propertyfield in propertyfields:
            if fields is not None:
                if propertyfield.name in fields:
                    if (propertyfield.data_type == "array2D" or propertyfield.data_type == "array") and propertyfield.primitive_data_type != "STRING":
                        fields_tmp.append(propertyfield.name)
            else:
                if (propertyfield.data_type == "array2D" or propertyfield.data_type == "array") and propertyfield.primitive_data_type != "STRING":
                    fields_tmp.append(propertyfield.name)

        fields = fields_tmp

        if len(fields) > 0:
            ind_field = 0
            ind_device = 0

            if len(device_names) > 1:
                self._colors_plot["#TOT"] = len(device_names)
            else:
                self._colors_plot["#TOT"] = len(fields)

            for device in device_names:

                if len(device_names) > 1:

                    for field in fields:
                        plot_name = device + "/" + field
                        self._colors_plot[plot_name] = ind_field

                    ind_device = ind_device + 1
                else:

                    for field in fields:
                        plot_name = field
                        self._colors_plot[plot_name] = ind_field

                        ind_field = ind_field + 1

        self._fields = fields or []
        self.field_widgets = {}

        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)
        self.mainlayout.setContentsMargins(0, 0, 0, 0)
        self.mainlayout.setSpacing(0)

        ###
        self.layout = QGridLayout()
        self.layout.setHorizontalSpacing(0)
        self.layout.setVerticalSpacing(0)
        groupBox = QGroupBox()
        groupBox.setLayout(self.layout)
        self.scroll = QScrollArea()
        self.scroll.setWidget(groupBox)
        self.scroll.setWidgetResizable(True)
        ###

        if len(self._fields) > 0:

            self.chart_widget = ChartWidget(self)
            for device in device_names:
                for field in self._fields:
                    if len(device_names) > 1:
                        plot_name = device + "/" + field
                    else:
                        plot_name = field

                    self.chart_widget.add_plot(title=plot_name,
                                               name=plot_name,
                                               show_grid=True,
                                               show_value_axis=True,
                                               dyn_cursor=True)

            self.layout.addWidget(self.chart_widget,0,0)
            self.layout.setColumnStretch(0,1)
            self.layout.setRowStretch(0, 1)

        # filter

        panel_filter = QWidget()

        panel_filter.setStyleSheet("background-color:transparent;")
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(0, 5, 0, 0)
        layout_filter.setSpacing(0)
        layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)
        self._text_filter = QLineEdit("")
        self._text_filter.installEventFilter(self)
        self.filter_changed.connect(self.filter_fields)
        layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
        layout_filter.addWidget(QLabel(" "), 0, 1)
        layout_filter.setColumnStretch(1, 0)
        self.layout.addWidget(panel_filter, 0, 1)

        if len(fields) == 1 and len(device_names) == 1:
            filter = False

        if not filter:
            panel_filter.hide()

        self.mainlayout.addWidget(panel_filter, 0, 0)
        self.mainlayout.addWidget(self.scroll, 1, 0)

        self.mainlayout.setRowStretch(0, 0)
        self.mainlayout.setRowStretch(1, 1)

        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()
        count = 0
        for name_chart in self.chart_widget.get_plot_items():
            if filtertxt is None or filtertxt == "":
                self.chart_widget.get_plot(name_chart).show()
                count = count + 1
            try:
                if name_chart.index(filtertxt) >= 0:
                    self.chart_widget.get_plot(name_chart).show()
                    count = count + 1
                else:
                    self.chart_widget.get_plot(name_chart).hide()
            except:
                self.chart_widget.get_plot(name_chart).hide()
        max_col_show = 3
        if count == 4:
            max_col_show = 2
        if count == 2:
            max_col_show = 1
        elif count >= 16:
            max_col_show = 4
        nrow = int(count/max_col_show) + 1
        self.chart_widget.handle_height(nrow)
        self.chart_widget.arrange_charts()

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (
                Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()
        return False

    def handle_event(self, name: str, value: Dict):

        if name in self._time_now:
            if formatting.current_milli_time() - self._time_now[name] < 20:
                return

        device = name.split("/")[0]

        self._time_now[name] = formatting.current_milli_time()

        ind_field = 0
        for field in self._fields:
            try:
                if value[field].size == 0:
                    value[field]=np.array([0,0])
            except:
                pass

            if len(self.fesa_comms) > 1:
                plot_name = device + "/" + field
            else:
                plot_name = field

            if len(value[field]) > 0:
                shape = value[field].shape
                if len(shape) > 1:
                    data = np.array(value[field][-1], dtype=np.float64)
                else:
                    data = np.array(value[field], dtype=np.float64)

                color_plot = Colors.getColor(self._colors_plot[plot_name] / self._colors_plot["#TOT"],
                                                           max=self._colors_plot["#TOT"],
                                                           palette=self.chart_palette)

                self.chart_widget.set_data(data,
                                        name=plot_name,
                                        name_pi=plot_name,
                                        skipFiniteCheck=True,
                                        antialias=False,
                                        pen=pg.mkPen({'color': color_plot, 'width': 1}),
                                        autoDownsample=True)
                ind_field = ind_field + 1

    def unsubscribe(self):
        if self.fesa_comms:
            for com in self.fesa_comms.values():
                com.unsubscribe()

    def subscribe(self, _cycle: str = None):
        """Initializes the FesaComm object and starts the subscription.

        If, '_cycle' is provided, it will override the 'cycle' attribute and start a new subscription.
        It is good practice to call self.unsubscribe() before changing the cycle the widget is subscribed to.

        Arguments:
            _cycle (str): Cycle name.
        """
        if _cycle is None:
            _cycle = self._cycle

        if self.fesa_comms is None:
            self.fesa_comms = {}
            for dev in self._devices:
                self.fesa_comms[dev] = fesacomm.FesaComm(dev,
                                               self._property,
                                               listener=self)

        self.cycle = _cycle
        for devcom in self.fesa_comms:
            print("subscribe to", devcom, self._property, self._cycle)
            self.fesa_comms[devcom].subscribe(_cycle)

    def light_(self):
        """Set light theme."""
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_WHITE + ";;color:" + Colors.STR_COLOR_BLACK + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT4GRAY + ";")
        if self.chart_widget is not None:
            self.chart_widget.light_()

    def dark_(self):
        """Set dark theme."""
        self._text_filter.setStyleSheet(
            "background-color:" + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";text-align:right;border:1px solid " + Colors.STR_COLOR_LIGHT1GRAY + ";")
        if self.chart_widget is not None:
            self.chart_widget.dark_()


class _Example(QMainWindow):

    def __init__(self):

        super().__init__()

        # "[LHC-BSRA]&d=BISwRefSB&p=Acquisition&f=aGMuIntensityRing,aGTotalIntensity,energy,gain&v=split",
        # devices_info = ccda.query_devices(device_query=["MKD.UA63.IPOC.CTC*"])
        # devices = []
        # for di in devices_info:
        #     devices.append(di.name)
        # print(devices)

        devices_info = ccda.query_devices(device_query=["Y*.BSC*"])
        devices = []
        for di in devices_info:
            if len(devices) < 20:
                devices.append(di.name)
        print(devices)

        # "[MDI]&c=MkIpocChannel&d=MKD.UA63.IPOC*&p=Waveform&f=MkIpocChannel&v=flat",
        self.saw = PropertySubscriptionArrayWidget(self,
                                                   # fields=["waveformData"],
                                                   device_names=devices,
                                                   # property_name="Waveform")#,
                                                   # device_names=["YHRS.BSC4820","YGPS.BSC4830"],
                                                   property_name="Acquisition")
                                                   # cycle_name="CPS.USER.ALL")

        self.saw.subscribe()

        self.setCentralWidget(self.saw)

        self.resize(1600, 600)
        self.move(100, 300)

    def light_(self):
        """Set light theme."""
        pass

    def dark_(self):
        """Set dark theme."""
        pass


if __name__ == '__main__':

    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    Colors.COLOR_LIGHT = False

    example = _Example()
    example.dark_()
    example.show()

    sys.exit(app.exec_())
