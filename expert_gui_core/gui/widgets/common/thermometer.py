import sys
from enum import Enum
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtGui import QPalette, QFont

from PyQt5.QtWidgets import QApplication,QWidget, QGridLayout, QMainWindow, QLabel

from expert_gui_core.gui.common.colors import Colors


class ThermoUpdateState(Enum):
    """
    Update state.
    """
    VALID = 0
    INVALID = 1
    OUT_OF_RANGE = 2
    BELOW_MIN = 3
    ABOVE_MAX = 4


class ThermometerWidget(QLabel):
    """
    Thermometer.

    :param parent: Parent object.
    :type parent: object
    :param font_size: Font size (default=9)
    :type font_size: int, optional
    """

    class _Level(Enum):
        """
        Levels.
        """
        Empty = fa.icons["thermometer-empty"]
        Half = fa.icons["thermometer-half"]
        Full = fa.icons["thermometer-full"]

    def __init__(self, parent, font_size=9):
        """
        Initialize class.
        """
        super().__init__(parent)
        qta.icon("fa5.clipboard")
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        self.font_fa.setPointSize(font_size + 2)
        self.setFont(self.font_fa)
        self.font().setPointSize(font_size + 2)
        self.setText(self._Level.Empty.value[0])
        self.setContentsMargins(0, 0, 0, 0)

        self._base_styles = f'ThermometerWidget {{ color: {Colors.STR_COLOR_LIGHT4GRAY}; padding-bottom: 5px; }};'

        self.setStyleSheet(self._base_styles)

    def update(self, result: ThermoUpdateState = None):
        """
        Update the widget.

        :param result: Valid or not
        :type result: UpdateState
        """
        if not result:
            return

        if result == result.VALID:
            self.setText(self._Level.Half.value[0])
            self.setStyleSheet("color:rgb(0,210,0);")
        else:
            self.setText(self._Level.Full.value[0])
            self.setStyleSheet("color:rgb(255,60,60);")


class _Example(QMainWindow):
    """
    Example class to test    
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """
        Init user interface.
        """
        w = QWidget()

        self._thermo = ThermometerWidget(self, font_size=30)
        self._thermo2 = ThermometerWidget(self, font_size=30)

        self._thermo.update(ThermoUpdateState.VALID)
        self._thermo2.update(ThermoUpdateState.OUT_OF_RANGE)

        mainLayout = QGridLayout()

        mainLayout.addWidget(self._thermo, 0, 0)
        mainLayout.addWidget(self._thermo2, 0, 1)

        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(100, 100)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
