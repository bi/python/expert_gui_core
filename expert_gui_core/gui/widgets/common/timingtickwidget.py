import pyqtgraph as pg

from PyQt5.QtCore import pyqtSignal

from PyQt5.QtGui import QPainter

from PyQt5.QtWidgets import QWidget

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm.data import DataObject


class TimingTickWidget(QWidget):
    """
    Header timing bar.
    
    :param size: not used.
    :type size: int, optional
    :param width_ms: total duration in ms (default=1000).
    :type width_ms: int, optional
    :param palette: Color palette (rainbow ocean summer autumn spring winter bbq winter hawai purple stroke, default=stroke).
    :type palette: str, optional
    :param auto: Automatic remapping and duration (default=True)
    :type auto: bool, optional
    """

    update_led_signal = pyqtSignal()

    def __init__(self, size=20, width_ms=1000, palette="stroke", auto=True):
        """
        Initialize class.
        """
        QWidget.__init__(self)
        self._data = DataObject(max=size, history=True)
        self._pen = {}
        self._size = size
        self._palette = palette
        self._time_recv = 0
        self._width_ms = width_ms
        self._auto = auto

    def recv(self, data, name="default"):
        """
        Data notification.

        :param data: Data received
        :type data: dict
        :param name: Name to identify data
        :type name: str,optional
        """
        if name not in self._pen:
            if Colors.COLOR_LIGHT:
                self._pen[name] = pg.mkPen(color=Colors.COLOR_LIGHT3GRAY, width=2)
            else:
                self._pen[name] = pg.mkPen(color=Colors.COLOR_LIGHT3GRAY, width=2)
        self._data.add(data, name=name)
        self._time_recv = data
        self.update()

    def light_(self):
        """
        Set light theme.
        """
        for name in self._pen:
            self._pen[name] = pg.mkPen(color=Colors.COLOR_LIGHT3GRAY, width=2)
        self.update()

    def dark_(self):
        """
        Set dark theme.
        """
        for name in self._pen:
            self._pen[name] = pg.mkPen(color=Colors.COLOR_LIGHT3GRAY, width=2)
        self.update()

    def paintEvent(self, e):
        """
        Paint event handling.
        """
        painter = QPainter()
        try:
            painter.begin(self)
        except:
            painter.end()
            painter.begin(self)
        w_rect = self.frameGeometry().width()
        h_rect = self.frameGeometry().height()
        w_draw = w_rect - 10
        h_draw = h_rect - 4
        wfirst = 1000000
        if self._time_recv != 0:
            index_name = 0
            ntick_shown = 0
            max_len = 0
            for name in self._data.keys():
                if index_name < 5:
                    painter.setPen(self._pen[name])
                    data = self._data.get(name)
                    for i in range(len(data)):
                        dt = self._time_recv - data[i]
                        if dt >= 0 and dt <= self._width_ms:
                            w = int(w_draw * dt / self._width_ms)
                            if i == 0:
                                wfirst = w
                            painter.drawLine(w_rect - w - 5, 2, w_rect - w - 5, h_rect - 2)
                            ntick_shown = ntick_shown + 1
                    if max_len < len(data):
                        max_len = len(data)
                    index_name = index_name + 1
                else:
                    break
            if self._auto and max_len > 5 and ntick_shown > 0:
                if ntick_shown <= 5:
                    self._width_ms = self._time_recv - data[0]
                    if self._width_ms == 0:
                        self._width_ms = 1000
                elif self._auto and wfirst < (self._width_ms / 10) and wfirst != 0:
                    self._width_ms = 10 * wfirst
            elif self._auto and wfirst < (self._width_ms / 10) and wfirst != 0:
                self._width_ms = 10 * wfirst
        painter.end()
