"""
This module provides a widget that subscribes to a FESA Device property
and dynamically display its data fields using customizable Field Widget instances.

The PropertySubscriptionWidget manages the overall layout and subscription process, while SubscriptionFieldView
is responsible for just updating the visual content of each field.

Usage:
    1. Subclass SubscriptionFieldWidget to create custom field widgets if needed.

    2. Instantiate PropertySubscriptionWidget with the necessary parameters, such as device name, property name,
       and fields to display.

    3. Call the `subscribe` method to start receiving updates from the FESA system.
    The widget will then start updating values.
"""
from __future__ import annotations

import logging
from abc import abstractmethod
from copy import deepcopy
from re import sub
from typing import List, Dict, NamedTuple, Any, Type, Tuple

import numpy as np
from PyQt5.QtCore import QMetaObject, Q_ARG, pyqtSignal, Qt, pyqtSlot
from PyQt5.QtWidgets import QWidget, QGridLayout, QHBoxLayout, QVBoxLayout, QLabel, QGroupBox, QScrollArea

from expert_gui_core.comm import fesacomm
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.tools import formatting


class FieldStyle(NamedTuple):
    DEFAULT_LABEL_STYLE = "font-weight: bold;"
    DEFAULT_VALUE_STYLE_LIGHT = f"color: {Colors.STR_COLOR_LIGHT2GRAY};"
    DEFAULT_VALUE_STYLE_DARK = f"color: {Colors.STR_COLOR_LIGHT3GRAY};"

    label_style: str = DEFAULT_LABEL_STYLE
    value_style: str = DEFAULT_VALUE_STYLE_LIGHT if Colors.COLOR_LIGHT else DEFAULT_VALUE_STYLE_DARK


class SubscriptionFieldView(QWidget):
    """Represents a widget that holds a label and the corresponding data field.

    This widget displays a specific field's value, dynamically updated by a parent
    `PropertySubscriptionWidget` via a PyQtSlot. The display format and styles can be customized.

    Arguments:
        field_name (str): The name of the field to be displayed.
        alias (str, optional): Alias to be used for the field, defaults to field_name.
        threshold (tuple[float, float], optional): Tuple defining minimum and maximum threshold values for the field.
        style (FieldStyle, optional): Style to be applied to the field widget.
        parent (QWidget, optional): The parent widget, defaults to None.

    Attributes:
        reset_value_label_signal (pyqtSignal): Signal that triggers resetting StyleSheet.
        update_tooltip_signal (pyqtSignal(str)): Signal that triggers updating widget's tooltip.
        label (QLabel): Label that holds alias or field name.
        value_label (QLabel): Label that holds the data in str format.

    """
    reset_value_label_signal = pyqtSignal()
    update_tooltip_signal = pyqtSignal(str)

    def __init__(self, field_name: str, alias: str = None, threshold: Tuple[float, float] = None,
                 style: FieldStyle = None, parent=None):
        super().__init__(parent)
        self.field_name = field_name
        self.alias = alias or field_name
        self.threshold = threshold
        self.parent = parent

        self.setMinimumHeight(22)

        self.label = QLabel(f"{self.alias}", self)
        self.value_label = QLabel("N/A", self)

        self._style = style
        self.apply_style(self._style or FieldStyle())

        layout = QHBoxLayout()
        layout.setContentsMargins(2, 2, 2, 2)
        layout.setSpacing(5)
        layout.addWidget(self.label)
        layout.addWidget(self.value_label)
        self.setLayout(layout)
        self._connect_signals()

    def _connect_signals(self):
        self.reset_value_label_signal.connect(self.reset_value_label)
        self.reset_value_label_signal.connect(self.reset_style)
        self.update_tooltip_signal.connect(self.update_tooltip)

    def apply_style(self, style: FieldStyle):
        """Updates the field's Stylesheet

        Arguments:
            style (FieldStyle): New label and value styling.
        """
        if style.label_style:
            self.label.setStyleSheet(style.label_style)
        if style.value_style:
            self.value_label.setStyleSheet(style.value_style)

    def update_value_label(self, new_value: Any, new_value_unit: str = None, new_value_format: Tuple = None):
        """Updates the value label with the new value, applying styles based on thresholds or status.

        The methods called within this function can be overwritten to create custom status-checking,
        threshold-checking or formatting functionalities

        Arguments:
            new_value (Any): The new value to be displayed.
            new_value_unit (str, optional): Unit of the new value, displayed next to it, defaults to None.
            new_value_format(Tuple, optional): Format for displaying the new value, defaults to None.
        """
        status = self.determine_status(new_value, new_value_format)
        self.check_threshold(new_value)
        new_value = self.format_new_value(new_value, new_value_format)

        if status is not None:
            new_value = str(status)
            self._apply_status_style(status)

        self._set_value_text(new_value, new_value_unit)

    @staticmethod
    def determine_status(new_value: int, new_value_format: Tuple) -> bool | None:
        """Determines boolean status for fields marked as 'status' upon PropertySubscriptionWidget construction.

        Arguments:
            new_value (int): Status coming from FESA Server. 0 or 1.
            new_value_format (Tuple): E.g., "intEnable": ("status", None).

        Returns:
            status (bool | None): If the value is from a status field, we get a boolean, otherwise a NoneType.
        """
        if new_value_format and new_value_format[0] == "status":
            return bool(new_value)
        return None

    def format_new_value(self, new_value: Any, new_value_format: Tuple) -> str:
        """Handles the formatting cases other than 'status'.

        Arguments:
            new_value (Any): Field values coming from FESA Server.
            new_value_format (Tuple): E.g., "intEnable": ("status", None).

        Returns:
            new_value (str): Formatted string.
        """
        if new_value_format:
            if new_value_format[0] == "date":
                return formatting.format_value_to_string(new_value, type_format=new_value_format[0],
                                                         type_factor=new_value_format[1])
            else:
                return formatting.format_value_to_string(new_value, type_format=new_value_format[0],
                                                         format_sci=new_value_format[1])
        elif self.field_name == "_time":
            return formatting.format_value_to_string(str(new_value / 1000.), "date")
        return new_value

    def check_threshold(self, new_value: Any):
        """Tests new_value against threshold set upon PropertySubscriptionWidget construction.

        If new_value is within the threshold boundaries the field widget is set to green, otherwise red.

        Arguments:
            new_value (Any): Incoming numeric value.
        """
        if self.threshold and isinstance(self.threshold, tuple) and len(self.threshold) == 2:
            min_threshold, max_threshold = self.threshold
            current_style = self.value_label.styleSheet()

            if not min_threshold <= new_value <= max_threshold:
                new_style = self.update_color_in_stylesheet(current_style, Colors.STR_COLOR_RED)
            else:
                new_style = self.update_color_in_stylesheet(current_style, Colors.STR_COLOR_GREEN)

            self.value_label.setStyleSheet(new_style)
        else:
            if self.threshold is not None:
                logging.warning(f"Invalid threshold param. Got {self.threshold}")
            self.reset_style()

    @staticmethod
    def update_color_in_stylesheet(current_style: str, new_color: str) -> str:
        """Updates the color in the current stylesheet without overriding other style attributes.

        Arguments:
            current_style (str): The current style string of the QLabel.
            new_color (str): The new color to be applied to the style.

        Returns:
            new_style (str): The updated style string with the new color.

        """
        if "color:" in current_style:
            new_style = sub(r"color:\s*[^;]+;", f"color: {new_color};", current_style)
        else:
            new_style = current_style + f" color: {new_color};"

        return new_style

    def _apply_status_style(self, status: bool):
        """Applies status-based color changes without overwriting the full stylesheet."""
        current_style = self.value_label.styleSheet()
        new_color = Colors.STR_COLOR_GREEN if status else Colors.STR_COLOR_RED
        new_style = self.update_color_in_stylesheet(current_style, new_color)
        self.value_label.setStyleSheet(new_style)

    def _set_value_text(self, new_value: Any, new_value_unit: str):
        """Updates the QLabel text, after adding units and trimming the content to one line."""
        new_value = str(new_value).replace('\n', ', ')
        if len(new_value) > 50:
            new_value = new_value[:47] + '...'
        new_value = f"{new_value} {new_value_unit}" if new_value_unit else new_value
        self.value_label.setText(new_value)

    @pyqtSlot()
    def reset_value_label(self):
        """Sets the QLabel text to 'N/A' and resets the ToolTip text."""
        self.value_label.setText("N/A")
        self.update_tooltip('')

    @pyqtSlot(str)
    def update_tooltip(self, new_cycle: str):
        """

        Arguments:
            new_cycle (str): New cycle name.
        """
        if not new_cycle:
            new_cycle = self.parent.cycle if self.parent.cycle else 'Null'
        self.setToolTip(
            f"{self.parent.device}/{self.parent.prop}#{self.field_name}@{new_cycle}"
        )

    @pyqtSlot()
    def reset_style(self):
        """Removes all status and threshold related colors. Sets StyleSheet back to the style attribute."""
        if self._style is None:
            l_style = "font-weight: bold;"
            v_style = f"color: {Colors.STR_COLOR_LIGHT2GRAY if Colors.COLOR_LIGHT else Colors.STR_COLOR_LIGHT3GRAY};"
            self.apply_style(FieldStyle(label_style=l_style, value_style=v_style))
        else:
            self.apply_style(self._style)

    def set_theme(self, light_mode: bool):
        """Sets the theme of the widget to either light or dark mode.

        Arguments:
            light_mode (bool): True for light mode, False for dark mode.
        """
        label_style = "font-weight: bold;"
        value_style = f"color: {Colors.STR_COLOR_LIGHT2GRAY if light_mode else Colors.STR_COLOR_LIGHT3GRAY};"
        self.apply_style(FieldStyle(label_style=label_style, value_style=value_style))

    def light_(self):
        pass
        # self.set_theme(light_mode=True)

    def dark_(self):
        self.set_theme(light_mode=False)


class PropertySubscriptionWidget(fesacomm.FesaCommListener, QWidget):
    """A widget that subscribes to a FESA properties and displays fields in real-time.

    Each PropertySubscriptionWidget instance bound to a FesaComm communication object and each field
    is displayed using a `SubscriptionFieldView` instance. If no fields are given to the constructor, this class will
    display all fields belonging to that property.

    Note:
        This widget is meant to help building quick and simple property visualization dashboards. It displays field data
        in a single line and, thus, it will not be appropriate for keeping track of longer structures like lists,
        dictionaries or matrices. If you wish to subscribe and inspect this type of data please refer to
        expert_gui_core.gui.widget.combiner.propertyfesawidget.

    Args:
        device_name (str): Name of the device to subscribe to.
        property_name (str): Name of the property to subscribe to.
        cycle_name (str, optional): Name of the cycle, defaults to "".
        fields (list[str], optional): List of fields to be displayed, defaults to None.
        aliases (dict[str, str], optional): Dictionary of field aliases, defaults to None.
        styles (dict[str, FieldStyle], optional): Dictionary of field styles, defaults to None.
        formats (dict[str, tuple], optional): Dictionary of field formats, defaults to None.
        units (dict[str, str], optional): Dictionary of field units, defaults to None.
        scales (dict[str, float], optional): Dictionary of field scales, defaults to None.
        title (str, optional): Title of the widget, defaults to None.
        main_layout_style (str, optional): CSS style for the main layout, defaults to None.
        thresholds (dict[str, Any], optional): Dictionary of field thresholds, defaults to None.
        subscription_field_widget (type, optional): Class of the widget used for each field,
                                                    defaults to `SubscriptionFieldView`.
        stick_to_top (bool, optional): Whether to align content to the top, defaults to True.
        show_last_update (bool, optional): Whether to show a "Last update" label, defaults to True.
        parent (QWidget, optional): Parent widget, defaults to None.

    Attributes:
        data_changed (pyqtSignal(dict)): Signal emitted when new data arrives.
        fesa_comm (FesaComm): Object that enables communication with FESA Server.
        field_widgets (Dict[str, SubscriptionFieldView]): Mapping of field name and widget for data visualization.

    Example:
        To create a simple `PropertySubscriptionWidget` and subscribe to a device in a Python shell:

        >>> from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget as PsW
        ... from PyQt5.QtWidgets import QApplication
        ... application = QApplication([])
        ... widget = PsW(device_name='PR.BPM', property_name='ExpertSetting', cycle_name='CPS.USER.TOF')
        ... widget.show()  # Show the widget
        ... widget.subscribe()  # Subscribe to the FESA system
        ... application.exec_()
    """

    data_changed = pyqtSignal(dict)

    def __init__(self, device_name: str, property_name: str, cycle_name: str = "",
                 fields: List[str] = None, aliases: Dict[str, str] = None,
                 styles: Dict[str, FieldStyle | dict | None] = None, formats: Dict[str, Tuple] = None,
                 units: Dict[str, str] = None,
                 scales: Dict[str, float] = None, title: str = None,
                 main_layout_style: str = None, thresholds: Dict[str, Any] = None,
                 subscription_field_widget: Type[SubscriptionFieldView] = SubscriptionFieldView,
                 stick_to_top: bool = True,
                 show_last_update: bool = True,
                 parent=None):
        super().__init__(parent)
        self.fesa_comm = None
        self._device = device_name
        self._property = property_name
        self._cycle = cycle_name
        self._fields = fields or []
        self._aliases = aliases or {}
        self._field_styles = styles or {}
        self._formats = formats or {}
        self._units = units or {}
        self._scales = scales or {}
        self.field_widgets = {}
        self._title_text = title
        self._content_widget_style = main_layout_style
        self._thresholds = thresholds or {}
        self._subscription_field_widget = subscription_field_widget
        self._stick_to_top = stick_to_top
        self._show_last_update = show_last_update

        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()

        self._validate_input()
        self._generate_default_styles()
        self._init_ui()

        self.data_changed.connect(self.update_value_labels)

    @property
    def device(self):
        return self._device

    @property
    def prop(self):
        return self._property

    @property
    def cycle(self):
        return self._cycle

    @cycle.setter
    def cycle(self, value):
        self._cycle = value
        self.reset_labels()

    def _validate_input(self):
        if not self._fields and self._aliases:
            raise ValueError("Cannot pass aliases without specifying fields.")
        if any(field not in self._fields for field in self._aliases.keys()):
            raise ValueError("Field names must have corresponding aliases.")
        if any(field not in self._fields for field in self._field_styles.keys()):
            raise ValueError("Field names must have corresponding styles.")

    def _generate_default_styles(self):
        """Generates default styles for fields if not provided."""
        for field in self._fields:
            if field not in self._field_styles and field is not None:
                self._field_styles[field] = None

    def _init_ui(self):
        """Initializes the user interface components."""
        self._setup_main_layout()
        self._setup_content_area()

    def _setup_main_layout(self):
        """Sets up the main layout including the title and scroll area."""

        self.main_layout = QVBoxLayout(self)

        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setSpacing(0)

        if self._title_text:
            title = QLabel(self._title_text)
            title.setStyleSheet(f"color: {Colors.STR_COLOR_BLUE};font-weight:bold;")
            title.setMaximumHeight(50)
            title.setAlignment(Qt.AlignCenter)
            self.main_layout.addWidget(title)

    def _setup_content_area(self):
        """Sets up the content area."""
        # Main widget for the scroll area
        self._content_widget = QWidget()
        if self._content_widget_style:
            self._content_widget.setStyleSheet(self._content_widget_style)
        self._content_layout = QGridLayout(self._content_widget)
        self._content_layout.setContentsMargins(20, 20, 20, 20)
        self._content_layout.setSpacing(0)

        self.groupBox = QGroupBox()
        self.groupBox.setLayout(self._content_layout)
        self.groupBox.setStyleSheet("border:none;background-color:" + Colors.STR_COLOR_TRANSPARENT + ";")
        self.scroll = QScrollArea()
        self.scroll.setWidget(self.groupBox)
        self.scroll.setWidgetResizable(True)

        # Align widgets to the top if stick_to_top is True
        if self._stick_to_top:
            self._content_layout.setAlignment(Qt.AlignTop)

        # Add fields or a label if no fields are present
        if not self._fields:
            self._no_fields_label = QLabel("After login, this widget will display all fields from the response.", self)
            self._no_fields_label.setAlignment(Qt.AlignCenter)
            self._content_layout.addWidget(self._no_fields_label, 0, 0)
        else:
            self._create_field_widgets(self._fields)

        self.main_layout.addWidget(self.scroll)

    def _create_field_widgets(self, fields: List[str]):
        """Creates SubscriptionFieldWidget instances for the given fields, aliases, and styles."""
        if hasattr(self, '_no_fields_label') and self._no_fields_label:
            self._no_fields_label.deleteLater()
            self._no_fields_label = None

        ind = 0
        for field in fields:
            alias = self._aliases.get(field, field)
            style = self._field_styles.get(field)
            if isinstance(style, dict):
                style = FieldStyle(**style)
            threshold = self._thresholds.get(field, None)
            if field not in self.field_widgets:
                field_widget = self._subscription_field_widget(field_name=field, alias=alias,
                                                               threshold=threshold, style=style, parent=self)
                self.field_widgets[field] = field_widget
                self._content_layout.addWidget(field_widget, ind, 0)
                ind = ind + 1

        if self._show_last_update:
            self._add_last_updated_label(ind)

    def _add_last_updated_label(self, row=0):
        """Adds a label in italic to display when the subscription last updated the other labels."""
        self._last_updated_label = QLabel("", self)
        self._last_updated_label.setStyleSheet(
            f"font-size:10px; font-style: italic; color: {Colors.STR_COLOR_BLUE};")
        self._content_layout.addWidget(self._last_updated_label, row, 0)

    @pyqtSlot(dict)
    def update_value_labels(self, response: Dict):
        """Updates the field widgets with the new values from the response.

        Arguments:
            response (Dict): Dictionary containing field names and their updated values.
        """
        for field, value in response.items():
            if field in self.field_widgets:
                value_unit = self._units[field] if field in self._units else None
                value_format = self._formats[field] if field in self._formats else None

                self.field_widgets[field].update_value_label(value, value_unit, value_format)
                self.field_widgets[field].update_tooltip_signal.emit(response['_cycle'])

        current_time = formatting.format_value_to_string(str(formatting.current_milli_time() / 1000.), "date")
        if self._show_last_update:
            self._last_updated_label.setText(f"Last update: {current_time}")

    def scale_values(self, response: Dict) -> Dict:
        """
        Scales scalar field values based on the 'scales' dictionary.

        Arguments:
            response (Dict): Dictionary containing field names and their updated values.

        Returns:
            scaled_response (Dict): Dictionary with scaled field values.
        """
        scaled_response = deepcopy(response)
        for field, value in response.items():
            if self._fields and field in self._fields:
                if np.isscalar(value):
                    scaled_response[field] = value * (self._scales[field] if field in self._scales else 1)
                else:
                    scaled_response[field] = value
                    # logging.warning(f"scale_values only supports scalar multiplication. Got {type(value)}")

        return scaled_response

    @abstractmethod
    def add_fields(self, response: Dict):
        """Implement this method to extend the response with new fields.

        Arguments:
            response (Dict): Dictionary containing field names and their updated values.

        Example:
            >>> def add_fields(s, r: Dict):
            ...     # This implementation adds a single calculated field.
            ...     from copy import deepcopy
            ...
            ...     new_response = deepcopy(r)
            ...     new_response['NewField'] = r['fieldA'] - r['fieldB']
            ...     return new_response
        """
        return response

    def process_response(self, response: Dict):
        """If 'add_fields' is implemented, this method adds new fields and scales the values.

        The scaling is determined by the 'scales' arguments passed in the constructor. And the new fields are defined
        by the implementation of the 'add_fields' method.

        Arguments:
            response (Dict): Dictionary containing field names and their updated values.

        Returns:
            new_response (Dict): Transformed response.
        """
        new_response = self.add_fields(response)
        new_response = self.scale_values(new_response)

        return new_response

    @pyqtSlot(dict)
    def _handle_event_main_thread(self, value: Dict):
        """Handles subscription response event in the main thread."""
        if not self._fields:
            self._create_field_widgets(list(value.keys()))
            self._fields = list(value.keys())

        self.update_value_labels(value)

    @pyqtSlot(str, dict)
    def handle_event(self, name: str, value: Dict):
        """This slot is called when an event is received.

        Before sending a data_changed signal, it will process the response, modifying the raw data accordingly
        to the 'fields' and 'scales' arguments passed in the constructor.

        It ensures that GUI updates are performed in the main thread.

        Arguments:
            name (str): Endpoint of the notification event (device/property).
            value (Dict): Dictionary containing field names and their updated values.
        """
        processed_response = self.process_response(value)

        if not self._fields:
            QMetaObject.invokeMethod(
                self, "_handle_event_main_thread",
                Qt.QueuedConnection,
                Q_ARG(dict, processed_response)
            )
        else:
            self.data_changed.emit(processed_response)

    def unsubscribe(self):
        if self.fesa_comm:
            self.fesa_comm.unsubscribe()

    def subscribe(self, _cycle: str = None):
        """Initializes the FesaComm object and starts the subscription.

        If, '_cycle' is provided, it will override the 'cycle' attribute and start a new subscription.
        It is good practice to call self.unsubscribe() before changing the cycle the widget is subscribed to.

        Arguments:
            _cycle (str): Cycle name.
        """
        if _cycle is None:
            _cycle = self._cycle

        if self.fesa_comm is None:
            self.fesa_comm = fesacomm.FesaComm(self._device, self._property, listener=self)

        self.cycle = _cycle
        self.fesa_comm.subscribe(_cycle)

    def reset_labels(self):
        """Recursively resets the labels on all SubscriptionFieldView objects."""
        for field_widget in self.field_widgets.values():
            field_widget.reset_value_label_signal.emit()

    def light_(self):
        """Set light theme."""
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_DWHITE)
        for field in self._fields:
            if field in self.field_widgets and field not in self._thresholds:
                self.field_widgets[field].light_()

    def dark_(self):
        """Set dark theme."""
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLACK)
        for field in self._fields:
            if field in self.field_widgets and field not in self._thresholds:
                self.field_widgets[field].dark_()
