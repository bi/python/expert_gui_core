import sys

from PyQt5.QtCore import Qt, QRect

from PyQt5.QtGui import QPainter, QPalette, QPen, QBrush

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QMainWindow, QPushButton

from expert_gui_core.gui.common.colors import Colors


class ToggleButton(QPushButton):
    """
    Toggle button. (ex:on/off).

    :param parent: Parent object.
    :type parent: object
    :param text: both text (default=["on","off"]).
    :type text: list, optional
    :param colors: 4 colors on/off in light and dark theme.
    :type color: list, optional
    :param colortext: 2 colors text in light and dark theme.
    :type colortext: list, optional
    :param colorsgb: 2 colors bg in light and dark theme.
    :type colorsbg: list, optional
    :param colorsborder: 2 colors on/off border in light and dark theme.
    :type colorsborder: list, optional
    :param width: Width component (default=40).
    :type width: int, optional 
    :param radius: Radius circle component (default=width/1.6).
    :type radius: int, optional
    :param noborderbutton: Show or not border (default=True).
    :type noborderbutton: bool, optional
    """

    def __init__(
            self,
            parent=None,
            text=["on", "off"],
            colors=[[Colors.COLOR_L4BLUE, Colors.COLOR_LIGHT1GRAY], [Colors.COLOR_L4BLUE, Colors.COLOR_LIGHT3GRAY]],
            colortext=[Colors.COLOR_DWHITE, Colors.COLOR_LBLACK],
            colorsbg=[Colors.COLOR_LBLACK, Colors.COLOR_DWHITE],
            colorsborder=[Colors.COLOR_LIGHT2GRAY, Colors.COLOR_LIGHT4GRAY],
            width=40,
            radius=None,
            noborderbutton=True):
        """
        Initialize class.
        """
        super().__init__(parent)

        self._text = text

        self._colors = colors
        self._colorstext = colortext
        self._colorbg = colorsbg
        self._colorsborder = colorsborder

        self._colortextused = colortext[0]
        self._colorused = colors[0]
        self._colorbgused = colorsbg[0]
        self._colorborderused = colorsborder[0]

        self._width = width
        if radius is None:
            self._radius = int(width / 1.6)
        else:
            self._radius = radius

        self._noborderbutton = noborderbutton

        self.setCheckable(True)
        self.setMinimumWidth(width)
        self.setMinimumHeight(int(2.3 * self._radius))

        self.setChecked(False)

    def set_colors(self, colors=[[Colors.COLOR_L4BLUE, Colors.COLOR_LIGHT1GRAY],
                                 [Colors.COLOR_L4BLUE, Colors.COLOR_LIGHT3GRAY]]):
        """
        Set colors.

        :param colors: 4 colors on/off in light and dark theme.
        :type color: list, optional
        """
        self._colors = colors
        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._colortextused = self._colorstext[1]
        self._colorused = self._colors[1]
        self._colorbgused = self._colorbg[1]
        self._colorborderused = self._colorsborder[1]
        self.update()

    def dark_(self):
        """
        Set dark theme.
        """
        self._colortextused = self._colorstext[0]
        self._colorused = self._colors[0]
        self._colorbgused = self._colorbg[0]
        self._colorborderused = self._colorsborder[0]
        self.update()

    def paintEvent(self, event):
        """
        Paint event.
        """

        # init param

        label = self._text[0] if self.isChecked() else self._text[1]
        bg_color = self._colorused[0] if self.isChecked() else self._colorused[1]
        radius = int(self._radius)
        width = self._width
        center = self.rect().center()

        # painter

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(self._colorbgused)

        # pen

        pen = QPen(self._colorborderused)
        pen.setWidth(2)
        painter.setPen(pen)

        # frame button

        painter.drawRoundedRect(QRect(-width, -radius, int(2 * width), int(2 * radius)), radius, radius)

        # button inside

        if self._noborderbutton:
            pen = QPen(bg_color)
            pen.setWidth(2)
            painter.setPen(pen)
        painter.setBrush(QBrush(bg_color))
        sw_rect = QRect(-int(0.5 * radius), -int(1. * radius), width + int(0.5 * radius), 2 * radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, int(1. * radius), int(1. * radius))

        # text

        pen.setColor(self._colortextused)
        painter.setPen(pen)
        painter.drawText(sw_rect, Qt.AlignCenter, label)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    MainWindow = QMainWindow()
    central_widget = QWidget()
    layout = QGridLayout(central_widget)

    sw1 = ToggleButton(central_widget,
                       text=["on", "off"],
                       width=25)

    sw2 = ToggleButton(
        central_widget,
        text=["", ""],
        width=18
    )

    sw3 = ToggleButton(
        central_widget,
        text=["", ""],
        width=25,
        noborderbutton=False
    )

    sw1.setChecked(False)
    sw1.set_colors(colors=[[Colors.COLOR_LIGHTORANGE, Colors.COLOR_LIGHT1GRAY],
                           [Colors.COLOR_LIGHTORANGE, Colors.COLOR_LIGHT3GRAY]])
    sw1.dark_()

    sw2.setChecked(True)
    sw2.dark_()

    sw3.setChecked(False)
    sw3.set_colors(
        colors=[[Colors.COLOR_LIGHTRED, Colors.COLOR_LIGHT1GRAY], [Colors.COLOR_LIGHTRED, Colors.COLOR_LIGHT3GRAY]])
    sw3.dark_()

    layout.addWidget(sw1, 0, 0)
    layout.addWidget(sw2, 1, 0)
    layout.addWidget(sw3, 2, 0)
    MainWindow.setCentralWidget(central_widget)
    MainWindow.resize(100, 100)
    MainWindow.show()

    app.exec()
