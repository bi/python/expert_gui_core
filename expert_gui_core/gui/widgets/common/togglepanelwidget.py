import sys
import fontawesome as fa
import qtawesome as qta

from PyQt5.QtCore import pyqtSignal, Qt

from PyQt5.QtGui import  QPalette, QFont

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QHBoxLayout, QMainWindow, QLabel, QPushButton

from expert_gui_core.gui.common.colors import Colors


class TogglePanelWidget(QWidget):
    """
    Toggle Panel.
    Panel with hide/show button.

    :param panel: Panel to toggle.
    :type panel: QWidget
    :param iconarrow: Specific icon name for arrow (default="caret-right").
    :type iconarrow: str, optional
    :param iconhide: Specific icon name for button hide (default=iconarrow).
    :type iconhide: str, optional
    :param iconshow: Specific icon name for button show (default="caret-left").
    :type iconshow: str, optional
    :param align: Component layout position (default="topleft").
    :type align: str, optional
    :param color: Color button (default=L4BLUE).
    :type color: str, optional
    """

    update_signal_show = pyqtSignal()
    update_signal_hide = pyqtSignal()

    def __init__(self, panel, iconhide=None, iconshow=None, align="topleft", iconarrow=None,
                 color=Colors.STR_COLOR_L4BLUE):
        QWidget.__init__(self)

        qta.icon("fa5.clipboard")

        self._panel = panel
        self._color = color

        self.iconarrow = iconarrow

        if align == "topleft":
            alignment = Qt.AlignLeft | Qt.AlignTop
        elif align == "topright":
            alignment = Qt.AlignRight | Qt.AlignTop
        elif align == "bottomright":
            alignment = Qt.AlignRight | Qt.AlignBottom
        elif align == "bottomleft":
            alignment = Qt.AlignLeft | Qt.AlignBottom

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        if iconshow is None:
            if self.iconarrow is None:
                if align == "topleft" or align == "bottomleft":
                    self._show_button = QPushButton(fa.icons["caret-left"])
                else:
                    self._show_button = QPushButton(fa.icons["caret-right"])
            else:
                self._show_button = QPushButton(fa.icons[self.iconarrow])
        else:
            self._show_button = QPushButton(fa.icons[iconshow])

        self._show_button.setFont(self.font_fa)
        min_size = 20
        self._show_button.setMaximumWidth(min_size)
        self._show_button.setMinimumWidth(min_size)
        self._show_button.setMinimumHeight(min_size)

        if iconhide is None:
            if self.iconarrow is None:
                if align == "topleft" or align == "bottomleft":
                    self._hide_button = QPushButton(fa.icons["caret-right"])
                else:
                    self._hide_button = QPushButton(fa.icons["caret-left"])
            else:
                self._hide_button = QPushButton(fa.icons[self.iconarrow])
        else:
            self._hide_button = QPushButton(fa.icons[iconhide])

        self._hide_button.setFont(self.font_fa)
        self._hide_button.setMaximumWidth(min_size)
        self._hide_button.setMinimumWidth(min_size)
        self._hide_button.setMinimumHeight(min_size)

        self._show_button.hide()

        label = QLabel("")
        self._hide_button.mousePressEvent = self.hide_
        self._show_button.mousePressEvent = self.show_

        vbox = QHBoxLayout(self)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.setSpacing(0)

        w = QWidget()
        self.layouttop = QGridLayout(w)
        self.layouttop.setContentsMargins(0, 0, 0, 0)
        self.layouttop.setSpacing(0)
        self.layouttop.addWidget(self._show_button, 0, 0, alignment=alignment)
        self.layouttop.addWidget(self._hide_button, 0, 1, alignment=alignment)
        self.layouttop.addWidget(label, 0, 2, alignment=alignment)

        vbox.addWidget(w)
        vbox.addWidget(panel)
        vbox.setStretchFactor(panel, 1)

        self.setStyleSheet("background-color:transparent;")

    def get_signal_hide(self):
        """
        Return signal hide.
        """
        print("HIDE")
        return self.update_signal_hide

    def get_signal_show(self):
        """
        Return signal show.
        """
        print("SHOW")
        return self.update_signal_show

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        try:
            self.font_fa.setPointSize(size)
            self._show_button.setFont(self.font_fa)
            self._hide_button.setFont(self.font_fa)
        except:
            pass

    def dark_(self):
        """
        Set dark theme.
        """
        try:
            self._hide_button.setStyleSheet(
                "color:" + self._color + ";" + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
            self._show_button.setStyleSheet(
                "color:" + self._color + ";" + ";background-color:" + Colors.STR_COLOR_L2BLACK + ";")
        except:
            pass

    def light_(self):
        """
        Set light theme.
        """
        try:
            self._hide_button.setStyleSheet(
                "color:" + self._color + ";" + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
            self._show_button.setStyleSheet(
                "color:" + self._color + ";" + ";background-color:" + Colors.STR_COLOR_DWHITE + ";")
        except:
            pass

    def show_(self, e):
        """
        Show panel.
        """
        self._panel.show()
        color = self._panel.palette().color(self._panel.backgroundRole())
        self._show_button.hide()
        self._hide_button.show()
        self.update_signal_show.emit()

    def hide_(self, e):
        """
        Hide panel.
        """
        self._panel.hide()
        self._show_button.show()
        self._hide_button.hide()
        self.update_signal_hide.emit()


class _Example(QMainWindow):
    """
    Example class to test    
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """
        Init user interface.
        """
        w = QWidget()

        mainLayout = QGridLayout()

        wtb = QWidget()
        tblayout = QGridLayout(wtb)
        tblayout.setContentsMargins(20, 20, 20, 20)
        l = QLabel("This is a test")
        l.setStyleSheet("background-color:red;")
        tblayout.addWidget(l, 0, 0)

        toggle_panel = TogglePanelWidget(wtb, iconshow="sliders-h", align="topright")

        mainLayout.addWidget(toggle_panel, 0, 0)

        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(300, 150)
        toggle_panel.dark_()
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(15)
    app.setFont(font_text_user)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
