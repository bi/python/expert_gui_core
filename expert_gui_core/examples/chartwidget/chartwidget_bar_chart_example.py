"""
Chart Widget Example with Bar Chart

This script demonstrates how to create a bar chart using the `ChartWidget` class.
It generates random data from a normal distribution and displays it in a bar chart.

Features:
- Generates random normal distribution data.
- Displays data in a bar chart.

Notes:
- Requires PyQt5 and expert_gui_core library for the ChartWidget.
"""
import sys
import numpy as np
import pyqtgraph as pg
from PyQt5 import QtWidgets

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    central_widget = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout(central_widget)

    chart_widget = ChartWidget(parent=central_widget, name=["BarChartExample"])
    layout.addWidget(chart_widget)

    chart_widget.add_plot(title="BarPlot", name_parent="BarChartExample")
    data = np.random.normal(0, 1, 1000)
    chart_widget.set_data(data=data,
                          name_parent="BarChartExample",
                          type_plotitem='b',
                          pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 2}))

    central_widget.setLayout(layout)

    main_window = QtWidgets.QMainWindow()
    main_window.setCentralWidget(central_widget)

    main_window.show()
    sys.exit(app.exec())