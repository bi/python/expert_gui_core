"""
Chart Widget Example with Line Plot and Statistics option enabled

This script demonstrates how to use the ChartWidget to create and update a Line plot
using PyQt5 and pyqtgraph. The plot is dynamically updated with random data while maintaining
a fixed-size FIFO buffer of 2000 points.

Features:
- Displays a line plot using the ChartWidget.
- The user can right-click on the plot and select the 'Statistics' and 'Fitting' options.
- Maintains a rolling window of 2000 points for efficient real-time visualization.
- Generates random data from a normal distribution to simulate dynamic data.
- Updates the plot at regular intervals without changing the total sample size.

Notes:
- Requires `expert_gui_core` for Colors and ChartWidget utilities.
- Uses a QTimer to trigger periodic updates to both the chart and sample size label.
- Implements a FIFO queue to manage data efficiently and maintain a consistent window size.
"""
import sys
import numpy as np
import pyqtgraph as pg
from collections import deque

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget


class ChartUpdater(QtCore.QObject):
    """
    Handles updating the ChartWidget with a fixed-size FIFO buffer.

    This class maintains a rolling window of 2000 points and updates the chart periodically.
    """

    def __init__(self, label, chart):
        super().__init__()
        self.window_size = 2000
        self.label = label
        self.chart_widget = chart
        self.data_queue = deque(maxlen=self.window_size)

    @QtCore.pyqtSlot()
    def update_chart(self):
        """Updates the chart with new random data while maintaining a fixed-size window."""
        new_data = np.random.normal(0, 1, 100)
        self.data_queue.extend(new_data)
        data_array = np.array(self.data_queue)

        self.chart_widget.set_data(data=data_array,
                                   name_parent="LineChartExample",
                                   name_pi="PlotItem",
                                   pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 2}))

        self.label.setText(f"Sample Size: {len(data_array)}")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    central_widget = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout(central_widget)

    chart_widget = ChartWidget(parent=central_widget, name=["LineChartExample"])
    chart_widget.add_plot(title="LineChartExample", name_parent="LineChartExample", name="PlotItem",
                          statistics=True, fitting=True)

    sample_size_label = QtWidgets.QLabel("Sample Size: ")
    layout.addWidget(sample_size_label)

    main_window = QtWidgets.QMainWindow()
    main_window.updater = ChartUpdater(sample_size_label, chart_widget)

    layout.addWidget(chart_widget)
    central_widget.setLayout(layout)
    main_window.setCentralWidget(central_widget)
    main_window.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(main_window.updater.update_chart)
    timer.start(1000)

    sys.exit(app.exec())

