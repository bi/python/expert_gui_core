"""
Chart Widget Example with Histogram

This script demonstrates how to use the ChartWidget to create and update a histogram plot
using PyQt5 and pyqtgraph. The histogram is dynamically updated with random data.

Features:
- Displays a histogram (bar chart) using the ChartWidget.
- Generates random data from a normal distribution to simulate dynamic data.
- Sample size is updated periodically, showing how the chart adapts to different data sizes.

Notes:
- Requires `expert_gui_core` for Colors and ChartWidget utilities.
- Uses a QTimer to trigger periodic updates to both the chart and sample size label.
- The sample size increases and decreases between 1,000 and 10,000, simulating varying data volumes.
"""
import sys
import numpy as np
import pyqtgraph as pg

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot, QObject

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget


class ChartUpdater(QtCore.QObject):
    def __init__(self, label, chart):
        super().__init__()
        self.size = 100
        self.step = 100
        self.label = label
        self.chart_widget = chart

    @QtCore.pyqtSlot()
    def update_chart(self):
        data = np.random.normal(0, 1, self.size)
        bins = max(1, int(np.ceil(np.sqrt(self.size))))
        hist, bins = np.histogram(data, bins)
        self.chart_widget.set_data(data=hist,
                                   datax=bins,
                                   name_parent="BarChartExample",
                                   type_plotitem='h',
                                   name_pi="PlotItem",
                                   pen=pg.mkPen({'color': Colors.strokeColor(1), 'width': 2}))

        self.size = self.size + self.step if self.size < 2000 else self.size - self.step
        if self.size <= 100 or self.size >= 2000:
            self.step = -self.step

        self.label.setText(f"Sample Size: {self.size}")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    central_widget = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout(central_widget)

    chart_widget = ChartWidget(parent=central_widget, name=["BarChartExample"])
    chart_widget.add_plot(title="BarChartExample", name_parent="BarChartExample", name="PlotItem")

    sample_size_label = QtWidgets.QLabel("Sample Size: ")
    layout.addWidget(sample_size_label)

    main_window = QtWidgets.QMainWindow()
    main_window.updater = ChartUpdater(sample_size_label, chart_widget)

    layout.addWidget(chart_widget)
    central_widget.setLayout(layout)
    main_window.setCentralWidget(central_widget)
    main_window.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(main_window.updater.update_chart)
    timer.start(1000)

    sys.exit(app.exec())
