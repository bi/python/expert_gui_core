"""
PropertySubscriptionWidget with Custom Styling

This example demonstrates how to create a PropertySubscriptionWidget, within a BIApplicationFrame
while choosing a subset of fields and giving them custom style.

Notes:
- This example uses a BaseApplicationFrame(BIApplicationFrame) to simplify the SubscriptionApp and
focus on the actual PropertySubscriptionWidget usage.
"""
import sys

from PyQt5.QtWidgets import QApplication
from typing_extensions import override

from expert_gui_core.examples.subscriptionwidget import BaseApplicationFrame
from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget, FieldStyle


class SubscriptionAppWithCustomStyling(BaseApplicationFrame):
    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

        self._init_ui()
        self.subscribe()

    @override
    def add_subscription_widget_example(self) -> PropertySubscriptionWidget:
        field_styles = {
            'frequency': FieldStyle(label_style="color: blue; font-weight: bold; font-size: 24px;",
                                    value_style="color: #00FF00; font-size: 24px;"),
            'pulseStartTS': FieldStyle(label_style="color: orange; font-weight: bold; font-size: 16px;",
                                       value_style="color: #00FF00; font-size: 16px;"),
            'pulseEndTS': FieldStyle(label_style="color:black; font-weight: bold; font-size: 12px;",
                                     value_style="color: #00FF00; font-size: 12px;")
        }

        ps_widget = PropertySubscriptionWidget(
            device_name='LHC40MHzFreqMeas.BST1',
            property_name='Acquisition',
            fields=['frequency', 'pulseStartTS', 'pulseEndTS'],
            aliases={'pulseStartTS': 'Pulse Start', 'pulseEndTS': 'Pulse End'},
            styles=field_styles
        )
        return ps_widget


if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    subscription_app = SubscriptionAppWithCustomStyling(application=app)
    subscription_app.show()

    sys.exit(app.exec_())