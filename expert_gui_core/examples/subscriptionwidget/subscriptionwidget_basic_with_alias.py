"""
PropertySubscriptionWidget with Field Aliases

This example demonstrates how to create a PropertySubscriptionWidget, within a BIApplicationFrame
while choosing a subset of fields and giving them aliases.

Features:
- Tests Layout specifying some fields from LHC40MHzFreqMeas.BST1/Acquisition automatically.
- Sets aliases to the field names.

Notes:
- This example uses a BaseApplicationFrame(BIApplicationFrame) to simplify the SubscriptionApp and
focus on the actual PropertySubscriptionWidget usage.
"""
import sys

from PyQt5.QtWidgets import QApplication
from typing_extensions import override

from expert_gui_core.examples.subscriptionwidget import BaseApplicationFrame
from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget

class SubscriptionAppWithFieldAliases(BaseApplicationFrame):

    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

        self._init_ui()
        self.subscribe()

    @override
    def add_subscription_widget_example(self) -> PropertySubscriptionWidget:
        ps_widget = PropertySubscriptionWidget(
            device_name='LHC40MHzFreqMeas.BST1',
            property_name='Acquisition',
            fields=['frequency', 'pulseStartTs', 'pulseEndTs'],
            aliases={'frequency': 'Frequency', 'pulseStartTs': 'Pulse Start', 'pulseEndTs': 'Pulse End'}
        )
        return ps_widget

if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    subscription_app = SubscriptionAppWithFieldAliases(application=app)
    subscription_app.show()

    sys.exit(app.exec_())