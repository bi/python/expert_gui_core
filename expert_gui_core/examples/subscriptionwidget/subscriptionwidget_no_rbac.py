"""
Basic PropertySubscriptionWidget without RBAC needed

This example demonstrates how to create a PropertySubscriptionWidget, within a BIApplicationFrame
for a Device Property that doesn't require rbac access.

Features:
- This Device Property has somme array fields which are displayed as cropped strings.

Notes:
- This example uses a BaseApplicationFrame(BIApplicationFrame) to simplify the SubscriptionApp and
focus on the actual PropertySubscriptionWidget usage.
"""
import sys

from PyQt5.QtWidgets import QApplication
from typing_extensions import override

from expert_gui_core.examples.subscriptionwidget import BaseApplicationFrame
from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget, FieldStyle


class SubscriptionAppWithoutRbac(BaseApplicationFrame):
    def __init__(self, application=None, example_title=None):
        super().__init__(example_title=example_title, q_app=application)

        self._init_ui()
        self.subscribe()

    @override
    def add_subscription_widget_example(self) -> PropertySubscriptionWidget:
        ps_widget = PropertySubscriptionWidget(device_name='PR.BPM',
                                               property_name='ExpertSetting',
                                               cycle_name="CPS.USER.TOF")
        return ps_widget

if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    subscription_app = SubscriptionAppWithoutRbac(application=app)
    subscription_app.show()

    sys.exit(app.exec_())