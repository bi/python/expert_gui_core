from abc import abstractclassmethod, abstractmethod

from expert_gui_core.gui import biapplicationframe
from expert_gui_core.gui.widgets.common.subscriptionwidget import PropertySubscriptionWidget

class BaseApplicationFrame(biapplicationframe.BIApplicationFrame):
    """
    Base class for demonstrating different configurations of PropertySubscriptionWidget.
    """

    def __init__(self, q_app=None, example_title="Property Subscription Widget Example", use_rbac=True):
        super().__init__(app=q_app, use_log_console=True, use_rbac=use_rbac, use_timing_bar=True)
        self.setWindowTitle(example_title)

        # In case subscription fails because of rbac access requirement, call self.subscribe on loginSucceeded
        if self.rba_widget:
            self.rba_widget.loginSucceeded.connect(self.on_pyrbac_login)
            self.rba_widget.loginSucceeded.connect(self.subscribe)

    def _init_ui(self):
        self.log_console.console.expanded = False
        self.subscription_widget = self.add_subscription_widget_example()
        self._tabs.addTab(self.subscription_widget, 'SubscriptionWidget')
        self._tabs.setTabsClosable(False)
        self.setGeometry(0, 0, 800, 1000)

    @abstractmethod
    def add_subscription_widget_example(self) -> PropertySubscriptionWidget:
        """ Create and add a PropertySubscriptionWidget instance to the layout with the given parameters.

        This should be overridden by subclasses to provide specific configuration.

        Returns:
            psw (PropertySubscriptionWidget): Newly instantiated widget.
        """
        raise NotImplementedError("Subclasses should implement this method.")

    def subscribe(self):
        """Calls the subscribe method from PropertySubscriptionWidget to start listening to the properties
        defined in _add_subscription_widget_example."""
        if self.subscription_widget is not None:
            self.subscription_widget.subscribe()

    def dark_(self):
        """Set dark theme."""
        super().dark_()
        if self.subscription_widget is not None:
            self.subscription_widget.dark_()

    def light_(self):
        """Set light theme."""
        super().light_()
        if self.subscription_widget is not None:
            self.subscription_widget.light_()
