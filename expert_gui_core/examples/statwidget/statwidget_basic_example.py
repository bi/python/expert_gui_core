"""
Statistics Widget Basic Example

This script demonstrates how to use the StatWidget to create and update a histogram plot
using PyQt5 and pyqtgraph. The histogram is dynamically updated with random data.

Features:
- Displays a histogram using the StatWidget.
- Generates random data from a normal distribution to simulate dynamic data.
- Sample size is updated periodically, showing how the chart adapts to different data sizes.

Notes:
- Requires `expert_gui_core` for StatWidget utilities.
- Uses a QTimer to trigger periodic updates to both the chart and sample size label.
- The sample size increases and decreases between 1,000 and 10,000, simulating varying data volumes.
"""
import sys
import numpy as np
import pyqtgraph as pg

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot, QObject

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget
from expert_gui_core.gui.widgets.pyqt.statwidget import StatWidget


class ChartUpdater(QtCore.QObject):
    def __init__(self, label, chart):
        super().__init__()
        self.size = 100
        self.step = 100
        self.label = label
        self.stat_widget = stat_widget

    @QtCore.pyqtSlot()
    def update_chart(self):
        data = np.random.normal(0, 1, self.size)
        self.stat_widget.set_data(data=data, size=self.size)

        self.size = self.size + self.step if self.size < 2000 else self.size - self.step
        if self.size <= 100 or self.size >= 2000:
            self.step = -self.step

        self.label.setText(f"Sample Size: {self.size}")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    central_widget = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout(central_widget)

    stat_widget = StatWidget()

    sample_size_label = QtWidgets.QLabel("Sample Size: ")
    layout.addWidget(sample_size_label)

    main_window = QtWidgets.QMainWindow()
    main_window.updater = ChartUpdater(sample_size_label, stat_widget)

    layout.addWidget(stat_widget)
    central_widget.setLayout(layout)
    main_window.setCentralWidget(central_widget)
    main_window.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(main_window.updater.update_chart)
    timer.start(1000)

    sys.exit(app.exec())
