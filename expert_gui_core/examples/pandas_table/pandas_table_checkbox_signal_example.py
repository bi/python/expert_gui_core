"""
Pandas Table connected to Signals from Checkboxes

This example demonstrates how to create a PyQt table widget from a Pandas DataFrame, with columns
that contain checkboxes. The table is set up to listen for state changes when a checkbox is clicked.

Features:
- The table is populated with random integer values, and two columns contain checkboxes (`MySelection`
  and `MyOtherSelection`).
- When a checkbox is toggled, a custom signal (`checkbox_state_changed`) is emitted with information
  about the cell position and the checkbox's state.
- A callback function connected to this signal prints the row, column, and new state of the checkbox.

Notes:
- Row selection behavior and alternating row colors are enabled but this has nothing to do with the checkboxes.
"""

import sys
import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QPalette

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt import pandas_table


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_L3BLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    def on_checkbox_state_changed(index: QtCore.QModelIndex, checked: bool):
        print(
            f"Checkbox at ({index.row()}, {index.column()}) is now {'checked' if checked else 'unchecked'}."
        )

    shape = (20, 5)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    df["MySelection"] = False
    df["MyOtherSelection"] = False

    checkable_columns = ["MySelection", "MyOtherSelection"]

    model = pandas_table.PandasTableModel(df, checkable_columns=checkable_columns)
    model.checkbox_state_changed.connect(on_checkbox_state_changed)

    view = pandas_table.PandasTableView(model=model)
    view.resize(800, 500)
    view.setAlternatingRowColors(True)
    view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
    view.show()

    sys.exit(app.exec())
