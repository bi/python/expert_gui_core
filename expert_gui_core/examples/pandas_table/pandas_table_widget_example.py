"""
Pandas Table Widget Example

This example demonstrates how to create a PyQt table widget populated from a Pandas DataFrame.
Instead of using a PandasTableModel and a PandasTableView, it starts from a PandasTableWidget.
the PandasTableWidget is a custom implementation of QTableView very similar to PandasTableView
but with a less flexible constructor.

Features:
- The table is initialized with random integer values and displayed using a `PandasTableModel`.
- An `InputValidationDelegate` is applied, which validates the data input before saving it to the model.
  This delegate intercepts the data input in editable cells, checks if the new value is valid, and logs
  a warning if the value is not accepted.
- The `InputValidationDelegate` emits a warning in the log when invalid data is encountered, providing
  a feedback mechanism.
"""

import sys

import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets

from expert_gui_core.gui.widgets.pyqt import pandas_table


class ModelUpdater(QtCore.QThread):
    def __init__(self, widget, parent=None):
        super().__init__(parent)
        self.table_widget = widget
        self.running = True

    def run(self):
        while self.running:
            self.table_widget.model.dataframe['Status'] = np.random.choice(
                [True, False], size=len(self.table_widget.model.dataframe)
            )
            self.table_widget.refresh()
            self.msleep(1000)  # Sleep for 1 second

    def stop(self):
        self.running = False
        self.quit()
        self.wait()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (20, 5)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    df['MySelection'] = False
    df['Status'] = np.random.choice([True, False], size=len(df))

    checkable_columns = ['MySelection']
    status_columns = ['Status']

    table_widget = pandas_table.PandasTableWidget(dataframe=df, checkable_columns=checkable_columns,
                                                  status_columns=status_columns)
    table_widget.resize(800, 500)
    table_widget.show()

    # Start the worker thread for updating the model
    updater_thread = ModelUpdater(widget=table_widget)
    updater_thread.start()


    def on_exit():
        updater_thread.stop()


    app.aboutToQuit.connect(on_exit)
    sys.exit(app.exec())
