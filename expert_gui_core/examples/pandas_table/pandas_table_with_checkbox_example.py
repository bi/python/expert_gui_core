"""
Pandas Table with Checkable and Boolean Status columns

This example demonstrates how to create a PyQt table widget populated from a Pandas DataFrame 
with checkable and status columns. The table is configured to allow interaction with checkboxes 
and visually indicate status changes based on boolean values.

Features:
- A 20x5 DataFrame is created with random integer values, and two additional columns: 
  - 'MySelection': A checkable column initialized with False values.
  - 'Status': A column indicating a boolean status, randomly set to True or False.
- The `PandasTableModel` is initialized with the DataFrame, specifying the checkable and status columns.
- The `_update_model` function updates the 'Status' column every 100 milliseconds, simulating real-time 
  changes in the status of each row.
- The view is configured to show alternating row colors for better readability and to disable editing.

Notes:
- The layoutChanged signal is emitted after updating the model to ensure the view reflects any changes.
- A QTimer is used to periodically trigger the status updates.
"""

import sys
import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets
from expert_gui_core.gui.widgets.pyqt import pandas_table

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (20, 5)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    df['MySelection'] = False
    df['Status'] = np.random.choice([True, False], size=len(df))  # Status column

    checkable_columns = ['MySelection']
    status_columns = ['Status']
    
    model = pandas_table.PandasTableModel(df, checkable_columns=checkable_columns, status_columns=status_columns)

    def _update_model():
        model.dataframe[status_columns[0]] = np.random.choice([True, False], size=len(model.dataframe))
        model.layoutChanged.emit()

    view = pandas_table.PandasTableView(model=model)
    view.resize(800, 500)
    view.setAlternatingRowColors(True)
    view.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(_update_model)
    timer.start(100)

    sys.exit(app.exec())