"""
Pandas Table basic example

This example demonstrates how to create a PyQt table widget populated from a large Pandas DataFrame
and dynamically update its contents.

Features:
- The table is initialized with random integer values across a 1000x1000 DataFrame.
- The `_update_model` function is called every 2 seconds, simulating real-time updates in the table.
- Two update approaches are shown in `_update_model`:
    1. Updating a single cell by changing its value directly.
    2. Replacing the entire DataFrame with a new one.
- The `layoutChanged.emit()` signal is triggered to notify the view of changes in the model, ensuring
  that updates are displayed immediately.

Notes:
- Alternating row colors are enabled for readability.
- Editing is disabled, and only single selection is allowed in the view.
"""

import sys
import pandas as pd
import numpy as np
from PyQt5 import QtCore, QtWidgets
from expert_gui_core.gui.widgets.pyqt import pandas_table

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (1000, 1000)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    model = pandas_table.PandasTableModel(df)

    def _update_model():
        new_value = np.random.randint(0, 100)

        model.dataframe.at[0, 0] = new_value
        model.layoutChanged.emit()

        # Alternative approach: Replace the entire DataFrame
        # model.dataframe = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))

    view = QtWidgets.QTableView()
    view.resize(800, 500)
    view.setAlternatingRowColors(True)
    view.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
    view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
    view.setModel(model)
    view.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(_update_model)
    timer.start(2000)

    sys.exit(app.exec())
