"""
Pandas Table with Boolean Painting Delegate

This example demonstrates how to create a PyQt table widget populated from a Pandas DataFrame,
adding a column with boolean values that displays color changes based on the cell's value.

Features:
- The table is populated with random integer values, and one column is dedicated to boolean values.
- A `BooleanPaintingDelegate` is applied to the status column, which highlights cells in green for `True`
  and red for `False`.
- This status column only works with boolean values.

Notes:
- The table view uses `NoEditTriggers`, preventing cell edits via double-clicking.
- Alternating row colors are enabled for better readability.
- A timer periodically updates the boolean values in the status column.
"""
import sys
import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets
from expert_gui_core.gui.widgets.pyqt import pandas_table

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (10000, 10000)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    bool_column = df.columns[0]
    df[bool_column] = np.random.choice([True, False], size=len(df))

    model = pandas_table.PandasTableModel(df, status_columns=[bool_column])

    def _update_model():
        model.dataframe[bool_column] = np.random.choice([True, False], size=len(model.dataframe))
        model.layoutChanged.emit()

    view = pandas_table.PandasTableView(model=model)
    view.resize(800, 500)
    view.setAlternatingRowColors(True)
    view.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
    view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
    view.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(_update_model)
    timer.start(100)

    sys.exit(app.exec())
