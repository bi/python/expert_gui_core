"""
Pandas Table Widget Align Text Example

This example builds upon the Pandas Table Widget Example, to show how to change table text alignment via
the pandas_table.DefaultDelegate.

Features:
- Custom QtWidgets.QStyledItemDelegate called LeftAlignmentDelegate.
"""

import sys

import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets, QtGui
from typing_extensions import override

from expert_gui_core.gui.widgets.pyqt import pandas_table


class ModelUpdater(QtCore.QThread):
    def __init__(self, widget, parent=None):
        super().__init__(parent)
        self.table_widget = widget
        self.running = True

    def run(self):
        while self.running:
            self.table_widget.model.dataframe['Status'] = np.random.choice(
                [True, False], size=len(self.table_widget.model.dataframe)
            )
            self.table_widget.refresh()
            self.msleep(1000)  # Sleep for 1 second

    def stop(self):
        self.running = False
        self.quit()
        self.wait()


class LeftAlignmentDelegate(pandas_table.DefaultDelegate):
    @override
    def initStyleOption(
            self, option: QtWidgets.QStyleOptionViewItem, index: QtCore.QModelIndex
    ) -> None:
        """
        Override initStyleOption to set left alignment.

        Args:
            option (QtWidgets.QStyleOptionViewItem): The style option to be modified.
            index (QtCore.QModelIndex): The model index representing the item for which the option is being initialized.
        """
        super().initStyleOption(option, index)

        # Set left alignment for text
        option.displayAlignment = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (20, 5)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    df['MySelection'] = False
    df['Status'] = np.random.choice([True, False], size=len(df))

    checkable_columns = ['MySelection']
    status_columns = ['Status']

    table_widget = pandas_table.PandasTableWidget(
        dataframe=df,
        checkable_columns=checkable_columns,
        status_columns=status_columns,
    )

    # Replace the default delegate with LeftAlignDelegate
    table_widget.setItemDelegate(LeftAlignmentDelegate(table_widget))

    table_widget.resize(800, 500)
    table_widget.show()

    # Start the worker thread for updating the model
    updater_thread = ModelUpdater(widget=table_widget)
    updater_thread.start()


    def on_exit():
        updater_thread.stop()


    app.aboutToQuit.connect(on_exit)
    sys.exit(app.exec())
