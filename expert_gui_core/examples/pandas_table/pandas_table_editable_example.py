"""
Editable Pandas Table with Input validation

This example demonstrates how to create a PyQt table widget populated from a Pandas DataFrame,
with input validation on editable cells and confirmation that the DataFrame updates upon editing.

Features:
- The table is initialized with random integer values and displayed using a `PandasTableModel`.
- An `InputValidationDelegate` is applied, which validates the data input before saving it to the model.
  This delegate intercepts the data input in editable cells, checks if the new value is valid, and logs
  a warning if the value is not accepted.
- The `InputValidationDelegate` emits a warning in the log when invalid data is encountered, providing
  a feedback mechanism.
- Includes a confirmation mechanism that logs the updated DataFrame whenever the table data is edited.

Notes:
- Alternating row colors are enabled for better readability.
- The table selection mode is set to single selection, allowing only one row to be selected at a time.
"""

import sys
import pandas as pd
import numpy as np
from PyQt5 import QtWidgets, QtCore

from expert_gui_core.gui.widgets.pyqt import pandas_table

@QtCore.pyqtSlot()
def on_data_changed():
    """Callback to confirm DataFrame updates."""
    print("DataFrame updated:")
    print(model.dataframe)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    # Create a DataFrame
    df = pd.DataFrame(np.random.randint(0, 100, size=(3, 4)), columns=list("ABCD"))

    # Create and set the Pandas table model
    model = pandas_table.PandasTableModel(df)

    # Connect dataChanged signal to the callback
    model.dataChanged.connect(on_data_changed)

    # Create and configure the table view
    view = pandas_table.PandasTableView()
    view.resize(800, 500)
    view.setAlternatingRowColors(True)
    view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
    view.setModel(model)
    view.show()

    sys.exit(app.exec())
