"""
Pandas Table Widget Styling Delegate Example

This example demonstrates how to customize the style of a pandas table using the
pandas_table.DefaultDelegate and subclassing it to create a custom delegate
called StyleOptionDelegate.

Features:
- Custom QtWidgets.QStyledItemDelegate called StyleOptionDelegate.
- Left text alignment with vertical center alignment.
- Bold and italic font style applied to table cells.
- Alternating row background colors for visual distinction.
- Conditional text color based on numeric values in the cells (green for values > 50, red for values <= 50).
- Custom editor behavior that aligns text to the bottom right in QLineEdit editors.
"""

import sys

import numpy as np
import pandas as pd
from PyQt5 import QtCore, QtWidgets, QtGui
from typing_extensions import override

from expert_gui_core.gui.widgets.pyqt import pandas_table


class ModelUpdater(QtCore.QThread):
    def __init__(self, widget, parent=None):
        super().__init__(parent)
        self.table_widget = widget
        self.running = True

    def run(self):
        while self.running:
            self.table_widget.model.dataframe['Status'] = np.random.choice(
                [True, False], size=len(self.table_widget.model.dataframe)
            )
            self.table_widget.refresh()
            self.msleep(1000)  # Sleep for 1 second

    def stop(self):
        self.running = False
        self.quit()
        self.wait()


class StyleOptionDelegate(pandas_table.DefaultDelegate):
    @override
    def initStyleOption(
            self, option: QtWidgets.QStyleOptionViewItem, index: QtCore.QModelIndex
    ) -> None:
        """
        Override initStyleOption to set left alignment and apply additional customizations.

        Args:
            option (QtWidgets.QStyleOptionViewItem): The style option to be modified.
            index (QtCore.QModelIndex): The model index representing the item for which the option is being initialized.
        """
        super().initStyleOption(option, index)

        # Set left alignment for text
        option.displayAlignment = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter

        # Customize the font to be bold and italic for demonstration purposes
        option.font.setBold(True)
        option.font.setItalic(True)

        # Apply a background color based on row index (alternating rows)
        if index.row() % 2 == 0:
            option.backgroundBrush = QtGui.QBrush(QtGui.QColor(240, 240, 240))  # Light gray
        else:
            option.backgroundBrush = QtGui.QBrush(QtGui.QColor(255, 255, 255))  # White

        # Set text color based on cell value (if the value is numeric)
        value = index.data(QtCore.Qt.ItemDataRole.DisplayRole)
        if int(value) > 50:
            option.palette.setColor(QtGui.QPalette.Text, QtGui.QColor("green"))
        else:
            option.palette.setColor(QtGui.QPalette.Text, QtGui.QColor("red"))

    @override
    def createEditor(
            self,
            parent: QtWidgets.QWidget,
            option: QtWidgets.QStyleOptionViewItem,
            index: QtCore.QModelIndex
    ) -> QtWidgets.QWidget:
        """
        Override createEditor to customize the editor widget and maintain alignment.

        Args:
            parent (QtWidgets.QWidget): The parent widget for the editor.
            option (QtWidgets.QStyleOptionViewItem): Style options for the editor.
            index (QtCore.QModelIndex): The index of the item being edited.

        Returns:
            QtWidgets.QWidget: The editor widget.
        """
        editor = super().createEditor(parent, option, index)

        # Apply alignment to the editor (e.g., QLineEdit)
        if isinstance(editor, QtWidgets.QLineEdit):
            editor.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)

        return editor


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    shape = (20, 5)
    df = pd.DataFrame(np.random.randint(0, 100, size=shape), columns=range(shape[1]))
    df['MySelection'] = False
    df['Status'] = np.random.choice([True, False], size=len(df))

    checkable_columns = ['MySelection']
    status_columns = ['Status']

    table_widget = pandas_table.PandasTableWidget(
        dataframe=df,
        checkable_columns=checkable_columns,
        status_columns=status_columns,
    )

    # Replace the default delegate with StyleOptionDelegate
    table_widget.setItemDelegate(StyleOptionDelegate(table_widget))

    # Resizing columns is not the something a StyleDelegate is supposed to do,
    # one can achieve this with resizeColumnToContents or resizeColumnsToContents.
    # Resize numeric columns
    # for col in range(5):
    #     table_widget.resizeColumnToContents(col)

    table_widget.resize(800, 500)
    table_widget.show()

    # Start the worker thread for updating the model
    updater_thread = ModelUpdater(widget=table_widget)
    updater_thread.start()


    def on_exit():
        updater_thread.stop()


    app.aboutToQuit.connect(on_exit)
    sys.exit(app.exec())
