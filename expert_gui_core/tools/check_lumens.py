import sys
import numpy as np
import os


def check_lumens_for_fecs(fecs_to_check="", dom_fecs_to_check=""):
    """Check whether lumens is there"""

    if fecs_to_check == "":
        return -1

    if dom_fecs_to_check == "":
        return -1

    dir_fec_dom = "/acc/dsc/" + dom_fecs_to_check + "/" + fecs_to_check + "/etc/systemd/lumens"
    try:
        os.chdir(dir_fec_dom)
        list_dir_fec_dom = os.listdir()
        for file_found in list_dir_fec_dom:
            # print(file_found)
            if file_found == "lumens_sequence.txt":
                return 1
    except:
        return -1

    return 0


def get_dom_fecs():
    """Get all dom names for specific fec"""

    dir_fesa = "/user/sbartped/fesa-class/fesa-class"
    dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "ctb", "tst", "sps", "ctf"}

    os.chdir(dir_fesa)
    list_dir_fesa = os.listdir()

    dom_fec = {}

    for dom in dir_dom:
        # print("domain : "+dom)
        dir_fec_dom = "/acc/dsc/" + dom
        os.chdir(dir_fec_dom)
        list_dir_fec_dom = os.listdir()
        for fec_found in list_dir_fec_dom:
            dom_fec[fec_found] = dom

    return dom_fec


def get_fecs_from_class(classes_to_find=[], operational=True):
    """Get all fec names running specific 
    FESA class using binary name _DU_M"""

    dir_fesa = "/user/sbartped/fesa-class/fesa-class"

    if operational:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "sps", "ctf"}
    else:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "ctb", "tst", "sps", "ctf"}

    os.chdir(dir_fesa)
    list_dir_fesa = os.listdir()

    class_fec = {}

    for dom in dir_dom:
        # print(" ")
        # print("domain : "+dom)
        # print(" ")
        dir_fec_dom = "/acc/dsc/" + dom
        os.chdir(dir_fec_dom)
        list_dir_fec_dom = os.listdir()
        for fec_found in list_dir_fec_dom:
            # print(fec_found)
            if len(classes_to_find) > 0:
                try:
                    os.chdir(dir_fec_dom + "/" + fec_found + "/bin")
                except:
                    continue
                list_dir_fec_bin = os.listdir()
                for fec_prog_found in list_dir_fec_bin:
                    index_fesa_bin = fec_prog_found.find('_DU_M')
                    index_fesa_specific_class = 0
                    if len(classes_to_find) > 0:
                        index_fesa_specific_class = -1
                        for class_to_find in classes_to_find:
                            if index_fesa_specific_class == -1:
                                index_fesa_specific_class = fec_prog_found.find(class_to_find + "_DU_M")
                    if index_fesa_bin != -1 and index_fesa_specific_class != -1:
                        fesa_class_name = fec_prog_found[:index_fesa_bin]
                        if fesa_class_name not in class_fec:
                            class_fec[fesa_class_name] = []
                        try:
                            class_fec[fesa_class_name].index(fec_found)
                        except:
                            class_fec[fesa_class_name].append(fec_found)
            else:
                if "all" not in class_fec:
                    class_fec["all"] = []
                if fec_found != "data" and fec_found not in dir_dom:
                    class_fec["all"].append(fec_found)

    return class_fec


if __name__ == "__main__":

    list_class = sys.argv[1:]

    dom_fecs = get_dom_fecs()

    if len(list_class) > 0:
        classes_to_find = list_class[0].split(",")
    else:
        classes_to_find = []
    fecs = get_fecs_from_class(classes_to_find=classes_to_find)
    dom = ""
    for class_name in fecs.keys():
        print("")
        print("class targeted : " + class_name)
        print("")
        dom_found = []
        for fec in fecs[class_name]:
            dom_found.append(dom_fecs[fec])
        idx = np.argsort(dom_found)
        listFec = np.array(fecs[class_name])[idx]
        for fec in listFec:
            if check_lumens_for_fecs(fec, dom_fecs[fec]) != 1:
                if dom != dom_fecs[fec]:
                    print("")
                    print(dom_fecs[fec])
                    print("")
                    dom = dom_fecs[fec]
                print("No lumens for : " + fec + " " + dom_fecs[fec])
    print("")
