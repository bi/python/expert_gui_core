import time
import logging
from datetime import datetime
from dateutil.parser import parse
from fxpmath import Fxp
from jpype.types import JShort, JInt, JLong, JChar, JByte, JBoolean, JDouble, JFloat, JString

from PyQt5.QtCore import QDateTime

nxcals_logger = logging.getLogger("nxcals_logger")
nxcals_logger.setLevel(logging.DEBUG)


def logger_nxcals():
    return nxcals_logger


def print_nxcals(*txt, level=logging.INFO):
    print(*txt)
    string_list = [str(element) for element in txt]
    delimiter = " "
    if level == logging.INFO:
        logger_nxcals().info(delimiter.join(string_list))
    elif level == logging.WARNING:
        logger_nxcals().warning(delimiter.join(string_list))
    elif level == logging.ERROR:
        logger_nxcals().error(delimiter.join(string_list))
    elif level == logging.DEBUG:
        logger_nxcals().debug(delimiter.join(string_list))
    elif level == logging.CRITICAL:
        logger_nxcals().critical(delimiter.join(string_list))


def current_milli_time():
    """Time now in ms"""
    return (time.time() * 1000)


def float_bin(num):
    """Convertion float to binary"""
    return bin(num)

def current_str_s_time():
    return QDateTime.currentDateTime().toString("yyyy-MM-dd HH:mm:ss")

def format_value_to_string(dat, type_format="str", type_factor=None, format_sci="{:.8e}"):
    """Format value to string in data/bin/hex/sci/fxp131/fun or none"""
    if type_format is not None:
        try:
            data = type_factor*dat
        except:
            data = dat
    else:
        data = dat
    if type_format == "fun":
        type_format = "str"
    if type_format == "str" or str(type(data)) == "str":
        return str(data)
    elif type_format == "date":
        return datetime.fromtimestamp(float(data)).isoformat(sep=' ', timespec='milliseconds')
    elif type_format == "idate":
        return str(parse(data).timestamp())
    elif type_format == "bin":
        return str(float_bin(int(data)))
    elif type_format == "ibin":
        return str(int(data, 2))
    elif type_format == "fxp131":
        fxp = Fxp(data, signed=True, raw=True, n_word=32, n_frac=31)
        return str(fxp.astype(float))
    elif type_format == "ifxp131":
        fdata = float(data)
        return str(round(fdata * (2 ** 31)))
    elif type_format == "hex":
        return hex(int(float(data))).upper()
    elif type_format == "ihex":
        return str(int(data, 0))
    elif type_format == "sci":
        fdata = float(data)
        return str(format_sci.format(fdata))
    elif type_format == "isci":
        return str(float(data))


def str_find(str, val):
    try:
        ind = str.index(val)
        return ind
    except:
        return -1
    return -1


def type_value(value, type):
    stype = str(type)
    if value == '' or value is None:
        return 0
    if str_find(stype, "nt") >= 0 or str_find(stype, "hort") >= 0 or str_find(stype, "yte") >= 0 or str_find(stype,
                                                                                                             "ong") >= 0 or str_find(
            stype, "har") >= 0:
        return int(float(value))
    elif str_find(stype, "loat") >= 0 or str_find(stype, "ouble") >= 0:
        return float(value)
    elif str_find(stype, "ool") >= 0:
        return bool(value)
    elif str_find(stype, "tr") >= 0:
        return str(value)
    return value


def type_value_to_java(value, type, si=-1):
    stype = str(type)
    le = 0
    try:
        le = len(value)
    except:
        le = 0
    if si == 0:
        le = 0
    if le == 0:
        if str_find(stype, "JShort") >= 0:
            return JShort(float(value))
        elif str_find(stype, "JInt") >= 0:
            return JInt(float(value))
        elif str_find(stype, "JLong") >= 0:
            return JLong(float(value))
        elif str_find(stype, "JChar") >= 0:
            return JChar(float(value))
        elif str_find(stype, "JByte") >= 0:
            return JByte(float(value))
        elif str_find(stype, "JBoolean") >= 0:
            return JBoolean(float(value))
        elif str_find(stype, "JChar") >= 0:
            return JChar(float(value))
        elif str_find(stype, "JDouble") >= 0:
            return JDouble(float(value))
        elif str_find(stype, "JFloat") >= 0:
            return JFloat(float(value))
        elif str_find(stype, "JString") >= 0:
            return JString(str(value))
        else:
            return type_value(value, type)
    else:
        for i in range(le):
            if str_find(stype, "JShort") >= 0:
                value[i] = JShort(float(value[i]))
            elif str_find(stype, "JInt") >= 0:
                value[i] = JInt(float(value[i]))
            elif str_find(stype, "JLong") >= 0:
                value[i] = JLong(float(value[i]))
            elif str_find(stype, "JChar") >= 0:
                value[i] = JChar(float(value[i]))
            elif str_find(stype, "JByte") >= 0:
                value[i] = JByte(float(value[i]))
            elif str_find(stype, "JBoolean") >= 0:
                value[i] = JBoolean(float(value[i]))
            elif str_find(stype, "JChar") >= 0:
                value[i] = JChar(float(value[i]))
            elif str_find(stype, "JDouble") >= 0:
                value[i] = JDouble(float(value[i]))
            elif str_find(stype, "JFloat") >= 0:
                value[i] = JFloat(float(value[i]))
            elif str_find(stype, "JString") >= 0:
                value[i] = JString(str(value[i]))
            else:
                value[i] = type_value(value[i], type)
    return value
